USE [master]
GO
/****** Object:  Database [carpower]    Script Date: 10/1/2023 8:23:32 PM ******/
CREATE DATABASE [carpower]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'carpower', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\carpower.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'carpower_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\carpower_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [carpower] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [carpower].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [carpower] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [carpower] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [carpower] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [carpower] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [carpower] SET ARITHABORT OFF 
GO
ALTER DATABASE [carpower] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [carpower] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [carpower] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [carpower] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [carpower] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [carpower] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [carpower] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [carpower] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [carpower] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [carpower] SET  DISABLE_BROKER 
GO
ALTER DATABASE [carpower] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [carpower] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [carpower] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [carpower] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [carpower] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [carpower] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [carpower] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [carpower] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [carpower] SET  MULTI_USER 
GO
ALTER DATABASE [carpower] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [carpower] SET DB_CHAINING OFF 
GO
ALTER DATABASE [carpower] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [carpower] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [carpower] SET DELAYED_DURABILITY = DISABLED 
GO
USE [carpower]
GO
/****** Object:  Table [dbo].[Ads]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ads](
	[ads_id] [int] IDENTITY(1,1) NOT NULL,
	[ads_name] [nvarchar](50) NOT NULL,
	[ads_imag] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Ads] PRIMARY KEY CLUSTERED 
(
	[ads_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblCategory]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblCategory](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](50) NULL,
	[Description] [nvarchar](550) NULL,
	[Picture] [nvarchar](100) NULL,
 CONSTRAINT [PK_TblCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblComment]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblComment](
	[CommentID] [int] IDENTITY(1,1) NOT NULL,
	[Comment] [nvarchar](550) NULL,
	[OrderDetailID] [int] NULL,
	[UserID] [int] NULL,
	[CommentDate] [datetime] NULL,
 CONSTRAINT [PK_TblComment] PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblCustomer]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblCustomer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NULL,
	[ContactName] [nvarchar](50) NULL,
	[ContactTitle] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[Regior] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Telegram] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblCustomer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblDescription]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblDescription](
	[DescriptionID] [int] IDENTITY(1,1) NOT NULL,
	[DescriptionDetailsProduct] [nvarchar](max) NULL,
	[CarMakes] [nvarchar](150) NULL,
	[CarModel] [nvarchar](150) NULL,
	[Year] [nvarchar](150) NULL,
	[Color] [nvarchar](150) NULL,
	[ProductID] [int] NULL,
 CONSTRAINT [PK_TblDescription] PRIMARY KEY CLUSTERED 
(
	[DescriptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblDocument]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblDocument](
	[DocumentID] [int] IDENTITY(1,1) NOT NULL,
	[DocumentName] [nvarchar](50) NULL,
	[DocumentAttach] [nvarchar](550) NULL,
	[ProducctID] [int] NULL,
 CONSTRAINT [PK_TblDocument] PRIMARY KEY CLUSTERED 
(
	[DocumentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblDocumentBuyer]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblDocumentBuyer](
	[DocumentBuyerID] [int] IDENTITY(1,1) NOT NULL,
	[DocumentBuyerName] [nvarchar](50) NULL,
	[DocumentBuyerAttach] [nvarchar](550) NULL,
	[OrderDetailID] [int] NULL,
	[DocumentBuyerTypeID] [int] NULL,
 CONSTRAINT [PK_TblDocumentBuyer] PRIMARY KEY CLUSTERED 
(
	[DocumentBuyerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblDocumentBuyerType]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblDocumentBuyerType](
	[DocumentBuyerTypeID] [int] IDENTITY(1,1) NOT NULL,
	[DocumentBuyerTypeName] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblDocumentBuyerType] PRIMARY KEY CLUSTERED 
(
	[DocumentBuyerTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblEmployees]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblEmployees](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[HireDate] [datetime] NULL,
	[Address] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[HomePhone] [nvarchar](50) NULL,
	[Photo] [nvarchar](50) NULL,
	[Note] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblEmployees] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblImage]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblImage](
	[ImageID] [int] IDENTITY(1,1) NOT NULL,
	[ImageName] [nvarchar](50) NULL,
	[ImageUrl] [nvarchar](50) NULL,
	[ProductID] [int] NULL,
 CONSTRAINT [PK_TblImage] PRIMARY KEY CLUSTERED 
(
	[ImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblOrderDetails]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblOrderDetails](
	[OrderDetailID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID] [int] NULL,
	[ProductID] [int] NULL,
	[UnitPrice] [decimal](18, 0) NULL,
	[Quantity] [smallint] NULL,
	[Discount] [float] NULL,
	[Amount] [decimal](18, 0) NULL,
	[Ordered] [bit] NULL,
	[StatusID] [int] NULL,
	[UserLog] [nvarchar](150) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_TblOrderDetails] PRIMARY KEY CLUSTERED 
(
	[OrderDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblOrders]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblOrders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NULL,
	[EmployeeID] [int] NULL,
	[Amount] [decimal](18, 0) NULL,
	[OrderDate] [datetime] NULL,
	[StatusID] [int] NULL,
	[Ordered] [bit] NULL,
	[RequiredDate] [datetime] NULL,
	[ShippedDate] [datetime] NULL,
	[ShpperID] [int] NULL,
	[Freight] [money] NULL,
	[ShipName] [nvarchar](50) NULL,
	[ShipAddress] [nvarchar](50) NULL,
	[ShipCity] [nvarchar](50) NULL,
	[ShipRegion] [nvarchar](50) NULL,
	[ShipPostalCode] [nvarchar](50) NULL,
	[ShipCountry] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblOrders] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblPosition]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblPosition](
	[PositionID] [int] IDENTITY(1,1) NOT NULL,
	[PositionName] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblPosition] PRIMARY KEY CLUSTERED 
(
	[PositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblProduct]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblProduct](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](50) NULL,
	[SupplierID] [int] NULL,
	[CategoryID] [int] NULL,
	[Quantity] [nvarchar](50) NULL,
	[UnitPrice] [money] NULL,
	[UnitInStock] [smallint] NULL,
	[image] [nvarchar](50) NULL,
	[detail] [nvarchar](500) NULL,
	[ProductDate] [datetime] NULL,
	[ShortNumber] [varchar](250) NULL,
	[EngineNumber] [varchar](250) NULL,
	[TagID] [nvarchar](500) NULL,
	[MV] [varchar](250) NULL,
	[CarManufactureDate] [varchar](250) NULL,
	[Color] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblProduct] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblQuotationDetails]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblQuotationDetails](
	[QuotationDetailID] [int] IDENTITY(1,1) NOT NULL,
	[QuotationID] [int] NULL,
	[ProductID] [int] NULL,
	[UnitPrice] [decimal](18, 0) NULL,
	[Quantity] [smallint] NULL,
	[Discount] [float] NULL,
	[Amount] [decimal](18, 0) NULL,
	[Quotationed] [bit] NULL,
	[StatusID] [int] NULL,
	[UserLog] [nvarchar](150) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_TblQuotationDetails] PRIMARY KEY CLUSTERED 
(
	[QuotationDetailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblQuotations]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblQuotations](
	[QuotationID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NULL,
	[EmployeeID] [int] NULL,
	[Amount] [decimal](18, 0) NULL,
	[QuotationDate] [datetime] NULL,
	[StatusID] [int] NULL,
	[Quotationed] [bit] NULL,
	[RequiredDate] [datetime] NULL,
	[ShippedDate] [datetime] NULL,
	[ShpperID] [int] NULL,
	[Freight] [money] NULL,
	[ShipName] [nvarchar](50) NULL,
	[ShipAddress] [nvarchar](50) NULL,
	[ShipCity] [nvarchar](50) NULL,
	[ShipRegion] [nvarchar](50) NULL,
	[ShipPostalCode] [nvarchar](50) NULL,
	[ShipCountry] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblQuotations] PRIMARY KEY CLUSTERED 
(
	[QuotationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblReply]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblReply](
	[ReplyID] [int] IDENTITY(1,1) NOT NULL,
	[Reply] [nvarchar](550) NULL,
	[CommentID] [int] NULL,
	[UserID] [int] NULL,
	[ReplyDate] [datetime] NULL,
 CONSTRAINT [PK_TblReply] PRIMARY KEY CLUSTERED 
(
	[ReplyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblRole]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblRole](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblRole] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblShippers]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblShippers](
	[ShipperID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblShippers] PRIMARY KEY CLUSTERED 
(
	[ShipperID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblStatus]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblStatus](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblStatus] PRIMARY KEY CLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblStatusOrder]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblStatusOrder](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[StatusName] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblStatusOrder] PRIMARY KEY CLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblSuppliers]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblSuppliers](
	[SupplierID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NULL,
	[ContactName] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[Region] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Fax] [nvarchar](50) NULL,
	[Website] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblSuppliers] PRIMARY KEY CLUSTERED 
(
	[SupplierID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblTest]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblTest](
	[ShipperID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[TestList] [nvarchar](50) NULL,
 CONSTRAINT [PK_TblTest] PRIMARY KEY CLUSTERED 
(
	[ShipperID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TblUser]    Script Date: 10/1/2023 8:23:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TblUser](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](500) NULL,
	[Password] [nvarchar](500) NULL,
	[RoleID] [int] NULL,
	[StatusID] [int] NULL,
	[PhoneNumber] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[Address] [nvarchar](500) NULL,
	[Image] [nvarchar](500) NULL,
	[Gender] [nvarchar](500) NULL,
	[Age] [int] NULL,
	[Position] [nvarchar](500) NULL,
	[IdCard] [nvarchar](500) NULL,
	[Passport] [nvarchar](500) NULL,
 CONSTRAINT [PK_TblAdmin] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Ads] ON 

INSERT [dbo].[Ads] ([ads_id], [ads_name], [ads_imag]) VALUES (5, N'', N'')
INSERT [dbo].[Ads] ([ads_id], [ads_name], [ads_imag]) VALUES (6, N'', N'')
SET IDENTITY_INSERT [dbo].[Ads] OFF
GO
SET IDENTITY_INSERT [dbo].[TblCategory] ON 

INSERT [dbo].[TblCategory] ([CategoryID], [CategoryName], [Description], [Picture]) VALUES (12, N'Kai Morning', N'Kia America, Inc. provides a wide range of cars that meet your lifestyle. Browse our luxury or sports sedans, hybrids, electric cars', N'images/Kia Morning 2010.jpg')
INSERT [dbo].[TblCategory] ([CategoryID], [CategoryName], [Description], [Picture]) VALUES (13, N'Toyota', N'Kia America, Inc. provides a wide range of cars that meet your lifestyle', N'images/Kai Morning 2021.jpg')
INSERT [dbo].[TblCategory] ([CategoryID], [CategoryName], [Description], [Picture]) VALUES (14, N'Lexus', N'Ford Cambodia', N'images/Ford mastang.jpg')
INSERT [dbo].[TblCategory] ([CategoryID], [CategoryName], [Description], [Picture]) VALUES (15, N'BMW', N'BMW', N'')
INSERT [dbo].[TblCategory] ([CategoryID], [CategoryName], [Description], [Picture]) VALUES (16, N'Accessories', N'sell accessories', NULL)
SET IDENTITY_INSERT [dbo].[TblCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[TblComment] ON 

INSERT [dbo].[TblComment] ([CommentID], [Comment], [OrderDetailID], [UserID], [CommentDate]) VALUES (26, N'សួស្តីបង', 251, 4, CAST(N'2023-09-17T15:28:27.850' AS DateTime))
INSERT [dbo].[TblComment] ([CommentID], [Comment], [OrderDetailID], [UserID], [CommentDate]) VALUES (29, N'សួស្តីបង', 290, 4, CAST(N'2023-09-30T15:52:44.573' AS DateTime))
INSERT [dbo].[TblComment] ([CommentID], [Comment], [OrderDetailID], [UserID], [CommentDate]) VALUES (31, N'Hello Suinuon', 345, 27, CAST(N'2023-09-30T20:53:42.600' AS DateTime))
INSERT [dbo].[TblComment] ([CommentID], [Comment], [OrderDetailID], [UserID], [CommentDate]) VALUES (32, N'Hello Suinuon', 346, 28, CAST(N'2023-09-30T22:07:05.487' AS DateTime))
INSERT [dbo].[TblComment] ([CommentID], [Comment], [OrderDetailID], [UserID], [CommentDate]) VALUES (33, N'Hello bong', 351, 34, CAST(N'2023-09-30T22:34:12.497' AS DateTime))
SET IDENTITY_INSERT [dbo].[TblComment] OFF
GO
SET IDENTITY_INSERT [dbo].[TblCustomer] ON 

INSERT [dbo].[TblCustomer] ([CustomerID], [CompanyName], [ContactName], [ContactTitle], [Address], [City], [Regior], [PostalCode], [Country], [Phone], [Telegram], [Fax]) VALUES (2, N'test01', N'test01', N'Sarayuth', N'Prek Pnov', N'Phnom Penh', N'khmer', N'121101', N'Cambodia', N'015417987', N'015417987', N'015417987')
INSERT [dbo].[TblCustomer] ([CustomerID], [CompanyName], [ContactName], [ContactTitle], [Address], [City], [Regior], [PostalCode], [Country], [Phone], [Telegram], [Fax]) VALUES (4, N'Sok Sinoun', N'Sok Sinoun', N'Sok Sinoun', N'Phnom penh', N'Phnom penh', N'', N'120101', N'Cambodia', N'0124179870', N'0124179870', N'0124179870')
INSERT [dbo].[TblCustomer] ([CustomerID], [CompanyName], [ContactName], [ContactTitle], [Address], [City], [Regior], [PostalCode], [Country], [Phone], [Telegram], [Fax]) VALUES (5, N'សុខម៉េង', N'សុខម៉េង', N'សុខម៉េង', N'Phnom penh', N'Phnom penh', N'', N'120101', N'Cambodia', N'0124179870', N'0124179870', N'0124179870')
SET IDENTITY_INSERT [dbo].[TblCustomer] OFF
GO
SET IDENTITY_INSERT [dbo].[TblDescription] ON 

INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (8, N'BMW', N'BMW', N'BMW ', N'2023', N'blue', 41)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (9, N'Next-Generation Ranger XLT ស៊េរីថ្មីឆ្នាំ 2023 រូបរាងថ្មីទាំងស្រុងកាន់តែស្រស់ស្អាត អំណាច ជារថយន្តPick Upដែលមានសមត្ថភាពដឹក ផ្ទុកទំនិញធ្ងន់ៗសម្រាប់បំពេញនូវគ្រប់តម្រូវការរបស់លោកអ្នកជាមួយភាពធន់ រឹងមាំ និងបើកបរបានគ្រប់ស្ថានភាពផ្លូវ ជាមួយប្រព័ន្ធសុវត្ថិភាព និងបច្ចេកវិទ្យាទំនើបចុងក្រោយ!', N'Ford', N'Ford', N'2019', N'White', 34)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (10, N'ឡានលក់ Kia Morning 2016 Full Options ឡាននៅស្អាតតុបតែងរួចរាល់អស់ហើយ យ៉ាន់និងសំបកថ្មី អំពូលគុជ កាមេរ៉ាមុខក្រោយ', N'2006', N'Kai', N'2006', N'Red', 33)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (11, N'gggggjjj', N'ccc', N'mokk', N'345', N'jjjjj', 37)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (12, N'Toyota Pruis 2010
ឡានស្អាត
ពិន្ទុខ្ពស់
ធនាកាត់ឈ្មោះបាន អត់បុក អត់ ក្រឡាប់ អត់ប្តូរពណ៌', N'Toyota', N'Toyota Pruis 2010', N'2010', N'White', 36)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (15, N'រំលស់ការទាប 0.57% Prius 2០12 option 3 ធានាឡានស្អាត', N'Toyota', N'Toyota prius 2012', N'2012', N'ស', 62)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (16, N'Lexus RX300t បើកដំបូលតម្លៃធូថ្លៃ', N'Lexus', N'Lexus RX 300t', N'2022', N' white', 56)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (18, N'តម្លៃទន់ៗToyota Tacomaឡានស្អាតទឹកថ្នាំសាច់ដែកខ្ចី', N'Toyota', N'Toyota Tacoma', N'2008', N'Silver', 55)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (19, N'ថ្មីខ្លាញ់គោ​ Toyota Wigo ធានាឡានស្អាត​អត់​បុក​អត់​ច្រេះ​ទឹក​ថ្នាំ​សុីន  ', N'Toyota', N'Toyota Wigo ', N'2022', N'Yellow', 58)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (20, N'Kia Morning 2021 ឡានក្រដាសពន្ធ ឡានស្អាត ស្រីន 1 ជុំ', N'Kia Morning', N'Kia Morning 2021', N'2021', N'White', 35)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (21, N'Lexus RX300 F-Sport 2019 Spec អាល្លឺម៉ង់ ម្ចាស់ដើម ថ្មីណាស់37000Km', N'Lexus ', N'Lexus RX300 F-Sport 2019', N'2019', N'White', 61)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (22, N'Kia Morning 2016 Full options លក់តម្លៃក្រោមទីផ្សាជូនហើយចរចារបានទៀត', N'Kia Morning', N'Kia Morning 2016', N'2016', N'ពណ៍ទឹកដោះគោ', 45)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (23, N'Kia Morning 2011 13000kmទេដូចឡានថ្មីបងប្អូន', N'Kia Morning ', N'Kia Morning 2011', N'2011', N'Silver', 63)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (24, N'Kia Morning 2009 តំលៃតែ៧៥០០ស្តេីងទេចរចាបានទៀត', N'Kia Morning', N'Kia Morning 2009', N'2009', N'Silver', 64)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (25, N'LEXUS NX300 2018 F_SPORT **Fulloption អារ៉ាប់', N'LEXUS NX300', N'LEXUS NX300 2018', N'2018', N'White', 68)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (26, N'LEXUS NX 200t ឆ្នាំ 2016', N'LEXUS NX 200t', N'LEXUS NX 200t  2016', N'2016', N'White', 69)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (27, N'រថយន្តថ្មីស៊េរីឆ្នាំ2023! Brand New Car 
Brand New Lexus LX 600 TurboSport 25 Speakers
Model Year: 2023
Exterior Color: White   
Interior Color: Cherry', N'Lexus NX200T Luxury', N'Lexus NX200T Luxury f Sport', N'2023', N'White', 70)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (28, N'ឡានប្រណិត Lexus NX200T Luxury តម្លៃ 105000$ ដល់ 125000$ សាកមួយពេញជិះបាន 445-520គីឡូ', N'Lexus NX200T', N'Lexus NX200T Luxury', N'2023', N'White', 71)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (29, N'ឡានប្រណិត Lexus NX200T Luxury តម្លៃ 105000$ ដល់ 125000$ សាកមួយពេញជិះបាន 445-520គីឡូ', N'Lexus NX200T', N'Lexus NX200T Luxury', N'2023', N'White', 71)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (30, N'LEXUS NX 200t ឆ្នាំ 2016', N'LEXUS NX 200t', N'LEXUS NX 200t 2016', N'2016', N'White', 72)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (31, N'ថយន្តថ្មីស៊េរីឆ្នាំ2023! Brand New Car 
Lexus NX200T Luxury ឡេីង  f Sport ប៉ូតុង9 Brand New Lexus LX 600 TurboSport 25 Speakers
Model Year: 2023
Exterior Color: White   
Interior Color: Cherry', N'Lexus ', N'Lexus NX200T Luxury', N'2023', N'White', 73)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (32, N'ថយន្តថ្មីស៊េរីឆ្នាំ2023! Brand New Car 
Lexus NX200T Luxury ឡេីង  f Sport ប៉ូតុង9 Brand New Lexus LX 600 TurboSport 25 Speakers
Model Year: 2023
Exterior Color: White   
Interior Color: Cherry', N'Lexus ', N'Lexus NX200T Luxury', N'2023', N'White', 73)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (33, N' The Lexus NX 200t crossover SUV and its hybrid variant, the NX 300h, slot below the RX in Lexus’ lineup. It seats up to five people and competes with the Acura RDX, BMW X3 and Audi Q5.', N'2017', N'Lexus NX 200t', N'2017', N'ស', 93)
INSERT [dbo].[TblDescription] ([DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color], [ProductID]) VALUES (34, N'Kia Carnival 2023 Full Options Noblesse 11កៅអី 3500cc ម៉ាស៊ីនសាំង ថ្មី 240Km មកដល់ខ្ទង់ខែ9 នេះហើយ កក់មុនតម្លៃពិសេស', N'2023', N'Kia', N'2023', N'ស', 94)
SET IDENTITY_INSERT [dbo].[TblDescription] OFF
GO
SET IDENTITY_INSERT [dbo].[TblDocument] ON 

INSERT [dbo].[TblDocument] ([DocumentID], [DocumentName], [DocumentAttach], [ProducctID]) VALUES (2, N'sarayuth', N'images/update table.jpg', 33)
INSERT [dbo].[TblDocument] ([DocumentID], [DocumentName], [DocumentAttach], [ProducctID]) VALUES (3, N'document', N'images/doc.jpg', 35)
SET IDENTITY_INSERT [dbo].[TblDocument] OFF
GO
SET IDENTITY_INSERT [dbo].[TblDocumentBuyer] ON 

INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (43, N'Doc Family', N'DocumentAttach/card.jpg', 231, 1)
INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (44, N'', N'DocumentAttach/IDCard.jpg', 251, 1)
INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (45, N'', N'DocumentAttach/FamilyBook.jpg', 251, 2)
INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (46, N'', N'DocumentAttach/LetterBookBriday.jpg', 251, 3)
INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (47, N'ID Card', N'DocumentAttach/IDCard.jpg', 304, 1)
INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (48, N'', N'DocumentAttach/IDCard.jpg', 324, 1)
INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (49, N'', N'DocumentAttach/FamilyBook.jpg', 324, 2)
INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (50, N'', N'DocumentAttach/LetterBookBriday.jpg', 324, 3)
INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (51, N'', N'DocumentAttach/IDCard.jpg', 351, 1)
INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (52, N'', N'DocumentAttach/FamilyBook.jpg', 351, 2)
INSERT [dbo].[TblDocumentBuyer] ([DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID], [DocumentBuyerTypeID]) VALUES (53, N'', N'DocumentAttach/LetterBookBriday.jpg', 351, 3)
SET IDENTITY_INSERT [dbo].[TblDocumentBuyer] OFF
GO
SET IDENTITY_INSERT [dbo].[TblDocumentBuyerType] ON 

INSERT [dbo].[TblDocumentBuyerType] ([DocumentBuyerTypeID], [DocumentBuyerTypeName]) VALUES (1, N'អត្តសញ្ញាណាប័ណ្ណ')
INSERT [dbo].[TblDocumentBuyerType] ([DocumentBuyerTypeID], [DocumentBuyerTypeName]) VALUES (2, N'សៀវភៅគ្រួសារ')
INSERT [dbo].[TblDocumentBuyerType] ([DocumentBuyerTypeID], [DocumentBuyerTypeName]) VALUES (3, N'សំបុត្រកំណើត')
SET IDENTITY_INSERT [dbo].[TblDocumentBuyerType] OFF
GO
SET IDENTITY_INSERT [dbo].[TblImage] ON 

INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (10, N'test', N'images/RangerXLT1.JPG', 34)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (12, N'Toyota', N'images/5.jpg', 33)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (13, N'Toyota', N'images/4.jpg', 33)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (14, N'', N'images/3.jpg', 33)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (16, N'', N'images/Ford.jpeg', 37)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (18, N'', N'images/Pruis 2012 1.jpg', 62)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (19, N'', N'images/Pruis 2012 2.jpg', 62)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (20, N'', N'images/Pruis 2012 3.jpg', 62)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (21, N'', N'images/Pruis 2012 4.jpg', 62)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (22, N'', N'images/Pruis 2012 5.jpg', 62)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (24, N'', N'images/Pruis 2012 6.jpg', 62)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (25, N'', N'images/Pruis 2012 7.jpg', 62)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (27, N'', N'images/Lexus RX 300 T 3.jpg', 56)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (28, N'', N'images/Lexus RX 300 T 1.jpg', 56)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (29, N'', N'images/Lexus RX 300 T 4.jpg', 56)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (30, N'', N'images/Lexus RX 300 T 2.jpg', 56)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (31, N'', N'images/Lexus RX 300 T 5.jpg', 56)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (32, N'', N'images/Lexus RX 300 T 6.jpg', 56)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (33, N'', N'images/tacoma 5.jpg', 55)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (34, N'', N'images/tacoma 7.jpg', 55)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (35, N'', N'images/tacoma1.jpg', 55)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (36, N'', N'images/tacoma 6.jpg', 55)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (37, N'', N'images/tacoma 4.jpg', 55)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (38, N'', N'images/tacoma 3.jpg', 55)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (39, N'', N'images/tacoma 2.jpg', 55)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (41, N'', N'images/Pruis 2010 1.jpg', 36)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (42, N'', N'images/Pruis 2010 2.jpg', 36)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (43, N'', N'images/Pruis 2010 3.jpg', 36)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (44, N'', N'images/Pruis 2010 4.jpg', 36)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (45, N'', N'images/Pruis 2010 5.jpg', 36)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (46, N'', N'images/Pruis 2010 6.jpg', 36)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (47, N'', N'images/Pruis 2010 7.jpg', 36)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (48, N'', N'images/Pruis 2010 8.jpg', 36)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (49, N'', N'images/Pruis 2010 9.jpg', 36)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (50, N'', N'images/wigo2.JPG', 58)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (51, N'', N'images/wigo3.JPG', 58)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (52, N'', N'images/wigo4.JPG', 58)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (53, N'', N'images/wigo5.JPG', 58)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (54, N'', N'images/wigo6.JPG', 58)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (55, N'', N'images/wigo7.JPG', 58)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (57, N'', N'images/kia morming 2021  5.jpg', 35)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (58, N'', N'images/kia morming 2021 2.jpg', 35)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (59, N'', N'images/kia morming 2021 3.jpg', 35)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (60, N'', N'images/kia morming 2021 4.jpg', 35)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (61, N'', N'images/kia morming 2021 6.jpg', 35)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (62, N'', N'images/kia morming 2021 7.jpg', 35)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (63, N'', N'images/kia morming 2021 8.jpg', 35)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (64, N'', N'images/kia morming 2021.jpg', 35)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (65, N'', N'images/Lexus RX300 F-Sport 2019 7.jpg', 61)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (66, N'', N'images/Lexus RX300 F-Sport 2019 8.jpg', 61)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (67, N'', N'images/Lexus RX300 F-Sport 2019 6.jpg', 61)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (68, N'', N'images/Lexus RX300 F-Sport 2019 5.jpg', 61)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (69, N'', N'images/Lexus RX300 F-Sport 2019 4.jpg', 61)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (70, N'', N'images/Lexus RX300 F-Sport 2019 3.jpg', 61)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (71, N'', N'images/Lexus RX300 F-Sport 2019 2.jpg', 61)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (72, N'', N'images/2016 (2).jpg', 45)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (73, N'', N'images/2016 1.jpg', 45)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (74, N'', N'images/2016 2.jpg', 45)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (75, N'', N'images/2016 3.jpg', 45)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (76, N'', N'images/2016 4.jpg', 45)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (77, N'', N'images/2016 5.jpg', 45)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (78, N'', N'images/2016 6.jpg', 45)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (79, N'', N'images/2016.jpg', 45)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (80, N'', N'images/Kia Morning 20111.jpg', 63)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (81, N'', N'images/Kia Morning 20112.jpg', 63)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (82, N'', N'images/Kia Morning 20113.jpg', 63)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (83, N'', N'images/Kia Morning 20114.jpg', 63)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (84, N'', N'images/Kia Morning 20115.jpg', 63)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (85, N'', N'images/Kia Morning 20115.jpg', 63)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (86, N'', N'images/Kia Morning 20116.jpg', 63)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (87, N'', N'images/Kia Morning 20117.jpg', 63)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (88, N'', N'images/Kia Morning 20118.jpg', 63)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (89, N'', N'images/Kia Morning 20997.jpg', 64)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (90, N'', N'images/Kia Morning 20997.jpg', 64)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (91, N'', N'images/Kia Morning 20994.jpg', 64)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (92, N'', N'images/Kia Morning 20993.jpg', 64)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (93, N'', N'images/Kia Morning 20993.jpg', 64)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (94, N'', N'images/Kia Morning 20992.jpg', 64)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (95, N'', N'images/Kia Morning 20991.jpg', 64)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (96, N'', N'images/Kia Morning 2099.jpg', 64)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (97, N'', N'images/3.jpg', 65)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (98, N'', N'images/2.jpg', 65)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (99, N'', N'images/4.jpg', 65)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (100, N'', N'images/5.jpg', 65)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (101, N'', N'images/LEXUS NX300 2018 1.jpg', 68)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (102, N'', N'images/LEXUS NX300 2018 2.jpg', 68)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (103, N'', N'images/LEXUS NX300 2018 3.jpg', 68)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (104, N'', N'images/LEXUS NX300 2018 4 (2).jpg', 68)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (105, N'', N'images/LEXUS NX300 2018 5.jpg', 68)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (106, N'', N'images/LEXUS NX300 2018 4.jpg', 68)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (107, N'', N'images/LEXUS NX 200t  2016 1.jpg', 69)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (108, N'', N'images/LEXUS NX 200t  2016 2.jpg', 69)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (109, N'', N'images/LEXUS NX 200t  2016 3.jpg', 69)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (110, N'', N'images/LEXUS NX 200t  2016 4.jpg', 69)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (111, N'', N'images/LEXUS NX 200t  2016 5.jpg', 69)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (112, N'', N'images/LEXUS NX 200t  2016.jpg', 69)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (113, N'', N'images/Lexus NX200T Luxury f Sport 2.jpg', 70)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (114, N'', N'images/Lexus NX200T Luxury f Sport 1.jpg', 70)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (115, N'', N'images/Lexus NX200T Luxury f Sport 4.jpg', 70)
GO
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (116, N'', N'images/Lexus NX200T Luxury f Sport 5.jpg', 70)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (117, N'', N'images/Lexus NX200T Luxury f Sport 6.jpg', 70)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (118, N'', N'images/Lexus NX200T Luxury f Sport.jpg', 70)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (119, N'', N'images/Lexus NX200T Luxury f Sport3.jpg', 70)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (120, N'', N'images/Lexus NX200T Luxury 1.jpg', 71)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (121, N'', N'images/Lexus NX200T Luxury 2.jpg', 71)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (122, N'', N'images/Lexus NX200T Luxury 3.jpg', 71)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (123, N'', N'images/Lexus NX200T Luxury 4.jpg', 71)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (124, N'', N'images/Lexus NX200T Luxury 5.jpg', 71)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (125, N'', N'images/Lexus NX200T Luxury.jpg', 71)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (134, N'', N'images/5.jpg', 73)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (135, N'', N'images/1.jpg', 73)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (136, N'', N'images/3.jpg', 73)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (137, N'', N'images/Lexus GS 300 Full P1 (2).jpg', 74)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (138, N'', N'images/Lexus GS 300 Full P1.jpg', 74)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (139, N'', N'images/Lexus GS 300 Full P12.jpg', 74)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (140, N'', N'images/Lexus GS 300 Full P13.jpg', 74)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (141, N'', N'images/Lexus GS 300 Full P14.jpg', 74)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (142, N'', N'images/Lexus GS 300 Full P15.jpg', 74)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (143, N'', N'images/Lexus GS 300 Full P16.jpg', 74)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (144, N'', N'images/RangerXLT12.JPG', 34)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (145, N'', N'images/RangerXLT13.JPG', 34)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (146, N'', N'images/RangerXLT13.JPG', 34)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (147, N'', N'images/RangerXLT15.JPG', 34)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (148, N'', N'images/RangerXLT16.JPG', 34)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (149, N'', N'images/RangerXLT112.JPG', 34)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (150, N'', N'images/RangerXLT111.JPG', 34)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (151, N'', N'images/RangerXLT110.JPG', 34)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (152, N'', N'images/RangerXLT113.JPG', 34)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (153, N'', N'images/bmw1.JPG', 72)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (154, N'', N'images/bmw2.JPG', 72)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (155, N'', N'images/bm3.JPG', 72)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (156, N'', N'images/bmw4.JPG', 72)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (157, N'', N'images/bmw5.JPG', 72)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (158, N'', N'images/bmw6.JPG', 72)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (159, N'', N'images/bmw7.JPG', 72)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (160, N'', N'images/bmw8.JPG', 72)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (161, N'', N'images/bmw9.JPG', 72)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (162, N'', N'images/Lexus1.JPG', 93)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (163, N'', N'images/Lexus2.JPG', 93)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (164, N'', N'images/Lexus3.JPG', 93)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (165, N'', N'images/Lexus4.JPG', 93)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (166, N'', N'images/Lexus6.JPG', 93)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (167, N'', N'images/Lexus7.JPG', 93)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (168, N'', N'images/Lexus8.JPG', 93)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (169, N'', N'images/Kia Carnival 2023 Full Options 2.jpg', 94)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (171, N'', N'images/Kia Carnival 2023 Full Options 3.jpg', 94)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (172, N'', N'images/Kia Carnival 2023 Full Options 4.jpg', 94)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (173, N'', N'images/Kia Carnival 2023 Full Options 5.jpg', 94)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (174, N'', N'images/Kia Carnival 2023 Full Options 6.jpg', 94)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (175, N'', N'images/Kia Carnival 2023 Full Options 7.jpg', 94)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (176, N'', N'images/Kia Carnival 2023 Full Options 8.jpg', 94)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (177, N'', N'images/Kia Carnival 2023 Full Options 6.jpg', 94)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (178, N'', N'images/Kia Carnival 2023 Full Options 6.jpg', 94)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (179, N'', N'images/Kia Carnival 2023 Full Options 7.jpg', 94)
INSERT [dbo].[TblImage] ([ImageID], [ImageName], [ImageUrl], [ProductID]) VALUES (180, N'', N'images/Kia Carnival 2023 Full Options 2.jpg', 94)
SET IDENTITY_INSERT [dbo].[TblImage] OFF
GO
SET IDENTITY_INSERT [dbo].[TblOrderDetails] ON 

INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (225, 97, 34, CAST(340000 AS Decimal(18, 0)), 1, NULL, CAST(340000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230903050057', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (229, 98, 33, CAST(10000 AS Decimal(18, 0)), 1, NULL, CAST(10000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230909105502', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (230, 99, 33, CAST(10000 AS Decimal(18, 0)), 1, NULL, CAST(10000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230909012219', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (231, 100, 36, CAST(2000 AS Decimal(18, 0)), 1, NULL, CAST(2000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230909034809', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (249, 101, 33, CAST(10000 AS Decimal(18, 0)), 1, NULL, CAST(10000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230917012644', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (250, 101, 42, CAST(20000 AS Decimal(18, 0)), 1, NULL, CAST(20000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230917012644', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (251, 101, 61, CAST(40000 AS Decimal(18, 0)), 1, NULL, CAST(40000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230917012644', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (273, 103, 46, CAST(8500 AS Decimal(18, 0)), 1, NULL, CAST(8500 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230922063426', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (274, 103, 80, CAST(6 AS Decimal(18, 0)), 1, NULL, CAST(6 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230922063426', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (279, 104, 35, CAST(20000 AS Decimal(18, 0)), 1, NULL, CAST(20000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230922065744', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (280, 104, 80, CAST(6 AS Decimal(18, 0)), 1, NULL, CAST(6 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230922065744', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (283, 105, 45, CAST(13000 AS Decimal(18, 0)), 1, NULL, CAST(13000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230922071028', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (284, 105, 53, CAST(12000 AS Decimal(18, 0)), 1, NULL, CAST(12000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230922071028', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (285, 106, 80, CAST(6 AS Decimal(18, 0)), 1, NULL, CAST(6 AS Decimal(18, 0)), 1, 4, N'LAPTOP-O5KHSPMS20230923091927', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (286, 106, 39, CAST(30000 AS Decimal(18, 0)), 1, NULL, CAST(30000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230923091927', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (287, 107, 65, CAST(6000 AS Decimal(18, 0)), 1, NULL, CAST(6000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230923091927', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (289, 108, 35, CAST(20000 AS Decimal(18, 0)), 1, NULL, CAST(20000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230923093102', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (290, 109, 68, CAST(30000 AS Decimal(18, 0)), 2, NULL, CAST(30000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230923093102', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (291, 110, 36, CAST(2000 AS Decimal(18, 0)), 1, NULL, CAST(2000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230923093102', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (303, 111, 37, CAST(150000 AS Decimal(18, 0)), 1, NULL, CAST(150000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230923051830', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (304, 112, 36, CAST(2000 AS Decimal(18, 0)), 1, NULL, CAST(2000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230923052457', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (308, 113, 42, CAST(20000 AS Decimal(18, 0)), 1, NULL, CAST(20000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230929075507', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (309, 113, 46, CAST(8500 AS Decimal(18, 0)), 1, NULL, CAST(8500 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230929075507', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (310, 114, 42, CAST(20000 AS Decimal(18, 0)), 1, NULL, CAST(20000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230929075814', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (311, 115, 35, CAST(20000 AS Decimal(18, 0)), 1, NULL, CAST(20000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230929080032', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (312, 116, 35, CAST(20000 AS Decimal(18, 0)), 3, NULL, CAST(20000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230929081020', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (313, 117, 42, CAST(20000 AS Decimal(18, 0)), 1, NULL, CAST(20000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230929081020', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (315, 119, 45, CAST(13000 AS Decimal(18, 0)), 1, NULL, CAST(13000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230929081619', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (316, 120, 45, CAST(13000 AS Decimal(18, 0)), 1, NULL, CAST(13000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230929082138', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (317, 121, 46, CAST(8500 AS Decimal(18, 0)), 1, NULL, CAST(8500 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230929082646', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (319, 122, 39, CAST(30000 AS Decimal(18, 0)), 1, NULL, CAST(30000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230930052533', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (321, 122, 49, CAST(12000 AS Decimal(18, 0)), 1, NULL, CAST(12000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230930052533', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (323, 123, 34, CAST(340000 AS Decimal(18, 0)), 1, NULL, CAST(340000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230930061505', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (324, 124, 53, CAST(12000 AS Decimal(18, 0)), 1, NULL, CAST(12000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230930061639', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (325, 125, 59, CAST(40000 AS Decimal(18, 0)), 1, NULL, CAST(40000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230930063123', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (328, 126, 34, CAST(340000 AS Decimal(18, 0)), 2, NULL, CAST(340000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230930065405', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (332, 127, 36, CAST(2000 AS Decimal(18, 0)), 1, NULL, CAST(2000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230930065940', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (335, 128, 33, CAST(10000 AS Decimal(18, 0)), 1, NULL, CAST(10000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230930071558', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (336, 129, 34, CAST(340000 AS Decimal(18, 0)), 1, NULL, CAST(340000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230930071720', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (337, 130, 39, CAST(30000 AS Decimal(18, 0)), 1, NULL, CAST(30000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230930071805', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (342, 131, 53, CAST(12000 AS Decimal(18, 0)), 1, NULL, CAST(12000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230930072833', 0)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (343, 111, 76, CAST(50 AS Decimal(18, 0)), 1, NULL, CAST(50 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20230923051830', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (344, 132, 70, CAST(102000 AS Decimal(18, 0)), 1, NULL, CAST(102000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230930084421', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (345, 133, 41, CAST(16000 AS Decimal(18, 0)), 1, NULL, CAST(16000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230930085140', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (346, 134, 63, CAST(8500 AS Decimal(18, 0)), 1, NULL, CAST(8500 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230930100447', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (350, 135, 33, CAST(10000 AS Decimal(18, 0)), 1, NULL, CAST(10000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230930102823', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (351, 136, 33, CAST(10000 AS Decimal(18, 0)), 1, NULL, CAST(10000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230930103228', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (352, 137, 33, CAST(10000 AS Decimal(18, 0)), 1, NULL, CAST(10000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20230930101630', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (353, 122, 46, CAST(8500 AS Decimal(18, 0)), 1, NULL, CAST(8500 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230930052533', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (354, 138, 33, CAST(10000 AS Decimal(18, 0)), 2, NULL, CAST(10000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20231001101141', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (355, 139, 33, CAST(10000 AS Decimal(18, 0)), 1, NULL, CAST(10000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20231001101449', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (356, 131, 33, CAST(10000 AS Decimal(18, 0)), 1, NULL, CAST(10000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230930072833', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (357, 140, 45, CAST(13000 AS Decimal(18, 0)), 1, NULL, CAST(13000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20231001101641', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (358, 141, 45, CAST(13000 AS Decimal(18, 0)), 1, NULL, CAST(13000 AS Decimal(18, 0)), 1, 3, N'LAPTOP-O5KHSPMS20231001104653', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (359, 111, 46, CAST(8500 AS Decimal(18, 0)), 1, NULL, CAST(8500 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230923051830', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (360, 142, 35, CAST(20000 AS Decimal(18, 0)), 1, NULL, CAST(20000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20231001011228', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (361, 143, 66, CAST(125000 AS Decimal(18, 0)), 3, NULL, CAST(125000 AS Decimal(18, 0)), 1, 2, N'LAPTOP-O5KHSPMS20231001022951', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (365, 144, 70, CAST(102000 AS Decimal(18, 0)), 1, NULL, CAST(102000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20231001042924', 1)
INSERT [dbo].[TblOrderDetails] ([OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Ordered], [StatusID], [UserLog], [Active]) VALUES (366, 145, 38, CAST(240000 AS Decimal(18, 0)), 1, NULL, CAST(240000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20231001043428', 1)
SET IDENTITY_INSERT [dbo].[TblOrderDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[TblOrders] ON 

INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (97, 4, 11, CAST(340000 AS Decimal(18, 0)), CAST(N'2023-09-03T17:01:25.947' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (98, 4, 11, CAST(10000 AS Decimal(18, 0)), CAST(N'2023-09-09T10:56:52.940' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (99, 4, 11, CAST(10000 AS Decimal(18, 0)), CAST(N'2023-09-09T13:22:48.357' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (100, 4, 11, CAST(2000 AS Decimal(18, 0)), CAST(N'2023-09-09T16:28:02.990' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (101, 4, 11, CAST(70000 AS Decimal(18, 0)), CAST(N'2023-09-17T13:28:13.680' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (102, 13, 11, CAST(2000 AS Decimal(18, 0)), CAST(N'2023-09-20T20:16:26.083' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (103, 4, 11, CAST(8506 AS Decimal(18, 0)), CAST(N'2023-09-22T18:47:11.570' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (104, 13, 11, CAST(20006 AS Decimal(18, 0)), CAST(N'2023-09-22T18:57:56.553' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (105, 13, 11, CAST(25000 AS Decimal(18, 0)), CAST(N'2023-09-22T19:10:34.310' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (106, 13, 11, CAST(30006 AS Decimal(18, 0)), CAST(N'2023-09-23T09:20:51.210' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (107, 13, 11, CAST(36006 AS Decimal(18, 0)), CAST(N'2023-09-23T09:21:27.317' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (108, 13, 11, CAST(20000 AS Decimal(18, 0)), CAST(N'2023-09-23T09:50:27.973' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (109, 4, 11, CAST(50000 AS Decimal(18, 0)), CAST(N'2023-09-23T09:51:07.030' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (110, 13, 11, CAST(82000 AS Decimal(18, 0)), CAST(N'2023-09-23T09:58:25.920' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (111, 16, 11, CAST(158550 AS Decimal(18, 0)), CAST(N'2023-09-23T17:18:50.557' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (112, 16, 11, CAST(2000 AS Decimal(18, 0)), CAST(N'2023-09-23T17:25:01.017' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (113, 17, 11, CAST(28500 AS Decimal(18, 0)), CAST(N'2023-09-29T19:57:11.270' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (114, 18, 11, CAST(20000 AS Decimal(18, 0)), CAST(N'2023-09-29T19:58:30.907' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (115, 18, 11, CAST(20000 AS Decimal(18, 0)), CAST(N'2023-09-29T20:01:08.597' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (116, 18, 11, CAST(20000 AS Decimal(18, 0)), CAST(N'2023-09-29T20:10:54.330' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (117, 18, 11, CAST(40000 AS Decimal(18, 0)), CAST(N'2023-09-29T20:11:21.373' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (118, 18, 11, CAST(80000 AS Decimal(18, 0)), CAST(N'2023-09-29T20:14:09.343' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (119, 18, 11, CAST(13000 AS Decimal(18, 0)), CAST(N'2023-09-29T20:20:13.973' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (120, 18, 11, CAST(13000 AS Decimal(18, 0)), CAST(N'2023-09-29T20:22:24.703' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (121, 18, 11, CAST(8500 AS Decimal(18, 0)), CAST(N'2023-09-29T20:26:57.887' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (122, 18, 11, CAST(50500 AS Decimal(18, 0)), CAST(N'2023-09-30T17:25:52.623' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (123, 4, 11, CAST(340000 AS Decimal(18, 0)), CAST(N'2023-09-30T18:15:24.260' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (124, 4, 11, CAST(12000 AS Decimal(18, 0)), CAST(N'2023-09-30T18:17:26.533' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (125, 4, 11, CAST(40000 AS Decimal(18, 0)), CAST(N'2023-09-30T18:31:31.590' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (126, 4, 11, CAST(680000 AS Decimal(18, 0)), CAST(N'2023-09-30T18:54:27.010' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (127, 23, 11, CAST(2000 AS Decimal(18, 0)), CAST(N'2023-09-30T19:00:45.193' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (128, 26, 11, CAST(10000 AS Decimal(18, 0)), CAST(N'2023-09-30T19:16:48.403' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (129, 25, 11, CAST(340000 AS Decimal(18, 0)), CAST(N'2023-09-30T19:17:33.823' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (130, 24, 11, CAST(30000 AS Decimal(18, 0)), CAST(N'2023-09-30T19:18:18.553' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (131, 19, 11, CAST(22000 AS Decimal(18, 0)), CAST(N'2023-09-30T19:28:42.873' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (132, 27, 11, CAST(102000 AS Decimal(18, 0)), CAST(N'2023-09-30T20:45:40.540' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (133, 27, 11, CAST(16000 AS Decimal(18, 0)), CAST(N'2023-09-30T20:53:25.990' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (134, 28, 11, CAST(8500 AS Decimal(18, 0)), CAST(N'2023-09-30T22:06:11.293' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (135, 33, NULL, CAST(10000 AS Decimal(18, 0)), CAST(N'2023-09-30T22:29:00.050' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (136, 34, 11, CAST(10000 AS Decimal(18, 0)), CAST(N'2023-09-30T22:33:31.067' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (137, 4, 11, CAST(10000 AS Decimal(18, 0)), CAST(N'2023-09-30T22:42:01.463' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (138, 11, NULL, CAST(20000 AS Decimal(18, 0)), CAST(N'2023-10-01T10:14:31.330' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (139, 19, NULL, CAST(10000 AS Decimal(18, 0)), CAST(N'2023-10-01T10:15:47.527' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (140, 11, NULL, CAST(13000 AS Decimal(18, 0)), CAST(N'2023-10-01T10:44:41.263' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (141, 35, 11, CAST(13000 AS Decimal(18, 0)), CAST(N'2023-10-01T10:47:55.630' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (142, 35, NULL, CAST(20000 AS Decimal(18, 0)), CAST(N'2023-10-01T13:15:47.963' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (143, 13, 11, CAST(125000 AS Decimal(18, 0)), CAST(N'2023-10-01T14:59:57.513' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (144, 40, NULL, CAST(102000 AS Decimal(18, 0)), CAST(N'2023-10-01T16:32:21.990' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblOrders] ([OrderID], [CustomerID], [EmployeeID], [Amount], [OrderDate], [StatusID], [Ordered], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (145, 4, 11, CAST(240000 AS Decimal(18, 0)), CAST(N'2023-10-01T16:40:40.853' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[TblOrders] OFF
GO
SET IDENTITY_INSERT [dbo].[TblPosition] ON 

INSERT [dbo].[TblPosition] ([PositionID], [PositionName]) VALUES (3, N'អ្នកគ្រប់គ្រង')
INSERT [dbo].[TblPosition] ([PositionID], [PositionName]) VALUES (4, N'បុគ្គលិត')
INSERT [dbo].[TblPosition] ([PositionID], [PositionName]) VALUES (5, N'បុគ្គលិត')
SET IDENTITY_INSERT [dbo].[TblPosition] OFF
GO
SET IDENTITY_INSERT [dbo].[TblProduct] ON 

INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (33, N'Kia Morning 2016', 2, 12, N'1', 10000.0000, 3, N'images/Kia  Morning 2010.jpg', N'Kia Carnival 2022 full options (Kia Company Car) for sell 65000usd', CAST(N'2023-08-17T10:35:30.543' AS DateTime), N'KNABA24438IS69874', N'64HE8382983', N'ភ្នំពេញ​ 2BS-5245', N'MV10162319', N'2008', N'Red')
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (34, N'Ranger XLT ', 51, 14, N'1', 340000.0000, 5, N'images/RangerXLT1.JPG', N'Ford', CAST(N'2023-09-17T00:00:00.000' AS DateTime), N'KNABA24438IS69867', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2023', N'White')
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (35, N'Kia Morning 2021', 52, 12, N'1', 20000.0000, 5, N'images/kia morming 2021.jpg', NULL, CAST(N'2023-08-21T10:35:30.543' AS DateTime), N'KNABA24438IS69861', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2023', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (36, N'Toyota Pruis 2010', 51, 13, N'1', 2000.0000, 5, N'images/Pruis 2010 1.jpg', N'Toyota Tacoma 2005
ឡានស្អាត
ពិន្ទុខ្ពស់
ធនាកាត់ឈ្មោះបាន អត់បុក អត់ ក្រឡាប់ អត់ប្តូរពណ៌', CAST(N'2023-08-23T10:35:30.543' AS DateTime), N'KNABA24438IS69862', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2021', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (37, N'BMW01', 52, 15, N'1', 150000.0000, 5, N'images/BMW1.jpg', NULL, CAST(N'2023-08-25T10:35:30.543' AS DateTime), N'KNABA24438IS69863', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2019', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (38, N'BMW02', 51, 15, N'1', 240000.0000, 4, N'images/BMW2.jpg', NULL, CAST(N'2023-08-29T10:35:30.543' AS DateTime), N'KNABA24438IS698674', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2018', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (39, N'BMW03', 52, 15, N'1', 30000.0000, 5, N'images/BMW3.jpg', NULL, CAST(N'2023-09-05T10:35:30.543' AS DateTime), N'KNABA24438IS69865', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2020', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (40, N'BMW04', 2, 15, N'1', 120000.0000, 6, N'images/BMW4.jpg', NULL, CAST(N'2023-09-05T10:35:30.543' AS DateTime), N'KNABA24438IS69866', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2022', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (41, N'Ford Ranger Raptor 2021', 2, 15, N'1', 16000.0000, 5, N'images/2021-Ford-Ranger-Raptor.jpg', NULL, CAST(N'2023-09-06T10:35:30.543' AS DateTime), N'KNABA24438IS69867', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2018', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (42, N'Toyota Vigo 09', 51, 13, N'1', 20000.0000, 5, N'images/Toyota Vigo 09.jpg', NULL, CAST(N'2023-09-07T10:35:30.543' AS DateTime), N'KNABA24438IS69868', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2019', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (45, N'Kia Morning 2016', 52, 12, N'2', 13000.0000, 5, N'images/2016.jpg', NULL, CAST(N'2023-09-07T10:35:30.543' AS DateTime), N'KNABA24438IS69869', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2012', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (46, N'Kia Morning 2008', 52, 12, N'2', 8500.0000, 3, N'images/2008.jpg', NULL, CAST(N'2023-09-07T10:35:30.543' AS DateTime), N'KNABA24438IS698610', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2019', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (47, N'Kai Morning 2009', 2, 12, N'1', 10000.0000, 5, N'images/2009.jpg', NULL, CAST(N'2023-09-08T10:35:30.543' AS DateTime), N'KNABA24438IS69869', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (48, N'Kia Morning 2011', 2, 12, N'1', 12000.0000, 5, N'images/2011.jpg', NULL, CAST(N'2023-09-08T10:35:30.543' AS DateTime), N'KNABA24438IS69868', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (49, N'Kia Morning 2012', 2, 12, N'1', 12000.0000, 4, N'images/2012.jpg', NULL, CAST(N'2023-09-08T10:35:30.543' AS DateTime), N'KNABA24438IS69867', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (51, N'Fortuner 2018 Bronze', 2, 13, N'1', 20000.0000, 5, N'images/Fortuner 2018 Bronze.jpg', NULL, CAST(N'2023-09-09T10:35:30.543' AS DateTime), N'KNABA24438IS69866', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (53, N'Kia Morning 2013', 2, 12, N'2', 12000.0000, 4, N'images/2013.jpg', NULL, CAST(N'2023-09-10T10:35:30.543' AS DateTime), N'KNABA24438IS69864', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (54, N'Toyota Camry Hybrid 2007', 2, 13, N'1', 14500.0000, 5, N'images/toyotaCamry1.jpg', NULL, CAST(N'2023-09-10T10:35:30.543' AS DateTime), N'KNABA24438IS69863', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (55, N'Toyota Tacoma 2008', 2, 13, N'1', 22000.0000, 5, N'images/tacoma1.jpg', NULL, CAST(N'2023-09-12T10:35:30.543' AS DateTime), N'KNABA24438IS69862', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (56, N'Lexus RX 300t', 2, 13, N'1', 30500.0000, 5, N'images/Lexus RX 300 T 3.jpg', NULL, CAST(N'2023-09-12T10:35:30.543' AS DateTime), N'KNABA24438IS69861', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (57, N'2021 Toyota HiLux Mako detailed', 2, 13, N'1', 51000.0000, 5, N'images/hiluxMako1.JPG', NULL, CAST(N'2023-09-12T10:35:30.543' AS DateTime), N'KNABA24438IS69861', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (58, N'Toyota Wigo 2022', 2, 13, N'1', 20000.0000, 5, N'images/wigo1.JPG', NULL, CAST(N'2023-09-13T10:35:30.543' AS DateTime), N'KNABA24438IS69862', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (59, N'BMW X1', 2, 15, N'1', 40000.0000, 5, N'images/BMX1.JPG', NULL, CAST(N'2023-09-13T10:35:30.543' AS DateTime), N'KNABA24438IS69863', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'NULMV10162315L', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (60, N'BMW X5M 2021', 2, 15, N'1', 50000.0000, 5, N'images/BMW X5M 1.JPG', NULL, CAST(N'2023-09-13T10:35:30.543' AS DateTime), N'KNABA24438IS69864', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (61, N'Lexus RX300 F-Sport 2019', 2, 14, N'1', 40000.0000, 5, N'images/Lexus RX300 F-Sport 2019 1.jpg', NULL, CAST(N'2023-09-14T10:35:30.543' AS DateTime), N'KNABA24438IS69866', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (62, N'Prius 2012 Option 3', 2, 13, N'1', 24000.0000, 5, N'images/Pruis 2012 1.jpg', NULL, CAST(N'2023-09-14T10:35:30.543' AS DateTime), N'KNABA24438IS69866', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162315', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (63, N'Kia Morning 2011', 2, 12, N'1', 8500.0000, 5, N'images/Kia Morning 20114.jpg', NULL, CAST(N'2023-09-14T10:35:30.543' AS DateTime), N'KNABA24438IS69867', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162311', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (64, N'Kia Morning 2009', 2, 12, N'1', 7500.0000, 5, N'images/Kia Morning 20995.jpg', NULL, CAST(N'2023-09-15T10:35:30.543' AS DateTime), N'KNABA24438IS69868', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162312', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (65, N'Kia Morning 2008', 2, 12, N'1', 6000.0000, 5, N'images/photo_12_2023-05-13_22-45-00.jpg', NULL, CAST(N'2023-09-15T10:35:30.543' AS DateTime), N'KNABA24438IS69868', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (66, N'Lexus 450e', 2, 14, N'1', 125000.0000, 4, N'images/Lexus 450e.jpg', NULL, CAST(N'2023-09-15T10:35:30.543' AS DateTime), N'KNABA24438IS69862', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (68, N'LEXUS NX300 2018', 2, 14, N'1', 30000.0000, 5, N'images/LEXUS NX300 2018 4.jpg', NULL, CAST(N'2023-09-16T10:35:30.543' AS DateTime), N'KNABA24438IS69861', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162310', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (69, N'LEXUS NX 200t  2016', 2, 14, N'1', 42000.0000, 5, N'images/LEXUS NX 200t  2016 3.jpg', NULL, CAST(N'2023-09-16T10:35:30.543' AS DateTime), N'KNABA24438IS6986', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162311', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (70, N'Lexus NX200T Luxury f Sport', 2, 14, N'2', 102000.0000, 5, N'images/Lexus NX200T Luxury f Sport 6.jpg', NULL, CAST(N'2023-09-16T10:35:30.543' AS DateTime), N'KNABA24438IS69862', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162312', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (71, N'Lexus NX200T Luxury', 2, 14, N'1', 500000.0000, 5, N'images/Lexus NX200T Luxury 4.jpg', NULL, CAST(N'2023-09-16T10:35:30.543' AS DateTime), N'KNABA24438IS69862', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162313', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (72, N'BMW X5', 2, 14, N'1', 32000.0000, 5, N'images/bmw1.JPG', N'BMW X5', CAST(N'2023-09-23T00:00:00.000' AS DateTime), N'KNABA24438IS69861', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (73, N'Luxury ឡេីង  f Sport ប៉ូតុង9 ', 2, 14, N'1', 52300.0000, 5, N'images/1.jpg', NULL, CAST(N'2023-09-17T10:35:30.543' AS DateTime), N'KNABA24438IS6986', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162310', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (74, N'Lexus GS 300 Full P1', 2, 14, N'1', 20000.0000, 5, N'images/Lexus GS 300 Full P1 (2).jpg', NULL, CAST(N'2023-09-17T10:35:30.543' AS DateTime), N'KNABA24438IS69862', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162312', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (75, N'Lexus GS 300 Full P1', 2, 14, N'1', 20000.0000, 5, N'images/Lexus GS 300 Full P1 (2).jpg', NULL, CAST(N'2023-09-17T10:35:30.543' AS DateTime), N'KNABA24438IS69862', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (76, N'តង់ក្របទ្បាន', 2, 16, N'1', 50.0000, 4, N'images/tent1.JPG', N'', CAST(N'2023-09-17T00:00:00.000' AS DateTime), N'KNABA24438IS69869', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (78, N'car ', 2, 16, N'1', 7.0000, 5, N'images/car_phone_holder.JPG', NULL, CAST(N'2023-09-17T12:39:15.680' AS DateTime), N'KNABA24438IS69868', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2010', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (81, N'ស្រោមដែលាងរថយន្ត', 2, 16, N'1', 2.0000, 5, N'images/Car wash gloves.JPG', N'ស្រោមដែលាងរថយន្ត', CAST(N'2023-09-17T00:00:00.000' AS DateTime), N'KNABA24438IS69860', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2019', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (82, N'ជ័រការពារឆ្នូត', 2, 16, N'1', 7.0000, 15, N'images/Stripprotectionesin.JPG', N'ជ័រការពារឆ្នូត', CAST(N'2023-09-18T00:00:00.000' AS DateTime), N'KNABA24438IS698602', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2019', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (85, N'ខ្នើយកលក', 2, 12, N'1', 7.0000, 5, N'images/Pillow.JPG', N'', CAST(N'2023-09-18T00:00:00.000' AS DateTime), N'KNABA24438IS69862', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2018', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (87, N'អង់តែងរថយន្ត', 2, 16, N'១', 5.0000, 5, N'images/AutoParts.JPG', N'អង់តែងរថយន្ត', CAST(N'2023-09-19T00:00:00.000' AS DateTime), N'KNABA24438IS69860', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2023', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (88, N'ជ័រចាប់ទូរស័ព្ទលើចង្កួត', 2, 16, N'1', 2.0000, 10, N'images/Phone strap on the steering wheel.JPG', N'ជ័រចាប់ទូរស័ព្ទលើចង្កួត', CAST(N'2023-09-19T00:00:00.000' AS DateTime), N'KNABA24438IS69861', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2021', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (89, N'គំរបវ៉ាកង់រថយន្ត', 2, 16, N'1', 3.0000, 50, N'images/Tire cover.JPG', N'គំរបវ៉ាកង់រថយន្ត', CAST(N'2023-09-19T00:00:00.000' AS DateTime), N'KNABA24438IS69866', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2020', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (90, N'កាបូបដាក់ទំនិញក្រោយកៅអី', 2, 16, N'1', 4.0000, 54, N'images/Seat bag.JPG', N'កាបូបដាក់ទំនិញក្រោយកៅអី', CAST(N'2023-09-19T00:00:00.000' AS DateTime), N'KNABA24438IS69866', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2012', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (91, N'ឈុតជ័រសម្រាប់ប៉ះកង់', 2, 16, N'1', 2.5000, 5, N'images/Rubber suit for bicycle.JPG', N'ឈុតជ័រសម្រាប់ប៉ះកង់', CAST(N'2023-09-19T00:00:00.000' AS DateTime), N'KNABA24438IS69861', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2015', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (92, N'របាំងថ្ងៃរថយន្ត', 2, 16, N'1', 6.0000, 32, N'images/Car sun visor.JPG', N'របាំងថ្ងៃរថយន្ត', CAST(N'2023-09-19T00:00:00.000' AS DateTime), N'KNABA24438IS69862', N'64EE8382983', N'ភ្នំពេញ​ 2WE-5245', N'MV10162316', N'2019', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (93, N'2017 Lexus NX 200t', 2, 14, N'1', 35285.0000, 5, N'images/Lexus1.JPG', N'2017 Lexus NX 200t', CAST(N'2023-09-23T00:00:00.000' AS DateTime), N'JT4RN13P7K0001611', N'64EB8382983', N'ភ្នំពេញ​ 2SD-5245', N'MV10162309', N'2017', NULL)
INSERT [dbo].[TblProduct] ([ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image], [detail], [ProductDate], [ShortNumber], [EngineNumber], [TagID], [MV], [CarManufactureDate], [Color]) VALUES (94, N' Kia Carnival 2023 Full Options', 2, 12, N'5', 21000.0000, 5, N'images/Kia Carnival 2023 Full Options 1.jpg', N'Kia Carnival 2023 Full Options Noblesse 11កៅអី 3500cc ម៉ាស៊ីនសាំង ថ្មី 240Km មកដល់ខ្ទង់ខែ9 នេះហើយ កក់មុនតម្លៃពិសេស', CAST(N'2023-10-01T00:00:00.000' AS DateTime), N'AT4N10P7K0001616', N'64CE8382910', N' ភ្នំពេញ​ AS-5245', N' BA10162315', N'2023', NULL)
SET IDENTITY_INSERT [dbo].[TblProduct] OFF
GO
SET IDENTITY_INSERT [dbo].[TblQuotationDetails] ON 

INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (3, 1, 37, CAST(150000 AS Decimal(18, 0)), 1, NULL, CAST(150000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230929072450', 1)
INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (4, 1, 46, CAST(8500 AS Decimal(18, 0)), 1, NULL, CAST(8500 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230929072450', 1)
INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (5, 2, 66, CAST(125000 AS Decimal(18, 0)), 1, NULL, CAST(125000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230929072450', 1)
INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (7, 4, 52, CAST(30000 AS Decimal(18, 0)), 1, NULL, CAST(30000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230929072927', 1)
INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (8, 5, 36, CAST(2000 AS Decimal(18, 0)), 1, NULL, CAST(2000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230930011542', 1)
INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (9, 6, 33, CAST(10000 AS Decimal(18, 0)), 2, NULL, CAST(10000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230930051647', 1)
INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (10, 7, 46, CAST(8500 AS Decimal(18, 0)), 1, NULL, CAST(8500 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230930083548', 1)
INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (11, 8, 33, CAST(10000 AS Decimal(18, 0)), 1, NULL, CAST(10000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20230930101630', 1)
INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (13, 9, 55, CAST(22000 AS Decimal(18, 0)), 1, NULL, CAST(22000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20231001022951', 1)
INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (16, 9, 66, CAST(125000 AS Decimal(18, 0)), 1, NULL, CAST(125000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20231001022951', 1)
INSERT [dbo].[TblQuotationDetails] ([QuotationDetailID], [QuotationID], [ProductID], [UnitPrice], [Quantity], [Discount], [Amount], [Quotationed], [StatusID], [UserLog], [Active]) VALUES (17, 10, 35, CAST(20000 AS Decimal(18, 0)), 1, NULL, CAST(20000 AS Decimal(18, 0)), 1, 1, N'LAPTOP-O5KHSPMS20231001062825', 1)
SET IDENTITY_INSERT [dbo].[TblQuotationDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[TblQuotations] ON 

INSERT [dbo].[TblQuotations] ([QuotationID], [CustomerID], [EmployeeID], [Amount], [QuotationDate], [StatusID], [Quotationed], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (1, 18, 11, CAST(158500 AS Decimal(18, 0)), CAST(N'2023-09-29T19:26:12.550' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblQuotations] ([QuotationID], [CustomerID], [EmployeeID], [Amount], [QuotationDate], [StatusID], [Quotationed], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (2, 18, 11, CAST(283500 AS Decimal(18, 0)), CAST(N'2023-09-29T19:29:00.577' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblQuotations] ([QuotationID], [CustomerID], [EmployeeID], [Amount], [QuotationDate], [StatusID], [Quotationed], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (4, 13, 11, CAST(30000 AS Decimal(18, 0)), CAST(N'2023-09-29T19:29:52.120' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblQuotations] ([QuotationID], [CustomerID], [EmployeeID], [Amount], [QuotationDate], [StatusID], [Quotationed], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (5, 18, 11, CAST(2000 AS Decimal(18, 0)), CAST(N'2023-09-30T13:24:11.317' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblQuotations] ([QuotationID], [CustomerID], [EmployeeID], [Amount], [QuotationDate], [StatusID], [Quotationed], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (6, 18, 11, CAST(10000 AS Decimal(18, 0)), CAST(N'2023-09-30T17:16:55.387' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblQuotations] ([QuotationID], [CustomerID], [EmployeeID], [Amount], [QuotationDate], [StatusID], [Quotationed], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (7, 18, 11, CAST(8500 AS Decimal(18, 0)), CAST(N'2023-09-30T20:38:59.360' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblQuotations] ([QuotationID], [CustomerID], [EmployeeID], [Amount], [QuotationDate], [StatusID], [Quotationed], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (8, 18, 11, CAST(10007 AS Decimal(18, 0)), CAST(N'2023-09-30T22:45:52.153' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblQuotations] ([QuotationID], [CustomerID], [EmployeeID], [Amount], [QuotationDate], [StatusID], [Quotationed], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (9, 36, 11, CAST(147000 AS Decimal(18, 0)), CAST(N'2023-10-01T15:06:05.873' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[TblQuotations] ([QuotationID], [CustomerID], [EmployeeID], [Amount], [QuotationDate], [StatusID], [Quotationed], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (10, 42, 11, CAST(20000 AS Decimal(18, 0)), CAST(N'2023-10-01T18:38:42.740' AS DateTime), 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[TblQuotations] OFF
GO
SET IDENTITY_INSERT [dbo].[TblReply] ON 

INSERT [dbo].[TblReply] ([ReplyID], [Reply], [CommentID], [UserID], [ReplyDate]) VALUES (33, N'ចាសសួស្តីបង តើបងចង់សួរពត័មានអំពីអ្វីដែរ?', 26, 4, CAST(N'2023-09-17T15:29:51.747' AS DateTime))
INSERT [dbo].[TblReply] ([ReplyID], [Reply], [CommentID], [UserID], [ReplyDate]) VALUES (36, N'សួស្តីបង តើបងចង់បានឡានម៉ាក់អ្វី', 29, 11, CAST(N'2023-09-30T15:53:48.313' AS DateTime))
INSERT [dbo].[TblReply] ([ReplyID], [Reply], [CommentID], [UserID], [ReplyDate]) VALUES (38, N'ខ្ញុំចង់បានឡានមួយគ្រឿង ម៉ាក់ Luxus តើវាតម្លៃប៉ុន្មាន', 29, 4, CAST(N'2023-09-30T15:54:16.033' AS DateTime))
INSERT [dbo].[TblReply] ([ReplyID], [Reply], [CommentID], [UserID], [ReplyDate]) VALUES (42, N'មានអ្វីចង់សួរអ្វីដែរ', 31, 11, CAST(N'2023-09-30T20:54:57.640' AS DateTime))
INSERT [dbo].[TblReply] ([ReplyID], [Reply], [CommentID], [UserID], [ReplyDate]) VALUES (43, N'Hello bong ', 33, 11, CAST(N'2023-09-30T22:38:04.137' AS DateTime))
SET IDENTITY_INSERT [dbo].[TblReply] OFF
GO
SET IDENTITY_INSERT [dbo].[TblRole] ON 

INSERT [dbo].[TblRole] ([RoleID], [RoleName]) VALUES (1, N'Guest')
INSERT [dbo].[TblRole] ([RoleID], [RoleName]) VALUES (2, N'User')
INSERT [dbo].[TblRole] ([RoleID], [RoleName]) VALUES (3, N'Admin')
SET IDENTITY_INSERT [dbo].[TblRole] OFF
GO
SET IDENTITY_INSERT [dbo].[TblShippers] ON 

INSERT [dbo].[TblShippers] ([ShipperID], [CompanyName], [Phone]) VALUES (5, N'ក្រុមហ៊ុន​លក់ទ្បាន carpower', N'015417987')
INSERT [dbo].[TblShippers] ([ShipperID], [CompanyName], [Phone]) VALUES (6, N'ក្រុមហ៊ុន​លក់ទ្បាន carpower', N'0977920939')
INSERT [dbo].[TblShippers] ([ShipperID], [CompanyName], [Phone]) VALUES (7, N'ក្រុមហ៊ុន​ Nham 24', N'0964459492')
SET IDENTITY_INSERT [dbo].[TblShippers] OFF
GO
SET IDENTITY_INSERT [dbo].[TblStatus] ON 

INSERT [dbo].[TblStatus] ([StatusID], [Status]) VALUES (1, N'Active')
INSERT [dbo].[TblStatus] ([StatusID], [Status]) VALUES (2, N'Disable')
SET IDENTITY_INSERT [dbo].[TblStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[TblStatusOrder] ON 

INSERT [dbo].[TblStatusOrder] ([StatusID], [StatusName]) VALUES (1, N'Ordering')
INSERT [dbo].[TblStatusOrder] ([StatusID], [StatusName]) VALUES (2, N'Proccessing')
INSERT [dbo].[TblStatusOrder] ([StatusID], [StatusName]) VALUES (3, N'Completed')
INSERT [dbo].[TblStatusOrder] ([StatusID], [StatusName]) VALUES (4, N'Cancel')
SET IDENTITY_INSERT [dbo].[TblStatusOrder] OFF
GO
SET IDENTITY_INSERT [dbo].[TblSuppliers] ON 

INSERT [dbo].[TblSuppliers] ([SupplierID], [CompanyName], [ContactName], [Address], [City], [Region], [PostalCode], [Country], [Phone], [Fax], [Website]) VALUES (2, N'AEU university', N'EK sarayuth', N'test', N'Phnom penh', N'khmer', N'no have', N'Cambodia', N'015417987', N'no have', N'carpower')
INSERT [dbo].[TblSuppliers] ([SupplierID], [CompanyName], [ContactName], [Address], [City], [Region], [PostalCode], [Country], [Phone], [Fax], [Website]) VALUES (51, N'yuth', N'father', N'kanal', N'phnom penh', N'cambodia', N'1201', N'cambodia', N'015417987', N'test', N'carpower')
INSERT [dbo].[TblSuppliers] ([SupplierID], [CompanyName], [ContactName], [Address], [City], [Region], [PostalCode], [Country], [Phone], [Fax], [Website]) VALUES (52, N'Sunday', N'Sunday', N'Sunday', N'Sunday', N'Sunday', N'1234', N'Sunday', N'1111', N'Sunday', N'Sunday')
SET IDENTITY_INSERT [dbo].[TblSuppliers] OFF
GO
SET IDENTITY_INSERT [dbo].[TblTest] ON 

INSERT [dbo].[TblTest] ([ShipperID], [CompanyName], [Phone], [TestList]) VALUES (1, N'yuth', NULL, NULL)
INSERT [dbo].[TblTest] ([ShipperID], [CompanyName], [Phone], [TestList]) VALUES (2, N'meng', NULL, NULL)
INSERT [dbo].[TblTest] ([ShipperID], [CompanyName], [Phone], [TestList]) VALUES (3, N'dom', NULL, NULL)
SET IDENTITY_INSERT [dbo].[TblTest] OFF
GO
SET IDENTITY_INSERT [dbo].[TblUser] ON 

INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (4, N'Sinoun', N'123456', 2, 1, N'093205555', N'teacher@gmail.com', N'ឃ្លាំង203ផ្លូវជាតិលេខ៣,ព្រៃព្រីងខាងត្បូង,ចោមចៅ3,ពោធិ៍សែនជ័យ,រាជធានីភ្នំពេញ', N'images', N'ប្រុស', 35, N'អ្នកទិញ', N'098765487', N'N12345678')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (11, N'Admin', N'Admin123', 3, 1, N'0977715491', N'carpoweradmin@gmail.com', N'ឃ្លាំង203ផ្លូវជាតិលេខ៣,ព្រៃព្រីងខាងត្បូង,ចោមចៅ3,ពោធិ៍សែនជ័យ,រាជធានីភ្នំពេញ', N'NULL', N'ប្រុស', 28, N'អ្នកលក់', N'098567432', N'N04019898')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (12, N'Phally', N'123', 3, 1, N'012852685', N'admin@carpower@gmail.com', N'ឃ្លាំង203ផ្លូវជាតិលេខ៣,ព្រៃព្រីងខាងត្បូង,ចោមចៅ3,ពោធិ៍សែនជ័យ,រាជធានីភ្នំពេញ', NULL, N'ស្រី', 37, N'លក់រថយន្ត', N'011182116', N'N04019898')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (13, N'Yuth', N'N04019898', 2, 1, N'015417987', N'rayuthek430@gmail.com', N'ខណ្ឌព្រែកព្នៅ​ស្រុកពញាឮខេត្តកណ្តាលភ្នំពេញ', NULL, N'ប្រុស', 21, N'អ្នកទិញ', N'011182121', N'N04019898')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (16, N'NewUser11', N'123', 2, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (17, N'Chan', N'chan123', 2, 1, N'0977715491', N'chan@gmail.com', N'ភ្នំពេញ', NULL, N'ប្រុស', 23, N'អ្នកទិញ', N'1509551', N'1ED009')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (18, N'test', N'12345', 2, 1, N'015417987', N'rayuthek430@gmail.com', N'Phnom penh', NULL, N'ប្រុស', 22, N'អ្នកទិញ', N'121', N'121')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (19, N'NewUser1', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (20, N'NewUser2', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (21, N'NewUser3', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (22, N'NewUser4', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (23, N'NewUser5', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (24, N'NewUser6', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (25, N'NewUser7', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (26, N'NewUser8', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (27, N'NewUser9', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (28, N'NewUser10', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (29, N'NewUser11', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (30, N'NewUser13', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (33, N'sinuon2', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (34, N'sinuon3', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (35, N'NewUser16', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (36, N'chan', N'123', 2, 1, N'0968635390', N'chan@gmail.com', N'ភ្នំពេញ សង្កាត់ទឹកល្អក់', NULL, N'ប្រុស', 22, N'អ្នកទិញ', N'', N'N04019810')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (40, N'sinuon6', N'123', 1, 1, N'093242455', N'NewUser1@gmail.com', N'Phnom Penh', NULL, N'ប្រុស', 30, N'អ្នកទិញ', N'0102003456', N'N010549494')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (41, N'abc', N'', 2, 1, N'099888888', N'', N'', NULL, N'ប្រុស', 0, N'អ្នកទិញ', N'', N'')
INSERT [dbo].[TblUser] ([UserID], [FullName], [Password], [RoleID], [StatusID], [PhoneNumber], [Email], [Address], [Image], [Gender], [Age], [Position], [IdCard], [Passport]) VALUES (42, N'NewUser1', N'123456', 2, 1, N'098765432', N'NewUser1@gmail.com', N'PP', NULL, N'ប្រុស', 29, N'អ្នកទិញ', N'N091772277', N'N009123456')
SET IDENTITY_INSERT [dbo].[TblUser] OFF
GO
ALTER TABLE [dbo].[TblComment] ADD  CONSTRAINT [DF_TblComment_CommentDate]  DEFAULT (getdate()) FOR [CommentDate]
GO
ALTER TABLE [dbo].[TblOrderDetails] ADD  CONSTRAINT [DF_TblOrderDetails_Ordered]  DEFAULT ((0)) FOR [Ordered]
GO
ALTER TABLE [dbo].[TblOrderDetails] ADD  CONSTRAINT [DF_TblOrderDetails_Active]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[TblOrders] ADD  CONSTRAINT [DF_TblOrders_OrderDate]  DEFAULT (getdate()) FOR [OrderDate]
GO
ALTER TABLE [dbo].[TblOrders] ADD  CONSTRAINT [DF_TblOrders_Ordered]  DEFAULT ((0)) FOR [Ordered]
GO
ALTER TABLE [dbo].[TblProduct] ADD  CONSTRAINT [DF_TblProduct_ProductDate]  DEFAULT (getdate()) FOR [ProductDate]
GO
ALTER TABLE [dbo].[TblQuotationDetails] ADD  CONSTRAINT [DF_TblQuotationDetails_Quotationed]  DEFAULT ((0)) FOR [Quotationed]
GO
ALTER TABLE [dbo].[TblQuotationDetails] ADD  CONSTRAINT [DF_TblQuotationDetails_Active]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[TblQuotations] ADD  CONSTRAINT [DF_TblQuotations_QuotationDate]  DEFAULT (getdate()) FOR [QuotationDate]
GO
ALTER TABLE [dbo].[TblQuotations] ADD  CONSTRAINT [DF_TblQuotations_Quotationed]  DEFAULT ((0)) FOR [Quotationed]
GO
ALTER TABLE [dbo].[TblReply] ADD  CONSTRAINT [DF_TblReply_ReplyDate]  DEFAULT (getdate()) FOR [ReplyDate]
GO
ALTER TABLE [dbo].[TblComment]  WITH CHECK ADD  CONSTRAINT [FK_TblComment_TblOrderDetails] FOREIGN KEY([OrderDetailID])
REFERENCES [dbo].[TblOrderDetails] ([OrderDetailID])
GO
ALTER TABLE [dbo].[TblComment] CHECK CONSTRAINT [FK_TblComment_TblOrderDetails]
GO
ALTER TABLE [dbo].[TblDescription]  WITH CHECK ADD  CONSTRAINT [FK_TblDescription_TblProduct] FOREIGN KEY([ProductID])
REFERENCES [dbo].[TblProduct] ([ProductID])
GO
ALTER TABLE [dbo].[TblDescription] CHECK CONSTRAINT [FK_TblDescription_TblProduct]
GO
ALTER TABLE [dbo].[TblDocument]  WITH CHECK ADD  CONSTRAINT [FK_TblDocument_TblProduct] FOREIGN KEY([ProducctID])
REFERENCES [dbo].[TblProduct] ([ProductID])
GO
ALTER TABLE [dbo].[TblDocument] CHECK CONSTRAINT [FK_TblDocument_TblProduct]
GO
ALTER TABLE [dbo].[TblDocumentBuyer]  WITH CHECK ADD  CONSTRAINT [FK_TblDocumentBuyer_TblDocumentBuyerType] FOREIGN KEY([DocumentBuyerTypeID])
REFERENCES [dbo].[TblDocumentBuyerType] ([DocumentBuyerTypeID])
GO
ALTER TABLE [dbo].[TblDocumentBuyer] CHECK CONSTRAINT [FK_TblDocumentBuyer_TblDocumentBuyerType]
GO
ALTER TABLE [dbo].[TblDocumentBuyer]  WITH CHECK ADD  CONSTRAINT [FK_TblDocumentBuyer_TblOrderDetails] FOREIGN KEY([OrderDetailID])
REFERENCES [dbo].[TblOrderDetails] ([OrderDetailID])
GO
ALTER TABLE [dbo].[TblDocumentBuyer] CHECK CONSTRAINT [FK_TblDocumentBuyer_TblOrderDetails]
GO
ALTER TABLE [dbo].[TblImage]  WITH CHECK ADD  CONSTRAINT [FK_TblImage_TblProduct] FOREIGN KEY([ProductID])
REFERENCES [dbo].[TblProduct] ([ProductID])
GO
ALTER TABLE [dbo].[TblImage] CHECK CONSTRAINT [FK_TblImage_TblProduct]
GO
ALTER TABLE [dbo].[TblOrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_TblOrderDetails_TblOrders] FOREIGN KEY([OrderID])
REFERENCES [dbo].[TblOrders] ([OrderID])
GO
ALTER TABLE [dbo].[TblOrderDetails] CHECK CONSTRAINT [FK_TblOrderDetails_TblOrders]
GO
ALTER TABLE [dbo].[TblOrders]  WITH CHECK ADD  CONSTRAINT [FK_TblOrders_TblUser] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[TblUser] ([UserID])
GO
ALTER TABLE [dbo].[TblOrders] CHECK CONSTRAINT [FK_TblOrders_TblUser]
GO
ALTER TABLE [dbo].[TblProduct]  WITH CHECK ADD  CONSTRAINT [FK_TblProduct_TblCategory1] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[TblCategory] ([CategoryID])
GO
ALTER TABLE [dbo].[TblProduct] CHECK CONSTRAINT [FK_TblProduct_TblCategory1]
GO
ALTER TABLE [dbo].[TblProduct]  WITH CHECK ADD  CONSTRAINT [FK_TblProduct_TblSuppliers] FOREIGN KEY([SupplierID])
REFERENCES [dbo].[TblSuppliers] ([SupplierID])
GO
ALTER TABLE [dbo].[TblProduct] CHECK CONSTRAINT [FK_TblProduct_TblSuppliers]
GO
ALTER TABLE [dbo].[TblReply]  WITH CHECK ADD  CONSTRAINT [FK_TblReply_TblComment] FOREIGN KEY([CommentID])
REFERENCES [dbo].[TblComment] ([CommentID])
GO
ALTER TABLE [dbo].[TblReply] CHECK CONSTRAINT [FK_TblReply_TblComment]
GO
ALTER TABLE [dbo].[TblUser]  WITH CHECK ADD  CONSTRAINT [FK_TblUser_TblRole] FOREIGN KEY([RoleID])
REFERENCES [dbo].[TblRole] ([RoleID])
GO
ALTER TABLE [dbo].[TblUser] CHECK CONSTRAINT [FK_TblUser_TblRole]
GO
ALTER TABLE [dbo].[TblUser]  WITH CHECK ADD  CONSTRAINT [FK_TblUser_TblStatus] FOREIGN KEY([StatusID])
REFERENCES [dbo].[TblStatus] ([StatusID])
GO
ALTER TABLE [dbo].[TblUser] CHECK CONSTRAINT [FK_TblUser_TblStatus]
GO
/****** Object:  StoredProcedure [dbo].[carpower_detail_imgs]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[carpower_detail_imgs]
    @ProductID int
AS
SELECT p.ProductID, p.ProductName, p.UnitPrice, i.ImageID, i.ImageUrl, p.image
FROM dbo.TblProduct p
INNER JOIN dbo.TblImage i
ON p.ProductID = i.ProductID
WHERE p.ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[carpower_list_description]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[carpower_list_description]
    @ProductID int
AS
SELECT 
    p.ProductID, p.ProductName, p.UnitPrice, p.detail, 
    d.DescriptionDetailsProduct, d.CarMakes, d.CarModel, 
    d.Color, d.Year
FROM 
    dbo.TblProduct p
    INNER JOIN dbo.TblDescription d 
        ON p.ProductID = d.ProductID
WHERE 
    p.ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[list_detail_single_img]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[list_detail_single_img]
@ProductID int
as
SELECT detail, image, UnitPrice, ProductName, ProductID
FROM     dbo.TblProduct

where ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[ListAllUser]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ListAllUser]
    
AS

SELECT COUNT(*)
FROM TblUser
GO
/****** Object:  StoredProcedure [dbo].[listDecription]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[listDecription]
@ProductID int
AS
SELECT D.DescriptionDetailsProduct, D.CarMakes, D.CarModel, D.Year, D.Color, P.ProductName, P.UnitPrice, P.detail, P.ProductID
FROM dbo.TblProduct P
INNER JOIN dbo.TblDescription D
ON P.ProductID = D.ProductID
WHERE P.ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[Pro_AddTocartAmount]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pro_AddTocartAmount]

@UserLog nvarchar(250)

as

SELECT sum(UnitPrice * Quantity)as Amount
      
  FROM [dbo].[TblOrderDetails]

  where UserLog = @UserLog

GO
/****** Object:  StoredProcedure [dbo].[Pro_AddTocartAmountQuotation]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_AddTocartAmountQuotation]

@UserLog nvarchar(250)

as

SELECT sum(UnitPrice * Quantity)as Amount
      
  FROM [dbo].[TblQuotationDetails]

  where UserLog = @UserLog
GO
/****** Object:  StoredProcedure [dbo].[pro_AdminAprovalOrder]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[pro_AdminAprovalOrder]

@OrderDetailID int
,@StatusID int
,@EmployeeID int
as

UPDATE [dbo].[TblOrderDetails]
   SET StatusID=@StatusID
 WHERE OrderDetailID=@OrderDetailID

UPDATE [dbo].[TblOrders] SET [EmployeeID] = @EmployeeID
     
 WHERE OrderID=(select top 1 OrderID from TblOrderDetails where OrderDetailID=@OrderDetailID)
GO
/****** Object:  StoredProcedure [dbo].[pro_AdminHideOrderDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[pro_AdminHideOrderDetail]

@OrderDetailID int
as

UPDATE [dbo].[TblOrderDetails]
   SET Active=0
 WHERE OrderDetailID=@OrderDetailID

GO
/****** Object:  StoredProcedure [dbo].[pro_AdminShowOrderDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[pro_AdminShowOrderDetail]

@OrderDetailID int
as

UPDATE [dbo].[TblOrderDetails]
   SET Active=1
 WHERE OrderDetailID=@OrderDetailID

GO
/****** Object:  StoredProcedure [dbo].[Pro_CountCommentReply]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_CountCommentReply]

as

SELECT       Count(distinct dbo.TblComment.CommentID) As CountComment, count( dbo.TblReply.ReplyID) as CountReply
FROM            dbo.TblComment INNER JOIN
                         dbo.TblReply ON dbo.TblComment.CommentID = dbo.TblReply.CommentID
GO
/****** Object:  StoredProcedure [dbo].[Pro_CountCustomer]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_CountCustomer]

as

SELECT count([UserID])as CountCustomer

  FROM [dbo].[TblUser]
Where RoleID in (1)
GO
/****** Object:  StoredProcedure [dbo].[Pro_CountDocument]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_CountDocument]

as

SELECT Count([DocumentBuyerID])as CountDocument
     
  FROM [dbo].[TblDocumentBuyer]
GO
/****** Object:  StoredProcedure [dbo].[Pro_CountOrderProduct]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_CountOrderProduct]

as

SELECT Count([OrderDetailID])as CountOrderProduct
     

  
  FROM [dbo].[TblOrderDetails]
  Where StatusID=1
GO
/****** Object:  StoredProcedure [dbo].[Pro_CountProccessingProduct]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_CountProccessingProduct]

as

SELECT Count([OrderDetailID])as CountProccessingProduct
     

  
  FROM [dbo].[TblOrderDetails]
  Where StatusID=2
GO
/****** Object:  StoredProcedure [dbo].[Pro_CountProductByCancel]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_CountProductByCancel]

as

SELECT Count([OrderDetailID])as CountProductByCancel
     

  
  FROM [dbo].[TblOrderDetails]
  Where StatusID=4
GO
/****** Object:  StoredProcedure [dbo].[Pro_CountProductBySale]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_CountProductBySale]

as

SELECT Count([OrderDetailID])as CountProductBySale
     

  
  FROM [dbo].[TblOrderDetails]
  Where StatusID=3
GO
/****** Object:  StoredProcedure [dbo].[Pro_CountQuotation]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_CountQuotation]

as

SELECT count([QuotationID])as CountQuotation

  FROM [dbo].[TblQuotations]
GO
/****** Object:  StoredProcedure [dbo].[Pro_CountUser]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_CountUser]

as

SELECT count([UserID])as CountUser

  FROM [dbo].[TblUser]
Where RoleID in (2,3)
GO
/****** Object:  StoredProcedure [dbo].[Pro_CutStock]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pro_CutStock]

@ProductID  int,
@Quantity int

as

update  dbo.TblProduct set UnitInStock -=@Quantity 

Where ProductID =@ProductID

--exec Pro_CutStock 34,2
GO
/****** Object:  StoredProcedure [dbo].[Pro_EditInvoiceInsertOrderDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_EditInvoiceInsertOrderDetail]

@OrderID int
,@ProductID int
,@UnitPrice decimal(18,0)
,@Quantity smallint
,@Amount decimal(18,0)
,@UserLog nvarchar(150)
,@RetureString nvarchar(50) output
as
if  not exists (select ProductID from TblProduct where ProductID=@ProductID and UnitInStock <=0)
begin

if not exists (select * from TblOrderDetails Where ProductID=@ProductID and UserLog=@UserLog)
begin
INSERT INTO [dbo].[TblOrderDetails]
           (OrderID,[ProductID]
           ,[UnitPrice]
           ,[Quantity]
           ,[Amount]
           ,[UserLog],StatusID,Ordered)
     VALUES
           (@OrderID,
       @ProductID
           ,@UnitPrice
           ,@Quantity
           ,@Amount
           ,@UserLog,1,1)
end
else
begin
 update TblOrderDetails set Quantity+=1 Where ProductID=@ProductID and UserLog=@UserLog
end

end
else
begin
set @RetureString='Not In Stock'
end
GO
/****** Object:  StoredProcedure [dbo].[Pro_EditInvoiceSelectOrderDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[Pro_EditInvoiceSelectOrderDetail]

@OrderID int

as

SELECT        dbo.TblOrderDetails.OrderDetailID, dbo.TblProduct.ProductID, dbo.TblProduct.ProductName, dbo.TblOrderDetails.UnitPrice, dbo.TblOrderDetails.Quantity, dbo.TblProduct.image,dbo.TblOrderDetails.UserLog
FROM            dbo.TblOrderDetails INNER JOIN
                         dbo.TblProduct ON dbo.TblOrderDetails.ProductID = dbo.TblProduct.ProductID
WHERE        (TblOrderDetails.OrderID = @OrderID) 
Order by dbo.TblOrderDetails.OrderDetailID desc
GO
/****** Object:  StoredProcedure [dbo].[Pro_EditInvoiceUpdateAmountOrder]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_EditInvoiceUpdateAmountOrder]

@OrderID int
,@Amount float

as

UPDATE [dbo].[TblOrders]
   SET [Amount] = @Amount
     
 WHERE OrderID=@OrderID
GO
/****** Object:  StoredProcedure [dbo].[Pro_InsertOrder]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Pro_InsertOrder]

@EmployeeID int
,
@CustomerID int
,@UserLog nvarchar(50)
,@Amount float
,@OrderIDOutput int output
as


INSERT INTO [dbo].[TblOrders]
           (EmployeeID,
		   [CustomerID],Amount,StatusID,Ordered)
     VALUES
           (@EmployeeID,
		   @CustomerID,@Amount,1,1)
set @OrderIDOutput=SCOPE_IDENTITY();
Update TblOrderDetails set OrderID=SCOPE_IDENTITY(),Ordered=1 where UserLog=@UserLog and Ordered=0


GO
/****** Object:  StoredProcedure [dbo].[Pro_InsertOrderDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pro_InsertOrderDetail]

@ProductID int
,@UnitPrice decimal(18,0)
,@Quantity smallint
,@Amount decimal(18,0)
,@UserLog nvarchar(150)
,@RetureString nvarchar(50) output
as
if  not exists (select ProductID from TblProduct where ProductID=@ProductID and UnitInStock <=0)
begin

if not exists (select * from TblOrderDetails Where ProductID=@ProductID and UserLog=@UserLog)
begin
INSERT INTO [dbo].[TblOrderDetails]
           ([ProductID]
           ,[UnitPrice]
           ,[Quantity]
           ,[Amount]
           ,[UserLog],StatusID)
     VALUES
           (@ProductID
           ,@UnitPrice
           ,@Quantity
           ,@Amount
           ,@UserLog,1)
end
else
begin
 update TblOrderDetails set Quantity+=1 Where ProductID=@ProductID and UserLog=@UserLog
end

end
else
begin
set @RetureString='Not In Stock'
end
GO
/****** Object:  StoredProcedure [dbo].[Pro_InsertOrderDetailForClient]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_InsertOrderDetailForClient]

@ProductID int
,@UnitPrice decimal(18,0)
,@Quantity smallint
,@Amount decimal(18,0)
,@UserLog nvarchar(150)

as


if not exists (select * from TblOrderDetails Where ProductID=@ProductID and UserLog=@UserLog)
begin
INSERT INTO [dbo].[TblOrderDetails]
           ([ProductID]
           ,[UnitPrice]
           ,[Quantity]
           ,[Amount]
           ,[UserLog],StatusID)
     VALUES
           (@ProductID
           ,@UnitPrice
           ,@Quantity
           ,@Amount
           ,@UserLog,1)
end
else
begin
 update TblOrderDetails set Quantity+=1 Where ProductID=@ProductID and UserLog=@UserLog
end


GO
/****** Object:  StoredProcedure [dbo].[Pro_InsertOrderForClient]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[Pro_InsertOrderForClient]


@CustomerID int
,@UserLog nvarchar(50)
,@Amount float
,@OrderIDOutput int output
as


INSERT INTO [dbo].[TblOrders]
           (
		   [CustomerID],Amount,StatusID,Ordered)
     VALUES
           (
		   @CustomerID,@Amount,1,1)
set @OrderIDOutput=SCOPE_IDENTITY();
Update TblOrderDetails set OrderID=SCOPE_IDENTITY(),Ordered=1 where UserLog=@UserLog and Ordered=0


GO
/****** Object:  StoredProcedure [dbo].[Pro_InsertQuotation]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[Pro_InsertQuotation]

@EmployeeID int
,
@CustomerID int
,@UserLog nvarchar(50)
,@Amount float
,@QuotationIDOutput int output
as


INSERT INTO [dbo].[TblQuotations]
           (EmployeeID,
       [CustomerID],Amount,StatusID,Quotationed)
     VALUES
           (@EmployeeID,
       @CustomerID,@Amount,1,1)
set @QuotationIDOutput=SCOPE_IDENTITY();
Update TblQuotationDetails set QuotationID=SCOPE_IDENTITY(),Quotationed=1 where UserLog=@UserLog and Quotationed=0
GO
/****** Object:  StoredProcedure [dbo].[Pro_InsertQuotationDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[Pro_InsertQuotationDetail]

@ProductID int
,@UnitPrice decimal(18,0)
,@Quantity smallint
,@Amount decimal(18,0)
,@UserLog nvarchar(150)

as
if not exists (select * from TblQuotationDetails Where ProductID=@ProductID and UserLog=@UserLog)
begin
INSERT INTO [dbo].[TblQuotationDetails]
           ([ProductID]
           ,[UnitPrice]
           ,[Quantity]
           ,[Amount]
           ,[UserLog],StatusID)
     VALUES
           (@ProductID
           ,@UnitPrice
           ,@Quantity
           ,@Amount
           ,@UserLog,1)
end
else
begin
 update TblQuotationDetails set Quantity+=1 Where ProductID=@ProductID and UserLog=@UserLog
end
GO
/****** Object:  StoredProcedure [dbo].[Pro_InvoiceEmployee]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pro_InvoiceEmployee]

@OrderID int

as
SELECT dbo.TblOrders.OrderID,dbo.TblOrders.OrderDate, dbo.TblUser.FullName, dbo.TblUser.Gender, dbo.TblUser.Age, dbo.TblUser.Position, dbo.TblUser.Address, dbo.TblUser.IdCard, dbo.TblUser.Passport
FROM     dbo.TblOrders INNER JOIN
                  dbo.TblUser ON dbo.TblOrders.EmployeeID = dbo.TblUser.UserID

Where dbo.TblUser.RoleID=3 and TblOrders.OrderID=@OrderID
GO
/****** Object:  StoredProcedure [dbo].[Pro_InvoiceOrder]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pro_InvoiceOrder]

@OrderID int 

as

SELECT dbo.TblOrders.OrderID, dbo.TblUser.FullName, dbo.TblUser.PhoneNumber, dbo.TblOrders.Amount, dbo.TblOrders.OrderDate, dbo.TblOrders.Ordered
FROM     dbo.TblOrders INNER JOIN
                  dbo.TblUser ON dbo.TblOrders.CustomerID = dbo.TblUser.UserID
Where dbo.TblOrders.OrderID=@OrderID
GO
/****** Object:  StoredProcedure [dbo].[Pro_InvoiceOrderDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pro_InvoiceOrderDetail]

@OrderID int 

as

SELECT dbo.TblOrderDetails.ProductID,dbo.TblProduct.image, dbo.TblProduct.ProductName, dbo.TblOrderDetails.UnitPrice, dbo.TblOrderDetails.Quantity, dbo.TblOrderDetails.Amount
FROM     dbo.TblOrderDetails INNER JOIN
                  dbo.TblProduct ON dbo.TblOrderDetails.ProductID = dbo.TblProduct.ProductID

Where dbo.TblOrderDetails.OrderID=@OrderID
GO
/****** Object:  StoredProcedure [dbo].[Pro_InvoiceOrderDetailProduct]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pro_InvoiceOrderDetailProduct]

@OrderID int

as

SELECT dbo.TblProduct.ProductName, dbo.TblProduct.ShortNumber, dbo.TblProduct.EngineNumber, dbo.TblProduct.TagID, dbo.TblProduct.MV, dbo.TblProduct.CarManufactureDate, dbo.TblCategory.CategoryName,TblProduct.color
FROM     dbo.TblOrderDetails INNER JOIN
                  dbo.TblProduct ON dbo.TblOrderDetails.ProductID = dbo.TblProduct.ProductID INNER JOIN
                  dbo.TblCategory ON dbo.TblProduct.CategoryID = dbo.TblCategory.CategoryID
Where dbo.TblOrderDetails.OrderID=@OrderID
GO
/****** Object:  StoredProcedure [dbo].[Pro_InvoiceQuotation]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_InvoiceQuotation]

@QuotationID int 

as

SELECT dbo.TblQuotations.QuotationID, dbo.TblUser.FullName, dbo.TblUser.PhoneNumber, dbo.TblQuotations.Amount, dbo.TblQuotations.QuotationDate, dbo.TblQuotations.Quotationed
FROM     dbo.TblQuotations INNER JOIN
                  dbo.TblUser ON dbo.TblQuotations.CustomerID = dbo.TblUser.UserID
Where dbo.TblQuotations.QuotationID=@QuotationID
GO
/****** Object:  StoredProcedure [dbo].[Pro_InvoiceQuotationDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_InvoiceQuotationDetail]

@QuotationID int 

as

SELECT dbo.TblQuotationDetails.ProductID,dbo.TblProduct.image, dbo.TblProduct.ProductName, dbo.TblQuotationDetails.UnitPrice, dbo.TblQuotationDetails.Quantity, dbo.TblQuotationDetails.Amount
FROM     dbo.TblQuotationDetails INNER JOIN
                  dbo.TblProduct ON dbo.TblQuotationDetails.ProductID = dbo.TblProduct.ProductID

Where dbo.TblQuotationDetails.QuotationID=@QuotationID
GO
/****** Object:  StoredProcedure [dbo].[Pro_InvoiceUser]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pro_InvoiceUser]

@OrderID int

as
SELECT dbo.TblOrders.OrderDate, dbo.TblUser.FullName, dbo.TblUser.Gender, dbo.TblUser.Age, dbo.TblUser.Position, dbo.TblUser.Address, dbo.TblUser.IdCard, dbo.TblUser.Passport,TblUser.PhoneNumber
FROM     dbo.TblOrders INNER JOIN
                  dbo.TblUser ON dbo.TblOrders.CustomerID = dbo.TblUser.UserID 
Where dbo.TblUser.RoleID=2 and TblOrders.OrderID=@OrderID
GO
/****** Object:  StoredProcedure [dbo].[Pro_List_Decription]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Pro_List_Decription]

@ProductID int

as

SELECT DescriptionDetailsProduct, CarMakes, CarModel, Year, Color, ProductID
FROM     dbo.TblDescription

WHERE ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[Pro_List_Img]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Pro_List_Img]

@ProductID int

as

SELECT image, ProductID
FROM     dbo.TblProduct

WHERE ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[Pro_ListOrderProduct]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Pro_ListOrderProduct]

@CategoryID int,
@ListAll int
,@ProductName nvarchar(250)

as
if(@ListAll=0)
begin
SELECT CategoryID, [image], [UnitInStock], [UnitPrice], [Quantity], [CategoryID], [ProductName], [ProductID], [detail] FROM [TblProduct] 
end
if(@ListAll=1)
begin
SELECT CategoryID, [image], [UnitInStock], [UnitPrice], [Quantity], [CategoryID], [ProductName], [ProductID], [detail] FROM [TblProduct] 
Where CategoryID=@CategoryID
end
if(@ListAll=2)
begin
SELECT CategoryID, [image], [UnitInStock], [UnitPrice], [Quantity], [CategoryID], [ProductName], [ProductID], [detail] FROM [TblProduct] 
Where ProductName like '%'+ @ProductName +'%'
end

GO
/****** Object:  StoredProcedure [dbo].[Pro_ListProductDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_ListProductDetail]

@ProductID int
as

SELECT dbo.TblCategory.CategoryName, dbo.TblProduct.ProductName, dbo.TblProduct.Quantity, dbo.TblProduct.UnitPrice, dbo.TblProduct.image, dbo.TblProduct.ProductID
FROM     dbo.TblCategory INNER JOIN
                  dbo.TblProduct ON dbo.TblCategory.CategoryID = dbo.TblProduct.CategoryID
where ProductID=@ProductID
GO
/****** Object:  StoredProcedure [dbo].[Pro_ListQuotationProduct]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[Pro_ListQuotationProduct]

@CategoryID int,
@ListAll int
,@ProductName nvarchar(250)

as
if(@ListAll=0)
begin
SELECT CategoryID, [image], [UnitInStock], [UnitPrice], [Quantity], [CategoryID], [ProductName], [ProductID], [detail] FROM [TblProduct] 
end
if(@ListAll=1)
begin
SELECT CategoryID, [image], [UnitInStock], [UnitPrice], [Quantity], [CategoryID], [ProductName], [ProductID], [detail] FROM [TblProduct] 
Where CategoryID=@CategoryID
end
if(@ListAll=2)
begin
SELECT CategoryID, [image], [UnitInStock], [UnitPrice], [Quantity], [CategoryID], [ProductName], [ProductID], [detail] FROM [TblProduct] 
Where ProductName like '%'+ @ProductName +'%'
end
GO
/****** Object:  StoredProcedure [dbo].[Pro_ListTotalProduct]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pro_ListTotalProduct]

as

SELECT sum(UnitInStock)as TotalProduct
FROM TblProduct
GO
/****** Object:  StoredProcedure [dbo].[Pro_RemoveProductFromToCart]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_RemoveProductFromToCart]

@OrderDetailID int

as

DELETE FROM [dbo].[TblOrderDetails]
      WHERE OrderDetailID=@OrderDetailID
GO
/****** Object:  StoredProcedure [dbo].[Pro_RemoveProductFromToCartQuotation]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_RemoveProductFromToCartQuotation]

@QuotationDetailID int

as

DELETE FROM [dbo].[TblQuotationDetails]
      WHERE QuotationDetailID=@QuotationDetailID
GO
/****** Object:  StoredProcedure [dbo].[Pro_ReportCheckStock]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_ReportCheckStock]

as

SELECT dbo.TblProduct.ProductID, dbo.TblProduct.ProductName, dbo.TblSuppliers.CompanyName, dbo.TblCategory.CategoryName, dbo.TblProduct.UnitInStock
FROM     dbo.TblProduct INNER JOIN
                  dbo.TblSuppliers ON dbo.TblProduct.SupplierID = dbo.TblSuppliers.SupplierID INNER JOIN
                  dbo.TblCategory ON dbo.TblProduct.CategoryID = dbo.TblCategory.CategoryID
GO
/****** Object:  StoredProcedure [dbo].[Pro_ReportCheckStockByCategory]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_ReportCheckStockByCategory]

as

SELECT   dbo.TblCategory.CategoryName,sum( dbo.TblProduct.UnitInStock)as TotalStock
FROM     dbo.TblProduct INNER JOIN
                
                  dbo.TblCategory ON dbo.TblProduct.CategoryID = dbo.TblCategory.CategoryID

Group by  dbo.TblCategory.CategoryName

Order by TotalStock desc
GO
/****** Object:  StoredProcedure [dbo].[Pro_ReportDailyOrder]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Pro_ReportDailyOrder]

@StartDate nvarchar(50),
@EndDate nvarchar(50)

as
SELECT dbo.TblOrders.OrderID, dbo.TblOrders.Amount, dbo.TblOrders.OrderDate, dbo.TblUser.FullName as CustomerName, TblUser_1.FullName AS EmployeeName,dbo.TblUser.PhoneNumber
FROM     dbo.TblOrders INNER JOIN
                  dbo.TblUser ON dbo.TblOrders.CustomerID = dbo.TblUser.UserID INNER JOIN
                  dbo.TblUser AS TblUser_1 ON dbo.TblOrders.EmployeeID = TblUser_1.UserID


Where OrderDate between @StartDate and @EndDate
order by dbo.TblOrders.OrderDate desc
--exec Pro_ReportDailyOrder '2023-09-02','2023-09-28'

--SELECT dbo.TblOrders.OrderID, dbo.TblOrders.Amount, dbo.TblOrders.OrderDate, dbo.TblUser.UserName
--FROM     dbo.TblOrders INNER JOIN
                  --dbo.TblUser ON dbo.TblOrders.CustomerID = dbo.TblUser.UserID
GO
/****** Object:  StoredProcedure [dbo].[Pro_ReportDailyOrderDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[Pro_ReportDailyOrderDetail]

@OrderID int

as

SELECT dbo.TblProduct.ProductName, dbo.TblOrderDetails.UnitPrice, dbo.TblOrderDetails.Quantity,(dbo.TblOrderDetails.UnitPrice * dbo.TblOrderDetails.Quantity)as Total
FROM     dbo.TblOrderDetails INNER JOIN
                  dbo.TblProduct ON dbo.TblOrderDetails.ProductID = dbo.TblProduct.ProductID
Where TblOrderDetails.OrderID = @OrderID
GO
/****** Object:  StoredProcedure [dbo].[Pro_ReportDailyQuotation]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_ReportDailyQuotation]

@StartDate nvarchar(50),
@EndDate nvarchar(50)

as
SELECT        dbo.TblQuotations.QuotationID, dbo.TblQuotations.Amount, dbo.TblQuotations.QuotationDate, dbo.TblUser.FullName, dbo.TblUser.PhoneNumber
FROM            dbo.TblQuotations INNER JOIN
                         dbo.TblUser ON dbo.TblQuotations.CustomerID = dbo.TblUser.UserID


Where QuotationDate between @StartDate and @EndDate
order by dbo.TblQuotations.QuotationDate desc
GO
/****** Object:  StoredProcedure [dbo].[Pro_ReportDailyQuotationDetail]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_ReportDailyQuotationDetail]

@QuotationID int

as

SELECT dbo.TblProduct.ProductName, dbo.TblQuotationDetails.UnitPrice, dbo.TblQuotationDetails.Quantity,(dbo.TblQuotationDetails.UnitPrice * dbo.TblQuotationDetails.Quantity)as Total
FROM     dbo.TblQuotationDetails INNER JOIN
                  dbo.TblProduct ON dbo.TblQuotationDetails.ProductID = dbo.TblProduct.ProductID
Where TblQuotationDetails.QuotationID = @QuotationID
GO
/****** Object:  StoredProcedure [dbo].[Pro_RestoreInStock]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_RestoreInStock]

@ProductID  int,
@Quantity int

as

update  dbo.TblProduct set UnitInStock +=@Quantity 

Where ProductID =@ProductID
GO
/****** Object:  StoredProcedure [dbo].[Pro_Search]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pro_Search]
@productName nvarchar(50)
as

SELECT [image], [UnitInStock], [UnitPrice], [Quantity], [CategoryID], [ProductName], [ProductID], [detail] FROM [TblProduct] Where ProductName like '%'+ @productName +'%'
GO
/****** Object:  StoredProcedure [dbo].[Pro_SearchProduct]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_SearchProduct]

@ProductID int

as

SELECT [ProductID]
      ,[ProductName]
      ,[SupplierID]
      ,[CategoryID]
      ,[Quantity]
      ,[UnitPrice]
      ,[UnitInStock]
      ,[image]
      ,[detail]
  FROM [dbo].[TblProduct]
  where ProductID=@ProductID
GO
/****** Object:  StoredProcedure [dbo].[Pro_SearchProductQuotation]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_SearchProductQuotation]

@ProductID int

as

SELECT [ProductID]
      ,[ProductName]
      ,[SupplierID]
      ,[CategoryID]
      ,[Quantity]
      ,[UnitPrice]
      ,[UnitInStock]
      ,[image]
      ,[detail]
  FROM [dbo].[TblProduct]
  where ProductID=@ProductID
GO
/****** Object:  StoredProcedure [dbo].[Pro_SumAmount]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_SumAmount]

as

SELECT sum([Amount]) as TotalAmount
 
  

  FROM [dbo].[TblOrders]
  where StatusID=1
GO
/****** Object:  StoredProcedure [dbo].[Pro_SumProductByCategory]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pro_SumProductByCategory]

as

SELECT         dbo.TblCategory.CategoryName, Count(dbo.TblProduct.ProductID) as CountProductByCategory
FROM            dbo.TblProduct INNER JOIN
                         dbo.TblCategory ON dbo.TblProduct.CategoryID = dbo.TblCategory.CategoryID
group by dbo.TblCategory.CategoryName,dbo.TblCategory.CategoryID
GO
/****** Object:  StoredProcedure [dbo].[Pro_UserLogin]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Pro_UserLogin]

@FullName nvarchar(500)
,@Password nvarchar(500)

as

SELECT [UserID]
      ,[FullName]
    ,RoleID
  FROM [dbo].[TblUser]
  Where FullName=@FullName and Password=@Password

  --exec Pro_UserLogin 'Tongheng','123456'
GO
/****** Object:  StoredProcedure [dbo].[ProcedureListProduct]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[ProcedureListProduct]

@ProductID int

as

SELECT dbo.TblCategory.CategoryID, dbo.TblCategory.CategoryName, dbo.TblProduct.ProductID, dbo.TblProduct.ProductName, dbo.TblProduct.CategoryID AS Expr1, dbo.TblProduct.UnitPrice, dbo.TblProduct.image, 
                  dbo.TblCategory.Description
FROM     dbo.TblCategory INNER JOIN
                  dbo.TblProduct ON dbo.TblCategory.CategoryID = dbo.TblProduct.CategoryID
WHERE ProductID = @ProductID
GO
/****** Object:  StoredProcedure [dbo].[Report_Daily]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Report_Daily]

@StartDate nvarchar(50),
@EndDate nvarchar(50)

as

SELECT dbo.TblOrders.OrderID, dbo.TblUser.FullName, dbo.TblUser.PhoneNumber, dbo.TblOrders.Amount, dbo.TblOrders.OrderDate
FROM     dbo.TblOrders INNER JOIN
                  dbo.TblUser ON dbo.TblOrders.CustomerID = dbo.TblUser.UserID
WHERE  (dbo.TblOrders.StatusID = 1) and dbo.TblOrders.OrderDate between @StartDate and @EndDate
				  order by dbo.TblOrders.OrderDate desc

GO
/****** Object:  StoredProcedure [dbo].[Report_DailyListProduct]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Report_DailyListProduct]
@StartDate nvarchar(50),
@EndDate nvarchar(50)
as

SELECT dbo.TblProduct.ProductID, dbo.TblProduct.ProductName, dbo.TblSuppliers.CompanyName, dbo.TblSuppliers.Phone, dbo.TblCategory.CategoryName, dbo.TblProduct.Quantity, dbo.TblProduct.UnitPrice, dbo.TblProduct.UnitInStock
FROM     dbo.TblProduct INNER JOIN
                  dbo.TblCategory ON dbo.TblProduct.CategoryID = dbo.TblCategory.CategoryID INNER JOIN
                  dbo.TblSuppliers ON dbo.TblProduct.SupplierID = dbo.TblSuppliers.SupplierID
Where dbo.TblProduct.ProductDate between @StartDate and @EndDate
				  order by dbo.TblSuppliers.CompanyName

GO
/****** Object:  StoredProcedure [dbo].[Report_ListProduct]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[Report_ListProduct]

as

SELECT dbo.TblProduct.ProductID, dbo.TblProduct.ProductName, dbo.TblSuppliers.CompanyName, dbo.TblSuppliers.Phone, dbo.TblCategory.CategoryName, dbo.TblProduct.Quantity, dbo.TblProduct.UnitPrice, dbo.TblProduct.UnitInStock
FROM     dbo.TblProduct INNER JOIN
                  dbo.TblCategory ON dbo.TblProduct.CategoryID = dbo.TblCategory.CategoryID INNER JOIN
                  dbo.TblSuppliers ON dbo.TblProduct.SupplierID = dbo.TblSuppliers.SupplierID

GO
/****** Object:  StoredProcedure [dbo].[Report_ListProductByCategory]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Report_ListProductByCategory]

@CategoryID  int

as

SELECT dbo.TblProduct.ProductID, dbo.TblProduct.ProductName, dbo.TblSuppliers.CompanyName, dbo.TblSuppliers.Phone, dbo.TblCategory.CategoryName, dbo.TblProduct.Quantity, dbo.TblProduct.UnitPrice, dbo.TblProduct.UnitInStock
FROM     dbo.TblProduct INNER JOIN
                  dbo.TblCategory ON dbo.TblProduct.CategoryID = dbo.TblCategory.CategoryID INNER JOIN
                  dbo.TblSuppliers ON dbo.TblProduct.SupplierID = dbo.TblSuppliers.SupplierID
Where dbo.TblProduct.CategoryID = @CategoryID
				  order by dbo.TblSuppliers.CompanyName

GO
/****** Object:  StoredProcedure [dbo].[Report_ListProductBySupplier]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[Report_ListProductBySupplier]

as

SELECT dbo.TblProduct.ProductID, dbo.TblProduct.ProductName, dbo.TblSuppliers.CompanyName, dbo.TblSuppliers.Phone, dbo.TblCategory.CategoryName, dbo.TblProduct.Quantity, dbo.TblProduct.UnitPrice, dbo.TblProduct.UnitInStock
FROM     dbo.TblProduct INNER JOIN
                  dbo.TblCategory ON dbo.TblProduct.CategoryID = dbo.TblCategory.CategoryID INNER JOIN
                  dbo.TblSuppliers ON dbo.TblProduct.SupplierID = dbo.TblSuppliers.SupplierID

				  order by dbo.TblSuppliers.CompanyName

GO
/****** Object:  StoredProcedure [dbo].[Report_ListProductInStock]    Script Date: 10/1/2023 8:23:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[Report_ListProductInStock]

as

SELECT dbo.TblProduct.ProductID, dbo.TblProduct.ProductName, dbo.TblSuppliers.CompanyName, dbo.TblSuppliers.Phone, dbo.TblCategory.CategoryName, dbo.TblProduct.Quantity, dbo.TblProduct.UnitPrice, dbo.TblProduct.UnitInStock
FROM     dbo.TblProduct INNER JOIN
                  dbo.TblCategory ON dbo.TblProduct.CategoryID = dbo.TblCategory.CategoryID INNER JOIN
                  dbo.TblSuppliers ON dbo.TblProduct.SupplierID = dbo.TblSuppliers.SupplierID
Where dbo.TblProduct.UnitInStock < 5
				  order by dbo.TblSuppliers.CompanyName

GO
USE [master]
GO
ALTER DATABASE [carpower] SET  READ_WRITE 
GO
