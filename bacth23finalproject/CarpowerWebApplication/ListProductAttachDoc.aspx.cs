﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class ListProductAttachDoc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["SUserID"] != null)
            {

            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            try
            {
                txtOrderDetailID.Text = Request.QueryString["OrderDetailID"];
            }
            catch (Exception)
            { }
        }

        protected void BtnInserUpload_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DocumentBuyer = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DocumentBuyer = new SqlCommand("INSERT INTO [dbo].[TblDocumentBuyer] ([DocumentBuyerName],[DocumentBuyerAttach],[OrderDetailID],DocumentBuyerTypeID) VALUES (@DocumentBuyerName,@DocumentBuyerAttach,@OrderDetailID,@DocumentBuyerTypeID)", con);

            DocumentBuyer.Parameters.AddWithValue("@DocumentBuyerName", txtDocumentBuyerAttach.Text);
            DocumentBuyer.Parameters.AddWithValue("@OrderDetailID", txtOrderDetailID.Text);

            if (FileUploadDocumentBuyerAttach.HasFile)
            {
                FileUploadDocumentBuyerAttach.SaveAs(Server.MapPath("DocumentAttach/" + FileUploadDocumentBuyerAttach.FileName));
                TxtDocumentBuyerAttachURL.Text = "DocumentAttach/" + FileUploadDocumentBuyerAttach.FileName;
            }

            DocumentBuyer.Parameters.AddWithValue("@DocumentBuyerAttach", TxtDocumentBuyerAttachURL.Text);
            DocumentBuyer.Parameters.AddWithValue("@DocumentBuyerTypeID", DDLDocumentBuyerType.SelectedValue);



            con.Open();
            DocumentBuyer.ExecuteNonQuery();
            con.Close();

            DataListDocumentBuyerAttach.DataBind();
        }
    }
}