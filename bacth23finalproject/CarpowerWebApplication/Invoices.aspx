﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Invoices.aspx.cs" Inherits="CarpowerWebApplication.Invoices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        body{
            background-color:white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div id="PrintInvoice" style="width: 900px; background-color:#e6e3e3">

        <asp:DataList ID="DataListInvoice" runat="server" DataSourceID="SqlDataSourceInvoiceEmployee">
            <ItemStyle Font-Names="Khmer OS Siemreap" Font-Size="Medium" />
            <ItemTemplate>
                <table border="0" width="900">
                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-lg-12">
                                    <img src="images/Banner invoice noBG.png" style="width: 865px" />
                                </div>
                            </div>
                            
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <h6>អាសយដ្ឋាន ឃ្លាំង 203 ផ្លូវជាតិលេខ៣ ,ព្រៃព្រីងខាងត្បូង, ចោមចៅ3, ពោធិ៍សែនជ័យ, រាជធានីភ្នំពេញ</h6>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 300px;">
                            <div class="col-lg-12">
                                <h6>Tel:</h6>
                                <h6>012 85 26 85</h6>
                                <h6>088 918 3168</h6>
                                <h6>016 866 789</h6>
                            </div>
                        </td>
                        <td style="width: 300px;">
                            <div class="col-lg-12" style="margin-top: 70px"><br />
                                <h5 style="font-family:'Khmer OS Muol'">លិខិតទិញ-លក់-រថយន្ត
                                </h5>
                            </div>
                        </td>
                        <td style="width: 300px;">
                            <div class="col-lg-12" style="margin-top: 40px">
                                <h6>Date :
                            <asp:Label ID="OrderDateLabel" runat="server" Text='<%# Eval("OrderDate") %>' /><br />
                                    Invoice ID: <asp:Label ID="OrderIDLabel" runat="server" Text='<%# Eval("OrderID") %>' />
                                </h6>
                            </div>
                            <br />
                        </td>
                    </tr>


                    <tr>
                        <td>

                            <div class="col-lg-12"><br />
                                <h6>នាងខ្ញុំឈ្មោះ
                            <asp:Label ID="FullNameLabel" runat="server" Text='<%# Eval("FullName") %>' />

                                </h6>
                            </div>
                        </td>
                        <td>
                            <div class="col-lg-12">
                                <h6>ភេទ
                            <asp:Label ID="GenderLabel" runat="server" Text='<%# Eval("Gender") %>' /></h6>

                            </div>
                        </td>
                        <td>
                            <div class="col-lg-12">
                                <h6>អាយុ
                            <asp:Label ID="AgeLabel" runat="server" Text='<%# Eval("Age") %>' />
                                    <span>ឆ្នាំ</span></h6>

                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6>មុខងារបច្ចុប្បន្ន: <span style="margin-left: 196px">
                                        <asp:Label ID="PositionLabel" runat="server" Text='<%# Eval("Position") %>' /></span></h6>

                                </div>
                            </div>
                        </td>
                    </tr>
                    <br />
                    <br />

                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6>អាស័យដ្ឋាន 
                            <asp:Label ID="AddressLabel" runat="server" Text='<%# Eval("Address") %>' /></h6>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div class="col-lg-12">
                                <h6>អត្តសញ្ញាណប័ណ្ណលេខ <span style="margin-left: 50px">
                                    <asp:Label ID="IdCardLabel" runat="server" Text='<%# Eval("IdCard") %>' /></span> </h6>
                            </div>
                        </td>
                        <td>
                            <div class="col-lg-12">
                                <h6>លិខិតឆ្លងដែន <span style="margin-left: 50px">
                                    <asp:Label ID="PassportLabel" runat="server" Text='<%# Eval("Passport") %>' /></span></h6>
                            </div>


                        </td>
                        <td></td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:DataList>

        <asp:SqlDataSource ID="SqlDataSourceInvoiceEmployee" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_InvoiceEmployee" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:QueryStringParameter Name="OrderID" QueryStringField="OrderID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>


        <asp:DataList ID="DataListInvoiceOrderDetailProduct" runat="server" DataSourceID="SqlDataSourceInvoiceOrderDetailProduct">
            <ItemStyle Font-Names="Khmer OS Siemreap" Font-Size="Medium" />
            <ItemTemplate>
                <table border="0" width="900">
                    <tr>
                        <td>
                            <div class="col-lg-12">
                                <h6>បានលក់រថយន្តលេខ <span style="margin-left: 25px">(១)គ្រឿង ម៉ាក</span> <span style="margin-left: 30px">
                                    <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' /></span></h6>
                            </div>
                        </td>
                        <td>
                            <div class="col-lg-12">
                                <p>
                                    ពណ័ <span>
                                        <asp:Label ID="colorLabel" runat="server" Text='<%# Eval("color") %>' /></span> ដែលមាន
                                </p>
                            </div>


                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div class="col-lg-12">
                                <h6>តូលេខ:
                            <asp:Label ID="ShortNumberLabel" runat="server" Text='<%# Eval("ShortNumber") %>' /></h6>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-lg-12">
                                <h6>លេខម៉ាស៊ីន: 
                            <asp:Label ID="EngineNumberLabel" runat="server" Text='<%# Eval("EngineNumber") %>' /></h6>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-lg-12">
                                <h6>ស្លាកលេខសំគាល់: 
                            <asp:Label ID="TagIDLabel" runat="server" Text='<%# Eval("TagID") %>' /></h6>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-lg-12">
                                <h6>លតប័ត្រ :<asp:Label ID="MVLabel" runat="server" Text='<%# Eval("MV") %>' /></h6>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-lg-12">
                                <h6>ឆ្នាំផលិត: 
                            <asp:Label ID="CarManufactureDateLabel" runat="server" Text='<%# Eval("CarManufactureDate") %>' /></h6>
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:Label ID="CategoryNameLabel" runat="server" Visible="false" Text='<%# Eval("CategoryName") %>' />
            </ItemTemplate>
        </asp:DataList>
        <asp:SqlDataSource ID="SqlDataSourceInvoiceOrderDetailProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_InvoiceOrderDetailProduct" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:QueryStringParameter Name="OrderID" QueryStringField="OrderID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="row">
            <div class="col-lg-12 text-left">
                <asp:DataList ID="DataListInvoiceUser" Width="100%" runat="server" DataSourceID="SqlDataSourceInvoiceUser">
                    <ItemStyle Font-Names="Khmer OS Siemreap" Font-Size="Medium" />
                    <ItemTemplate>
                        <table border="0" width="900">
                            <tr>
                                <td>
                                    <div class="col-lg-12">
                                        <h6>ទៅអោយឈ្មោះ:
                            <span>
                                <asp:Label ID="FullNameLabel" runat="server" Text='<%# Eval("FullName") %>' /></h6>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    <div class="col-lg-12">
                                        <h6>ភេទ: 
                            <asp:Label ID="GenderLabel" runat="server" Text='<%# Eval("Gender") %>' /></h6>
                                    </div>
                                </td>
                                <td>
                                    <div class="col-lg-12">
                                        <h6>អាយុ:
                            <asp:Label ID="AgeLabel" runat="server" Text='<%# Eval("Age") %>' /></h6>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="col-lg-12">
                                        <h6>មុខរបរបច្ចុប្បន្ធ:
                            <asp:Label ID="PositionLabel" runat="server" Text='<%# Eval("Position") %>' /></h6>
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="col-lg-12">
                                        <h6>អាស័យដ្ឋាន  
                            <asp:Label ID="AddressLabel" runat="server" Text='<%# Eval("Address") %>' /></h6>
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <div class="col-lg-12">
                                        <h6>អត្តសញ្ញាណប័ណ្ណ: 
                            <asp:Label ID="IdCardLabel" runat="server" Text='<%# Eval("IdCard") %>' /></h6>
                                    </div>
                                </td>
                                <td>
                                    <div class="col-lg-12">
                                        <h6>លេខទូរសព្ទ: 
                            <asp:Label ID="PhoneNumberLabel" runat="server" Text='<%# Eval("PhoneNumber") %>' /></h6>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </table>

                        <asp:Label ID="PassportLabel" runat="server" Visible="false" Text='<%# Eval("Passport") %>' />

                    </ItemTemplate>
                </asp:DataList>
                <asp:SqlDataSource ID="SqlDataSourceInvoiceUser" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_InvoiceUser" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="OrderID" QueryStringField="OrderID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:DataList ID="DataListTotalAmount" Width="100%" runat="server" DataSourceID="SqlDataSourceTotalAmount">
                    <ItemStyle Font-Names="Khmer OS Siemreap" Font-Size="Medium" />
                    <ItemTemplate>
                        <table border="0" width="900">
                            <tr>
                                <td colspan="3">តម្លៃ :............
                <asp:Label ID="AmountLabel" runat="server" Text='<%# Eval("Amount") %>' />....................................................................................................
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">កក់មុន :........................................................
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">នៅខ្វះ :........................................................សន្យាសងគ្រប់នៅថ្ងៃទី........ ខែ........ ឆ្នាំ២០២....
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <br />
                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp ថ្ងៃក្រោយបើមានជនណាមកអះអាងថារថយន្តខាងលើជារថយន្តរបស់ខ្លួន<br />
                                    នាងខ្ញុំឈ្មោះ 
                <asp:Label ID="FullNameLabel" runat="server" Text='<%# Eval("FullName") %>' />
                                    សូមទទួលខុសត្រូវចំពោះមុខច្បាប់ ។
                                   
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <br />
                                    <b><u>បញ្ជាក់៖</u></b> ធានាថាមានពន្ធត្រឹមត្រូវ របស់ទិញហើយដូរវិញមិនបាន ។
                        
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: right">
                                    <div class="col-lg-12 text-right">
                                        <br />
                                        រាជធានីភ្នំពេញ ថ្ងៃទី.... ខែ........ ឆ្នាំ២០២....
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table border="0" width="900">
                                        <tr>
                                            <td>
                                                <div class="col-lg-12">
                                                    អ្នកលក់
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-lg-12">
                                                    សាក្សីអ្នកលក់
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-lg-12">
                                                    សាក្សីអ្នកទិញ
                                                </div>
                                            </td>
                                            <td>
                                                <div class="col-lg-12">
                                                    អ្នកទិញ
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    
                    </ItemTemplate>
                </asp:DataList>
                <asp:SqlDataSource ID="SqlDataSourceTotalAmount" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrders.Amount,dbo.TblUser.FullName
 FROM     dbo.TblOrders INNER JOIN
                  dbo.TblUser ON dbo.TblOrders.EmployeeID = dbo.TblUser.UserID

Where dbo.TblUser.RoleID=3 and TblOrders.OrderID=@OrderID">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="OrderID" QueryStringField="OrderID" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
    </div>
    <br />
    <script language="javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
    <input type="button" onclick="printDiv('PrintInvoice')" value="Print" class="btn btn-primary" />
    <br />
</asp:Content>
