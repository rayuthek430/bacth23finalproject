﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class ClientRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnRegister_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand CreateUser = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            CreateUser = new SqlCommand("INSERT INTO [dbo].[TblUser] ([FullName],[Password] ,[RoleID],[StatusID],[PhoneNumber],[Email],[Address],[Image],[Gender],[Age],[Position],[IdCard],[Passport])VALUES (@FullName,@Password,@RoleID,@StatusID,@PhoneNumber,@Email,@Address,@Image,@Gender,@Age,@Position,@IdCard,@Passport)", con);

            CreateUser.Parameters.AddWithValue("@FullName", TxtGetUserName.Text);
            CreateUser.Parameters.AddWithValue("@Password", TxtGetUserPassword.Text);

            CreateUser.Parameters.AddWithValue("@RoleID", 1);
            CreateUser.Parameters.AddWithValue("@StatusID", 1);

            CreateUser.Parameters.AddWithValue("@PhoneNumber", TextGetPhoneNumber.Text);
            CreateUser.Parameters.AddWithValue("@Email", TextGetEmail.Text);

            CreateUser.Parameters.AddWithValue("@Address", TextGetAddress.Text);
            CreateUser.Parameters.AddWithValue("@Image", DBNull.Value); // null

            CreateUser.Parameters.AddWithValue("@Age", txtGender.Text);
            CreateUser.Parameters.AddWithValue("@IdCard", txtIDCard.Text);
            CreateUser.Parameters.AddWithValue("@Passport", txtPasSport.Text);

            CreateUser.Parameters.AddWithValue("@Position", "អ្នកទិញ");

            // Get the selected value from the DropDownList
            string selectedGender = genderDropdown.SelectedValue;
            CreateUser.Parameters.AddWithValue("@Gender", selectedGender);


            con.Open();
            CreateUser.ExecuteNonQuery();
            con.Close();

            Response.Redirect("OrderProduct.aspx?url=" + Server.UrlEncode(Request.Url.AbsoluteUri));

            TxtGetUserName.Text = "";
            TxtGetUserPassword.Text = "";
            TextGetPhoneNumber.Text = "";
            TextGetEmail.Text = "";
            TextGetAddress.Text = "";
        }

        protected void BtnSampleData_Click(object sender, EventArgs e)
        {
            TxtGetUserName.Text = "NewUser1";
            txtGender.Text = "30";
            txtPasSport.Text = "N010549494";
            txtIDCard.Text = "0102003456";
            TxtGetUserPassword.Text = "123";

            TextGetPhoneNumber.Text = "093242455";
            TextGetEmail.Text = "NewUser1@gmail.com";
            TextGetAddress.Text = "Phnom Penh";
        }
    }
}