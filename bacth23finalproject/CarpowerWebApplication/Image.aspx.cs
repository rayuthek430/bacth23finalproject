﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class FormImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnInsertImage_Click(object sender, EventArgs e)
        {

            SqlConnection con = new SqlConnection();

            SqlCommand InsertImage = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertImage = new SqlCommand("INSERT INTO [dbo].[TblImage]([ImageName],[ImageUrl],[ProductID]) VALUES (@ImageName,@ImageUrl,@ProductID)", con);

            InsertImage.Parameters.AddWithValue("@ImageName", TxtImageName.Text);
            
            if(FileUploadImageS.HasFile)
            {
                FileUploadImageS.SaveAs(Server.MapPath("images/" + FileUploadImageS.FileName));
                ImgURL.Text = "images/" + FileUploadImageS.FileName;
            }
            Imgs.ImageUrl = ImgURL.Text;
            InsertImage.Parameters.AddWithValue("@ImageUrl", ImgURL.Text);
            InsertImage.Parameters.AddWithValue("@ProductID", DropDownListProductID.SelectedValue);


            con.Open();
            InsertImage.ExecuteNonQuery();
            con.Close();
            
            TxtImage.Text = "";
            TxtImageName.Text = "";
            GridViewFormImage.DataBind();
            
        }

        protected void btnSearchImage_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [ImageID],[ImageName],[ImageUrl],[ProductID] FROM [dbo].[TblImage] WHERE ImageID = " + TxtImage.Text;
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {
                TxtImage.Text = DTable1.Rows[0]["ImageID"].ToString(); // img id
                TxtImageName.Text = DTable1.Rows[0]["ImageName"].ToString();
                Imgs.ImageUrl = DTable1.Rows[0]["ImageUrl"].ToString();
            }

            con.Close();
        }

        protected void btnUpdateImage_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdateImage = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdateImage = new SqlCommand("UPDATE [dbo].[TblImage] SET [ImageName] = @ImageName,[ImageUrl] = @ImageUrl,[ProductID] = @ProductID WHERE [ImageID] = @ImageID", con);

            UpdateImage.Parameters.AddWithValue("@ImageID", TxtImage.Text);
            UpdateImage.Parameters.AddWithValue("@ImageName", TxtImageName.Text);

            if (FileUploadImageS.HasFile)
            {
                FileUploadImageS.SaveAs(Server.MapPath("images/" + FileUploadImageS.FileName));
                ImgURL.Text = "images/" + FileUploadImageS.FileName;
            }
            Imgs.ImageUrl = ImgURL.Text;
            UpdateImage.Parameters.AddWithValue("@ImageUrl", ImgURL.Text);
            UpdateImage.Parameters.AddWithValue("@ProductID", DropDownListProductID.SelectedValue);

            con.Open();
            UpdateImage.ExecuteNonQuery();
            con.Close();
            TxtImageName.Text = "";
            GridViewFormImage.DataBind();
        }

        protected void btnDeleteImage_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteImage = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteImage = new SqlCommand("DELETE FROM [dbo].[TblImage] WHERE ImageID​​ = @ImageID", con);

            DeleteImage.Parameters.AddWithValue("@ImageID", TxtImage.Text);

            con.Open();
            DeleteImage.ExecuteNonQuery();
            con.Close();
            TxtImageName.Text = "";

            GridViewFormImage.DataBind();

        }

        protected void btnClearImage_Click(object sender, EventArgs e)
        {
            TxtImageName.Text = "";
        }
    }
}