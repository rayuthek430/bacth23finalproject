﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormDecription : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearchDecription_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [DescriptionID] ,[DescriptionDetailsProduct],[CarMakes],[CarModel],[Year],[Color],[ProductID] FROM [dbo].[TblDescription] WHERE DescriptionID =" + decriptionID.Text;
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {

                decriptionID.Text = DTable1.Rows[0]["DescriptionID"].ToString();
                decriptionDetailProduct.Text = DTable1.Rows[0]["DescriptionDetailsProduct"].ToString();
                carMakes.Text = DTable1.Rows[0]["CarMakes"].ToString();
                carModel.Text = DTable1.Rows[0]["CarModel"].ToString();
                year.Text = DTable1.Rows[0]["Year"].ToString();
                color.Text = DTable1.Rows[0]["Color"].ToString();

                DropDownListProductIDFormTableProduct.SelectedValue = DTable1.Rows[0]["ProductID"].ToString();


            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No data is returned');", true);
            }

            con.Close();
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            decriptionID.Focus();
            decriptionDetailProduct.Text="";
            carMakes.Text = "";
            carModel.Text = "";
            year.Text = "";
            color.Text = "";
        }

        protected void btnUpdateDecription_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand updateDecription = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            updateDecription = new SqlCommand("UPDATE [dbo].[TblDescription] SET [DescriptionDetailsProduct] = @DescriptionDetailsProduct,[CarMakes] = @CarMakes,[CarModel] = @CarModel,[Year] = @Year,[Color] = @Color,[ProductID] = @ProductID WHERE DescriptionID = @DescriptionID", con);

            updateDecription.Parameters.AddWithValue("@DescriptionID", decriptionID.Text);
            updateDecription.Parameters.AddWithValue("@DescriptionDetailsProduct", decriptionDetailProduct.Text);
            updateDecription.Parameters.AddWithValue("@CarMakes", carMakes.Text);
            updateDecription.Parameters.AddWithValue("@CarModel", carModel.Text);
            updateDecription.Parameters.AddWithValue("@Year", year.Text);
            updateDecription.Parameters.AddWithValue("@Color", color.Text);
            updateDecription.Parameters.AddWithValue("@ProductID", DropDownListProductIDFormTableProduct.SelectedValue);

            con.Open();
            updateDecription.ExecuteNonQuery();
            con.Close();
            decriptionDetailProduct.Text = "";
            carMakes.Text = "";
            carModel.Text = "";
            year.Text = "";
            color.Text = "";

            GridViewDecription.DataBind();
        }

        protected void btnInsertDecription_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertdECRIPTION = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertdECRIPTION = new SqlCommand("INSERT INTO [dbo].[TblDescription] ([DescriptionDetailsProduct],[CarMakes],[CarModel],[Year],[Color],[ProductID]) VALUES (@DescriptionDetailsProduct,@CarMakes,@CarModel,@Year,@Color,@ProductID)", con);

            InsertdECRIPTION.Parameters.AddWithValue("@DescriptionDetailsProduct", decriptionDetailProduct.Text);
            InsertdECRIPTION.Parameters.AddWithValue("@CarMakes", carMakes.Text);
            InsertdECRIPTION.Parameters.AddWithValue("@CarModel", carModel.Text);
            InsertdECRIPTION.Parameters.AddWithValue("@Year", year.Text);
            InsertdECRIPTION.Parameters.AddWithValue("@Color", color.Text);
            InsertdECRIPTION.Parameters.AddWithValue("@ProductID", DropDownListProductIDFormTableProduct.SelectedValue);

            con.Open();
            InsertdECRIPTION.ExecuteNonQuery();
            con.Close();
            decriptionDetailProduct.Text = "";
            carMakes.Text = "";
            carModel.Text = "";
            year.Text = "";
            color.Text = "";
            GridViewDecription.DataBind();
        }

        protected void btnDeleteDecription_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteDec = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteDec = new SqlCommand("DELETE FROM [dbo].[TblDescription] WHERE DescriptionID = @DescriptionID", con);

            DeleteDec.Parameters.AddWithValue("@DescriptionID", decriptionID.Text);

            con.Open();
            DeleteDec.ExecuteNonQuery();
            con.Close();
            GridViewDecription.DataBind();

        }

        protected void ButtonBackToProduct_Click(object sender, EventArgs e)
        {
            Response.Redirect("Product.aspx");
        }
    }
}