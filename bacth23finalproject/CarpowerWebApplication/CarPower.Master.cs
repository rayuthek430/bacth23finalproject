﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class CarPower : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (Session["SFullName"] != null)
                {
                    LbtnLogin.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ចាកចេញ";
                }
                else
                {
                    LbtnLogin.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ចូលប្រព័ន្ធ";
                }

                if (Session["SRoleID"].ToString() == "1")
                {
                    // User Guest
                    User.Visible = true;
                    Admin.Visible = false;

                }
                else if (Session["SRoleID"].ToString() == "2")
                {
                    // User 
                    User.Visible = false;
                    Admin.Visible = true;
                }
                else if (Session["SRoleID"].ToString() == "3")
                {
                    // Admin 
                    User.Visible = false;
                    Admin.Visible = true;
                }
            }
            catch (Exception) { }
        }

        protected void LbtnLogin_Click(object sender, EventArgs e)
        {
            Session["SUserID"] = null;
            Session["SFullName"] = null;
            Session["SRoleID"] = null;
            Session["UserLog"] = null;
            Response.Redirect("Login.aspx");

        }
    }
}