﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertAds_Click(object sender, EventArgs e)
        {
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery",
               new ScriptResourceDefinition
               {
                   Path = "~/Scripts/jquery-1.7.2.min.js",
                   DebugPath = "~/Scripts/jquery-1.7.2.js",
                   CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.4.1.min.js",
                   CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.4.1.js"
               });

            SqlConnection con = new SqlConnection();

            SqlCommand InsertAds = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertAds = new SqlCommand("INSERT INTO [dbo].[Ads] ([ads_name],[ads_imag]) VALUES (@ads_name,@ads_imag)", con);

            InsertAds.Parameters.AddWithValue("@ads_name", AdsTitle.Text);
            
            

            if (UploadAds.HasFile)
            {
                UploadAds.SaveAs(Server.MapPath("images/" + UploadAds.FileName));
                AdsURL.Text = "images/" + UploadAds.FileName;
            }
            Ads.ImageUrl = AdsURL.Text;

            InsertAds.Parameters.AddWithValue("@ads_imag", AdsURL.Text);

            con.Open();
            InsertAds.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('បានជោគជ័យ!', 'ការបញ្ចូលទិន្នន័យបានជោគជ័យ.', 'success');", true);
            ListData.DataBind();
        }

        protected void btnSearchAds_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [ads_id],[ads_name],[ads_imag] FROM [dbo].[Ads] WHERE ads_id = " + DropDownListAds.SelectedValue; // your id
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {
                DropDownListAds.SelectedValue = DTable1.Rows[0]["ads_id"].ToString();
                AdsTitle.Text = DTable1.Rows[0]["ads_name"].ToString();
                Ads.ImageUrl = DTable1.Rows[0]["ads_imag"].ToString();
            }

            con.Close();
        }

        protected void btnClearsAds_Click(object sender, EventArgs e)
        {
            AdsTitle.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteAds = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteAds = new SqlCommand("DELETE FROM[dbo].[Ads] WHERE ads_id = @ads_id", con);

            DeleteAds.Parameters.AddWithValue("@ads_id", DropDownListAds.SelectedValue);

            con.Open();
            DeleteAds.ExecuteNonQuery();
            con.Close();
            //GridViewProduct.DataBind();
            if (AdsTitle.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('មិនមានទិន្នន័យ', 'សូមរើសយកទិន្នន័យណាមួយ.', 'success');", true);
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('បានជោគជ័យ!', 'ទិន្នន័យលុបបានជោគជ័យ.', 'success');", true);

            // Clear
            AdsTitle.Text = "";
        }

        protected void btnUpdateAds_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdateAds = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdateAds = new SqlCommand("UPDATE [dbo].[Ads] SET [ads_name] = @ads_name,[ads_imag] = @ads_imag WHERE ads_id = @ads_id", con);

            UpdateAds.Parameters.AddWithValue("@ads_id", DropDownListAds.SelectedValue);
            UpdateAds.Parameters.AddWithValue("@ads_name", AdsTitle.Text);


            if (UploadAds.HasFile)
            {
                UploadAds.SaveAs(Server.MapPath("images/" + UploadAds.FileName));
                AdsURL.Text = "images/" + UploadAds.FileName;
            }
            Ads.ImageUrl = AdsURL.Text;

            UpdateAds.Parameters.AddWithValue("@ads_imag", AdsURL.Text);

            con.Open();
            UpdateAds.ExecuteNonQuery();
            con.Close();
        }
    }
}