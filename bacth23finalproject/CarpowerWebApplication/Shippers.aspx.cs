﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormShippers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearchShippers_Click(object sender, EventArgs e)
        {

        }

        protected void BtnInsert_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertShippers = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertShippers = new SqlCommand("INSERT INTO [dbo].[TblShippers]([CompanyName],[Phone])VALUES(@CompanyName,@Phone)", con);

            InsertShippers.Parameters.AddWithValue("@CompanyName", txtSippersCompanyName.Text);
            InsertShippers.Parameters.AddWithValue("@Phone", txtSippersPhoneNumber.Text);

            con.Open();
            InsertShippers.ExecuteNonQuery();
            con.Close();
            GridViewShippers.DataBind();
        }

        protected void btnSearchShippers_Click1(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [ShipperID],[CompanyName],[Phone] FROM [dbo].[TblShippers] WHERE ShipperID =" + txtSippersID.Text; // your id
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {

                txtSippersID.Text = DTable1.Rows[0]["ShipperID"].ToString();
                txtSippersCompanyName.Text = DTable1.Rows[0]["CompanyName"].ToString();
                txtSippersPhoneNumber.Text = DTable1.Rows[0]["Phone"].ToString();
                // id
            }

            con.Close();
        }

        protected void btnUpdateShippers_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertShippers = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertShippers = new SqlCommand("UPDATE [dbo].[TblShippers] SET [CompanyName] = @CompanyName,[Phone] = @Phone WHERE ShipperID = @ShipperID", con);

            InsertShippers.Parameters.AddWithValue("@ShipperID", txtSippersID.Text);
            InsertShippers.Parameters.AddWithValue("@CompanyName", txtSippersCompanyName.Text);
            InsertShippers.Parameters.AddWithValue("@Phone", txtSippersPhoneNumber.Text);

            //FileUploadImage.SaveAs(Server.MapPath("images/" + FileUploadImage.FileName));
            //TxtImageURL.Text = "images/" + FileUploadImage.FileName;
            //InsertProduct.Parameters.AddWithValue("@ProductImage", TxtImageURL.Text);

            con.Open();
            InsertShippers.ExecuteNonQuery();
            con.Close();
            GridViewShippers.DataBind();
        }

        protected void btnDeleteShippers_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertShippers = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertShippers = new SqlCommand("DELETE FROM [dbo].[TblShippers] WHERE ShipperID = @ShipperID", con);

            InsertShippers.Parameters.AddWithValue("@ShipperID", txtSippersID.Text);

            con.Open();
            InsertShippers.ExecuteNonQuery();
            con.Close();
            GridViewShippers.DataBind();

            // Clear
            txtSippersID.Text = "";
            txtSippersCompanyName.Text = "";
            txtSippersPhoneNumber.Text = "";
        }

        protected void btnClearShippersForm_Click(object sender, EventArgs e)
        {
            // Clear
            txtSippersID.Text = "";
            txtSippersCompanyName.Text = "";
            txtSippersPhoneNumber.Text = "";
            txtSippersPhoneNumber.Focus(); // Set input focuss to a control
        }
    }
}