﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="EditInvoice.aspx.cs" Inherits="CarpowerWebApplication.EditInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <asp:TextBox ID="TxtCategoryID" Visible="false" runat="server"></asp:TextBox>
        <asp:TextBox ID="TxtShowAll" runat="server" Visible="false" Text="0"></asp:TextBox>

        <div class="row">
            <div class="col-lg-5">
                <asp:TextBox ID="TxtSearch" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="col-lg-1">
                <asp:Button ID="BtnSearch" runat="server" Text="ស្វែងរក" CssClass="btn btn-info" OnClick="BtnSearch_Click" Style="font-family: 'Khmer OS Siemreap'" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-6">


                <asp:DataList ID="DataListProductByCategory" Width="100%" runat="server" DataKeyField="CategoryID" DataSourceID="SqlDataSourceListProductByCategory" RepeatDirection="Horizontal" OnItemCommand="DataListProductByCategory_ItemCommand">
                    <itemtemplate>


                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="ByCategory" CommandArgument='<%# Eval("CategoryID") %>' BackColor="#007BFF" CssClass="btn btn-success" ForeColor="White" Font-Size="Medium" Height="40px">

                            <asp:Label ID="CategoryNameLabel" runat="server" Text='<%# Eval("CategoryName") %>' />
                        </asp:LinkButton></itemtemplate></asp:DataList><asp:SqlDataSource ID="SqlDataSourceListProductByCategory" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [CategoryID], [CategoryName] FROM [TblCategory] union select 0,'Show All'"></asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-6">
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="Pro_ListOrderProduct" SelectCommandType="StoredProcedure">
                    <selectparameters>
                        <asp:ControlParameter ControlID="TxtCategoryID" Name="CategoryID" PropertyName="Text" Type="Int32" />
                        <asp:ControlParameter ControlID="TxtShowAll" Name="ListAll" PropertyName="Text" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter ControlID="TxtSearch" DefaultValue="0" Name="ProductName" PropertyName="Text" Type="String" />
                    </selectparameters>
                </asp:SqlDataSource>
                <asp:DataList ID="DataList1" runat="server" DataKeyField="ProductID" DataSourceID="SqlDataSource2" RepeatDirection="Horizontal" RepeatColumns="4" OnItemCommand="DataList1_ItemCommand">
                    <itemtemplate>
                        <a href="EditInvoice.aspx?CategoryID=<%# Eval("CategoryID") %>&ProductID=<%# Eval("ProductID") %>"></a>
                        <div class="card" style="width: 10rem;">
                            <asp:Image ID="ListImg" runat="server" CssClass="card-img-top" ImageUrl='<%# Eval("image") %>' />
                            <div class="card-body">
                                <h5>
                                    <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' Font-Size="Small" />
                                </h5>
                                <asp:Label ID="Label1" runat="server" Style="font-size: 14px;">តម្លៃ <span style="color: red;"><%# "$" + String.Format("{0:#,0.##}", Eval("UnitPrice")) %></span>
                                </asp:Label></div><asp:LinkButton ID="LbtnAddToCart" CommandArgument='<%# Eval("ProductID") %>' CommandName="AddToCart" runat="server">Add More</asp:LinkButton></div></itemtemplate></asp:DataList></div><div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-9">
                        <asp:TextBox ID="TxtSearchInvoice" CssClass="form-control" runat="server"></asp:TextBox></div><div class="col-lg-3">
                        <asp:Button ID="BtnSearchInvoice" CssClass="btn btn-primary" runat="server" Text="Edit Invoice" OnClick="BtnSearchInvoice_Click" />
                    </div>
                </div>
                <asp:Label ID="LblCutSotck" runat="server" ForeColor="Red"></asp:Label><asp:TextBox ID="TxtUserLog" runat="server" Visible="false"></asp:TextBox><div class="col-lg-11">
                    <asp:GridView ID="GridViewListOrderProduct" CssClass="table table-bordered border-primary" runat="server" AutoGenerateColumns="False" DataKeyNames="OrderDetailID" OnDataBound="GridViewListOrderProduct_DataBound" OnSelectedIndexChanged="GridViewListOrderProduct_SelectedIndexChanged">
                        <columns>
                            <asp:BoundField DataField="OrderDetailID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="OrderDetailID" />
                            <asp:BoundField DataField="ProductID" HeaderText="" SortExpression="ProductID" />
                            <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                            <asp:BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                            <asp:TemplateField HeaderText="image" SortExpression="image">
                                <edititemtemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox></edititemtemplate><itemtemplate>
                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("image") %>' Width="80px" />
                                </itemtemplate>
                            </asp:TemplateField>
                            <asp:CommandField SelectText="Remove" ShowSelectButton="True" />
                        </columns>
                    </asp:GridView>
                </div>
                <asp:SqlDataSource ID="SqlDataSourceListOrderProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrderDetails.OrderDetailID, TblProduct.ProductID,TblProduct.ProductName, TblOrderDetails.UnitPrice, TblOrderDetails.Quantity, TblProduct.image FROM TblOrderDetails INNER JOIN TblProduct ON TblOrderDetails.ProductID = TblProduct.ProductID WHERE (TblOrderDetails.UserLog = @UserLog) AND (TblOrderDetails.Ordered = 0) ORDER BY TblOrderDetails.OrderDetailID DESC">
                    <selectparameters>
                        <asp:SessionParameter Name="UserLog" SessionField="UserLog" />
                    </selectparameters>
                </asp:SqlDataSource>
                <div class="row">
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-5">
                    </div>
                    <div class="col-lg-6 text-left">
                        <div id="DivTotalAmount" runat="server" style="font-family: 'Khmer OS Siemreap'">
                            តម្លៃសរុប: $ <asp:Label ID="LblTotalAmount" Text="0" CssClass="total-amount" runat="server"></asp:Label><asp:LinkButton ID="LbtnOrderNow" runat="server" class="btn btn-outline-primary" OnClick="LbtnOrderNow_Click" Style="margin-left: 10px">បញ្ជាទិញ</asp:LinkButton></div></div></div></div></div></div></asp:Content>