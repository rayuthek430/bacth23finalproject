﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="CarpowerWebApplication.WebFormProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .btn {
            font-family:'Khmer OS Siemreap';
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2>Manage Product</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label8" runat="server" Text="លេខសម្គាល់ផលិតផល" style="font-family:'Khmer OS Siemreap';"></asp:Label></b>
                    <asp:TextBox ID="txtProductID" runat="server" CssClass="form-control"></asp:TextBox>
                    <%--<b>
                        <asp:Label ID="Label7" runat="server" Text="Product name"></asp:Label></b>
                    <asp:TextBox ID="txtProductName" runat="server" CssClass="form-control"></asp:TextBox>--%>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label3" runat="server" Text="លេខសម្គាល់អ្នកផ្គត់ផ្គង់" style="font-family:'Khmer OS Siemreap';"></asp:Label></b>
                    <asp:DropDownList ID="DropDownListSupplierID" runat="server" CssClass="form-control" DataSourceID="SqlDataSourceShippers" DataTextField="SupplierID" DataValueField="SupplierID"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceShippers" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [SupplierID] FROM [TblProduct]"></asp:SqlDataSource>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label1" runat="server" Text="ប្រភេទទ្បាន" style="font-family:'Khmer OS Siemreap';"></asp:Label></b>
                    <asp:DropDownList ID="DropDownListCategoryID" runat="server" CssClass="form-control" DataSourceID="SqlDataSourceListCategory" DataTextField="CategoryName" DataValueField="CategoryID"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceListCategory" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [CategoryName], [CategoryID] FROM [TblCategory]"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceCategory" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblCategory.CategoryName, TblProduct.CategoryID FROM TblCategory INNER JOIN TblProduct ON TblCategory.CategoryID = TblProduct.CategoryID"></asp:SqlDataSource>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label5" runat="server" Text="ចំនូន" style="font-family:'Khmer OS Siemreap';"></asp:Label></b>
                    <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label6" runat="server" Text="តម្លៃ" style="font-family:'Khmer OS Siemreap';"></asp:Label></b>
                    <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label2" runat="server" Text="UnitlnStock"></asp:Label></b>
                    <asp:TextBox ID="txtUnitlnStock" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label7" runat="server" Text="ឈ្មោះ​ផលិតផល" style="font-family:'Khmer OS Siemreap';"></asp:Label></b>
                    <asp:TextBox ID="txtProductName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label4" runat="server" Text="លេខតួររថយន្ត" style="font-family:'Khmer OS Siemreap';"></asp:Label>

                    </b>
                    <asp:TextBox ID="txtShortNumber" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label9" runat="server" Text="លេខម៉ាស៊ីន" style="font-family:'Khmer OS Siemreap';"></asp:Label>
                    </b>
                    <asp:TextBox ID="textEngineNumber" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label10" runat="server" Text="ស្លាកលេខសំគាល់" style="font-family:'Khmer OS Siemreap';"></asp:Label>
                    </b>
                    <asp:TextBox ID="TagIDCar" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label11" runat="server" Text="លតប័ត្រ" style="font-family:'Khmer OS Siemreap';"></asp:Label>
                    </b>
                    <asp:TextBox ID="MVCar" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label12" runat="server" Text="ឆ្នាំផលិត" style="font-family:'Khmer OS Siemreap';"></asp:Label>
                    </b>
                    <asp:TextBox ID="textCarManufactureDate" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label13" runat="server" Text="កាលបរិច្ឆេទ" style="font-family:'Khmer OS Siemreap';"></asp:Label>
                    </b>

                    <asp:TextBox ID="txtProductDate" runat="server" required="required" TextMode="Date" Enabled="True" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label14" runat="server" Text="ពណ័មានលម្អិត" style="font-family:'Khmer OS Siemreap';"></asp:Label>
                    </b>
                     <asp:TextBox ID="textDailProduct" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <asp:Image ID="ProductImage" runat="server" Height="332px" Width="401px" Style="object-fit: cover" />
                    <br />
                    <br />
                    <asp:TextBox ID="ProductImageURL" runat="server" CssClass="form-control"></asp:TextBox>
                    <br />
                    <asp:FileUpload ID="UploadPicturePro" runat="server" />
                </div>
            </div>

            <div class="col-6">
                <div class="mb-3">
                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Text="បញ្ចូល" OnClick="btnInsertProduct_Click1" OnClientClick="return checkIsnull();" />
                    <asp:Button ID="Button2" runat="server" CssClass="btn btn-success" Text="កែ" OnClick="btnUpdateProduct_Click" />
                    <asp:Button ID="Button3" runat="server" CssClass="btn btn-danger" Text="លុប" OnClick="btnDeleteProduct_Click" />
                    <asp:Button ID="Button4" runat="server" CssClass="btn btn-secondary" Text="ស្វែងរក" OnClick="btnSearch_Click" />
                    <asp:Button ID="Button5" runat="server" CssClass="btn btn-warning" Text="សម្អាត" OnClick="btnClear_Click" />
                    <asp:Button ID="Button6" runat="server" CssClass="btn btn-outline-success" Text="បន្ថែមការពិពណ៌នា" OnClick="ButtonInSertDecription_Click" /><br /><br />
                    <asp:Button ID="Button7" runat="server" CssClass="btn btn-outline-info" Text="បន្ថែមរូបភាព" OnClick="ButtonInSertImg_Click" style="margin-right: 375px;" />
                </div>
            </div>
        </div>



        <%-- <div class="row">
            <div class="col-6">
                <asp:Button ID="btnInsertProduct" runat="server" CssClass="btn btn-primary" Text="Insert" OnClick="btnInsertProduct_Click1" OnClientClick="return checkIsnull();" />
                <asp:Button ID="btnUpdateProduct" runat="server" CssClass="btn btn-success" Text="Update" OnClick="btnUpdateProduct_Click" />
                <asp:Button ID="btnDeleteProduct" runat="server" CssClass="btn btn-danger" Text="Delete" OnClick="btnDeleteProduct_Click" />
                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-secondary" Text="Search" OnClick="btnSearch_Click" />
                <asp:Button ID="btnClear" runat="server" CssClass="btn btn-warning" Text="Clear" OnClick="btnClear_Click" />
                <asp:Button ID="ButtonInSertDecription" runat="server" CssClass="btn btn-outline-success" Text="Decription" OnClick="ButtonInSertDecription_Click" />
                <asp:Button ID="ButtonInSertImg" runat="server" CssClass="btn btn-outline-info" Text="Add Image" OnClick="ButtonInSertImg_Click" />
            </div>
            <div class="col-6"></div>
        </div>--%>

        <br />
        <hr />
        <div class="row">
            <div class="col-12">
                <h3>ទំនិញ</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">

                <asp:GridView ID="GridViewProduct" CssClass="table table-striped table-hover" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductID" DataSourceID="SqlDataSourceListGridViewProduct" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="ProductID" HeaderText="ProductID" InsertVisible="False" ReadOnly="True" SortExpression="ProductID" />
                        <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                        <asp:BoundField DataField="SupplierID" HeaderText="SupplierID" SortExpression="SupplierID" />
                        <asp:BoundField DataField="CategoryID" HeaderText="CategoryID" SortExpression="CategoryID" />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                        <asp:BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
                        <asp:BoundField DataField="UnitInStock" HeaderText="UnitInStock" SortExpression="UnitInStock" />
                        <asp:BoundField DataField="image" HeaderText="image" SortExpression="image" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListGridViewProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [ProductID], [ProductName], [SupplierID], [CategoryID], [Quantity], [UnitPrice], [UnitInStock], [image] FROM [TblProduct]"></asp:SqlDataSource>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        <%--var Productname = document.getElementById("<%= txtProductName.ClientID %>").value;--%>
        <%--var Quantity = document.getElementById("<%= txtQuantity.ClientID %>").value;--%>

            //function checkIsnull() {
            //    if (Productname.trim() === '') {
            //        Swal.fire({
            //            title: 'Error!',
            //            text: 'Please enter your product name',
            //            icon: 'error',
            //            confirmButtonText: 'OK'
            //        })
            //        return false;
            //    } else {
            //        return true;
            //    }

            //    if (Quantity == "") {
            //        Swal.fire({
            //            title: 'Error!',
            //            text: 'Please enter your quantity',
            //            icon: 'error',
            //            confirmButtonText: 'OK'
            //        })
            //        return false;
            //    }
            //}


    </script>
</asp:Content>
