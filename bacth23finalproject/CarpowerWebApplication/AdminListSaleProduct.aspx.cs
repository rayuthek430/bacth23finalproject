﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class AdminListSaleProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["SFullName"] != null)
            {
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }



        protected void GridViewListOrderProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand InsertData = new SqlCommand();

            InsertData = new SqlCommand("pro_AdminAprovalOrder", con);
            InsertData.CommandType = CommandType.StoredProcedure;

            InsertData.Parameters.AddWithValue("@OrderDetailID", GridViewListOrderProduct.SelectedRow.Cells[0].Text.ToString());


            InsertData.Parameters.AddWithValue("@StatusID", 2);
            InsertData.Parameters.AddWithValue("@EmployeeID", Session["SUserID"]);
            con.Open();

            InsertData.ExecuteNonQuery();

            con.Close();
            con.Dispose();
            GridViewListOrderProduct.DataBind();
        }

        protected void GridViewListOrderProduct_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ViewInvoice")
            {
                Response.Redirect("Invoices.aspx?OrderID=" + e.CommandArgument.ToString());
            }
        }
    }
}