﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="ListAllForm.aspx.cs" Inherits="CarpowerWebApplication.ListAllForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .card-title {
            font-size: medium;
            font-family: 'Khmer OS Siemreap';
        }

        .btn-primary {
            font-size: medium;
            font-family: 'Khmer OS Siemreap';
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <br />

    <div class="container">
        <div class ="card-title">
            <h1>ការគ្រប់គ្រងបញ្ជូលទិន្នន័យរបស់ Product </h1>
        </div>

        <br />
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">ការគ្រប់គ្រង​បញ្ចូលទិន្នន័យរបស់រថយន្ត</h5>
                        <%--<p class="card-text"></p>--%>
                        <br />
                        <a class="btn btn-primary" href="Product.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">ការគ្រប់គ្រងបញ្ជូលទិន្នន័យរបស់ប្រភេទរថយន្ត</h5>
                        <%--<p class="card-text"></p>--%>
                        <br />
                        <a class="btn btn-primary" href="Category.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <%--<div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">​ការគ្រប់គ្រងនៃការកម្មង់ទិញ</h5>
                        <br />
                        <a class="btn btn-primary" href="Orders.aspx">ចូល</a>
                    </div>
                </div>
            </div>--%>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">ការគ្រប់គ្រងនៃការបញ្ចូលគណនី</h5>
                        <br />
                        <a class="btn btn-primary" href="UserControl.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
        </div>


    </div>
</asp:Content>
