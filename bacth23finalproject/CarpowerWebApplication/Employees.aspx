﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Employees.aspx.cs" Inherits="CarpowerWebApplication.WebFormEmployees" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2>Form Employees</h2>
        <br />
        <div class="row">
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label1" runat="server" Text="Employees id:"></asp:Label>
                    </b>
                    <asp:DropDownList ID="EmployeesID" runat="server" CssClass="form-control" DataSourceID="SqlDataSourceEmployees" DataTextField="EmployeeID" DataValueField="EmployeeID"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceEmployees" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [EmployeeID] FROM [TblEmployees]"></asp:SqlDataSource>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label2" runat="server" Text="First name:"></asp:Label>
                    </b>

                    <asp:TextBox ID="txtEmployeesFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label3" runat="server" Text="Last name:"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtEmployeesLastName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label4" runat="server" Text="Title:"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtEmployeesTitle" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label5" runat="server" Text="Birth date:"></asp:Label>
                    </b>

                    <%-- <asp:TextBox ID="txtEmployeesBirthDate" runat="server" CssClass="form-control"></asp:TextBox>--%>
                    <asp:TextBox ID="txtEmployeesBirthDate" runat="server" required="required" TextMode="Date" Enabled="True" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label6" runat="server" Text="Hire date:"></asp:Label>
                    </b>

                    <asp:TextBox ID="txtHireDate" runat="server" required="required" TextMode="Date" Enabled="True" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label7" runat="server" Text="Address:"></asp:Label>
                    </b>

                    <asp:TextBox ID="txtEmployeesAddress" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label8" runat="server" Text="City:"></asp:Label>
                    </b>

                    <asp:TextBox ID="txtEmployessCity" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label9" runat="server" Text="Region:"></asp:Label>
                    </b>

                    <asp:TextBox ID="txtEmployeesRegion" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label10" runat="server" Text="Postal code:"></asp:Label>
                    </b>

                    <asp:TextBox ID="txtEmployeesPostalCode" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label11" runat="server" Text="Country:"></asp:Label>
                    </b>

                    <asp:TextBox ID="txtEmployeesCountry" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label12" runat="server" Text="Home phone:"></asp:Label>
                    </b>

                    <asp:TextBox ID="txtEmployeesHomePhone" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Image ID="EmployeesImage" runat="server" Height="221px" Width="423px" CssClass="card-img-top" />

                    <br />
                    <br />
                    <asp:TextBox ID="EmployeesURL" runat="server" CssClass="form-control"></asp:TextBox>
                    <br />
                    <asp:FileUpload ID="EmployeesUpload" runat="server" CssClass="form-control" />
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
                        <asp:Label ID="Label14" runat="server" Text="Note:"></asp:Label>
                    </b>

                    <asp:TextBox ID="txtEmployeesNote" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">

                <%--Button--%>
                <asp:Button ID="btnInsertEmployees" runat="server" CssClass="btn btn-primary" Text="Insert" OnClick="btnInsertEmployees_Click" />
                <asp:Button ID="btnUpdateEmployees" runat="server" CssClass="btn btn-secondary" Text="Update" OnClick="btnUpdateEmployees_Click" />
                <asp:Button ID="btnDeleteEmployees" runat="server" CssClass="btn btn-danger" Text="Delete" OnClick="btnDeleteEmployees_Click" />
                <asp:Button ID="btnSearchEmployees" runat="server" CssClass="btn btn-secondary" Text="Search" OnClick="btnSearchEmployees_Click" />
                <asp:Button ID="btnClearEmployees" runat="server" CssClass="btn btn-warning" Text="Clear" OnClick="btnClearEmployees_Click" />
            </div>
            <div class="col-6"></div>
        </div>
        <br />
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewEmployees" CssClass="table table-striped table-hover" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="EmployeeID" DataSourceID="SqlDataSourceListGridViewEmployees">
                    <Columns>
                        <asp:BoundField DataField="EmployeeID" HeaderText="EmployeeID" InsertVisible="False" ReadOnly="True" SortExpression="EmployeeID" />
                        <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                        <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
                        <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                        <asp:BoundField DataField="BirthDate" HeaderText="BirthDate" SortExpression="BirthDate" />
                        <asp:BoundField DataField="HireDate" HeaderText="HireDate" SortExpression="HireDate" />
                        <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                        <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                        <asp:BoundField DataField="Region" HeaderText="Region" SortExpression="Region" />
                        <asp:BoundField DataField="PostalCode" HeaderText="PostalCode" SortExpression="PostalCode" />
                        <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
                        <asp:BoundField DataField="HomePhone" HeaderText="HomePhone" SortExpression="HomePhone" />
                        <asp:BoundField DataField="Photo" HeaderText="Photo" SortExpression="Photo" />
                        <asp:BoundField DataField="Note" HeaderText="Note" SortExpression="Note" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListGridViewEmployees" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [EmployeeID], [FirstName], [LastName], [Title], [BirthDate], [HireDate], [Address], [City], [Region], [PostalCode], [Country], [HomePhone], [Photo], [Note] FROM [TblEmployees]"></asp:SqlDataSource>
            </div>

        </div>


    </div>
</asp:Content>
