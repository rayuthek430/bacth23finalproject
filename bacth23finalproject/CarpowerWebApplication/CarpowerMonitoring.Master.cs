﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class CarpowerMonitoring : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["SFullName"] != null)
            {
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            if (Session["SFullName"] != null)
            {
                LblUserName.Text = Session["SFullName"].ToString();
                LbtnLogin.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ចាកចេញ";
            }
            else
            {
                LbtnLogin.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ចូលប្រព័ន្ធ";
            }
        }

        protected void LbtnLogin_Click(object sender, EventArgs e)
        {
            Session["SUserID"] = null;
            Session["SFullName"] = null;
            Session["SRoleID"] = null;
            Session["UserLog"] = null;
            Response.Redirect("Login.aspx");
        }
    }
}