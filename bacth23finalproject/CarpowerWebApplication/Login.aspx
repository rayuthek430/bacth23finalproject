﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CarpowerWebApplication.WebFormLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
         
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".box-menu").addClass("d-none");
            $("#test").addClass("d-none");
        });
    </script>


    <%-- Login --%>
    <%--<div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>ចូលប្រព័ន្ធ</h4>
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <asp:Label ID="Label1" runat="server" CssClass="form-label" Text="ឈ្មោះ​អ្នកប្រើប្រាស់"></asp:Label>
                            <asp:TextBox ID="TxtUserName" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="mb-3">
                            <asp:Label ID="Label2" runat="server" CssClass="form-label" Text="ពាក្យសម្ងាត់"></asp:Label>
                            <asp:TextBox ID="TxtPassword" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <asp:Button ID="BtnLogin" runat="server" CssClass="btn btn-primary" Text="ចូល" OnClick="BtnLogin_Click" />
                            <asp:Button ID="BtnCreateNewUser" runat="server" CssClass="btn btn-info" Text="មិនទាន់មាន?" OnClick="BtnCreateNewUser_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--%>
    
        <div class="col-8">
            <center>
                <div class="card-body" style="background-color: #ebf2fa; width: 495px">
                    <div class="text-center">
                         <img src="images/Car-Power-Logo.png" alt="" style="width: 200px; object-fit: cover;">
                    </div>

                    <div class="mb-3" style="width:362px">
                         <asp:TextBox ID="TxtUserName" CssClass="form-control" runat="server"  placeholder="ឈ្មោះ​អ្នកប្រើប្រាស់"></asp:TextBox>
                        
                    </div>
                    <div class="mb-3" style="width:362px">
                      <asp:TextBox ID="TxtPassword" CssClass="form-control" runat="server"  placeholder="ពាក្យសម្ងាត់"></asp:TextBox>
                    </div>

                    <div class="text-center" style="width:362px">
                       <asp:Button ID="BtnLogin" runat="server" CssClass="btn btn-color px-5 mb-5 w-100" Text="ចូល" OnClick="BtnLogin_Click" style="background-color: #0e1c36; color: #fff;" />
                    </div>
                    <div class="form-text text-center mb-5 text-dark">
                        មិនទាន់មាន? <asp:Button ID="BtnCreateNewUser" runat="server" CssClass="btn btn-primary btn-sm" Text="មិនទាន់មាន?" OnClick="BtnCreateNewUser_Click" />
                    </div>
                    
                </div>
            </center>

        </div>







</asp:Content>
