﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="UserControl.aspx.cs" Inherits="CarpowerWebApplication.UserControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2 style="font-family: 'Khmer OS Siemreap'">គ្រប់គ្រងគណនី</h2>
        <br />
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label1" runat="server" Text="លេខសម្គាល់" Font-Names="Khmer OS Siemreap"></asp:Label></b>
                    <asp:TextBox ID="txtUserID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <%--<b style="font-family: Arial, Helvetica, sans-serif; font-size: 17px">
    <asp:Label ID="Label2" runat="server" Text="Gender"></asp:Label></b>
<asp:TextBox ID="UserGender" runat="server" CssClass="form-control"></asp:TextBox>--%>
                        <%-- <label for="genderDropdown" class="form-label">ភេទ</label>--%>
                        <asp:Label ID="Label2" runat="server" Text="ភេទ" Font-Names="Khmer OS Siemreap"></asp:Label></b>
                    <asp:DropDownList ID="UserGender" runat="server" CssClass="form-control" aria-label=".form-select-lg example">
                        <asp:ListItem Value="ប្រុស">ប្រុស</asp:ListItem>
                        <asp:ListItem Value="ស្រី">ស្រី</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <asp:Button ID="ButtonInsertUserControl" runat="server" CssClass="btn btn-info" Text="បញ្ចូល" Font-Names="Khmer OS Siemreap" Style="margin-top: 24px" OnClick="ButtonInsertUserControl_Click" />
                    <asp:Button ID="ButtonSerarchUserControl" runat="server" CssClass="btn btn-primary" Text="ស្វែងរក" Font-Names="Khmer OS Siemreap" Style="margin-top: 24px" OnClick="ButtonSerarchUserControl_Click" />
                    <asp:Button ID="ButtonUpdateUserControl" runat="server" CssClass="btn btn-success" Text="កែ" Font-Names="Khmer OS Siemreap" Style="margin-top: 24px" OnClick="ButtonUpdateUserControl_Click" />
                    <asp:Button ID="ButtonDeleteUserControl" runat="server" CssClass="btn btn-danger" Text="លុប" Font-Names="Khmer OS Siemreap" Style="margin-top: 24px" OnClick="ButtonDeleteUserControl_Click" />
                    <asp:Button ID="ButtonSearchUserControl" runat="server" CssClass="btn btn-warning" Text="សម្អាត" Font-Names="Khmer OS Siemreap" Style="margin-top: 24px" OnClick="ButtonSearchUserControl_Click" />

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label3" runat="server" Text="ឈ្មោះ" Font-Names="Khmer OS Siemreap"></asp:Label>
                    </b>
                    <asp:TextBox ID="UserName" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label4" runat="server" Text="លេខទូរស័ព្ទ" Font-Names="Khmer OS Siemreap"></asp:Label>
                    </b>
                    <asp:TextBox ID="PhoneNumber" runat="server" CssClass="form-control"></asp:TextBox>


                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label5" runat="server" Text="អត្តសញ្ញាណប័ណ្" Font-Names="Khmer OS Siemreap"></asp:Label>
                    </b>
                    <asp:TextBox ID="UserIDCard" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label6" runat="server" Text="អាយុ" Font-Names="Khmer OS Siemreap"></asp:Label>
                    </b>
                    <asp:TextBox ID="UserAge" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label8" runat="server" Text="លិខិតឆ្លងដែន" Font-Names="Khmer OS Siemreap"></asp:Label>
                    </b>
                    <asp:TextBox ID="UserPassport" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label9" runat="server" Text="អ៊ីមែល" Font-Names="Khmer OS Siemreap"></asp:Label>
                    </b>
                    <asp:TextBox ID="UserEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label10" runat="server" Text="ពាក្យសម្ងាត់" Font-Names="Khmer OS Siemreap"></asp:Label>
                    </b>
                    <asp:TextBox ID="UserPassword" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">

                    <b>
                        <asp:Label ID="Label11" runat="server" Text="អាស័យដ្ឋាន" Font-Names="Khmer OS Siemreap"></asp:Label>
                    </b>
                    <asp:TextBox ID="Address" runat="server" CssClass="form-control" Height="150px" TextMode="MultiLine" Width="418px"></asp:TextBox>

                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label7" runat="server" Text="មុខងារ" Font-Names="Khmer OS Siemreap"></asp:Label></b>
                    <asp:DropDownList ID="DropDownChooesRole" runat="server" CssClass="form-control" aria-label=".form-select-lg example">
                        <asp:ListItem Value="1">Guest</asp:ListItem>
                        <asp:ListItem Value="2">User</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <asp:Button ID="BtnSampleData" runat="server" CssClass="btn btn-warning" Text="Sample Data" Style="margin-top: 24px" OnClick="BtnSampleData_Click" />
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewListUser" CssClass="table table-bordered border-primary" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="UserID" DataSourceID="SqlDataSourceListUser">
                    <Columns>
                        <asp:BoundField DataField="UserID" HeaderText="លេខ​" InsertVisible="False" ReadOnly="True" SortExpression="UserID" />
                        <asp:BoundField DataField="FullName" HeaderText="ឈ្មោះ" SortExpression="FullName" />
                        <asp:BoundField DataField="RoleName" HeaderText="តួនាទី" SortExpression="RoleName" />
                        <asp:BoundField DataField="Password" HeaderText="ពាក្យសម្ងាត់" SortExpression="Password" />
                        <asp:BoundField DataField="PhoneNumber" HeaderText="លេខទូរស័ព្ទ" SortExpression="PhoneNumber" />
                        <asp:BoundField DataField="Email" HeaderText="អ៊ីមែល" SortExpression="Email" />
                        <asp:BoundField DataField="Address" HeaderText="អាស័យដ្ឋាន" SortExpression="Address" />
                        <asp:BoundField DataField="Gender" HeaderText="ភេទ" SortExpression="Gender" />
                        <asp:BoundField DataField="Age" HeaderText="អាយុ" SortExpression="Age" />
                        <asp:BoundField DataField="Position" HeaderText="Position" SortExpression="Position" />
                        <asp:BoundField DataField="IdCard" HeaderText="អត្តសញ្ញាណប័ណ្" SortExpression="IdCard" />
                        <asp:BoundField DataField="Passport" HeaderText="លិខិតឆ្លងដែន" SortExpression="Passport" />
                    </Columns>
                    <HeaderStyle Font-Names="Khmer OS Siemreap" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListUser" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT dbo.TblUser.UserID, dbo.TblUser.FullName, dbo.TblUser.Password, dbo.TblUser.PhoneNumber, dbo.TblUser.Email, dbo.TblUser.Address, dbo.TblUser.Gender, dbo.TblUser.Age, dbo.TblUser.Position, dbo.TblUser.IdCard, 
                  dbo.TblUser.Passport, dbo.TblRole.RoleName
FROM     dbo.TblUser INNER JOIN
                  dbo.TblRole ON dbo.TblUser.RoleID = dbo.TblRole.RoleID order by TblUser.UserID desc"></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
