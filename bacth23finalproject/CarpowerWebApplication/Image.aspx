﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Image.aspx.cs" Inherits="CarpowerWebApplication.FormImage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .applyFont{
            font-family:'Khmer OS Siemreap';
        }
        btn{
            font-family:'Khmer OS Siemreap';
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2 style="font-family:'Khmer OS Siemreap'">បន្ថែមរូបភាព</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <asp:Label ID="Lable1" CssClass="applyFont" runat="server" Text="លេខសម្គាល់រូបភាព"></asp:Label>
                    <asp:TextBox ID="TxtImage" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <asp:Label ID="Label2" CssClass="applyFont" runat="server" Text="ឈ្មោះរូបភាព"></asp:Label>
                    <asp:TextBox ID="TxtImageName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <%--<asp:Label ID="Label3" runat="server" Text="Image url:"></asp:Label>
                    <asp:TextBox ID="TxtImageUrl" runat="server" CssClass="form-control"></asp:TextBox>--%>
                    <asp:Image ID="Imgs" runat="server" Height="221px" Width="323px" />
                    <br /><br />
                    <asp:TextBox ID="ImgURL" runat="server" CssClass="form-control"></asp:TextBox>
                    <br />
                    <asp:FileUpload ID="FileUploadImageS" runat="server" />
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <asp:Label ID="Label5" CssClass="applyFont" runat="server" Text="ឈ្មោះទ្បាន"></asp:Label>
                    <%-- <asp:TextBox ID="TxtProductionid" runat="server" CssClass="form-control"></asp:TextBox>--%>
                    <asp:DropDownList ID="DropDownListProductID" runat="server" CssClass="form-control" DataSourceID="SqlDataSourceListProdctID" DataTextField="ProductName" DataValueField="ProductID"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceListProdctID" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [ProductID], [ProductName] FROM [TblProduct] WHERE ([ProductID] = @ProductID)">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="ProductID" QueryStringField="ProductID" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
            <div class="col-4">
            </div>
        </div>


        <div class="row">
            <div class="col-6">

                <div class="mb-3">
                    <asp:Button ID="btnInsertImage" runat="server" CssClass="btn btn-primary" Text="បញ្ចូល" OnClick="btnInsertImage_Click" />
                    <asp:Button ID="btnUpdateImage" runat="server" CssClass="btn btn-secondary" Text="កែ" OnClick="btnUpdateImage_Click" />
                    <asp:Button ID="btnDeleteImage" runat="server" CssClass="btn btn-danger" Text="លុប" OnClick="btnDeleteImage_Click" />
                    <asp:Button ID="btnSearchImage" runat="server" CssClass="btn btn-info" Text="ស្វែងរក" OnClick="btnSearchImage_Click" />
                    <asp:Button ID="btnClearImage" runat="server" CssClass="btn btn-warning" Text="សម្អាត" OnClick="btnClearImage_Click" />
                </div>


            </div>
            <div class="col-6">
            </div>

        </div>

         <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewFormImage" CssClass="table table-bordered border-primary" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ImageID" DataSourceID="SqlDataSourceListGridViewFormImage">
                    <Columns>
                        <asp:BoundField DataField="ImageID" HeaderText="ImageID" InsertVisible="False" ReadOnly="True" SortExpression="ImageID" />
                        <asp:BoundField DataField="ImageName" HeaderText="ImageName" SortExpression="ImageName" />
                        <%--<asp:BoundField DataField="PositionID" HeaderText="PositionID" SortExpression="PositionID" />--%>
                        
                        <asp:TemplateField HeaderText="ImageUrl" SortExpression="ImageUrl">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ImageUrl") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" Height="83px" ImageUrl='<%# Eval("ImageUrl") %>' Width="68px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListGridViewFormImage" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [ImageID], [ImageName], [ImageUrl] FROM [TblImage] WHERE ([ProductID] = @ProductID)">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="ProductID" QueryStringField="ProductID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
          
        </div>
        
    </div>
</asp:Content>
