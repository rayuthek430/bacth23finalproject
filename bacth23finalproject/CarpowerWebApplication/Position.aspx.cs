﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormPosition : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertPosition_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertPosition = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertPosition = new SqlCommand("INSERT INTO [dbo].[TblPosition] ([PositionName]) VALUES (@PositionName)", con);

            InsertPosition.Parameters.AddWithValue("@PositionName", TxtPositionName.Text);
            


            con.Open();
            InsertPosition.ExecuteNonQuery();
            con.Close();
            GridViewPosition.DataBind();
        }

        protected void btnSearchPosition_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [PositionID],[PositionName] FROM [dbo].[TblPosition] WHERE PositionID = " + TxtPositionID.Text; // your id
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {
                TxtPositionID.Text = DTable1.Rows[0]["PositionID"].ToString();
                TxtPositionName.Text = DTable1.Rows[0]["PositionName"].ToString();
                

            }

            con.Close();
        }

        protected void btnUpdatePosition_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdatepOSITION = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdatepOSITION = new SqlCommand("UPDATE [dbo].[TblPosition]SET [PositionName] = @PositionName WHERE [PositionID] = @PositionID", con);

            UpdatepOSITION.Parameters.AddWithValue("@PositionID", TxtPositionID.Text);
            UpdatepOSITION.Parameters.AddWithValue("@PositionName", TxtPositionName.Text);
            




            con.Open();
            UpdatepOSITION.ExecuteNonQuery();
            con.Close();
            GridViewPosition.DataBind();
        }

        protected void btnDeletePosition_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeletePosition = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeletePosition = new SqlCommand("DELETE FROM [dbo].[TblPosition] WHERE PositionID = @PositionID", con);

            DeletePosition.Parameters.AddWithValue("@PositionID", TxtPositionID.Text);

            con.Open();
            DeletePosition.ExecuteNonQuery();
            con.Close();
            GridViewPosition.DataBind();

            TxtPositionName.Text = "";
        }

        protected void btnClearPosition_Click(object sender, EventArgs e)
        {
            TxtPositionName.Text = "";
        }
    }
}