﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Category.aspx.cs" Inherits="CarpowerWebApplication.WebFormCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .btn {
            font-family: 'Khmer OS Siemreap';
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2>Form Category</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family: 'Khmer OS Siemreap';">ជ្រើសរើស</b>
                    <%--auto increment--%>

                    <%--<asp:Label ID="Label1" Visible="false" runat="server" Text="Category id:"></asp:Label>
                    <asp:TextBox ID="txtCategoryID" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>--%>

                    <asp:DropDownList ID="DropDownListCategory" CssClass="form-control" runat="server" DataSourceID="SqlDataSourceListDropDownCategory" DataTextField="CategoryName" DataValueField="CategoryID"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceListDropDownCategory" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [CategoryID], [CategoryName] FROM [TblCategory]"></asp:SqlDataSource>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label2" runat="server" Text="ឈ្មោះ​ប្រភេទទ្បាន" Style="font-family: 'Khmer OS Siemreap';"></asp:Label>

                    </b>
                    <asp:TextBox ID="txtCategoryName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <%-- <div class="row">
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label3" runat="server" Text="Description:"></asp:Label>
                    <asp:TextBox ID="txtCategoryDescription" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Image ID="CategoryImage" runat="server" Height="221px" Width="423px" />
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:TextBox ID="TextBoxPictureURL" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:FileUpload ID="FileUploadPictureCategory" runat="server" />
                </div>
            </div>
        </div>--%>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label3" runat="server" Text="ពិពណ៌នា" Style="font-family: 'Khmer OS Siemreap';"></asp:Label>

                    </b>
                    <asp:TextBox ID="txtCategoryDescription" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <asp:Image ID="CategoryImage" runat="server" Height="221px" Width="423px" />
                    <br />
                    <br />
                    <asp:TextBox ID="TextBoxPictureURL" runat="server" CssClass="form-control"></asp:TextBox>
                    <br />
                    <asp:FileUpload ID="FileUploadPictureCategory" runat="server" />
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-6">
                <%--Button--%>
                <asp:Button ID="btnInsertCategor" runat="server" CssClass="btn btn-primary" Text="បញ្ចូល" OnClick="btnInsertCategor_Click2" />
                <asp:Button ID="btnUpdateCategor" runat="server" CssClass="btn btn-secondary" Text="កែ" OnClick="btnUpdateCategor_Click" />
                <asp:Button ID="btnDeleteCategor" runat="server" CssClass="btn btn-danger" Text="លុប" OnClick="btnDeleteCategor_Click" />
                <asp:Button ID="btnSearchCategory" runat="server" CssClass="btn btn-info" Text="ស្វែងរក" OnClick="btnSearchCategory_Click" />
                <asp:Button ID="btnClearCategory" runat="server" CssClass="btn btn-secondary" Text="សម្អាត" OnClick="btnClearCategory_Click" />
            </div>
            <div class="col-6">
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewFormCategory" CssClass="table table-striped table-hover" runat="server" AutoGenerateColumns="False" DataKeyNames="CategoryID" DataSourceID="SqlDataSourceListGridViewCategory">
                    <Columns>
                        <asp:BoundField DataField="CategoryName" HeaderText="ឈ្មោះ​" SortExpression="CategoryName" />
                        <asp:BoundField DataField="Description" HeaderText="ពិពណ៌នា" SortExpression="Description" />
                        <asp:BoundField DataField="Picture" HeaderText="រូបភាព" SortExpression="Picture" />
                        <asp:BoundField DataField="CategoryID" HeaderText="លេខសម្គាល់" InsertVisible="False" ReadOnly="True" SortExpression="CategoryID" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListGridViewCategory" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [CategoryName], [Description], [Picture], [CategoryID] FROM [TblCategory]"></asp:SqlDataSource>
            </div>
        </div>



    </div>
</asp:Content>
