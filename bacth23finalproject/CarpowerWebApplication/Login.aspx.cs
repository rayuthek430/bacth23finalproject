﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand SearchData = new SqlCommand();
            con.Open();
            SearchData = new SqlCommand("Pro_UserLogin", con);
            SearchData.CommandType = CommandType.StoredProcedure;
            SearchData.Parameters.AddWithValue("@FullName", TxtUserName.Text);
            SearchData.Parameters.AddWithValue("@Password", TxtPassword.Text);

            SqlDataAdapter SDA = new SqlDataAdapter(SearchData);
            DataSet DS = new DataSet();
            SDA.Fill(DS);
            DataTable DT = new DataTable();
            DT = DS.Tables[0];
            if (DT.Rows.Count > 0)
            {
                Session["SUserID"] = DT.Rows[0]["UserID"].ToString();
                Session["SFullName"] = DT.Rows[0]["FullName"].ToString();
                Session["SRoleID"] = DT.Rows[0]["RoleID"].ToString();

                // Guest
                if (Session["SRoleID"].ToString() == "1")
                {

                    Response.Redirect("OrderProduct.aspx?url=" + Server.UrlEncode(Request.Url.AbsoluteUri));

                }
                // user
                else if (Session["SRoleID"].ToString() == "2")
                {
                    Response.Redirect("Sale.aspx?url=" + Server.UrlEncode(Request.Url.AbsoluteUri));
                }

                // admin
                else if (Session["SRoleID"].ToString() == "3")
                {

                    Response.Redirect("Sale.aspx?url=" + Server.UrlEncode(Request.Url.AbsoluteUri));

                }

            }

            con.Close();
            con.Dispose();
        }

        protected void BtnCreateNewUser_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["SUserID"])))
            {
                Response.Redirect("ClientRegister.aspx?url=" + Server.UrlEncode(Request.Url.AbsoluteUri));
            }

        }
    }
}