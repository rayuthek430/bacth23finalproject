﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="ListSaleProduct.aspx.cs" Inherits="CarpowerWebApplication.ListSaleProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#test").addClass("d-none");
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-lg-12">
        <nav class="navbar navbar-expand navbar-light bg-light">
            <p class="text-center navbar-brand mb-0" style="margin: 12px; font-family: 'Khmer OS Siemreap'; font-size: larger;">​ការបញ្ចាទិញរបស់ខ្ញុំ</p>
        </nav>
    </div>
    <br />
    <div class="col-lg-12">
        <asp:GridView ID="GridViewListOrderProduct" CssClass="table table-bordered border-primary" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceListOrderProduct" OnRowCommand="GridViewListOrderProduct_RowCommand" Font-Names="Khmer OS Siemreap" Font-Size="Medium">

            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="ViewInvioce" CommandArgument='<%# Bind("OrderID") %>' Text="មើលវិក័យបត្រ"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="OrderDetailID" HeaderText="ID" SortExpression="OrderDetailID" Visible="false" />
                <asp:BoundField DataField="ProductID" HeaderText="លេខឡាន" SortExpression="ProductID" />
                <asp:BoundField DataField="ProductName" HeaderText="ប្រភេទទ្បាន" SortExpression="ProductName" />
                <asp:BoundField DataField="UnitPrice" HeaderText="តម្លៃ" SortExpression="UnitPrice" />
                <asp:BoundField DataField="Quantity" HeaderText="ចំនួន" SortExpression="Quantity" />
                <asp:BoundField DataField="Amount" HeaderText="ចំនួនទឹកប្រាក់" SortExpression="Amount" />
                <%--<asp:BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate" />--%>
                <asp:TemplateField HeaderText="រូបភាព" SortExpression="image">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" Height="96px" ImageUrl='<%# Eval("image") %>' Width="84px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="AttachDoc" CommandArgument='<%# Bind("OrderDetailID") %>' Text="ភ្ជាប់ឯកសារ"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandArgument='<%# Bind("OrderDetailID") %>' CommandName="Comments" Text="មតិយោបល់"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="StatusName" HeaderText="ស្ថានភាព" SortExpression="StatusName" />

            </Columns>
        </asp:GridView>
    </div>
    <asp:SqlDataSource ID="SqlDataSourceListOrderProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrders.OrderID,TblOrderDetails.OrderDetailID,TblProduct.ProductID,TblProduct.ProductName, TblOrderDetails.UnitPrice, TblOrderDetails.Quantity, (TblOrderDetails.UnitPrice * TblOrderDetails.Quantity)as Amount, TblOrders.OrderDate, TblProduct.image, TblStatusOrder.StatusName FROM TblOrderDetails INNER JOIN TblProduct ON TblOrderDetails.ProductID = TblProduct.ProductID INNER JOIN TblOrders ON TblOrderDetails.OrderID = TblOrders.OrderID INNER JOIN TblStatusOrder ON TblOrderDetails.StatusID = TblStatusOrder.StatusID WHERE (TblOrders.Ordered = 1)AND (dbo.TblOrders.CustomerID = @CustomerID) and (TblOrderDetails.StatusID<>3)
  ORDER BY TblOrderDetails.OrderDetailID DESC">
        <SelectParameters>
            <asp:SessionParameter Name="CustomerID" SessionField="SUserID" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>
