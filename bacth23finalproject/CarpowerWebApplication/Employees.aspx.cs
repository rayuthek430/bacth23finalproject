﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormEmployees : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        // Insert
        protected void btnInsertEmployees_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertEmployess = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertEmployess = new SqlCommand("INSERT INTO [dbo].[TblEmployees] ([FirstName],[LastName],[Title],[BirthDate],[HireDate],[Address],[City],[Region],[PostalCode],[Country],[HomePhone],[Photo],[Note]) VALUES (@FirstName,@LastName,@Title,@BirthDate,@HireDate,@Address,@City,@Region,@PostalCode,@Country,@HomePhone,@Photo,@Note)", con);

            InsertEmployess.Parameters.AddWithValue("@FirstName", txtEmployeesFirstName.Text);
            InsertEmployess.Parameters.AddWithValue("@LastName", txtEmployeesLastName.Text);
            InsertEmployess.Parameters.AddWithValue("@Title", txtEmployeesTitle.Text);
            InsertEmployess.Parameters.AddWithValue("@BirthDate", txtEmployeesBirthDate.Text);
            InsertEmployess.Parameters.AddWithValue("@HireDate", txtHireDate.Text);
            InsertEmployess.Parameters.AddWithValue("@Address", txtEmployeesAddress.Text);
            InsertEmployess.Parameters.AddWithValue("@City", txtEmployessCity.Text);
            InsertEmployess.Parameters.AddWithValue("@Region", txtEmployeesRegion.Text);
            InsertEmployess.Parameters.AddWithValue("@PostalCode", txtEmployeesPostalCode.Text);
            InsertEmployess.Parameters.AddWithValue("@Country", txtEmployeesCountry.Text);
            InsertEmployess.Parameters.AddWithValue("@HomePhone", txtEmployeesHomePhone.Text);
            InsertEmployess.Parameters.AddWithValue("@Note", txtEmployeesNote.Text);

            if (EmployeesUpload.HasFile) 
            {
                EmployeesUpload.SaveAs(Server.MapPath("images/" + EmployeesUpload.FileName));
                EmployeesURL.Text = "images/" + EmployeesUpload.FileName;
                
            }

            EmployeesImage.ImageUrl = EmployeesURL.Text;
            InsertEmployess.Parameters.AddWithValue("@Photo", EmployeesURL.Text);

            con.Open();
            InsertEmployess.ExecuteNonQuery();
            con.Close();
            GridViewEmployees.DataBind(); 
        }

        protected void btnSearchEmployees_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [EmployeeID],[FirstName],[LastName],[Title],[BirthDate],[HireDate],[Address],[City],[Region],[PostalCode],[Country],[HomePhone],[Photo],[Note] FROM [dbo].[TblEmployees] WHERE EmployeeID = " + EmployeesID.SelectedValue; // your id
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {

                EmployeesID.Text = DTable1.Rows[0]["EmployeeID"].ToString();
                txtEmployeesFirstName.Text = DTable1.Rows[0]["FirstName"].ToString();
                txtEmployeesLastName.Text = DTable1.Rows[0]["LastName"].ToString();
                txtEmployeesTitle.Text = DTable1.Rows[0]["Title"].ToString();
                txtEmployeesBirthDate.Text = DTable1.Rows[0]["BirthDate"].ToString();
                txtHireDate.Text = DTable1.Rows[0]["HireDate"].ToString();

                txtEmployeesAddress.Text = DTable1.Rows[0]["Address"].ToString();
                txtEmployessCity.Text = DTable1.Rows[0]["City"].ToString();
                txtEmployeesRegion.Text = DTable1.Rows[0]["Region"].ToString();
                txtEmployeesPostalCode.Text = DTable1.Rows[0]["PostalCode"].ToString();
                txtEmployeesCountry.Text = DTable1.Rows[0]["Country"].ToString();
                txtEmployeesHomePhone.Text = DTable1.Rows[0]["HomePhone"].ToString();
                txtEmployeesNote.Text = DTable1.Rows[0]["Note"].ToString();

                
                EmployeesImage.ImageUrl = DTable1.Rows[0]["Photo"].ToString();
                
            }

            con.Close();
        }

        protected void btnUpdateEmployees_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdateEmployess = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdateEmployess = new SqlCommand("DELETE FROM [dbo].[TblEmployees] WHERE EmployeeID = @EmployeeID", con);

            UpdateEmployess.Parameters.AddWithValue("@EmployeeID", EmployeesID.Text);
            UpdateEmployess.Parameters.AddWithValue("@FirstName", txtEmployeesFirstName.Text);
            UpdateEmployess.Parameters.AddWithValue("@LastName", txtEmployeesLastName.Text);
            UpdateEmployess.Parameters.AddWithValue("@Title", txtEmployeesTitle.Text);
            UpdateEmployess.Parameters.AddWithValue("@BirthDate", txtEmployeesBirthDate.Text);
            UpdateEmployess.Parameters.AddWithValue("@HireDate", txtHireDate.Text);
            UpdateEmployess.Parameters.AddWithValue("@Address", txtEmployeesAddress.Text);
            UpdateEmployess.Parameters.AddWithValue("@City", txtEmployessCity.Text);
            UpdateEmployess.Parameters.AddWithValue("@Region", txtEmployeesRegion.Text);
            UpdateEmployess.Parameters.AddWithValue("@PostalCode", txtEmployeesPostalCode.Text);
            UpdateEmployess.Parameters.AddWithValue("@Country", txtEmployeesCountry.Text);
            UpdateEmployess.Parameters.AddWithValue("@HomePhone", txtEmployeesHomePhone.Text);
            UpdateEmployess.Parameters.AddWithValue("@Note", txtEmployeesNote.Text);

            if (EmployeesUpload.HasFile) 
            {
                EmployeesUpload.SaveAs(Server.MapPath("images/" + EmployeesUpload.FileName));
                EmployeesURL.Text = "images/" + EmployeesUpload.FileName;
                
            }

            EmployeesImage.ImageUrl = EmployeesURL.Text;
            UpdateEmployess.Parameters.AddWithValue("@Photo", EmployeesURL.Text);

            con.Open();
            UpdateEmployess.ExecuteNonQuery();
            con.Close();
            GridViewEmployees.DataBind(); 
        }

        protected void btnDeleteEmployees_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteEmployees = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteEmployees = new SqlCommand("DELETE FROM [dbo].[TblEmployees] WHERE EmployeeID = @EmployeeID", con);

            DeleteEmployees.Parameters.AddWithValue("@EmployeeID", EmployeesID.Text);

            con.Open();
            DeleteEmployees.ExecuteNonQuery();
            con.Close();
            GridViewEmployees.DataBind(); 

            // Clear
            txtEmployeesFirstName.Text = "";
            txtEmployeesLastName.Text = "";
            txtEmployeesTitle.Text = "";
            txtEmployeesBirthDate.Text = "";
            txtHireDate.Text = "";
            txtEmployeesBirthDate.Text = "";
            txtHireDate.Text = "";
            txtEmployeesAddress.Text = "";
            txtEmployessCity.Text = "";
            txtEmployeesRegion.Text = "";
            txtEmployeesPostalCode.Text = "";
            txtEmployeesCountry.Text = "";
            txtEmployeesHomePhone.Text = "";
            txtEmployeesNote.Text = "";
        }

        protected void btnClearEmployees_Click(object sender, EventArgs e)
        {
            txtEmployeesFirstName.Text = "";
            txtEmployeesLastName.Text = "";
            txtEmployeesTitle.Text = "";
            txtEmployeesBirthDate.Text = "";
            txtHireDate.Text = "";
            txtEmployeesBirthDate.Text = "";
            txtHireDate.Text = "";
            txtEmployeesAddress.Text = "";
            txtEmployessCity.Text = "";
            txtEmployeesRegion.Text = "";
            txtEmployeesPostalCode.Text = "";
            txtEmployeesCountry.Text = "";
            txtEmployeesHomePhone.Text = "";
            txtEmployeesNote.Text = "";
            EmployeesImage.ImageUrl = "";
        }
    }
}