﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Ads.aspx.cs" Inherits="CarpowerWebApplication.WebFormTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2 class="text-right">Form ads</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-4" style="width: 55%">
                    <asp:Label runat="server" Text="Ads id"></asp:Label>
                    <%-- <asp:TextBox ID="AdsId" runat="server" CssClass="form-control"></asp:TextBox>--%>
                    <asp:DropDownList ID="DropDownListAds" runat="server" CssClass="form-control" DataSourceID="SqlDataSourceListDropdrownAds" DataTextField="ads_name" DataValueField="ads_id"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceListDropdrownAds" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [ads_name], [ads_id], [ads_imag] FROM [Ads]"></asp:SqlDataSource>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-4" style="width: 55%">
                    <asp:Label runat="server" Text="Ads title"></asp:Label>
                    <asp:TextBox ID="AdsTitle" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div>
            <div class="row">
                <div class="col-6">
                    <div class="mb-4" style="width: 55%">
                        <asp:Image ID="Ads" runat="server" Height="221px" Width="323px" />
                        <br />
                        <br />
                        <asp:TextBox ID="AdsURL" runat="server" CssClass="form-control"></asp:TextBox>
                        <br />
                        <asp:FileUpload ID="UploadAds" runat="server" />
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-4" style="width: 55%"></div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-6">
                <asp:Button ID="btnInsertAds" runat="server" CssClass="btn btn-outline-primary" Text="Insert" OnClick="btnInsertAds_Click" />
                <asp:Button ID="btnSearchAds" runat="server" CssClass="btn btn-outline-secondary" Text="Search" OnClick="btnSearchAds_Click" />
                <asp:Button ID="btnUpdateAds" runat="server" CssClass="btn btn-outline-success" Text="Update" OnClick="btnUpdateAds_Click" />
                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-outline-danger" Text="Delete" OnClick="btnDelete_Click" />
                <asp:Button ID="btnClearsAds" runat="server" CssClass="btn btn-outline-warning" Text="Clear" OnClick="btnClearsAds_Click" />
            </div>
            <div class="col-6"></div>
        </div>
        <br />
        <div class="table-responsive">
            <div class="container">
                <asp:GridView ID="ListData" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ads_id" DataSourceID="SqlDataSourceListData" CssClass="table table-striped table-hover">
                    <Columns>
                        <asp:BoundField DataField="ads_id" HeaderText="ads_id" InsertVisible="False" ReadOnly="True" SortExpression="ads_id" />
                        <asp:BoundField DataField="ads_name" HeaderText="ads_name" SortExpression="ads_name" />
                        <asp:BoundField DataField="ads_imag" HeaderText="ads_imag" SortExpression="ads_imag" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListData" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [ads_id], [ads_name], [ads_imag] FROM [Ads]"></asp:SqlDataSource>
            </div>
        </div>
    </div>
    <script>
        var AdsTitle = document.getElementById("<%= AdsTitle.ClientID %>").value;
        if (AdsTitle == "") {
            Swal.fire({
                title: 'Error!',
                text: 'Please enter your company name',
                icon: 'error',
                confirmButtonText: 'OK'
            })
            return false;
        }
    </script>
</asp:Content>
