﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertCategor_Click(object sender, EventArgs e)
        {
            
        }

        protected void btnInsertCategor_Click1(object sender, EventArgs e)
        {

        }

        protected void btnInsertCategor_Click2(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertCategory = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertCategory = new SqlCommand("INSERT INTO [dbo].[TblCategory] ([CategoryName],[Description],[Picture])VALUES(@CategoryName,@Descriptio,@Picture)", con);

            InsertCategory.Parameters.AddWithValue("@CategoryName", txtCategoryName.Text);
            InsertCategory.Parameters.AddWithValue("@Descriptio", txtCategoryDescription.Text);


            if (FileUploadPictureCategory.HasFile)
            {
                FileUploadPictureCategory.SaveAs(Server.MapPath("images/" + FileUploadPictureCategory.FileName));
                TextBoxPictureURL.Text = "images/" + FileUploadPictureCategory.FileName;
            }

            CategoryImage.ImageUrl = TextBoxPictureURL.Text;
           

            InsertCategory.Parameters.AddWithValue("@Picture", TextBoxPictureURL.Text);

            con.Open();
            InsertCategory.ExecuteNonQuery();
            con.Close();

            GridViewFormCategory.DataBind();
        }

        protected void btnSearchCategory_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [CategoryID],[CategoryName],[Description],[Picture] FROM [dbo].[TblCategory] WHERE CategoryID = " + DropDownListCategory.SelectedValue;
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {

                DropDownListCategory.SelectedValue = DTable1.Rows[0]["CategoryID"].ToString();
                txtCategoryName.Text = DTable1.Rows[0]["CategoryName"].ToString();
                txtCategoryDescription.Text = DTable1.Rows[0]["Description"].ToString();
                CategoryImage.ImageUrl = DTable1.Rows[0]["Picture"].ToString();
                // id
            } else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No data is returned');", true);
            }

            con.Close();
        }

        protected void btnUpdateCategor_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand updateCategory = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            updateCategory = new SqlCommand("UPDATE [dbo].[TblCategory] SET [CategoryName] = @CategoryName, [Description] = @Description, [Picture] = @Picture WHERE CategoryID = @CategoryID", con);

            updateCategory.Parameters.AddWithValue("@CategoryID", DropDownListCategory.SelectedValue);
            updateCategory.Parameters.AddWithValue("@CategoryName", txtCategoryName.Text);
            updateCategory.Parameters.AddWithValue("@Description", txtCategoryDescription.Text);
            //updateCategory.Parameters.AddWithValue("@Picture", txtCategoryDescription.Text);
            


            if (FileUploadPictureCategory.HasFile)
            {
                FileUploadPictureCategory.SaveAs(Server.MapPath("images/" + FileUploadPictureCategory.FileName));
                TextBoxPictureURL.Text = "images/" + FileUploadPictureCategory.FileName;
            }

            CategoryImage.ImageUrl = TextBoxPictureURL.Text;


            updateCategory.Parameters.AddWithValue("@Picture", TextBoxPictureURL.Text);

            con.Open();
            updateCategory.ExecuteNonQuery();
            con.Close();

            GridViewFormCategory.DataBind();
        }

        protected void btnDeleteCategor_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteCategory = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteCategory = new SqlCommand("DELETE FROM [dbo].[TblCategory] WHERE CategoryID = @CategoryID", con);

            DeleteCategory.Parameters.AddWithValue("@CategoryID", DropDownListCategory.SelectedValue);

            con.Open();
            DeleteCategory.ExecuteNonQuery();
            con.Close();

            // Clear
            DropDownListCategory.Focus();
            txtCategoryName.Text = "";
            txtCategoryDescription.Text = "";

            GridViewFormCategory.DataBind();
        }

        protected void btnClearCategory_Click(object sender, EventArgs e)
        {
           
            txtCategoryName.Text = "";
            txtCategoryDescription.Text = "";
            CategoryImage.ImageUrl = "";
            DropDownListCategory.Focus();
        }
    }
}