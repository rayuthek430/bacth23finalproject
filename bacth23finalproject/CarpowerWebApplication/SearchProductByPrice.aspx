﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="SearchProductByPrice.aspx.cs" Inherits="CarpowerWebApplication.SearchProductByPrice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="row">
        <div class="col-lg-6">
            <asp:TextBox ID="TxtSearchProductByPrice" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="col-lg-6">
            <asp:Button ID="BtnSearchProductByPrice" runat="server" Text="ស្វែវរក" CssClass="btn btn-outline-primary" />
        </div>
    </div>

</asp:Content>
