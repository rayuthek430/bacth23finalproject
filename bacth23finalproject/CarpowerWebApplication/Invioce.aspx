﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="Invioce.aspx.cs" Inherits="CarpowerWebApplication.Invioce" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .myContent {
            margin-left: 180px;
            background-color: #e6e3e3;
        }

        .table-hover {
            width: 95%;
            text-align: center;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <asp:DataList ID="DataListInvoiceOrder" runat="server" DataKeyField="OrderID" DataSourceID="SqlDataSourceInvoiceOrder" CssClass="myContent">
            <ItemTemplate>

                <div class="row">
                    <div class="col-lg-12">
                        <img src="images/Banner invoice noBG.png" style="width: 865px" />
                    </div>
                </div>
                <br />
                <div class="row" style="margin-left: 58px">
                    <div class="col-lg-12">
                        <h6>អាសយដ្ឋាន ឃ្លាំង 203 ផ្លូវជាតិលេខ៣ ,ព្រៃព្រីងខាងត្បូង, ចោមចៅ3, ពោធិ៍សែនជ័យ, រាជធានីភ្នំពេញ</h6>
                    </div>
                </div>
                <div class="row" style="margin-left: 58px">
                    <div class="col-lg-4">
                        <h6>Tel:</h6>
                        <h6>012 85 26 85</h6>
                        <h6>088 918 3168</h6>
                        <h6>016 866 789</h6>
                    </div>
                    <div class="col-lg-4" style="margin-top: 70px">
                        <h5>វិក្កយបត្-លក់-រថយន្ត
                        </h5>
                    </div>
                    <div class="col-lg-4" style="margin-top: 40px">
                        <h6>Date :
            <asp:Label ID="Label1" runat="server" Text='<%# Eval("OrderDate") %>' /></h6>
                    </div>
                </div>
                <br />

                <div class="row" style="margin-left: 58px; display:none;">
                    <div class="col-lg-12">
                        OrderID:
            <asp:Label ID="OrderIDLabel" runat="server" Text='<%# Eval("OrderID") %>' />
                        <br />
                    </div>
                </div>
                <div class="row" style="margin-left: 58px">
                    <div class="col-lg-12">
                        ឈ្មោះ​:
            <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("FullName") %>' />
                        <br />
                    </div>
                </div>
                <div class="row" style="margin-left: 58px">
                    <div class="col-lg-12">
                        លេខទូរសព្:
            <asp:Label ID="PhoneNumberLabel" runat="server" Text='<%# Eval("PhoneNumber") %>' />
                        <br />
                    </div>
                </div>
                <div class="row" style="margin-left: 58px">
                    <div class="col-lg-12">
                        កាល​បរិច្ឆេទ​បញ្ជា​ទិញ:
            <asp:Label ID="OrderDateLabel" runat="server" Text='<%# Eval("OrderDate") %>' />
                        <br />
                    </div>
                </div>
                <div class="row" style="margin-left: 58px; display:none">
                    <div class="col-lg-12">
                        Ordered:
            <asp:Label ID="OrderedLabel" runat="server" Text='<%# Eval("Ordered") %>' />
                        <br />
                    </div>
                </div>


                <br />
                <br />
                <asp:GridView CssClass="table table-hover" ID="GridViewInvoiceOrderDetail" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceInvoiceOrderDetail">
                    <Columns>
                        <asp:BoundField DataField="ProductID" HeaderText="លេខសម្គាល់" SortExpression="ProductID" />
                        <asp:TemplateField HeaderText="រូបភាព" SortExpression="image">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("image") %>' Width="64px" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ProductName" HeaderText="ឈ្មោះ" SortExpression="ProductName" />
                        <asp:BoundField DataField="UnitPrice" HeaderText="តម្លៃ" SortExpression="UnitPrice" />
                        <asp:BoundField DataField="Quantity" HeaderText="ចំនួន" SortExpression="Quantity" />
                        <asp:BoundField DataField="Amount" HeaderText="ចំនួនទឹកប្រាក់" SortExpression="Amount" />
                    </Columns>
                </asp:GridView>
                <div class="row" style="margin-left: 58px">
                    <div class="col-lg-12">
                        ចំនួនទឹកប្រាក់សរុប:
                        <asp:Label ID="AmountLabel" runat="server" Text='<%# Eval("Amount") %>' />
                    </div>
                </div>
                <br />
                <asp:SqlDataSource ID="SqlDataSourceInvoiceOrderDetail" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_InvoiceOrderDetail" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="OrderIDLabel" Name="OrderID" PropertyName="Text" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br />
                <br />
            </ItemTemplate>
        </asp:DataList>

        <asp:SqlDataSource ID="SqlDataSourceInvoiceOrder" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_InvoiceOrder" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:QueryStringParameter Name="OrderID" QueryStringField="OrderID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>
