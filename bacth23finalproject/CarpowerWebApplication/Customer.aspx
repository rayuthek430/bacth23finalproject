﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Customer.aspx.cs" Inherits="CarpowerWebApplication.WebFormCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .applyFont{
            font-weight:bold;
            font-size:17px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2>Form Customer</h2>
        <div class="row">
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label1" CssClass="applyFont" runat="server" Text="Customer id:"></asp:Label>
                    <asp:TextBox ID="txtCustomerID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label2" CssClass="applyFont" runat="server" Text="Company name:"></asp:Label>
                    <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label3" CssClass="applyFont" runat="server" Text="Contact name:"></asp:Label>
                    <asp:TextBox ID="txtContactName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label4" CssClass="applyFont" runat="server" Text="Contact title:"></asp:Label>
                    <asp:TextBox ID="txtCustomerContactTitle" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label5" CssClass="applyFont" runat="server" Text="Address:"></asp:Label>
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label6" CssClass="applyFont" runat="server" Text="City:"></asp:Label>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label7" CssClass="applyFont" runat="server" Text="Region: "></asp:Label>
                    <asp:TextBox ID="txtRegion" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label8" CssClass="applyFont" runat="server" Text="PostalCode:"></asp:Label>
                    <asp:TextBox ID="txtPostalCode" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Labe9" CssClass="applyFont" runat="server" Text="Country:"></asp:Label>
                    <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label9" CssClass="applyFont" runat="server" Text="Phone:"></asp:Label>
                    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label10" CssClass="applyFont" runat="server" Text="Telegram:"></asp:Label>
                    <asp:TextBox ID="txtTelegram" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label11" CssClass="applyFont" runat="server" Text="Fax:"></asp:Label>
                    <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        

        <div class="row">
            <div class="col-6">
                <%--Button--%>
                <asp:Button ID="btnInsertCustomer" runat="server" CssClass="btn btn-primary" Text="Insert" OnClick="btnInsertCustomer_Click" />
                <asp:Button ID="btnUpdateCustomer" runat="server" CssClass="btn btn-success" Text="Update" OnClick="btnUpdateCustomer_Click" />
                <asp:Button ID="btnDeleteCustomer" runat="server" CssClass="btn btn-danger" Text="Delete" OnClick="btnDeleteCustomer_Click" />
                <asp:Button ID="btnClearCustomer" runat="server" CssClass="btn btn-warning" Text="Clear" OnClick="btnClearCustomer_Click" />
                <asp:Button ID="btnSearchCustomer" runat="server" CssClass="btn btn-secondary" Text="Search" OnClick="btnSearchCustomer_Click" />
            </div>
            <div class="col-6"></div>
        </div>
        <br />
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridView1" CssClass="table table-striped table-hover" runat="server" AutoGenerateColumns="False" DataKeyNames="CustomerID" DataSourceID="SqlDataSourceListGridViewCustomer" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="CustomerID" HeaderText="CustomerID" InsertVisible="False" ReadOnly="True" SortExpression="CustomerID" />
                        <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
                        <asp:BoundField DataField="ContactName" HeaderText="ContactName" SortExpression="ContactName" />
                        <asp:BoundField DataField="ContactTitle" HeaderText="ContactTitle" SortExpression="ContactTitle" />
                        <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                        <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                        <asp:BoundField DataField="Regior" HeaderText="Regior" SortExpression="Regior" />
                        <asp:BoundField DataField="PostalCode" HeaderText="PostalCode" SortExpression="PostalCode" />
                        <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
                        <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                        <asp:BoundField DataField="Telegram" HeaderText="Telegram" SortExpression="Telegram" />
                        <asp:BoundField DataField="Fax" HeaderText="Fax" SortExpression="Fax" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListGridViewCustomer" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [CustomerID], [CompanyName], [ContactName], [ContactTitle], [Address], [City], [Regior], [PostalCode], [Country], [Phone], [Telegram], [Fax] FROM [TblCustomer]"></asp:SqlDataSource>
            </div>
        </div>


<%--         <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalToggleLabel">Modal 1</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Show a second modal and hide this one with the button below.
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" data-bs-target="#exampleModalToggle2" data-bs-toggle="modal" data-bs-dismiss="modal">Open second modal</button>
                    </div>
                </div>
            </div>
        </div>
       
        <a class="btn btn-primary" data-bs-toggle="modal" href="#exampleModalToggle" role="button">Open first modal</a>--%>

    </div>
</asp:Content>
