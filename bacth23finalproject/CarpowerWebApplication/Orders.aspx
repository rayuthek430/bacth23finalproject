﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="CarpowerWebApplication.WebFormOrders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .applyFont{
            font-weight:bold;
            font-size:17px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2>Form order</h2>
        <div class="row">
            <div class="col-4">
                <asp:Label ID="Label1" CssClass="applyFont" runat="server" Text="Order id"></asp:Label>
                <asp:TextBox ID="txtOrdersID" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-4">
                <asp:Label ID="Label2" CssClass="applyFont" runat="server" Text="Customer id"></asp:Label>
                <asp:TextBox ID="txtCustomerID" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-4">
                <asp:Label ID="Label3" CssClass="applyFont" runat="server" Text="Employee id"></asp:Label>
                <asp:TextBox ID="txtEmployeeID" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <asp:Label ID="Label4" CssClass="applyFont" runat="server" Text="Order date"></asp:Label>
                <asp:TextBox ID="txtOrderDate" runat="server" required="required" TextMode="Date" Enabled="True" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-4">
                <asp:Label ID="Label5" CssClass="applyFont" runat="server" Text="Required date:"></asp:Label>
                <asp:TextBox ID="txtRequiredDate" runat="server" CssClass="form-control" TextMode="Date" Enabled="True"></asp:TextBox>
            </div>
            <div class="col-4">
                <asp:Label ID="Label6" CssClass="applyFont" runat="server" Text="Shipped date"></asp:Label>
                <asp:TextBox ID="txtShippedDate" runat="server" CssClass="form-control" TextMode="Date" Enabled="True"></asp:TextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <asp:Label ID="Label7" CssClass="applyFont" runat="server" Text="Shipper id"></asp:Label>
                <asp:TextBox ID="txtShipperID" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-4">
                <asp:Label ID="Label8" CssClass="applyFont" runat="server" Text="Freight"></asp:Label>
                <asp:TextBox ID="txtFreight" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-4">
                <asp:Label ID="Label9" CssClass="applyFont" runat="server" Text="Shipp name"></asp:Label>
                <asp:TextBox ID="txtShippName" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <asp:Label ID="Label10" CssClass="applyFont" runat="server" Text="Shipp address"></asp:Label>
                <asp:TextBox ID="txtShippAdress" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-4">
                <asp:Label ID="Label11" CssClass="applyFont" runat="server" Text="Shipp city"></asp:Label>
                <asp:TextBox ID="txtShippCity" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-4">
                <asp:Label ID="Label12" CssClass="applyFont" runat="server" Text="Shipp region"></asp:Label>
                <asp:TextBox ID="txtShippRegion" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <asp:Label ID="Label13" CssClass="applyFont" runat="server" Text="Shipp postal code"></asp:Label>
                <asp:TextBox ID="txtPostalCode" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-4">
                <asp:Label ID="Label14" CssClass="applyFont" runat="server" Text="Shipp country"></asp:Label>
                <asp:TextBox ID="txtShipCountry" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-4">
            </div>
        </div>
        <br />

        <div class="row">
            <div class="col-6">
                <%--Button--%>
                <asp:Button ID="btnInsertShippers" runat="server" CssClass="btn btn-primary" Text="Insert" OnClick="btnInsertShippers_Click" />
                <asp:Button ID="btnUpdateShippers" runat="server" CssClass="btn btn-info" Text="Update" OnClick="btnUpdateShippers_Click" />
                <asp:Button ID="btnDeleteShippers" runat="server" CssClass="btn btn-danger" Text="Delete" OnClick="btnDeleteShippers_Click" />
                <asp:Button ID="btnSearchShippers" runat="server" CssClass="btn btn-secondary" Text="Search" OnClick="btnSearchShippers_Click" />
                <asp:Button ID="btnClearShippers" runat="server" CssClass="btn btn-warning" Text="Clear" OnClick="btnClearShippers_Click" />
            </div>
            <div class="col-6"></div>
        </div>

        <br />
         <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewOrder" CssClass="table table-striped table-hover" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="OrderID" DataSourceID="SqlDataSourceListGridOrders">
                    <Columns>
                        <asp:BoundField DataField="OrderID" HeaderText="OrderID" InsertVisible="False" ReadOnly="True" SortExpression="OrderID" />
                        <asp:BoundField DataField="CustomerID" HeaderText="CustomerID" SortExpression="CustomerID" />
                        <asp:BoundField DataField="EmployeeID" HeaderText="EmployeeID" SortExpression="EmployeeID" />
                        <asp:BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate" />
                        <asp:BoundField DataField="RequiredDate" HeaderText="RequiredDate" SortExpression="RequiredDate" />
                        <asp:BoundField DataField="ShippedDate" HeaderText="ShippedDate" SortExpression="ShippedDate" />
                        <asp:BoundField DataField="ShpperID" HeaderText="ShpperID" SortExpression="ShpperID" />
                        <asp:BoundField DataField="Freight" HeaderText="Freight" SortExpression="Freight" />
                        <asp:BoundField DataField="ShipName" HeaderText="ShipName" SortExpression="ShipName" />
                        <asp:BoundField DataField="ShipAddress" HeaderText="ShipAddress" SortExpression="ShipAddress" />
                        <asp:BoundField DataField="ShipCity" HeaderText="ShipCity" SortExpression="ShipCity" />
                        <asp:BoundField DataField="ShipRegion" HeaderText="ShipRegion" SortExpression="ShipRegion" />
                        <asp:BoundField DataField="ShipPostalCode" HeaderText="ShipPostalCode" SortExpression="ShipPostalCode" />
                        <asp:BoundField DataField="ShipCountry" HeaderText="ShipCountry" SortExpression="ShipCountry" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListGridOrders" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [OrderID], [CustomerID], [EmployeeID], [OrderDate], [RequiredDate], [ShippedDate], [ShpperID], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry] FROM [TblOrders]"></asp:SqlDataSource>
            </div>
            
        </div>

    </div>

</asp:Content>
