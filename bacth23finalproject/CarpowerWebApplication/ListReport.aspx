﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="ListReport.aspx.cs" Inherits="CarpowerWebApplication.ListReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .card-title {
            font-size: medium;
            font-family: 'Khmer OS Siemreap';
        }

        .btn-primary {
            font-size: medium;
            font-family: 'Khmer OS Siemreap';
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <h2 style="width: 100%; height: 31px; font-family: 'Khmer OS Siemreap'">របាយការណ៍</h2>
    <br />
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">របាយការណ៍ លក់ប្រចាំថ្ងៃ</h5>

                        <br />
                        <a class="btn btn-primary" href="ReportDaily.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">របាយការណ៍ បញ្ជីឈ្មោះឡាន តាមថ្ងៃ</h5>
                        <br />
                        <a class="btn btn-primary" href="Report_DailyListProduct.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">	របាយការណ៍ តម្លៃផលិតផល</h5>
                        <br />
                        <a class="btn btn-primary" href="Report_ListProduct.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">	របាយការណ៍ ផលិតផលតាមប្រភេទ</h5>
                        <br />
                        <a class="btn btn-primary" href="Report_ListProductByCategory.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
        </div>
        <br />
        <div class="row">
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">របាយការណ៍ អ្នកផ្គត់ផ្គង់​</h5>
                        <br />
                        <a class="btn btn-primary" href="Report_ListProductBySupplier.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">របាយការណ៍ ផលិតផលនៅសល់តិចជាង ៥</h5>
                        <br />
                        <a class="btn btn-primary" href="Report_ListProductInStock.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">របាយការណ៍ Quotation​</h5>
                        <br />
                        <a class="btn btn-primary" href="ReportQuotation.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">របាយការណ៍នៃការបញ្ជាទិញ</h5>
                        <br />
                        <a class="btn btn-primary" href="ReportOrderStatus.aspx">បង្ហាញ</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
