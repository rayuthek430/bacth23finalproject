﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="ClientRegister.aspx.cs" Inherits="CarpowerWebApplication.ClientRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
        }

        .containers {
            max-width: 684px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
            background-color: #fff;
            box-shadow: 0 2px 5px rgba(0,0,0,0.1);
        }

        h1 {
            font-size: 24px;
            margin-bottom: 20px;
            color: #333;
            text-align: center;
        }

        .form-label {
            font-weight: bold;
            color: #333;
        }

        .form-control {
            width: 100%;
            padding: 10px;
            margin-bottom: 15px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        .btn {
            background-color: rgb(63, 149, 248);
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-weight: bold;
            text-transform: uppercase;
            letter-spacing: 1px;
            transition: background-color 0.3s ease;
        }

            .btn:hover {
                background-color: #45a049;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".box-menu").addClass("d-none");
            $('[id$=TextGetPhoneNumber]').on('keyup', function (e) {
                input = $(e.target);
                let value = input.val();
                value = value.replace(/\D/g, '');
                input.val(value);
            });
        })

    </script>
    <br />
    <div class="containers" style="margin-right: 450px">
        <h1>ចុះឈ្មោះដើម្បីទិញរថយន្តរបស់អ្នក</h1>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label for="fullName" class="form-label">ឈ្មោះ</label>
                    <asp:TextBox ID="TxtGetUserName" CssClass="form-control" runat="server" placeholder="ឈ្មោះ"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label class="form-label">អ៊ីមែល</label>
                    <asp:TextBox ID="TextGetEmail" CssClass="form-control" runat="server" placeholder="អ៊ីមែល"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label for="email" class="form-label">អាសយដ្ឋាន</label>
                    <asp:TextBox ID="TextGetAddress" CssClass="form-control" runat="server" placeholder="អាសយដ្ឋាន"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label class="form-label">លេខទូរសព្</label>
                    <asp:TextBox ID="TextGetPhoneNumber" name="pass" CssClass="form-control" runat="server" placeholder="លេខទូរសព្"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label for="password" class="form-label">ពាក្យសម្ងាត់</label>
                    <asp:TextBox ID="TxtGetUserPassword" CssClass="form-control" runat="server" placeholder="ពាក្យសម្ងាត់"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="confirmPassword" class="form-label">បញ្ជាក់ពាក្យសម្ងាត់</label>
                    <input class="form-control" id="confirmPassword" placeholder="បញ្ជាក់ពាក្យសម្ងាត់របស់អ្នក">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label for="password" class="form-label">អាយុ</label>
                    <asp:TextBox ID="txtGender" CssClass="form-control" runat="server" placeholder="អាយុ"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <label for="genderDropdown" class="form-label">ភេទ</label>
                <asp:DropDownList ID="genderDropdown" runat="server" CssClass="form-control" aria-label=".form-select-lg example">
                    <asp:ListItem Value="ប្រុស">ប្រុស</asp:ListItem>
                    <asp:ListItem Value="ស្រី">ស្រី</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label for="password" class="form-label">អត្តសញ្ញាណប័ណ្</label>
                    <asp:TextBox ID="txtIDCard" CssClass="form-control" runat="server" placeholder="អត្តសញ្ញាណប័ណ្"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="password" class="form-label">លិខិតឆ្លងដែន</label>
                    <asp:TextBox ID="txtPasSport" CssClass="form-control" runat="server" placeholder="លិខិតឆ្លងដែន"></asp:TextBox>
                </div>
            </div>
        </div>

        <asp:Button ID="BtnRegister" runat="server" CssClass="btn btn-primary" Text="ចុះឈ្មោះ​ឥឡូវនេះ" OnClick="BtnRegister_Click" />
        <asp:Button ID="BtnSampleData" runat="server" Text="Sample Data" CssClass="btn btn-primary"  OnClick="BtnSampleData_Click" />
    </div>
</asp:Content>
