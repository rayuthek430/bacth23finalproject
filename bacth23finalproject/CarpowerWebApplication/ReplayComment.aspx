﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="ReplayComment.aspx.cs" Inherits="CarpowerWebApplication.ReplayComment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2>តប comment</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label1" runat="server" Text="Reply id"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtReplyID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label2" runat="server" Text="Reply"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtReply" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label3" runat="server" Text="Comment id"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtCommentID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label4" runat="server" Text="User id"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtUserID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label5" runat="server" Text="Reply date"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtReplyDate" runat="server" required="required" TextMode="Date" Enabled="True" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <asp:Button ID="BtnInserComment" runat="server" CssClass="btn btn-outline-primary" Text="Insert" OnClick="BtnInserComment_Click" />
                    <asp:Button ID="BtnSearchComment" runat="server" CssClass="btn btn-outline-secondary" Text="Search" OnClick="BtnSearchComment_Click" />
                    <asp:Button ID="BtnUpdateComment" runat="server" CssClass="btn btn-outline-success" Text="Update" OnClick="BtnUpdateComment_Click" Width="84px" />
                    <asp:Button ID="BtnDeleteComment" runat="server" CssClass="btn btn-outline-danger" Text="Delete" OnClick="BtnDeleteComment_Click" />
                    <asp:Button ID="BtnClearComment" runat="server" CssClass="btn btn-outline-warning" Text="Clear" OnClick="BtnClearComment_Click" />
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewReplyComment" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ReplyID" DataSourceID="SqlDataSourceGridViewReplyComment" CssClass="table table-striped table-hover">
                    <Columns>
                        <asp:BoundField DataField="ReplyID" HeaderText="ReplyID" InsertVisible="False" ReadOnly="True" SortExpression="ReplyID" />
                        <asp:BoundField DataField="Reply" HeaderText="Reply" SortExpression="Reply" />
                        <asp:BoundField DataField="CommentID" HeaderText="CommentID" SortExpression="CommentID" />
                        <asp:BoundField DataField="UserID" HeaderText="UserID" SortExpression="UserID" />
                        <asp:BoundField DataField="ReplyDate" HeaderText="ReplyDate" SortExpression="ReplyDate" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceGridViewReplyComment" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [ReplyID], [Reply], [CommentID], [UserID], [ReplyDate] FROM [TblReply]"></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
