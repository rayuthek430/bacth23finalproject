﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocumentFormat.OpenXml.Spreadsheet;

namespace CarpowerWebApplication
{
    public partial class UserControl : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSerarchUserControl_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [UserID],[FullName],[Password],[RoleID],[StatusID],[PhoneNumber],[Email],[Address],[Image],[Gender],[Age],[Position],[IdCard],[Passport]FROM [dbo].[TblUser] WHERE UserID = " + txtUserID.Text;
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {
                txtUserID.Text = DTable1.Rows[0]["UserID"].ToString();
                UserGender.Text = DTable1.Rows[0]["Gender"].ToString();
                UserName.Text = DTable1.Rows[0]["FullName"].ToString();
                PhoneNumber.Text = DTable1.Rows[0]["PhoneNumber"].ToString();
                Address.Text = DTable1.Rows[0]["Address"].ToString();

                UserAge.Text = DTable1.Rows[0]["Age"].ToString();
                //UserPosition.Text = DTable1.Rows[0]["Position"].ToString();
                UserPassport.Text = DTable1.Rows[0]["Passport"].ToString();
                UserEmail.Text = DTable1.Rows[0]["Email"].ToString();
                UserPassword.Text = DTable1.Rows[0]["Passport"].ToString();

                UserIDCard.Text = DTable1.Rows[0]["IdCard"].ToString();
            }

            con.Close();
        }

        protected void ButtonDeleteUserControl_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteProduct = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteProduct = new SqlCommand("DELETE FROM [dbo].[TblUser] WHERE UserID = @UserID", con);

            DeleteProduct.Parameters.AddWithValue("@UserID", txtUserID.Text);

            con.Open();
            DeleteProduct.ExecuteNonQuery();
            con.Close();
            GridViewListUser.DataBind();

            // Clear
            txtUserID.Text = "";
            UserGender.Text = "";
            UserName.Text = "";
            PhoneNumber.Text = "";
            UserIDCard.Text = "";
            UserAge.Text = "";
            //UserPosition.Text = "";
            UserPassport.Text = "";
            UserEmail.Text = "";
            Address.Text = "";
            UserPassword.Text = "";
        }

        protected void ButtonInsertUserControl_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand CreateUser = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            CreateUser = new SqlCommand("INSERT INTO [dbo].[TblUser] ([FullName],[Password] ,[RoleID],[StatusID],[PhoneNumber],[Email],[Address],[Image],[Gender],[Age],[Position],[IdCard],[Passport])VALUES (@FullName,@Password,@RoleID,@StatusID,@PhoneNumber,@Email,@Address,@Image,@Gender,@Age,@Position,@IdCard,@Passport)", con);

            CreateUser.Parameters.AddWithValue("@FullName", UserName.Text);
            CreateUser.Parameters.AddWithValue("@Password", UserPassword.Text);

            CreateUser.Parameters.AddWithValue("@RoleID", 2);
            CreateUser.Parameters.AddWithValue("@StatusID", 1);

            CreateUser.Parameters.AddWithValue("@PhoneNumber", PhoneNumber.Text);
            CreateUser.Parameters.AddWithValue("@Email", UserEmail.Text);

            CreateUser.Parameters.AddWithValue("@Address", Address.Text);
            CreateUser.Parameters.AddWithValue("@Image", DBNull.Value); // null

            CreateUser.Parameters.AddWithValue("@Age", UserAge.Text);
            CreateUser.Parameters.AddWithValue("@IdCard", UserIDCard.Text);
            CreateUser.Parameters.AddWithValue("@Passport", UserPassport.Text);

            CreateUser.Parameters.AddWithValue("@Position", "អ្នកទិញ");

            // Get the selected value from the DropDownList
            string selectedGender = UserGender.SelectedValue;
            CreateUser.Parameters.AddWithValue("@Gender", selectedGender);


            con.Open();
            CreateUser.ExecuteNonQuery();
            con.Close();
            GridViewListUser.DataBind();
        }

        protected void ButtonSearchUserControl_Click(object sender, EventArgs e)
        {
            // Clear
            txtUserID.Text = "";
            UserGender.Text = "";
            UserName.Text = "";
            PhoneNumber.Text = "";
            UserIDCard.Text = "";
            UserAge.Text = "";
            //UserPosition.Text = "";
            UserPassport.Text = "";
            UserEmail.Text = "";
            Address.Text = "";
            UserPassword.Text = "";
        }

        protected void ButtonUpdateUserControl_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdateUser = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdateUser = new SqlCommand("UPDATE [dbo].[TblUser] SET [FullName] = @FullName,[Password] = @Password,[RoleID] = @RoleID,[StatusID] = @StatusID,[PhoneNumber] = @PhoneNumber,[Email] = @Email,[Address] = @Address,[Image] = @Image,[Gender] = @Gender,[Age] = @Age,[Position] = @Position,[IdCard] = @IdCard,[Passport] = @Passport WHERE UserID = @UserID", con);

            UpdateUser.Parameters.AddWithValue("@UserID", txtUserID.Text);

            UpdateUser.Parameters.AddWithValue("@FullName", UserName.Text);
            UpdateUser.Parameters.AddWithValue("@Password", UserPassword.Text);
            UpdateUser.Parameters.AddWithValue("@RoleID", DropDownChooesRole.SelectedValue);
            UpdateUser.Parameters.AddWithValue("@StatusID", 1);
            UpdateUser.Parameters.AddWithValue("@PhoneNumber", PhoneNumber.Text);
            UpdateUser.Parameters.AddWithValue("@Email", UserEmail.Text);
            UpdateUser.Parameters.AddWithValue("@Address", Address.Text);
            UpdateUser.Parameters.AddWithValue("@Image", DBNull.Value); // null
            string selectedGender = UserGender.SelectedValue;
            UpdateUser.Parameters.AddWithValue("@Gender", selectedGender); 
            UpdateUser.Parameters.AddWithValue("@Age", UserAge.Text);
            UpdateUser.Parameters.AddWithValue("@Position", "អ្នកទិញ");
            UpdateUser.Parameters.AddWithValue("@IdCard", UserIDCard.Text);
            UpdateUser.Parameters.AddWithValue("@Passport", UserPassport.Text);
            con.Open();
            UpdateUser.ExecuteNonQuery();
            con.Close();
            GridViewListUser.DataBind();

            // Clear
            txtUserID.Text = "";
            UserGender.Text = "";
            UserName.Text = "";
            PhoneNumber.Text = "";
            UserIDCard.Text = "";
            UserAge.Text = "";
            UserPassport.Text = "";
            UserEmail.Text = "";
            Address.Text = "";
            UserPassword.Text = "";

        }

        protected void BtnSampleData_Click(object sender, EventArgs e)
        {
           
            UserGender.Text = "ប្រុស";
            UserName.Text = "NewUser1";
            PhoneNumber.Text = "098765432";
            UserIDCard.Text = "N091772277";
            UserAge.Text = "29";
            UserPassport.Text = "N009123456";
            UserEmail.Text = "NewUser1@gmail.com";
            Address.Text = "PP";
            UserPassword.Text = "123456";
        }
    }
}