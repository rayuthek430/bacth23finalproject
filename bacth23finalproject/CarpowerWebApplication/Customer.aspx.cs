﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertCustomer_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertCustomer = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertCustomer = new SqlCommand("INSERT INTO [dbo].[TblCustomer] ([CompanyName],[ContactName],[ContactTitle],[Address],[City],[Regior],[PostalCode],[Country],[Phone],[Telegram],[Fax]) VALUES (@CompanyName,@ContactName,@ContactTitle,@Address,@City,@Regior,@PostalCode,@Country,@Phone,@Telegram,@Fax)", con);

            InsertCustomer.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text);
            InsertCustomer.Parameters.AddWithValue("@ContactName", txtContactName.Text);
            InsertCustomer.Parameters.AddWithValue("@ContactTitle", txtCustomerContactTitle.Text);
            InsertCustomer.Parameters.AddWithValue("@Address", txtAddress.Text);
            InsertCustomer.Parameters.AddWithValue("@City", txtCity.Text);
            InsertCustomer.Parameters.AddWithValue("@Regior", txtRegion.Text);
            InsertCustomer.Parameters.AddWithValue("@PostalCode", txtPostalCode.Text);
            InsertCustomer.Parameters.AddWithValue("@Country", txtCountry.Text);
            InsertCustomer.Parameters.AddWithValue("@Phone", txtPhone.Text);
            InsertCustomer.Parameters.AddWithValue("@Telegram", txtTelegram.Text);
            InsertCustomer.Parameters.AddWithValue("@Fax", txtFax.Text);



            con.Open();
            InsertCustomer.ExecuteNonQuery();
            con.Close();
            GridView1.DataBind();
        }

        protected void btnSearchCustomer_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [CustomerID] ,[CompanyName],[ContactName],[ContactTitle],[Address],[City],[Regior],[PostalCode],[Country],[Phone],[Telegram],[Fax] FROM [dbo].[TblCustomer] WHERE CustomerID = " + txtCustomerID.Text;
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {
                txtCustomerID.Text = DTable1.Rows[0]["CustomerID"].ToString();

                txtCompanyName.Text = DTable1.Rows[0]["CompanyName"].ToString();
                txtContactName.Text = DTable1.Rows[0]["ContactName"].ToString();
                txtCustomerContactTitle.Text = DTable1.Rows[0]["ContactTitle"].ToString();
                txtAddress.Text = DTable1.Rows[0]["Address"].ToString();
                txtCity.Text = DTable1.Rows[0]["City"].ToString();
                txtRegion.Text = DTable1.Rows[0]["Regior"].ToString();
                txtPostalCode.Text = DTable1.Rows[0]["PostalCode"].ToString();
                txtCountry.Text = DTable1.Rows[0]["Country"].ToString();
                txtPhone.Text = DTable1.Rows[0]["Phone"].ToString();
                txtTelegram.Text = DTable1.Rows[0]["Telegram"].ToString();
                txtFax.Text = DTable1.Rows[0]["Fax"].ToString();
            } else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No data is returned');", true);
            }

            con.Close();
        }

        protected void btnUpdateCustomer_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdatepCustomer = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdatepCustomer = new SqlCommand("UPDATE [dbo].[TblCustomer] SET [CompanyName] = @CompanyName,[ContactName] = @ContactName,[ContactTitle] = @ContactTitle,[Address] = @Address,[City] = @City,[Regior] = @Regior,[PostalCode] = @PostalCode,[Country] = @Country,[Phone] = @Phone,[Telegram] = @Telegram,[Fax] = @Fax WHERE [CustomerID] = @CustomerID", con);

            UpdatepCustomer.Parameters.AddWithValue("@CustomerID", txtCustomerID.Text);

            UpdatepCustomer.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text);
            UpdatepCustomer.Parameters.AddWithValue("@ContactName", txtContactName.Text);
            UpdatepCustomer.Parameters.AddWithValue("@ContactTitle", txtCustomerContactTitle.Text);
            UpdatepCustomer.Parameters.AddWithValue("@Address", txtAddress.Text);
            UpdatepCustomer.Parameters.AddWithValue("@City", txtCity.Text);
            UpdatepCustomer.Parameters.AddWithValue("@Regior", txtRegion.Text);
            UpdatepCustomer.Parameters.AddWithValue("@PostalCode", txtPostalCode.Text);
            UpdatepCustomer.Parameters.AddWithValue("@Country", txtCountry.Text);
            UpdatepCustomer.Parameters.AddWithValue("@Phone", txtPhone.Text);
            UpdatepCustomer.Parameters.AddWithValue("@Telegram", txtTelegram.Text);
            UpdatepCustomer.Parameters.AddWithValue("@Fax", txtFax.Text);




            con.Open();
            UpdatepCustomer.ExecuteNonQuery();
            con.Close();
            GridView1.DataBind();
        }

        protected void btnDeleteCustomer_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteCustomer = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteCustomer = new SqlCommand("DELETE FROM [dbo].[TblCustomer] WHERE CustomerID = @CustomerID", con);

            DeleteCustomer.Parameters.AddWithValue("@CustomerID", txtCustomerID.Text);

            con.Open();
            DeleteCustomer.ExecuteNonQuery();
            con.Close();
            GridView1.DataBind();

            txtCompanyName.Text = "";
            txtContactName.Text = "";
            txtCustomerContactTitle.Text = "";
            txtAddress.Text = "";
            txtCity.Text =
            txtRegion.Text = "";
            txtPostalCode.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
            txtTelegram.Text = "";
            txtFax.Text = "";
        }

        protected void btnClearCustomer_Click(object sender, EventArgs e)
        {
            txtCompanyName.Text = "";
            txtContactName.Text = "";
            txtCustomerContactTitle.Text = "";
            txtAddress.Text = "";
            txtCity.Text =
            txtRegion.Text = "";
            txtPostalCode.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
            txtTelegram.Text = "";
            txtFax.Text = "";
        }
    }
}