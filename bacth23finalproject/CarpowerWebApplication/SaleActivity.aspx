﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="SaleActivity.aspx.cs" Inherits="CarpowerWebApplication.SaleActivity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f2f2f2;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }

        .card-container {
            display: flex;
            flex-wrap: wrap;
            gap: 20px;
        }

        .card {
            width: 250px;
            background-color: #fff;
            border-radius: 12px;
            overflow: hidden;
            transition: transform 0.2s ease-in-out;
        }

            .card:hover {
                transform: scale(1.05);
                box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
            }

        .card-content {
            padding: 20px;
            text-align: left;
        }

        .icon {
            font-size: 24px;
            margin-bottom: 10px;
        }

        .card-titles {
            font-size: 1.5rem;
            font-weight: bold;
            margin-bottom: 10px;
        }

        .card-description {
            font-size: medium;
            font-family: 'Khmer OS Siemreap';
        }

        .ListTotalProduct {
            width: 300px !important;
            color: #da8810;
        }

        .ListTotalProduct1 {
            width: 300px !important;
            color: #da8810;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />

    <div class="container">
        <h3 style="margin-right: 86%; font-family: 'Khmer OS Siemreap'">សកម្មភាព</h3>
        <br />


        <div class="card-container">
            <div class="card">
                <div class="card-content">
                    <i class="ph-duotone ph-cards icon"></i>
                    <h2 class="card-title">
                        <asp:DataList ID="DataList9" runat="server" DataSourceID="SqlDataSource9">
                            <ItemTemplate>
                                &nbsp;<asp:Label ID="Column1Label" runat="server" Text='<%#""+ String.Format("{0:C}", Eval("TotalAmount")) %>' CssClass="ListTotalProduct" />
                                <br />
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="SqlDataSource9" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_SumAmount" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    </h2>
                    <p class="card-description">ចំនួន ទឹកប្រាក់បានលក់ឡាន</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <i class="ph-duotone ph-cards icon"></i>
                    <h2 class="card-title">
                        <asp:DataList ID="ListTotalProduct" runat="server" DataSourceID="SqlDataSourceListTotalProduct">
                            <ItemTemplate>
                                &nbsp;<asp:Label ID="Column1Label" runat="server" Text='<%# Eval("TotalProduct") %>' CssClass="ListTotalProduct" />
                                <br />
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="SqlDataSourceListTotalProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_ListTotalProduct" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    </h2>
                    <p class="card-description">ចំនួន ឡាន</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <i class="ph-duotone ph-cards icon"></i>
                    <h2 class="card-title">
                        <asp:DataList ID="DataList8" RepeatDirection="Horizontal" runat="server" DataSourceID="SqlDataSource8" Font-Size="16px" RepeatColumns="2">
                            <ItemTemplate>
                                <asp:Label ID="Column1Label" runat="server" Text='<%# Eval("CategoryName") %>' CssClass="ListTotalProduct" />
                                (<asp:Label ID="Label1" runat="server" Text='<%# Eval("CountProductByCategory") %>' CssClass="ListTotalProduct" />)
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="SqlDataSource8" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_SumProductByCategory" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    </h2>
                    <p class="card-description">ចំនួន ឡានតាមប្រភេទ</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <h4 class="card-title">
                        <asp:DataList ID="ListTotalUser" runat="server" DataSourceID="SqlDataSourceListAllUser">
                            <ItemTemplate>
                                &nbsp;<asp:Label ID="Column1Label" runat="server" Text='<%# Eval("CountUser") %>' CssClass="ListTotalProduct" Font-Size="XX-Large" />
                                <br />
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="SqlDataSourceListAllUser" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_CountUser" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    </h4>
                    <p class="card-description">ចំនួន អ្នកប្រើ</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <h4 class="card-title">
                        <asp:DataList ID="DataList6" runat="server" DataSourceID="SqlDataSource6">
                            <ItemTemplate>
                                &nbsp;<asp:Label ID="Column1Label" runat="server" Text='<%# Eval("CountCustomer") %>' CssClass="ListTotalProduct" Font-Size="XX-Large" />
                                <br />
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_CountCustomer" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    </h4>
                    <p class="card-description">ចំនួន អតិថិជន</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <asp:DataList ID="DataListAllComment" runat="server" DataSourceID="SqlDataSource10">
                        <ItemTemplate>
                            &nbsp;<asp:Label ID="CommentCountLabel" runat="server" Text='<%# Eval("CountOrderProduct") %>' CssClass="ListTotalProduct1" Font-Size="XX-Large" />
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="SqlDataSource10" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_CountOrderProduct" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSourceListAllComment" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT COUNT([CommentID]) AS CommentCount
FROM [carpower].[dbo].[TblComment]"></asp:SqlDataSource>
                    <p class="card-description">ចំនួន អ្នកបញ្ជាទិញ</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource1">
                        <ItemTemplate>
                            &nbsp;<asp:Label ID="CommentCountLabel" runat="server" Text='<%# Eval("CountProccessingProduct") %>' CssClass="ListTotalProduct1" Font-Size="XX-Large" />
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_CountProccessingProduct" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    <p class="card-description">ចំនួន កំពុងពិនិត្យឡាន</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <asp:DataList ID="DataList2" runat="server" DataSourceID="SqlDataSource2">
                        <ItemTemplate>
                            &nbsp;<asp:Label ID="CommentCountLabel" runat="server" Text='<%# Eval("CountProductBySale") %>' CssClass="ListTotalProduct1" Font-Size="XX-Large" />
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_CountProductBySale" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    <p class="card-description">ចំនួន ឡានដែលបានលក់</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <asp:DataList ID="DataList7" runat="server" DataSourceID="SqlDataSource7">
                        <ItemTemplate>
                            &nbsp;<asp:Label ID="CommentCountLabel" runat="server" Text='<%# Eval("CountQuotation") %>' CssClass="ListTotalProduct1" Font-Size="XX-Large" />
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_CountQuotation" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    <p class="card-description">ចំនួន Quotation</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <asp:DataList ID="DataList4" runat="server" DataSourceID="SqlDataSource4">
                        <ItemTemplate>
                            &nbsp;<asp:Label ID="CommentCountLabel" runat="server" Text='<%# Eval("CountProductByCancel") %>' CssClass="ListTotalProduct1" Font-Size="XX-Large" />
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_CountProductByCancel" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    <p class="card-description">ចំនួន ឡានដែលមិនបានទិញ</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <asp:DataList ID="DataList3" runat="server" DataSourceID="SqlDataSource3">
                        <ItemTemplate>
                            &nbsp;<asp:Label ID="CommentCountLabel" runat="server" Text='<%# Eval("CountComment") %>' CssClass="ListTotalProduct1" Font-Size="XX-Large" />
                            (<asp:Label ID="Label2" runat="server" Text='<%# Eval("CountReply") %>' CssClass="ListTotalProduct1" />)
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_CountCommentReply" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    <p class="card-description">ចំនួន ការសួរនាំ និងការឆ្លើយតប</p>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <asp:DataList ID="DataList5" runat="server" DataSourceID="SqlDataSource5">
                        <ItemTemplate>
                            &nbsp;<asp:Label ID="CommentCountLabel" runat="server" Text='<%# Eval("CountDocument") %>' CssClass="ListTotalProduct1" Font-Size="XX-Large" />
                            <br />
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_CountDocument" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    <p class="card-description">ចំនួន ឯកសារពីអ្នកទិញឡាន</p>
                </div>
            </div>
        </div>
        <br />

    </div>
</asp:Content>
