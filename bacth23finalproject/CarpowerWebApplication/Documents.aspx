﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Documents.aspx.cs" Inherits="CarpowerWebApplication.Documents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2>បញ្ជូលឯកសារ</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label1" runat="server" Text="Document id"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtDocumentID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label2" runat="server" Text="Document name"></asp:Label>
                        <asp:TextBox ID="textDocumentName" runat="server" CssClass="form-control"></asp:TextBox>
                    </b>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <%--upload--%>
                <div class="mb-3">
                    <asp:Image ID="DocumentAttch" runat="server" Height="221px" Width="323px" />
                    <br />
                    <br />
                    <asp:TextBox ID="DocumentURL" runat="server" CssClass="form-control"></asp:TextBox>
                    <br />
                    <asp:FileUpload ID="UploadDocumentAttch" runat="server" />
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label3" runat="server" Text="Product id"></asp:Label>
                    </b>
                    <asp:TextBox ID="textDocumentProductID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-6">
                    <asp:Button ID="btnInsertAds" runat="server" CssClass="btn btn-outline-primary" Text="Insert" OnClick="btnInsertAds_Click" />
                    <asp:Button ID="btnSearchAds" runat="server" CssClass="btn btn-outline-secondary" Text="Search" OnClick="btnSearchAds_Click" />
                    <asp:Button ID="btnUpdateAds" runat="server" CssClass="btn btn-outline-success" Text="Update" OnClick="btnUpdateAds_Click" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-outline-danger" Text="Delete" OnClick="btnDelete_Click" />
                    <asp:Button ID="btnClearsAds" runat="server" CssClass="btn btn-outline-warning" Text="Clear" OnClick="btnClearsAds_Click" />
                </div>
                <div class="col-6"></div>
            </div>
        </div>
        <br />
        <div class="table-responsive">
            <div class="container">
                <asp:GridView ID="GridViewListDocument" runat="server" AutoGenerateColumns="False" DataKeyNames="DocumentID" DataSourceID="SqlDataSourceGridViewListDocument" CssClass="table table-striped table-hover">
                    <Columns>
                        <asp:BoundField DataField="DocumentID" HeaderText="DocumentID" InsertVisible="False" ReadOnly="True" SortExpression="DocumentID" />
                        <asp:BoundField DataField="DocumentName" HeaderText="DocumentName" SortExpression="DocumentName" />
                        <asp:BoundField DataField="DocumentAttach" HeaderText="DocumentAttach" SortExpression="DocumentAttach" />
                        <asp:BoundField DataField="ProducctID" HeaderText="ProducctID" SortExpression="ProducctID" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceGridViewListDocument" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [DocumentID], [DocumentName], [DocumentAttach], [ProducctID] FROM [TblDocument]"></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
