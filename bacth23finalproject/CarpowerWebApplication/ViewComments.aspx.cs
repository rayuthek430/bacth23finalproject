﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class ViewComments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnInserComment_Click(object sender, EventArgs e)
        {
            SqlCommand ReplyComment = new SqlCommand();
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            ReplyComment = new SqlCommand("INSERT INTO [dbo].[TblReply]([Reply],[CommentID],[UserID]) VALUES (@Reply,@CommentID,@UserID)", con);

            ReplyComment.Parameters.AddWithValue("@Reply", txtReply.Text);
            ReplyComment.Parameters.AddWithValue("@CommentID", txtCommentID.Text);
            ReplyComment.Parameters.AddWithValue("@UserID", Session["SUserID"]);

            con.Open();
            ReplyComment.ExecuteNonQuery();
            con.Close();
            DataListOrderDoc.DataBind();
        }

        protected void DataListOrderDetailComments_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "Comment")
            {
                string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
                SqlConnection con = new SqlConnection(conStr);
                con.Open();
                string searchdatastu = "SELECT [CommentID] ,[Comment],[OrderDetailID],[UserID],[CommentDate]FROM [dbo].[TblComment] WHERE CommentID = " + e.CommandArgument.ToString();
                SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
                DataSet dataset1 = new System.Data.DataSet();
                dataAd1.Fill(dataset1);
                DataTable DTable1 = new DataTable();
                DTable1 = dataset1.Tables[0];
                if (DTable1.Rows.Count > 0)
                {

                    txtCommentID.Text = DTable1.Rows[0]["CommentID"].ToString();
                    TxtTextComment.Text = DTable1.Rows[0]["Comment"].ToString();

                }

                con.Close();
            }
        }

        protected void DataListOrderDetailReply_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "Reply")
            {
                string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
                SqlConnection con = new SqlConnection(conStr);
                con.Open();
                string searchdatastu = "SELECT [ReplyID],[Reply],[CommentID],[UserID] FROM [dbo].[TblReply] WHERE ReplyID = " + e.CommandArgument.ToString();
                SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
                DataSet dataset1 = new System.Data.DataSet();
                dataAd1.Fill(dataset1);
                DataTable DTable1 = new DataTable();
                DTable1 = dataset1.Tables[0];
                if (DTable1.Rows.Count > 0)
                {
                    txtReplyID.Text = DTable1.Rows[0]["ReplyID"].ToString();
                    TxtTextComment.Text = DTable1.Rows[0]["Reply"].ToString();
                    txtCommentID.Text = DTable1.Rows[0]["CommentID"].ToString();
                }
                con.Close();
            }
            if (e.CommandName == "RemoveComment")
            {

                SqlCommand DeleteComment = new SqlCommand();
                string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
                SqlConnection con = new SqlConnection(conStr);
                DeleteComment = new SqlCommand("delete from TblReply where CommentID=@CommentID;DELETE FROM [dbo].[TblComment] WHERe CommentID=@CommentID", con);

                DeleteComment.Parameters.AddWithValue("@CommentID", e.CommandArgument.ToString());

                con.Open();
                DeleteComment.ExecuteNonQuery();
                con.Close();
                DataListOrderDoc.DataBind();
            }

            if (e.CommandName == "RemoveReply")
            {
                SqlCommand DeleteReply = new SqlCommand();

                string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
                SqlConnection con = new SqlConnection(conStr);
                DeleteReply = new SqlCommand("DELETE FROM [dbo].[TblReply] WHERE ReplyID =@ReplyID", con);

                DeleteReply.Parameters.AddWithValue("@ReplyID", e.CommandArgument.ToString());

                con.Open();
                DeleteReply.ExecuteNonQuery();
                con.Close();
                DataListOrderDoc.DataBind();
            }
        }
    }
}