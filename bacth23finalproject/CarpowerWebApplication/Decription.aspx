﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Decription.aspx.cs" Inherits="CarpowerWebApplication.WebFormDecription" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h2>Manage Decription</h2>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label7" runat="server" Text="ឈ្មោះរថយន្ត" style="font-family:'Khmer OS Siemreap'"></asp:Label></b>
                    <%--<asp:TextBox ID="productID" runat="server" CssClass="form-control"></asp:TextBox>--%>
                    <asp:DropDownList ID="DropDownListProductIDFormTableProduct" runat="server" CssClass="form-control" DataSourceID="SqlDataSourceListProductID" DataTextField="ProductName" DataValueField="ProductID"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSourceListProductID" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [ProductID], [ProductName] FROM [TblProduct] WHERE ([ProductID] = @ProductID)">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="ProductID" QueryStringField="ProductID" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label1" runat="server" Text="លេខសម្គាល់ការពិពណ៌នា" style="font-family:'Khmer OS Siemreap'"></asp:Label></b>
                    <asp:TextBox ID="decriptionID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label4" runat="server" Text="ម៉ាលរថយន្ត" style="font-family:'Khmer OS Siemreap'"></asp:Label></b>
                    <asp:TextBox ID="carModel" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label3" runat="server" Text="ឆ្នាំផលិត" style="font-family:'Khmer OS Siemreap'"></asp:Label></b>
                    <asp:TextBox ID="carMakes" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-6">
                <div class="mb-3">

                    <b>
                        <asp:Label ID="Label6" runat="server" Text="ពណ៌" style="font-family:'Khmer OS Siemreap'"></asp:Label></b>
                    <asp:TextBox ID="color" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label5" runat="server" Text="ឆ្នាំ" style="font-family:'Khmer OS Siemreap'"></asp:Label></b>
                    <asp:TextBox ID="year" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label2" runat="server" Text="ពិពណ៌នាអំពីផលិតផលលម្អិត" style="font-family:'Khmer OS Siemreap'"></asp:Label>

                    </b>
                    <asp:TextBox ID="decriptionDetailProduct" runat="server" CssClass="form-control" Height="100px" TextMode="MultiLine" Width="630px"></asp:TextBox>
                </div>
            </div>

            <div class="col-6">
                <div class="mb-3"> 
                    
                <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" Text="បញ្ចូល" OnClick="btnInsertDecription_Click" />
                <asp:Button ID="Button2" runat="server" CssClass="btn btn-success" Text="កែ" OnClick="btnUpdateDecription_Click" />
                <asp:Button ID="Button3" runat="server" CssClass="btn btn-danger" Text="លុប" OnClick="btnDeleteDecription_Click" />
                <asp:Button ID="Button4" runat="server" CssClass="btn btn-secondary" Text="ស្វែងរក" OnClick="btnSearchDecription_Click" />
                <asp:Button ID="Button5" runat="server" CssClass="btn btn-warning" Text="សម្អាត" OnClick="btnClear_Click" />
                <asp:Button ID="ButtonBackToProduct" runat="server" CssClass="btn btn-dark" Text="ត្រទ្បប់" OnClick="ButtonBackToProduct_Click" />
                </div>
            </div>
        </div>

        <br />
       <%-- <div class="row">
            <div class="col-12">
                <asp:Button ID="btnInsertDecription" runat="server" CssClass="btn btn-primary" Text="Insert" OnClick="btnInsertDecription_Click" />
                <asp:Button ID="btnUpdateDecription" runat="server" CssClass="btn btn-secondary" Text="Update" OnClick="btnUpdateDecription_Click" />
                <asp:Button ID="btnDeleteDecription" runat="server" CssClass="btn btn-danger" Text="Delete" OnClick="btnDeleteDecription_Click" />
                <asp:Button ID="btnSearchDecription" runat="server" CssClass="btn btn-danger" Text="Search" OnClick="btnSearchDecription_Click" />
                <asp:Button ID="btnClear" runat="server" CssClass="btn btn-warning" Text="Clear" OnClick="btnClear_Click" />
            </div>
        </div>--%>
        <br />
    </div>
    <div class="row">
        <div class="col-12">
            <asp:GridView ID="GridViewDecription" CssClass="table table-bordered border-primary" runat="server" AutoGenerateColumns="False" DataKeyNames="DescriptionID" DataSourceID="SqlDataSourceListProductDescription">
                <Columns>
                    <asp:BoundField DataField="DescriptionID" HeaderText="លេខសម្គាល់ការពិពណ៌នា" InsertVisible="False" ReadOnly="True" SortExpression="DescriptionID" />
                    <asp:BoundField DataField="DescriptionDetailsProduct" HeaderText="ពិពណ៌នាអំពីផលិតផលលម្អិត" SortExpression="DescriptionDetailsProduct" />
                    <asp:BoundField DataField="CarMakes" HeaderText="ម៉ាលរថយន្ត" SortExpression="CarMakes" />
                    <asp:BoundField DataField="CarModel" HeaderText="ម៉ាលរថយន្ត" SortExpression="CarModel" />
                    <asp:BoundField DataField="Year" HeaderText="ឆ្នាំ" SortExpression="Year" />
                    <asp:BoundField DataField="Color" HeaderText="ពណ៌" SortExpression="Color" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSourceListProductDescription" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [DescriptionID], [DescriptionDetailsProduct], [CarMakes], [CarModel], [Year], [Color] FROM [TblDescription] WHERE ([ProductID] = @ProductID)">
                <SelectParameters>
                    <asp:QueryStringParameter Name="ProductID" QueryStringField="ProductID" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
