﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="ReportQuotation.aspx.cs" Inherits="CarpowerWebApplication.ReportQuotation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">



        <div class="row">
            <div class="col-lg-12 text-center border-bottom">
                <h3 cssclass="text-center" style="color: #000080" class="auto-style1"><strong>&nbsp;Daily Report</strong></h3>
            </div>
        </div>
        <br />
        <input type="button" class="btn btn-primary" onclick="printDiv('PrintReportDaily')" value="Print" />
        <asp:Button ID="BtnPDF" runat="server" Text="To PDF" CssClass="btn btn-primary" OnClick="BtnPDF_Click" />
        <asp:Button ID="BtnExcel" CssClass="btn btn-primary" runat="server" Text="To Excel" OnClick="BtnExcel_Click" />
        <br />
        <div class="row">

            <div class="col-lg-5">
                From Date
                    <asp:TextBox runat="server" TextMode="Date" Enabled="True" name="BrandName" ID="txtFromDate" class="form-control input-sm"></asp:TextBox>
            </div>
            <div class="col-lg-5">
                To Date
                    <asp:TextBox runat="server" TextMode="Date" Enabled="True" name="BrandName" ID="txtToDate" class="form-control input-sm"></asp:TextBox>
            </div>
            <div class="col-lg-2">

                <br />
                <asp:Button ID="btnLoad" CssClass="btn btn-primary" runat="server" Text="Load" OnClick="btnLoad_Click" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-12">
                <div id="PrintReportDaily">
                    <table border="0" width="900">
                        <tr>
                            <td colspan="3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="images/Banner invoice noBG.png" style="width: 865px" />
                                    </div>
                                </div>

                            </td>
                        </tr>

                        <tr>
                            <td colspan="3">
                                <div class="row">
                                    <div class="col-lg-12 text-center" style="font-family: 'Khmer OS Siemreap'; font-size: large">
                                        <h6>អាសយដ្ឋាន ឃ្លាំង 203 ផ្លូវជាតិលេខ៣ ,ព្រៃព្រីងខាងត្បូង, ចោមចៅ3, ពោធិ៍សែនជ័យ, រាជធានីភ្នំពេញ</h6>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 300px;">
                                <div class="col-lg-12">
                                    <h6>Tel:</h6>
                                    <h6>012 85 26 85</h6>
                                    <h6>088 918 3168</h6>
                                    <h6>016 866 789</h6>
                                </div>
                            </td>
                            <td style="width: 450px;">
                                <div class="col-lg-12" style="margin-top: 70px">
                                    <br />
                                    <h5 style="font-family: 'Khmer OS Muol'">របាយការណ៍ Quotation
                                    </h5>
                                </div>
                            </td>
                            <td style="width: 150px;">
                                <div class="col-lg-12" style="margin-top: 40px">
                                </div>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:DataList ID="DataListReportDailyOrder" runat="server" DataKeyField="QuotationID" DataSourceID="SqlDataSourceDailyReport">

                                    <ItemTemplate>
                                        <br />

                                        <asp:Label ID="QuotationIDLabel" Visible="false" runat="server" Text='<%# Eval("QuotationID") %>' />
                                        <div class="row border-bottom" style="color: brown">
                                            <div class="col-lg-6" style="font-size: 20px; font-family: Calibri">

                                                <asp:Label ID="QuotationDateLabel" runat="server" Text='<%# Eval("QuotationDate") %>' Font-Size="20px" Font-Bold="True" />
                                            </div>
                                            <div class="col-lg-6 text-right " style="font-size: 20px; font-family: Calibri">
                                                Total Amount:
                <asp:Label ID="AmountLabel" runat="server" Text='<%#""+ String.Format("{0:C}", Eval("Amount")) %>' Font-Size="20px" Font-Bold="True" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-lg-11">
                                                User :
                <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("FullName") %>' />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-1"></div>
                                            <div class="col-lg-11">
                                                Customer : 
                <asp:Label ID="ContactNameLabel" runat="server" Text='<%# Eval("PhoneNumber") %>' />
                                            </div>
                                        </div>



                                        <div class="row">

                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-10">
                                                <asp:GridView ID="GridViewListOrderDetail" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceReportDailyOrderDetail" Width="800px" ShowHeader="False">
                                                    <Columns>
                                                        <asp:BoundField DataField="ProductName" ItemStyle-Width="400px" HeaderText="ProductName" SortExpression="ProductName" />
                                                        <asp:BoundField DataField="UnitPrice" ItemStyle-Width="400px" Visible="false" HeaderText="UnitPrice" SortExpression="UnitPrice" />
                                                        <asp:BoundField DataField="Quantity" ItemStyle-Width="80px" HeaderText="Quantity" SortExpression="Quantity" />
                                                        <asp:BoundField DataField="Total" ItemStyle-Width="120px" HeaderText="Total" ReadOnly="True" SortExpression="Total" DataFormatString="{0:C}" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                        <asp:SqlDataSource ID="SqlDataSourceReportDailyOrderDetail" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_ReportDailyQuotationDetail" SelectCommandType="StoredProcedure">
                                            <SelectParameters>
                                                <asp:ControlParameter ControlID="QuotationIDLabel" Name="QuotationID" PropertyName="Text" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>


        <asp:SqlDataSource ID="SqlDataSourceDailyReport" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_ReportDailyQuotation" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="txtFromDate" DbType="String" Name="StartDate" PropertyName="Text" />
                <asp:ControlParameter ControlID="txtToDate" DbType="String" Name="EndDate" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>

    </div>


</asp:Content>
