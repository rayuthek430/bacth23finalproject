﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="FormSupplier.aspx.cs" Inherits="CarpowerWebApplication.WebFormSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2 class="text-right">Form Supplier</h2>
        <br />
        <div class="row">
            <div class="col-4">
                <div class="mb-4" style="width: 55%">
                    <asp:Label ID="Label1" runat="server" Text="SupplierID:" CssClass="right-align text-right"></asp:Label>
                    <asp:TextBox ID="txtSupplierID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label2" runat="server" Text="CompanyName:"></asp:Label>
                    <asp:TextBox ID="txtCompanyName" runat="server" CssClass="form-control"></asp:TextBox>
                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCompanyName" ErrorMessage="Please enter your company name"></asp:RequiredFieldValidator>--%>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label3" runat="server" Text="ContactName:"></asp:Label>
                    <asp:TextBox ID="txtContactName" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label4" runat="server" Text="Address:"></asp:Label>
                    <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label5" runat="server" Text="City:"></asp:Label>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label6" runat="server" Text="Region: "></asp:Label>
                    <asp:TextBox ID="txtRegion" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label7" runat="server" Text="PostalCode:"></asp:Label>
                    <asp:TextBox ID="txtPostalCode" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label8" runat="server" Text="Country:"></asp:Label>
                    <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label9" runat="server" Text="Phone:"></asp:Label>
                    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label10" runat="server" Text="Fax:"></asp:Label>
                    <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label11" runat="server" Text="Website:"></asp:Label>
                    <asp:TextBox ID="txtWebsite" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="d-grid gap-2 d-md-block">
                <%--Button--%>
                <asp:Button ID="btnInsertSupplier" runat="server" CssClass="btn btn-primary" Text="Insert" OnClick="btnInsertSupplier_Click" OnClientClick="return validateForm();" />
                <asp:Button ID="btnUpdateSupplier" runat="server" CssClass="btn btn-secondary" Text="Update" OnClick="btnUpdateSupplier_Click" />
                <asp:Button ID="btnDeleteSupplier" runat="server" CssClass="btn btn-danger" Text="Delete" OnClick="btnDeleteSupplier_Click" />
                <asp:Button ID="btnSearchSupplier" runat="server" CssClass="btn btn-info" Text="Search" OnClick="btnSearchSupplier_Click" OnClientClick="return checkSearch()"/>
                <asp:Button ID="btnClear" runat="server" CssClass="btn btn-secondary" Text="Clear" OnClick="btnClear_Click" />
            </div>
        </div>
        <br />

        <div class="table-responsive">
            <div class="container">
                <asp:GridView ID="GridViewSupplier" CssClass="table table-striped table-hover" runat="server" AutoGenerateColumns="False" DataKeyNames="SupplierID" DataSourceID="SqlDataSourceListGridViewSuppliers">
                    <Columns>
                      <%--  <asp:BoundField DataField="SupplierID" HeaderText="SupplierID" InsertVisible="False" ReadOnly="True" SortExpression="SupplierID" />--%>
                        <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
                        <asp:BoundField DataField="ContactName" HeaderText="ContactName" SortExpression="ContactName" />
                        <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                        <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                        <asp:BoundField DataField="Region" HeaderText="Region" SortExpression="Region" />
                        <asp:BoundField DataField="PostalCode" HeaderText="PostalCode" SortExpression="PostalCode" />
                        <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
                        <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                        <asp:BoundField DataField="Fax" HeaderText="Fax" SortExpression="Fax" />
                        <asp:BoundField DataField="Website" HeaderText="Website" SortExpression="Website" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListGridViewSuppliers" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [SupplierID], [CompanyName], [ContactName], [Address], [City], [Region], [PostalCode], [Country], [Phone], [Fax], [Website] FROM [TblSuppliers]"></asp:SqlDataSource>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        <%--var Supplier = document.getElementById("<%= txtSupplierID.ClientID %>").value;
        var companyName = document.getElementById("<%= txtCompanyName.ClientID %>").value;
        var ContactName = document.getElementById("<%= txtContactName.ClientID %>").value;
        var address = document.getElementById("<%= txtAddress.ClientID %>").value;
        var city = document.getElementById("<%= txtCity.ClientID %>").value;
        var region = document.getElementById("<%= txtRegion.ClientID %>").value;
        var postalCode = document.getElementById("<%= txtPostalCode.ClientID %>").value;
        var country = document.getElementById("<%= txtCountry.ClientID %>").value;
        var phone = document.getElementById("<%= txtPhone.ClientID %>").value;
        var fax = document.getElementById("<%= txtFax.ClientID %>").value;
        var website = document.getElementById("<%= txtWebsite.ClientID %>").value;--%>

        //console.log('yuth', Supplier)

        //function checkSearch() {
        //    if (Supplier === "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your ',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
                
        //    }
            
        //}

        //function validateForm() {
           

        //    if (companyName == "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your company name',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
        //        return false;
        //    }
        //    if (ContactName == "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your contact name',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
        //        return false;
        //    }
        //    if (address == "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your adress',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
        //        return false;
        //    }
        //    if (city == "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your city',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
        //        return false;
        //    }
        //    if (region == "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your region',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
        //        return false;
        //    }
        //    if (postalCode == "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your postal code',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
        //        return false;
        //    }
        //    if (country == "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your country',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
        //        return false;
        //    }
        //    if (phone == "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your phone number',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
        //        return false;
        //    }
        //    if (fax == "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your fax',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
        //        return false;
        //    }
        //    if (website == "") {
        //        Swal.fire({
        //            title: 'Error!',
        //            text: 'Please enter your website',
        //            icon: 'error',
        //            confirmButtonText: 'OK'
        //        })
        //        return false;
        //    }
        //}

    </script>

</asp:Content>
