﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class Documents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertAds_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertAds = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertAds = new SqlCommand("INSERT INTO [dbo].[TblDocument] ([DocumentName],[DocumentAttach],[ProducctID]) VALUES (@DocumentName,@DocumentAttach,@ProducctID)", con);

            InsertAds.Parameters.AddWithValue("@DocumentName", textDocumentName.Text);
            InsertAds.Parameters.AddWithValue("@ProducctID", textDocumentProductID.Text);



            if (UploadDocumentAttch.HasFile)
            {
                UploadDocumentAttch.SaveAs(Server.MapPath("images/" + UploadDocumentAttch.FileName));
                DocumentURL.Text = "images/" + UploadDocumentAttch.FileName;
            }
            DocumentAttch.ImageUrl = DocumentURL.Text;
            InsertAds.Parameters.AddWithValue("@DocumentAttach", DocumentURL.Text);



            con.Open();
            InsertAds.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('បានជោគជ័យ!', 'ការបញ្ចូលទិន្នន័យបានជោគជ័យ.', 'success');", true);
            GridViewListDocument.DataBind();


        }

        protected void btnSearchAds_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [DocumentID] ,[DocumentName],[DocumentAttach],[ProducctID]FROM [dbo].[TblDocument]​ where DocumentID = " + txtDocumentID.Text;
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {
                txtDocumentID.Text = DTable1.Rows[0]["DocumentID"].ToString();

                textDocumentName.Text = DTable1.Rows[0]["DocumentName"].ToString();
                textDocumentProductID.Text = DTable1.Rows[0]["ProducctID"].ToString();


                DocumentAttch.ImageUrl = DTable1.Rows[0]["DocumentAttach"].ToString();
            }

            con.Close();
        }

        protected void btnUpdateAds_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdateDocument = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdateDocument = new SqlCommand("UPDATE [dbo].[TblDocument] SET [DocumentName] = @DocumentName,[DocumentAttach] = @DocumentAttach,[ProducctID] = @ProducctID WHERE DocumentID = @DocumentID", con);

            UpdateDocument.Parameters.AddWithValue("@DocumentID", txtDocumentID.Text);
            UpdateDocument.Parameters.AddWithValue("@DocumentName", textDocumentName.Text);


            if (UploadDocumentAttch.HasFile)
            {
                UploadDocumentAttch.SaveAs(Server.MapPath("images/" + UploadDocumentAttch.FileName));
                DocumentURL.Text = "images/" + UploadDocumentAttch.FileName;
            }
            DocumentAttch.ImageUrl = DocumentURL.Text;
            UpdateDocument.Parameters.AddWithValue("@DocumentAttach", DocumentURL.Text);
            UpdateDocument.Parameters.AddWithValue("@ProducctID", textDocumentProductID.Text);

            con.Open();
            UpdateDocument.ExecuteNonQuery();
            con.Close();
            GridViewListDocument.DataBind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteDocument = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteDocument = new SqlCommand("DELETE FROM [dbo].[TblDocument] WHERE DocumentID = @DocumentID", con);

            DeleteDocument.Parameters.AddWithValue("@DocumentID", txtDocumentID.Text);

            con.Open();
            DeleteDocument.ExecuteNonQuery();
            con.Close();
            //GridViewProduct.DataBind();
            if (textDocumentName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('មិនមានទិន្នន័យ', 'សូមរើសយកទិន្នន័យណាមួយ.', 'success');", true);
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('បានជោគជ័យ!', 'ទិន្នន័យលុបបានជោគជ័យ.', 'success');", true);

            
            textDocumentName.Text = "";
            GridViewListDocument.DataBind();
        }

        protected void btnClearsAds_Click(object sender, EventArgs e)
        {
            textDocumentName.Text = "";
        }
    }
}