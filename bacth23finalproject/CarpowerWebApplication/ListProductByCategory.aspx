﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="ListProductByCategory.aspx.cs" Inherits="CarpowerWebApplication.ListProductByCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--Old style--%>
    <%-- <asp:DataList ID="DataListProductByCategory" runat="server" DataKeyField="ProductID" DataSourceID="SqlDataSourceListProductByCategory" RepeatColumns="3" RepeatDirection="Horizontal">
        <ItemTemplate>
            <a href="ProductListByCategory.aspx?ProductID=<%# Eval("ProductID") %>">
                <div class="card" style="width: 18rem;">
                    
                    <asp:Image ID="ProductList" CssClass="card-img-top" runat="server" ImageUrl='<%# Eval("image") %>' />
                   
                    <asp:Label ID="imageLabel" Visible="false" runat="server" Text='<%# Eval("image") %>' />
                    <div class="card-body">
                        <h5>
                            <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' />
                        </h5>
                        <p>ឡានមូល ស្អាតខ្លាំង</p>
                        <asp:Label ID="Label1" runat="server" CssClass="domlai" Text='<%#  "$" + Eval("UnitPrice") %>' />
                    </div>
                </div>
                <asp:Label ID="ProductIDLabel" Visible="false" runat="server" Text='<%# Eval("ProductID") %>' />
                <asp:Label ID="UnitPriceLabel" Visible="false" runat="server" Text='<%# Eval("UnitPrice") %>' />
            </a>
        </ItemTemplate>
    </asp:DataList>--%>
    <%--<asp:SqlDataSource ID="SqlDataSourceListProductByCategory" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [ProductID], [ProductName], [UnitPrice], [image] FROM [TblProduct] WHERE ([CategoryID] = @CategoryID)">
        <SelectParameters>
            <asp:QueryStringParameter Name="CategoryID" QueryStringField="CategoryId" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>--%>

    <%--New style--%>
    <div class="row justify-content-center">
        <div class="row">
            <asp:DataList ID="DataList1" runat="server" DataKeyField="ProductID" DataSourceID="SqlDataSourceListProductByCategory" RepeatColumns="4" RepeatDirection="Horizontal">
                <ItemTemplate>
                    <a href="ProductDetail.aspx?ProductID=<%# Eval("ProductID") %>">
                        <div class="card" style="width: 18rem;">
                            <%-- new--%>
                            <asp:Image ID="ProductList" CssClass="card-img-top" runat="server" ImageUrl='<%# Eval("image") %>' />
                            <%--old--%>
                            <asp:Label ID="imageLabel" Visible="false" runat="server" Text='<%# Eval("image") %>' />
                            <div class="card-body">
                                <h5>
                                    <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' />
                                </h5>
                                <asp:Label ID="Label1" runat="server" CssClass="domlai">
                                    តម្លៃ <span style="color: red;"><%# "$" + String.Format("{0:#,0.##}", Eval("UnitPrice")) %></span>
                                </asp:Label>
                            </div>
                        </div>
                        <asp:Label ID="ProductIDLabel" Visible="false" runat="server" Text='<%# Eval("ProductID") %>' />
                        <asp:Label ID="UnitPriceLabel" Visible="false" runat="server" Text='<%# Eval("UnitPrice") %>' />
                    </a>
                </ItemTemplate>
            </asp:DataList>
            <asp:SqlDataSource ID="SqlDataSourceListProductByCategory" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [ProductID], [ProductName], [UnitPrice], [image] FROM [TblProduct] WHERE ([CategoryID] = @CategoryID)">
                <SelectParameters>
                    <asp:QueryStringParameter Name="CategoryID" QueryStringField="CategoryId" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>
