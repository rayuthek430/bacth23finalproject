﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="InvoiceQuotation.aspx.cs" Inherits="CarpowerWebApplication.InvoiceQuotation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="PrintInvoice" style="width: 900px">
        <asp:DataList ID="DataListInvoiceOrder" runat="server" DataKeyField="QuotationID" DataSourceID="SqlDataSourceInvoiceOrder" CssClass="myContent">
            <ItemTemplate>
                <table border="0" width="900">
                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-lg-12">
                                    <img src="images/Banner invoice noBG.png" style="width: 865px" />
                                </div>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-lg-12 text-center" style="font-family:'Khmer OS Siemreap'; font-size:large">
                                        <h6>អាសយដ្ឋាន ឃ្លាំង 203 ផ្លូវជាតិលេខ៣ ,ព្រៃព្រីងខាងត្បូង, ចោមចៅ3, ពោធិ៍សែនជ័យ, រាជធានីភ្នំពេញ</h6>
                                    </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 300px;">
                            <div class="col-lg-12">
                                <h6>Tel:</h6>
                                <h6>012 85 26 85</h6>
                                <h6>088 918 3168</h6>
                                <h6>016 866 789</h6>
                            </div>
                        </td>
                        <td style="width: 300px;">
                            <div class="col-lg-12" style="margin-top: 70px">
                                <br />
                                <h5 style="font-family: 'Khmer OS Muol'">Quote រថយន្ត
                                </h5>
                            </div>
                        </td>
                        <td style="width: 300px;">
                            <div class="col-lg-12" style="margin-top: 40px">
                                <h6>Date :
            <asp:Label ID="QuotationDateLabel2" runat="server" Text='<%# Eval("QuotationDate") %>' />
                                   
                                </h6>
                            </div>
                            <br />
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-lg-12">
                                លេខ Quote:
            <asp:Label ID="QuotationIDLabel" runat="server" Text='<%# Eval("QuotationID") %>' />
                                <br />
                            </div>
                        </td>
                        <td>
                            <div class="col-lg-12">
                                ឈ្មោះ
            <asp:Label ID="UserNameLabel" runat="server" Text='<%# Eval("FullName") %>' />
                                <br />
                            </div>
                        </td>
                        <td>
                            <div class="col-lg-12">
                                លេខទូរសព្ទ:
            <asp:Label ID="PhoneNumberLabel" runat="server" Text='<%# Eval("PhoneNumber") %>' />
                                <br />
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:GridView CssClass="table table-hover" ID="GridViewInvoiceOrderDetail" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceInvoiceOrderDetail">
                                <Columns>
                                    <asp:BoundField DataField="ProductID" HeaderText="ProductID" SortExpression="ProductID" />
                                    <asp:TemplateField HeaderText="រូបភាព" SortExpression="image">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("image") %>' Width="64px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                                    <asp:BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <div class="row" style="margin-left: 58px">
                                <div class="col-lg-12">
                                    សរុប:
        <asp:Label ID="AmountLabel" runat="server" Text='<%# Eval("Amount") %>' />
                                </div>
                            </div>
                            <br />
                            <asp:SqlDataSource ID="SqlDataSourceInvoiceOrderDetail" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_InvoiceQuotationDetail" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="QuotationIDLabel" Name="QuotationID" PropertyName="Text" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>

                <br />
                <br />
            </ItemTemplate>
        </asp:DataList>

        <asp:SqlDataSource ID="SqlDataSourceInvoiceOrder" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_InvoiceQuotation" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:QueryStringParameter Name="QuotationID" QueryStringField="QuotationID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    <script language="javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
    <input type="button" onclick="printDiv('PrintInvoice')" value="Print" class="btn btn-primary" />

</asp:Content>
