﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class AdminShowSaleProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridViewListOrderProduct_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GridViewListOrderProduct_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
            if (e.CommandName == "ViewInvoice")
            {
                Response.Redirect("Invoices.aspx?OrderID=" + e.CommandArgument.ToString());
            }
        }
    }
}