﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Shippers.aspx.cs" Inherits="CarpowerWebApplication.WebFormShippers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h2>Form Shippers</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family:Arial, Helvetica, sans-serif; font-size:17px">
                        <asp:Label ID="Label1" runat="server" Text="Sippers id:"></asp:Label>

                    </b>
                    <asp:TextBox ID="txtSippersID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family:Arial, Helvetica, sans-serif; font-size:17px">
                        <asp:Label ID="Label2" runat="server" Text="Company name:"></asp:Label>

                    </b>
                    <asp:TextBox ID="txtSippersCompanyName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b style="font-family:Arial, Helvetica, sans-serif; font-size:17px">
                        <asp:Label ID="Label3" runat="server" Text="Phone number:"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtSippersPhoneNumber" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <%--Button--%>
                <asp:Button ID="btnInsertShippers" runat="server" CssClass="btn btn-primary" Text="Insert" OnClick="BtnInsert_Click" />
                <asp:Button ID="btnUpdateShippers" runat="server" CssClass="btn btn-success" Text="Update" OnClick="btnUpdateShippers_Click" />
                <asp:Button ID="btnDeleteShippers" runat="server" CssClass="btn btn-danger" Text="Delete" OnClick="btnDeleteShippers_Click" />
                <asp:Button ID="btnSearchShippers" runat="server" CssClass="btn btn-info" Text="Search" OnClick="btnSearchShippers_Click1" />
                <asp:Button ID="btnClearShippersForm" runat="server" CssClass="btn btn-secondary" Text="Clear" OnClick="btnClearShippersForm_Click" />
            </div>
            <div class="col-6"></div>
        </div>


        <br />
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewShippers" CssClass="table table-striped table-hover" runat="server" AutoGenerateColumns="False" DataKeyNames="ShipperID" DataSourceID="SqlDataSourceListGridViewShippers">
                    <Columns>
                        <asp:BoundField DataField="ShipperID" HeaderText="ShipperID" InsertVisible="False" ReadOnly="True" SortExpression="ShipperID" />
                        <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
                        <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListGridViewShippers" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [ShipperID], [CompanyName], [Phone] FROM [TblShippers]"></asp:SqlDataSource>
            </div>
        </div>

    </div>
</asp:Content>
