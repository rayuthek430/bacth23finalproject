﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormOrders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertShippers_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertOrders = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertOrders = new SqlCommand("INSERT INTO [dbo].[TblOrders] ([CustomerID],[EmployeeID],[OrderDate],[RequiredDate],[ShippedDate],[ShpperID],[Freight],[ShipName],[ShipAddress],[ShipCity],[ShipRegion],[ShipPostalCode],[ShipCountry]) VALUES (@CustomerID,@EmployeeID,@OrderDate ,@RequiredDate,@ShippedDate,@ShpperID,@Freight,@ShipName,@ShipAddress,@ShipCity,@ShipRegion,@ShipPostalCode,@ShipCountry)", con);

            InsertOrders.Parameters.AddWithValue("@CustomerID", txtCustomerID.Text);
            InsertOrders.Parameters.AddWithValue("@EmployeeID", txtEmployeeID.Text);
            InsertOrders.Parameters.AddWithValue("@OrderDate", txtOrderDate.Text);
            InsertOrders.Parameters.AddWithValue("@RequiredDate", txtRequiredDate.Text);
            InsertOrders.Parameters.AddWithValue("@ShippedDate", txtShippedDate.Text);
            InsertOrders.Parameters.AddWithValue("@ShpperID", txtShipperID.Text);
            InsertOrders.Parameters.AddWithValue("@Freight", txtFreight.Text);
            InsertOrders.Parameters.AddWithValue("@ShipName", txtShippName.Text);
            InsertOrders.Parameters.AddWithValue("@ShipAddress", txtShippAdress.Text);
            InsertOrders.Parameters.AddWithValue("@ShipCity", txtShippCity.Text);
            InsertOrders.Parameters.AddWithValue("@ShipRegion", txtShippRegion.Text);
            InsertOrders.Parameters.AddWithValue("@ShipPostalCode", txtPostalCode.Text);
            InsertOrders.Parameters.AddWithValue("@ShipCountry", txtShipCountry.Text);

            con.Open();
            InsertOrders.ExecuteNonQuery();
            con.Close();
            GridViewOrder.DataBind();
        }

        protected void btnSearchShippers_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [OrderID],[CustomerID],[EmployeeID],[OrderDate],[RequiredDate],[ShippedDate],[ShpperID],[Freight],[ShipName],[ShipAddress],[ShipCity],[ShipRegion],[ShipPostalCode],[ShipCountry] FROM [dbo].[TblOrders] WHERE OrderID = " + txtOrdersID.Text; // your id
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {

                txtOrdersID.Text = DTable1.Rows[0]["OrderID"].ToString();
                txtCustomerID.Text = DTable1.Rows[0]["CustomerID"].ToString();
                txtEmployeeID.Text = DTable1.Rows[0]["EmployeeID"].ToString();
                txtOrderDate.Text = DTable1.Rows[0]["OrderDate"].ToString();
                txtRequiredDate.Text = DTable1.Rows[0]["RequiredDate"].ToString();
                txtShippedDate.Text = DTable1.Rows[0]["ShippedDate"].ToString();
                txtShipperID.Text = DTable1.Rows[0]["ShpperID"].ToString();
                txtFreight.Text = DTable1.Rows[0]["Freight"].ToString();
                txtShippName.Text = DTable1.Rows[0]["ShipName"].ToString();
                txtShippAdress.Text = DTable1.Rows[0]["ShipAddress"].ToString();
                txtShippCity.Text = DTable1.Rows[0]["ShipCity"].ToString();
                txtShippRegion.Text = DTable1.Rows[0]["ShipRegion"].ToString();
                txtPostalCode.Text = DTable1.Rows[0]["ShipPostalCode"].ToString();
                txtShipCountry.Text = DTable1.Rows[0]["ShipCountry"].ToString();
                // id
            } else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('មិនមាន !');", true); ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('មិនមាន !');", true);

            }

            

            con.Close();
        }

        protected void btnUpdateShippers_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdateOrders = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
             
            UpdateOrders = new SqlCommand("UPDATE [dbo].[TblOrders] SET [CustomerID] = @CustomerID,[EmployeeID] = @EmployeeID,[OrderDate] = @OrderDate,[RequiredDate] = @RequiredDate,[ShippedDate] = @ShippedDate,[ShpperID] = @ShpperID,[Freight] = @Freight,[ShipName] = @ShipName,[ShipAddress] = @ShipAddress,[ShipCity] = @ShipCity,[ShipRegion] = @ShipRegion,[ShipPostalCode] = @ShipPostalCode,[ShipCountry] = @ShipCountry WHERE OrderID = @OrderID", con);

            UpdateOrders.Parameters.AddWithValue("@OrderID", txtOrdersID.Text);
            UpdateOrders.Parameters.AddWithValue("@CustomerID", txtCustomerID.Text);
            UpdateOrders.Parameters.AddWithValue("@EmployeeID", txtEmployeeID.Text);
            UpdateOrders.Parameters.AddWithValue("@OrderDate", txtOrderDate.Text);
            UpdateOrders.Parameters.AddWithValue("@RequiredDate", txtRequiredDate.Text);
            UpdateOrders.Parameters.AddWithValue("@ShippedDate", txtShippedDate.Text);
            UpdateOrders.Parameters.AddWithValue("@ShpperID", txtShipperID.Text);
            UpdateOrders.Parameters.AddWithValue("@Freight", txtFreight.Text);
            UpdateOrders.Parameters.AddWithValue("@ShipName", txtShippName.Text);
            UpdateOrders.Parameters.AddWithValue("@ShipAddress", txtShippAdress.Text);
            UpdateOrders.Parameters.AddWithValue("@ShipCity", txtShippCity.Text);
            UpdateOrders.Parameters.AddWithValue("@ShipRegion", txtShippRegion.Text);
            UpdateOrders.Parameters.AddWithValue("@ShipPostalCode", txtPostalCode.Text);
            UpdateOrders.Parameters.AddWithValue("@ShipCountry", txtShipCountry.Text);

            con.Open();
            UpdateOrders.ExecuteNonQuery();
            con.Close();
            GridViewOrder.DataBind();
        }

        protected void btnDeleteShippers_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteOrder = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteOrder = new SqlCommand("DELETE FROM [dbo].[TblOrders] WHERE OrderID = @OrderID", con);

            DeleteOrder.Parameters.AddWithValue("@OrderID", txtOrdersID.Text);

            con.Open();
            DeleteOrder.ExecuteNonQuery();
            con.Close();
            GridViewOrder.DataBind();

            // Clear
            txtCustomerID.Text = "";
            txtEmployeeID.Text = "";
            txtOrderDate.Text = "";
            txtRequiredDate.Text = "";
            txtShippedDate.Text = "";
            txtShipperID.Text = "";
            txtFreight.Text = "";
            txtShippName.Text = "";
            txtShippAdress.Text = "";
            txtShippCity.Text = "";
            txtShippRegion.Text = "";
            txtPostalCode.Text = "";
            txtShipCountry.Text = "";
        }

        protected void btnClearShippers_Click(object sender, EventArgs e)
        {
            txtCustomerID.Text = "";
            txtEmployeeID.Text = "";
            txtOrderDate.Text = "";
            txtRequiredDate.Text = "";
            txtShippedDate.Text = "";
            txtShipperID.Text = "";
            txtFreight.Text = "";
            txtShippName.Text = "";
            txtShippAdress.Text = "";
            txtShippCity.Text = "";
            txtShippRegion.Text = "";
            txtPostalCode.Text = "";
            txtShipCountry.Text = "";
        }
    }
}