﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class DocumentBuyer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertAds_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DocumentBuyer = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DocumentBuyer = new SqlCommand("INSERT INTO [dbo].[TblDocumentBuyer] ([DocumentBuyerName],[DocumentBuyerAttach],[OrderDetailID]) VALUES (@DocumentBuyerName,@DocumentBuyerAttach,@OrderDetailID)", con);

            DocumentBuyer.Parameters.AddWithValue("@DocumentBuyerName", txtDocumentBuyerName.Text);
            DocumentBuyer.Parameters.AddWithValue("@OrderDetailID", BuyerOrderDetailID.Text);



            if (UploadDocumentBuyerAttch.HasFile)
            {
                UploadDocumentBuyerAttch.SaveAs(Server.MapPath("images/" + UploadDocumentBuyerAttch.FileName));
                DocumentBuyerURL.Text = "images/" + UploadDocumentBuyerAttch.FileName;
            }
            DocumentBuyerAttch.ImageUrl = DocumentBuyerURL.Text;
            DocumentBuyer.Parameters.AddWithValue("@DocumentBuyerAttach", DocumentBuyerURL.Text);



            con.Open();
            DocumentBuyer.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('បានជោគជ័យ!', 'ការបញ្ចូលទិន្នន័យបានជោគជ័យ.', 'success');", true);
            GridViewListDocumentBuyer.DataBind();
        }

        protected void btnSearchAds_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [DocumentBuyerID] ,[DocumentBuyerName],[DocumentBuyerAttach],[OrderDetailID] FROM [dbo].[TblDocumentBuyer] where DocumentBuyerID = " + txtDocumentBuyerID.Text;
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {
                txtDocumentBuyerID.Text = DTable1.Rows[0]["DocumentBuyerID"].ToString();

                txtDocumentBuyerName.Text = DTable1.Rows[0]["DocumentBuyerName"].ToString();
                BuyerOrderDetailID.Text = DTable1.Rows[0]["OrderDetailID"].ToString();


                DocumentBuyerAttch.ImageUrl = DTable1.Rows[0]["DocumentBuyerAttach"].ToString();
            }

            con.Close();
        }

        protected void btnUpdateAds_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdateDocumentBuyer = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdateDocumentBuyer = new SqlCommand("UPDATE [dbo].[TblDocumentBuyer] SET [DocumentBuyerName] = @DocumentBuyerName,[DocumentBuyerAttach] = @DocumentBuyerAttach,[OrderDetailID] = @OrderDetailID WHERE DocumentBuyerID =@DocumentBuyerID", con);

            UpdateDocumentBuyer.Parameters.AddWithValue("@DocumentBuyerID", txtDocumentBuyerID.Text);
            UpdateDocumentBuyer.Parameters.AddWithValue("@DocumentBuyerName", txtDocumentBuyerName.Text);
            UpdateDocumentBuyer.Parameters.AddWithValue("@OrderDetailID", BuyerOrderDetailID.Text);



            if (UploadDocumentBuyerAttch.HasFile)
            {
                UploadDocumentBuyerAttch.SaveAs(Server.MapPath("images/" + UploadDocumentBuyerAttch.FileName));
                DocumentBuyerURL.Text = "images/" + UploadDocumentBuyerAttch.FileName;
            }
            DocumentBuyerAttch.ImageUrl = DocumentBuyerURL.Text;
            UpdateDocumentBuyer.Parameters.AddWithValue("@DocumentBuyerAttach", DocumentBuyerURL.Text);



            con.Open();
            UpdateDocumentBuyer.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('បានជោគជ័យ!', 'ការបញ្ចូលទិន្នន័យបានជោគជ័យ.', 'success');", true);
            GridViewListDocumentBuyer.DataBind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteDocumentBuyer = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteDocumentBuyer = new SqlCommand("DELETE FROM [dbo].[TblDocumentBuyer] WHERE DocumentBuyerID =@DocumentBuyerID", con);

            DeleteDocumentBuyer.Parameters.AddWithValue("@DocumentBuyerID", txtDocumentBuyerID.Text);

            con.Open();
            DeleteDocumentBuyer.ExecuteNonQuery();
            con.Close();
            //GridViewProduct.DataBind();
            if (txtDocumentBuyerName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('មិនមានទិន្នន័យ', 'សូមរើសយកទិន្នន័យណាមួយ.', 'success');", true);
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('បានជោគជ័យ!', 'ទិន្នន័យលុបបានជោគជ័យ.', 'success');", true);

            txtDocumentBuyerName.Text = "";
            GridViewListDocumentBuyer.DataBind();
        }

        protected void btnClearsAds_Click(object sender, EventArgs e)
        {
            txtDocumentBuyerID.Text = "";
            txtDocumentBuyerID.Focus();
        }
    }
}