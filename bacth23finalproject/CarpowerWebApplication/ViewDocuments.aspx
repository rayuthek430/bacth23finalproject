﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="ViewDocuments.aspx.cs" Inherits="CarpowerWebApplication.ViewDocuments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <asp:DataList ID="DataListOrderDoc" Width="100%" runat="server" DataKeyField="OrderID" DataSourceID="SqlDataSourceOrderDoc">
            <ItemTemplate>
                <div class="list-group" style="font-family:'Khmer OS Siemreap';">
                    <a href="#" class="list-group-item list-group-item-action active">លេខវិក័យបត្រ:
            <asp:Label ID="OrderIDLabel" runat="server" Text='<%# Eval("OrderID") %>' />

                        អតិថិជន:
            <asp:Label ID="CustomerNameLabel" runat="server" Text='<%# Eval("CustomerName") %>' />

                        ម្ចាស់ឃ្លាំង:
            <asp:Label ID="EmployeeLabel" runat="server" Text='<%# Eval("Employee") %>' />

                        តម្លៃសរុប:
            <asp:Label ID="AmountLabel" runat="server" Text='<%# Eval("Amount") %>' />

                        ស្ថានភាព:
            <asp:Label ID="StatusNameLabel" runat="server" Text='<%# Eval("StatusName") %>' />

                        ថ្ងៃបញ្ជាទិញ:
            <asp:Label ID="OrderDateLabel" runat="server" Text='<%# Eval("OrderDate") %>' />
                    </a>

                    <div class="row">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-lg-11">

                            <asp:DataList ID="DataListOrderDetailDoc" Width="100%" runat="server" DataKeyField="OrderDetailID" DataSourceID="SqlDataSourceOrderDetailDoc">
                                <ItemTemplate>
                                    <a href="#" class="list-group-item list-group-item-action">
                                        <asp:Image ID="ListImg" runat="server" Width="80" ImageUrl='<%# Eval("image") %>' />
                                        លេខសំគាល់:
                    <asp:Label ID="OrderDetailIDLabel" runat="server" Text='<%# Eval("OrderDetailID") %>' />

                                        ឈ្មោះផលិតផល:
                    <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' />



                                        តម្លៃ:
                    <asp:Label ID="UnitPriceLabel" runat="server" Text='<%# Eval("UnitPrice") %>' />

                                        ចំនួន:
                    <asp:Label ID="QuantityLabel" runat="server" Text='<%# Eval("Quantity") %>' />

                                        សរុប:
                    <asp:Label ID="AmountLabel" runat="server" Text='<%# Eval("Amount") %>' />
                                    </a>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-1">
                                        </div>
                                        <div class="col-lg-11">
                                            <div class="list-group">
                                                <a href="#" class="list-group-item list-group-item-action active">
                                                    <asp:DataList ID="DataListOrderDetailDocuments" runat="server" RepeatDirection="Horizontal" DataKeyField="DocumentBuyerID" DataSourceID="SqlDataSourceOrderDetailDocuments">
                                                        <ItemTemplate>
                                                            <a target="_blank" style="color: white;" href="<%# Eval("DocumentBuyerAttach") %>">



                                                                <asp:Label ID="DocumentBuyerTypeNameLabel" runat="server" Text='<%# Eval("DocumentBuyerTypeName") %>' />
                                                                , 
                                                            </a>

                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:SqlDataSource ID="SqlDataSourceOrderDetailDocuments" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblDocumentBuyer.DocumentBuyerID, TblDocumentBuyer.DocumentBuyerName, TblDocumentBuyer.DocumentBuyerAttach, TblDocumentBuyerType.DocumentBuyerTypeName FROM TblDocumentBuyer INNER JOIN TblDocumentBuyerType ON TblDocumentBuyer.DocumentBuyerTypeID = TblDocumentBuyerType.DocumentBuyerTypeID WHERE (TblDocumentBuyer.OrderDetailID = @OrderDetailID)">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="OrderDetailIDLabel" Name="OrderDetailID" PropertyName="Text" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </div>
                    <asp:SqlDataSource ID="SqlDataSourceOrderDetailDoc" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrderDetails.OrderDetailID, TblProduct.ProductName, TblProduct.image, TblOrderDetails.UnitPrice, TblOrderDetails.Quantity, TblOrderDetails.Amount FROM TblOrderDetails INNER JOIN TblProduct ON TblOrderDetails.ProductID = TblProduct.ProductID WHERE (TblOrderDetails.OrderID = @OrderID)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="OrderIDLabel" Name="OrderID" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
            </ItemTemplate>
        </asp:DataList>
        <asp:SqlDataSource ID="SqlDataSourceOrderDoc" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrders.OrderID, TblUser.FullName AS CustomerName, TblUser_1.FullName AS Employee, TblOrders.Amount, TblStatusOrder.StatusName, TblOrders.OrderDate FROM TblUser AS TblUser_1 INNER JOIN TblOrders INNER JOIN TblUser ON TblOrders.CustomerID = TblUser.UserID ON TblUser_1.UserID = TblOrders.EmployeeID INNER JOIN TblStatusOrder ON TblOrders.StatusID = TblStatusOrder.StatusID  order  by TblOrders.OrderID Desc"></asp:SqlDataSource>
    </div>

</asp:Content>
