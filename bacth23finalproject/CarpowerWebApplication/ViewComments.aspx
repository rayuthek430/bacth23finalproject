﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="ViewComments.aspx.cs" Inherits="CarpowerWebApplication.ViewComments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <br />
        <h2 style="font-family:'Khmer OS Siemreap';">តប Comment</h2>
        <br />
        <div class="row" style="display:none;">
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label1" runat="server" Text="Reply id"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtReplyID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-4">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label3" runat="server" Text="Comment id"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtCommentID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>Text Comment
                    </b>
                    <asp:TextBox ID="TxtTextComment" Enabled="false" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label2" runat="server" Text="Reply"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtReply" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-6">
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <asp:Button ID="BtnInserComment" runat="server" CssClass="btn btn-outline-primary" Text="Insert" OnClick="BtnInserComment_Click" />
                </div>
            </div>
        </div>
        <br />


        <asp:DataList ID="DataListOrderDoc" Width="100%" runat="server" DataKeyField="OrderID" DataSourceID="SqlDataSourceOrderDoc">
            <ItemTemplate>
                <div class="list-group" style="font-family:'Khmer OS Siemreap';">
                    <a href="#" class="list-group-item list-group-item-action active">លេខវិក័យបត្រ:
            <asp:Label ID="OrderIDLabel" runat="server" Text='<%# Eval("OrderID") %>' />

                        ស្ថានភាព:
            <asp:Label ID="StatusNameLabel" runat="server" Text='<%# Eval("StatusName") %>' />

                        ថ្ងៃបញ្ជាទិញ:
            <asp:Label ID="OrderDateLabel" runat="server" Text='<%# Eval("OrderDate") %>' />
                        អតិថិជន:
            <asp:Label ID="CustomerNameLabel" runat="server" Text='<%# Eval("CustomerName") %>' />

                        ម្ចាស់ឃ្លាំង:
            <asp:Label ID="EmployeeLabel" runat="server" Text='<%# Eval("Employee") %>' />

                        តម្លៃសរុប:
            <asp:Label ID="AmountLabel" runat="server" Text='<%# Eval("Amount") %>' />

                    </a>
                    <div class="row">
                        <div class="col-lg-1">
                        </div>
                        <div class="col-lg-11">

                            <asp:DataList ID="DataListOrderDetailDoc" Width="100%" runat="server" DataKeyField="OrderDetailID" DataSourceID="SqlDataSourceOrderDetailDoc">
                                <ItemTemplate>
                                    <a href="#" class="list-group-item list-group-item-action">
                                        <asp:Image ID="ListImg" runat="server" Width="80" ImageUrl='<%# Eval("image") %>' />
                                        លេខសំគាល់:
                    <asp:Label ID="OrderDetailIDLabel" runat="server" Text='<%# Eval("OrderDetailID") %>' />

                                        ឈ្មោះផលិតផល:
                    <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' />



                                        តម្លៃ:
                    <asp:Label ID="UnitPriceLabel" runat="server" Text='<%# Eval("UnitPrice") %>' />

                                        ចំនួន:
                    <asp:Label ID="QuantityLabel" runat="server" Text='<%# Eval("Quantity") %>' />

                                        សរុប:
                    <asp:Label ID="AmountLabel" runat="server" Text='<%# Eval("Amount") %>' />
                                    </a>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-1">
                                        </div>
                                        <div class="col-lg-11">
                                            <asp:DataList Width="100%" ID="DataListOrderDetailComments" runat="server" DataKeyField="CommentID" DataSourceID="SqlDataSourceOrderDetailComments" OnItemCommand="DataListOrderDetailComments_ItemCommand">
                                                <ItemTemplate>
                                                    <div class="list-group">
                                                        <div class="row">
                                                            <div class="col-lg-12">

                                                                <a href="#" class="list-group-item list-group-item-action active">
                                                                    <asp:Label Visible="false" ID="CommentIDLabel" runat="server" Text='<%# Eval("CommentID") %>' />

                                                                    សំណួរអតិថិជន: [<asp:Label ID="FullNameLabel" runat="server" Text='<%# Eval("FullName") %>' />
                                                                    |
                                                            <asp:Label ID="CommentDateLabel" runat="server" Text='<%# Eval("CommentDate") %>' />]
                                            <br />
                                                                    <asp:Label ID="CommentLabel" runat="server" Text='<%# Eval("Comment") %>' />
                                                                    <br />

                                                                </a>
                                                                <asp:LinkButton ID="LbtnReplyComment" CommandName="Comment" CommandArgument='<%# Eval("CommentID") %>' runat="server">Reply Now</asp:LinkButton>
                                                                |
                                                                <asp:LinkButton ID="LbtnRemoveReply" CommandName="RemoveComment" CommandArgument='<%# Eval("CommentID") %>' runat="server">Remove</asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-1">
                                                            </div>
                                                            <div class="col-lg-11">
                                                                <asp:DataList Width="100%" ID="DataListOrderDetailReply" runat="server" DataKeyField="ReplyID" DataSourceID="SqlDataSourceOrderDetailReply" OnItemCommand="DataListOrderDetailReply_ItemCommand">
                                                                    <ItemTemplate>
                                                                        <a href="#" class="list-group-item list-group-item-action">
                                                                            <asp:Label ID="ReplyIDLabel" Visible="false" runat="server" Text='<%# Eval("ReplyID") %>' />

                                                                            ការឆ្លើយតប: [<asp:Label ID="FullNameLabel" runat="server" Text='<%# Eval("FullName") %>' />
                                                                            |
                                                                            <asp:Label ID="ReplyDateLabel" runat="server" Text='<%# Eval("ReplyDate") %>' />]
                                                   <br />
                                                                            <asp:Label ID="ReplyLabel" ForeColor="Blue" runat="server" Text='<%# Eval("Reply") %>' />
                                                                            <br />
                                                                            <asp:LinkButton ID="LbtnReply" CommandName="Reply" CommandArgument='<%# Eval("ReplyID") %>' runat="server">Reply Now</asp:LinkButton>
                                                                            |
                                                                            <asp:LinkButton ID="LbtnRemoveReply" CommandName="RemoveReply" CommandArgument='<%# Eval("ReplyID") %>' runat="server">Remove</asp:LinkButton>
                                                                        </a>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </div>
                                                        </div>
                                                        <asp:SqlDataSource ID="SqlDataSourceOrderDetailReply" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblReply.CommentID, TblReply.ReplyID, TblReply.Reply, TblUser.FullName, TblReply.ReplyDate FROM TblReply INNER JOIN TblUser ON TblReply.UserID = TblUser.UserID WHERE (TblReply.CommentID = @CommentID)">
                                                            <SelectParameters>
                                                                <asp:ControlParameter ControlID="CommentIDLabel" Name="CommentID" PropertyName="Text" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                    </div>
                                    <asp:SqlDataSource ID="SqlDataSourceOrderDetailComments" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblComment.CommentID,TblComment.CommentDate, TblComment.Comment, TblUser.FullName FROM TblComment INNER JOIN TblUser ON TblComment.UserID = TblUser.UserID WHERE (TblComment.OrderDetailID = @OrderDetailID)">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="OrderDetailIDLabel" Name="OrderDetailID" PropertyName="Text" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </div>
                    <asp:SqlDataSource ID="SqlDataSourceOrderDetailDoc" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrderDetails.OrderDetailID, TblProduct.ProductName, TblProduct.image, TblOrderDetails.UnitPrice, TblOrderDetails.Quantity, TblOrderDetails.Amount FROM TblOrderDetails INNER JOIN TblProduct ON TblOrderDetails.ProductID = TblProduct.ProductID WHERE (TblOrderDetails.OrderID = @OrderID)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="OrderIDLabel" Name="OrderID" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>
            </ItemTemplate>
        </asp:DataList>
        <asp:SqlDataSource ID="SqlDataSourceOrderDoc" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrders.OrderID, TblUser.FullName AS CustomerName, TblUser_1.FullName AS Employee, TblOrders.Amount, TblStatusOrder.StatusName, TblOrders.OrderDate FROM TblUser AS TblUser_1 INNER JOIN TblOrders INNER JOIN TblUser ON TblOrders.CustomerID = TblUser.UserID ON TblUser_1.UserID = TblOrders.EmployeeID INNER JOIN TblStatusOrder ON TblOrders.StatusID = TblStatusOrder.StatusID Order  by TblOrders.OrderID Desc"></asp:SqlDataSource>
    </div>
</asp:Content>
