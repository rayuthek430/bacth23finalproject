﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class ListProductComments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["SUserID"] != null)
            {
                txtUserID.Text = Session["SUserID"].ToString();
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            try
            {
                txtOrderDetailID.Text = Request.QueryString["OrderDetailID"];
            }
            catch (Exception)
            { }
        }

        protected void BtnInserComment_Click(object sender, EventArgs e)
        {
            SqlCommand InsertComment = new SqlCommand();
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            InsertComment = new SqlCommand("INSERT INTO [dbo].[TblComment] ([Comment],[OrderDetailID],[UserID]) VALUES (@Comment,@OrderDetailID,@UserID)", con);

            InsertComment.Parameters.AddWithValue("@Comment", txtComment.Text);
            InsertComment.Parameters.AddWithValue("@OrderDetailID", txtOrderDetailID.Text);
            InsertComment.Parameters.AddWithValue("@UserID", txtUserID.Text);


            con.Open();
            InsertComment.ExecuteNonQuery();
            con.Close();
            DataListProductComments.DataBind();
        }

        protected void DataListProductComments_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "BuyerReply")
            {
                txtcommentID.Text = e.CommandArgument.ToString();
                BtnInserComment.Visible = false;
                BtnInsertReply.Visible = true;
            }

            if (e.CommandName == "RemoveComment")
            {
                SqlCommand DeleteComment = new SqlCommand();
                string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
                SqlConnection con = new SqlConnection(conStr);
                DeleteComment = new SqlCommand("delete from TblReply where CommentID=@CommentID;DELETE FROM [dbo].[TblComment] WHERe CommentID=@CommentID and UserID=@UserID", con);

                DeleteComment.Parameters.AddWithValue("@UserID", Session["SUserID"]);

                DeleteComment.Parameters.AddWithValue("@CommentID", e.CommandArgument.ToString());

                con.Open();
                DeleteComment.ExecuteNonQuery();
                con.Close();
                DataListProductComments.DataBind();

            }
        }

        protected void DataListProductReply_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "SellerReply")
            {
                txtcommentID.Text = e.CommandArgument.ToString();
                BtnInserComment.Visible = false;
                BtnInsertReply.Visible = true;
            }
            if (e.CommandName == "RemoveReply")
            {
                SqlCommand DeleteReply = new SqlCommand();

                string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
                SqlConnection con = new SqlConnection(conStr);
                DeleteReply = new SqlCommand("DELETE FROM [dbo].[TblReply] WHERE ReplyID=@ReplyID and UserID=@UserID", con);
                DeleteReply.Parameters.AddWithValue("@UserID", Session["SUserID"]);
                DeleteReply.Parameters.AddWithValue("@ReplyID", e.CommandArgument.ToString());

                con.Open();
                DeleteReply.ExecuteNonQuery();
                con.Close();
                DataListProductComments.DataBind();
            }
        }

        protected void BtnInsertReply_Click(object sender, EventArgs e)
        {
            SqlCommand ReplyComment = new SqlCommand();
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            ReplyComment = new SqlCommand("INSERT INTO [dbo].[TblReply]([Reply],[CommentID],[UserID]) VALUES (@Reply,@CommentID,@UserID)", con);

            ReplyComment.Parameters.AddWithValue("@Reply", txtComment.Text);
            ReplyComment.Parameters.AddWithValue("@CommentID", txtcommentID.Text);
            ReplyComment.Parameters.AddWithValue("@UserID", txtUserID.Text);


            con.Open();
            ReplyComment.ExecuteNonQuery();
            con.Close();
            DataListProductComments.DataBind();
        }

        protected void BtnNewComment_Click(object sender, EventArgs e)
        {
            BtnInserComment.Visible = true;
            BtnInsertReply.Visible = false;
        }
    }
}