﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="CarpowerWebApplication.ProductListByCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
    <style>
        #ContentPlaceHolder1_DataList2 {
            cursor:pointer;
            margin-bottom:20px;
        }
        .zoom-image:hover {
            transform: scale(1.2);
            transition: transform 0.3s ease;
            cursor: pointer;
        }
        #myModal{
            margin-top:159px;
        }
            
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid">

        <%--model--%>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <img id="modalImage" src="" alt="Modal Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
        <%--end model--%>

        <div id="imageModal" class="modal">
            <span class="close" onclick="closeModal()">&times;</span>
            <img id="currentImageView" class="modal-content" src="" alt="Current Image">
        </div>

        <%-- list product --%>
        <div class="row">
            <asp:DataList ID="DataListProctByCategory" runat="server" DataKeyField="ProductID" DataSourceID="SqlDataSourceListProductByCategory" OnItemCommand="DataListProctByCategory_ItemCommand">
                <ItemTemplate>
                    <div class="row gx-4 gx-lg-5 align-items-center my-5">
                        <div class="col-lg-5">
                            <%--<div id="carouselExample" class="carousel slide" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                    <asp:DataList ID="DataList2" runat="server" DataKeyField="ImageID" DataSourceID="SqlDataSource2" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                        <ItemStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <div class="carousel-item <%# Container.ItemIndex == 0 ? "active" : "" %>">
                                                <asp:Image ID="Image2" runat="server" CssClass="img-fluid zoom-image" ImageUrl='<%# Eval("ImageUrl") %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>--%>
                            <br />
                            <br />
                             <asp:Image ID="Image1" CssClass="img-fluid rounded mb-4 mb-lg-0" runat="server" ImageUrl='<%# Eval("image") %>' Width="400px" Height="300px" />
                        </div>
                       
                        <div class="col-lg-7">
                            <h1>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("ProductName") %>' /></h1>
                            <hr />
                            <asp:Label ID="Label3" runat="server" Text='<%# String.Format("{0:C0}", Eval("UnitPrice")) %>' ForeColor="red" Font-Bold="true" Font-Size="26px" />
                            <br />
                            <br />
                            <asp:LinkButton ID="LbtnOrderNow" runat="server" CommandName="OrderNow" CommandArgument='<%# Eval("ProductID") %>' CssClass="btn btn-outline-info btn-sm">
                               <span class="icon"><ion-icon name="cart-outline"></ion-icon> បញ្ជាទិញឥឡូវនេះ</span> 
                            </asp:LinkButton>
                            <br />

                            <asp:DataList ID="DataListDocument" runat="server" DataSourceID="SqlDataSourceListProductDocument">
                                <ItemTemplate>
                                    <br />
                                    <a href='<%# Eval("DocumentAttach") %>' target="_blank" style="color: red;">មើលឯកសារ</a>
                                </ItemTemplate>
                            </asp:DataList>
                            <asp:SqlDataSource ID="SqlDataSourceListProductDocument" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [DocumentAttach] FROM [TblDocument] WHERE ([ProducctID] = @ProducctID)">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="ProducctID" QueryStringField="ProductID" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <br />
                            <div id="disclaimer">
                                <p>
                                    ការមិនទទួលខុសត្រូវ / Disclaimer
                                <br>
                                    យើងខ្ញុំមិនគ្រប់គ្រងមាតិកា ដែលបានបង្ហោះឡើងដោយសមាជិកឡើយ។ ដូច្នេះយើងមិនទទួលខុសត្រូវលើការផ្សាយផលិតផលនេះទេ ហើយក៏មិនធានាចំពោះបញ្ហាដែលទាក់ទងដោយផ្ទាល់ ឬ ប្រយោលទៅនឹងសកម្មភាព ឬ អសកម្មណាមួយឡើយ។
                                </p>
                                <p>
                                    We does not control the content posted by members and therefore assumes no responsibility and disclaims any liability for any consequence relating directly or indirectly to any action or inaction.
                                </p>
                            </div>
                        </div>
                    </div>
                    <asp:Label ID="CategoryNameLabel" Visible="false" runat="server" Text='<%# Eval("CategoryName") %>' />
                    <asp:Label ID="ProductNameLabel" Visible="false" runat="server" Text='<%# Eval("ProductName") %>' />
                    <asp:Label ID="QuantityLabel" Visible="false" runat="server" Text='<%# Eval("Quantity") %>' />
                    <asp:Label ID="UnitPriceLabel" Visible="false" runat="server" Text='<%# Eval("UnitPrice") %>' />
                    <asp:Label ID="imageLabel" Visible="false" runat="server" Text='<%# Eval("image") %>' />
                    <asp:Label ID="ProductIDLabel" Visible="false" runat="server" Text='<%# Eval("ProductID") %>' />
                </ItemTemplate>
            </asp:DataList>
            <br />
            <%--List sub img--%>
            <asp:DataList ID="DataList2" runat="server" DataKeyField="ImageID" DataSourceID="SqlDataSource2" RepeatDirection="Horizontal" RepeatLayout="Flow">
                <ItemStyle HorizontalAlign="Left" />
                <ItemTemplate>
                    <asp:Label ID="ImageIDLabel" runat="server" Text='<%# Eval("ImageID") %>' Visible="False" />
                    <asp:Label ID="ImageNameLabel" runat="server" Text='<%# Eval("ImageName") %>' Visible="False" />
                    <asp:Label ID="ImageUrlLabel" runat="server" Text='<%# Eval("ImageUrl") %>' Visible="False" />
                    <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' Visible="False" />
                    <asp:Image ID="Image2" runat="server" CssClass="img-thumbnail small-image" ImageUrl='<%# Eval("ImageUrl") %>' Style="width: 100px; height: 100px; object-fit: cover" onclick='<%# "openModal(\"" + Eval("ImageUrl").ToString().Replace("\"", "\\\"") + "\"); return false;" %>' />

                </ItemTemplate>
            </asp:DataList>
           
            <br />
            <br />
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT * FROM [TblImage] WHERE ([ProductID] = @ProductID)">
                <SelectParameters>
                    <asp:QueryStringParameter Name="ProductID" QueryStringField="ProductID" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
             
            <%-- list detail--%>
            <asp:SqlDataSource ID="SqlDataSourceListProductByCategory" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Pro_ListProductDetail" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:QueryStringParameter Name="ProductID" QueryStringField="ProductID" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <br />
<br />
            <asp:DataList ID="DataList1" runat="server" DataKeyField="DescriptionID" DataSourceID="SqlDataSource1">
                <ItemTemplate>
                    <asp:Label ID="DescriptionIDLabel" runat="server" Text='<%# Eval("DescriptionID") %>' Visible="False" />
                    <asp:Label ID="ProductIDLabel" runat="server" Text='<%# Eval("ProductID") %>' Visible="False" />
                    <div class="row">
                        <div class="col-lg-1">
                            Name:
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("CarModel") %>' CssClass="text-info" Font-Bold="true" Font-Size="16px" />
                        </div>
                        <div class="col-lg-1">
                            Car makes:
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="CarMakesLabel" runat="server" Text='<%# Eval("CarMakes") %>' CssClass="text-info" Font-Bold="true" Font-Size="16px" />
                        </div>
                        <div class="col-lg-1">
                            Car model:
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="CarModelLabel" runat="server" Text='<%# Eval("CarModel") %>' CssClass="text-info" Font-Bold="true" Font-Size="16px" />
                        </div>
                        <div class="col-lg-1">
                            car year:
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="YearLabel" runat="server" Text='<%# Eval("Year") %>' CssClass="text-info" Font-Bold="true" Font-Size="16px" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1">
                            Car color:
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="ColorLabel" runat="server" Text='<%# Eval("Color") %>' CssClass="text-info" Font-Bold="true" Font-Size="16px" />
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="DescriptionDetailsProductLabel" runat="server" Text='<%# Eval("DescriptionDetailsProduct") %>' CssClass="text-black" Font-Bold="false" Font-Size="20px" />
                </ItemTemplate>
            </asp:DataList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT * FROM [TblDescription] WHERE ([ProductID] = @ProductID)">
                <SelectParameters>
                    <asp:QueryStringParameter Name="ProductID" QueryStringField="ProductID" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            <%--// list detail--%>
        </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
    <script>

        function openModal(imageUrl) {
            document.getElementById('modalImage').src = imageUrl;
            $('#myModal').modal('show');
        }

        /* build zoom*/
        document.addEventListener("DOMContentLoaded", function () {
            var zoomImages = document.querySelectorAll(".zoom-image");

            zoomImages.forEach(function (image) {
                image.addEventListener("mousemove", function (event) {
                    var boundingRect = image.getBoundingClientRect();
                    var offsetX = event.clientX - boundingRect.left;
                    var offsetY = event.clientY - boundingRect.top;
                    var scale = 1.5;

                    image.style.transformOrigin = offsetX + "px " + offsetY + "px";
                    image.style.transform = "scale(" + scale + ")";
                });

                image.addEventListener("mouseleave", function () {
                    image.style.transformOrigin = "center center";
                    image.style.transform = "scale(1)";
                });
            });
        });
    </script>



</asp:Content>
