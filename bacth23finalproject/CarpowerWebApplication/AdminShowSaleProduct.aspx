﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="AdminShowSaleProduct.aspx.cs" Inherits="CarpowerWebApplication.AdminShowSaleProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <br />
    
 <div class="row">
     <div class="col-6">
         <div class="mb-3" style="font-family: 'Khmer OS Siemreap'; font-size: medium;margin-right:200px";>
             <h5>បង្ហាញការបញ្ជាទិញ</h5>
         </div>
     </div>
     <div class="col-6">
         

     </div>
 </div>


 <br />
 <br />
 <div class="container">
     <div class="col-lg-12">
         <asp:GridView ID="GridViewListOrderProduct" CssClass="table table-bordered border-primary" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceListOrderProduct" OnSelectedIndexChanged="GridViewListOrderProduct_SelectedIndexChanged" OnRowCommand="GridViewListOrderProduct_RowCommand" Font-Names="Khmer OS Siemreap">
             <Columns>
                 <asp:BoundField DataField="OrderDetailID" HeaderText="លេខសម្គាល់" SortExpression="OrderDetailID" />
                 <asp:BoundField DataField="ProductName" HeaderText="ឈ្មោះទំនិញ" SortExpression="ProductName" />
                 <asp:BoundField DataField="UnitPrice" HeaderText="តម្លៃ" SortExpression="UnitPrice" />
                 <asp:BoundField DataField="Quantity" HeaderText="ចំនូន" SortExpression="Quantity" />
                 <asp:BoundField DataField="Amount" HeaderText="ចំនួនទឹកប្រាក់" SortExpression="Amount" />
                 <asp:BoundField DataField="OrderDate" HeaderText="កាល​បរិច្ឆេទ​បញ្ជា​ទិញ" SortExpression="OrderDate" />
                 <asp:TemplateField HeaderText="រូបភាព" SortExpression="image">
                     <EditItemTemplate>
                         <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox>
                     </EditItemTemplate>
                     <ItemTemplate>
                         <asp:Image ID="Image1" runat="server" Height="96px" ImageUrl='<%# Eval("image") %>' Width="84px" />
                     </ItemTemplate>
                 </asp:TemplateField>
                 <asp:BoundField DataField="StatusName" HeaderText="ស្ថានភាព" SortExpression="StatusName" />
                 
                
                  <asp:TemplateField ShowHeader="False">
                     <ItemTemplate>
                         <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="ViewInvoice" CommandArgument='<%# Eval("OrderID") %>' Text="វិក្កយបត្រ"></asp:LinkButton>
                     </ItemTemplate>
                 </asp:TemplateField>
             </Columns>
         </asp:GridView>
     </div>
 </div>
 <asp:SqlDataSource ID="SqlDataSourceListOrderProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrderDetails.OrderID, TblOrderDetails.OrderDetailID,TblProduct.ProductName, TblOrderDetails.UnitPrice, TblOrderDetails.Quantity, TblOrderDetails.Amount, TblOrders.OrderDate, TblProduct.image, TblStatusOrder.StatusName FROM TblOrderDetails INNER JOIN TblProduct ON TblOrderDetails.ProductID = TblProduct.ProductID INNER JOIN TblOrders ON TblOrderDetails.OrderID = TblOrders.OrderID INNER JOIN TblStatusOrder ON TblOrderDetails.StatusID = TblStatusOrder.StatusID WHERE (TblOrders.Ordered = 1) and (TblOrderDetails.StatusID=3) ORDER BY TblOrderDetails.OrderDetailID DESC">
    
     </asp:SqlDataSource>

</asp:Content>
