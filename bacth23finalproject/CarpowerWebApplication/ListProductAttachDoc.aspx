﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="ListProductAttachDoc.aspx.cs" Inherits="CarpowerWebApplication.ListProductAttachDoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="col-lg-12">
        <asp:GridView ID="GridViewListOrderProduct" CssClass="table table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceListOrderProduct">
            <Columns>
                <asp:BoundField DataField="OrderDetailID" HeaderText="ID" Visible="false" SortExpression="OrderDetailID" />
                <asp:BoundField DataField="ProductName" HeaderText="ប្រភេទទ្បាន" SortExpression="ProductName" />
                <asp:BoundField DataField="UnitPrice" HeaderText="តម្" SortExpression="UnitPrice" />
                <asp:BoundField DataField="Quantity" HeaderText="ចំនួន" SortExpression="Quantity" />
                <asp:BoundField DataField="Amount" HeaderText="ចំនួនទឹកប្រាក់" SortExpression="Amount" />
                <%--<asp:BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate" />--%>
                <asp:TemplateField HeaderText="រូបភាព" SortExpression="image">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" Height="96px" ImageUrl='<%# Eval("image") %>' Width="84px" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="StatusName" HeaderText="ស្ថានភាព" SortExpression="StatusName" />

            </Columns>
        </asp:GridView>
    </div>
    <asp:SqlDataSource ID="SqlDataSourceListOrderProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrderDetails.OrderDetailID,TblProduct.ProductName, TblOrderDetails.UnitPrice, TblOrderDetails.Quantity, (TblOrderDetails.UnitPrice * TblOrderDetails.Quantity)as Amount, TblOrders.OrderDate, TblProduct.image, TblStatusOrder.StatusName FROM TblOrderDetails INNER JOIN TblProduct ON TblOrderDetails.ProductID = TblProduct.ProductID INNER JOIN TblOrders ON TblOrderDetails.OrderID = TblOrders.OrderID INNER JOIN TblStatusOrder ON TblOrderDetails.StatusID = TblStatusOrder.StatusID WHERE (TblOrderDetails.OrderDetailID = @OrderDetailID) ORDER BY TblOrderDetails.OrderDetailID DESC">
        <SelectParameters>
            <asp:QueryStringParameter Name="OrderDetailID" QueryStringField="OrderDetailID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />

    <div class="row" runat="server" id="Buyer">


        <asp:TextBox ID="txtOrderDetailID" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>


        <asp:TextBox ID="TxtDocumentBuyerAttachURL" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>

        <div class="col-2">
            Document Type:<br />
            <asp:DropDownList ID="DDLDocumentBuyerType" CssClass="form-control" runat="server" DataSourceID="SqlDataSourceListDocumentBuyerType" DataTextField="DocumentBuyerTypeName" DataValueField="DocumentBuyerTypeID"></asp:DropDownList>
            <asp:SqlDataSource ID="SqlDataSourceListDocumentBuyerType" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [DocumentBuyerTypeID], [DocumentBuyerTypeName] FROM [TblDocumentBuyerType]"></asp:SqlDataSource>
        </div>
        <div class="col-4">
            <div class="mb-3">
                Document Name:<br />
                <asp:TextBox ID="txtDocumentBuyerAttach" runat="server" CssClass="form-control"></asp:TextBox>

            </div>
        </div>
        <div class="col-4">
            <div class="mb-3">

                <br />
                <asp:FileUpload ID="FileUploadDocumentBuyerAttach" CssClass="form-control" runat="server" />
            </div>
        </div>
        <div class="col-1">
            <div class="mb-3">
                <br />
                <asp:Button ID="BtnInserUpload" runat="server" CssClass="btn btn-outline-primary" Text="Upload" OnClick="BtnInserUpload_Click" />
            </div>
        </div>
    </div>



    <br />
    <asp:DataList ID="DataListDocumentBuyerAttach" runat="server" DataKeyField="DocumentBuyerID" DataSourceID="SqlDataSourceListDocumentBuyerAttach">
        <ItemTemplate>

            <asp:Label ID="DocumentBuyerIDLabel" runat="server" Text='<%# Eval("DocumentBuyerID") %>' Visible="False" />

           - 
            <asp:Label ID="DocumentBuyerTypeNameLabel" runat="server" Text='<%# Eval("DocumentBuyerTypeName") %>' />

            <a href='<%# Eval("DocumentBuyerAttach") %>' target="_blank">View Documet</a>
            <br />

        </ItemTemplate>
    </asp:DataList>

    <asp:SqlDataSource ID="SqlDataSourceListDocumentBuyerAttach" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblDocumentBuyer.DocumentBuyerID, TblDocumentBuyer.DocumentBuyerName, TblDocumentBuyerType.DocumentBuyerTypeName, TblDocumentBuyer.DocumentBuyerAttach FROM TblDocumentBuyer INNER JOIN TblDocumentBuyerType ON TblDocumentBuyer.DocumentBuyerTypeID = TblDocumentBuyerType.DocumentBuyerTypeID WHERE (TblDocumentBuyer.OrderDetailID = @OrderDetailID)">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtOrderDetailID" Name="OrderDetailID" PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>
