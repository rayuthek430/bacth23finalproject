﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="AboutUS.aspx.cs" Inherits="CarpowerWebApplication.WebFormAboutUS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container" style="margin-left:100px">
        <section class="about-us">
            <div class="about">
                <img src="images/car power banner.jpg" class="pic">
                
                <div class="text">
                    <br />
                    <h2>About Us</h2>
                    <h5 style="font-family:'Khmer OS Siemreap'">ក្រុមហ៊ុន CAR POWER ទិញ លក់~បង់រំលោះរថយន្ត និងគ្រឿងបន្លាស់នាំចូលផ្ទាល់ពីប្រទេសកូរ៉េ និងអាមេរិច</h5>
                    <p style="font-family:'Khmer OS Siemreap'">ទីតាំងចោមចៅក្រុងភ្នំពេញ ផ្លូវជាតិលេខ3 ហួសរង្វង់មូលចោមចៅ 100m ស្ដាំដៃ ជិតហាងបបរសហ្សុីន</p>
                    <div class="data">
                        <a href="https://goo.gl/maps/A66z5NDSTDXbJUmV8" class="hire">ផែនទី</a>
                    </div>
                    
                </div>
            </div>
        </section>
    </div>


</asp:Content>
