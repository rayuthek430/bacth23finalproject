﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class EditInvoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DivTotalAmount.Visible = true;
            if (Session["SUserID"] != null)
            {
            }
            else
            {
                Response.Redirect("Login.aspx");
            }

            if (Session["UserLog"] != null)
            {

            }
            else
            {
                Session["UserLog"] = System.Net.Dns.GetHostName() + "" + DateTime.Now.ToString("yyyyMMddhhmmss");
            }
            //TxtUserLog.Text = Session["UserLog"].ToString();
            if (!Page.IsPostBack)
            {
                try
                {

                    //SearchProduct();
                }
                catch (Exception)
                {
                }


            }
        }


        protected void InsertOrderDetail(int OrderID, int ProductID, double UnitPrice, int Quantity, double Amount, string UserLog)
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand InsertData = new SqlCommand();

            InsertData = new SqlCommand("Pro_EditInvoiceInsertOrderDetail", con);
            InsertData.CommandType = CommandType.StoredProcedure;
            InsertData.Parameters.AddWithValue("@OrderID", OrderID);
            InsertData.Parameters.AddWithValue("@ProductID", ProductID);

            InsertData.Parameters.AddWithValue("@UnitPrice", UnitPrice);
            InsertData.Parameters.AddWithValue("@Quantity", Quantity);
            InsertData.Parameters.AddWithValue("@Amount", Amount);
            InsertData.Parameters.AddWithValue("@UserLog", UserLog);
            InsertData.Parameters.Add("@RetureString", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;
            con.Open();

            InsertData.ExecuteNonQuery();

            con.Close();
            con.Dispose();
            LblCutSotck.Text = InsertData.Parameters["@RetureString"].Value.ToString();
            GridViewListOrderProduct.DataBind();
        }

        protected void SearchProduct(int ProductID)
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand SearchData = new SqlCommand();
            con.Open();
            SearchData = new SqlCommand("Pro_SearchProduct", con);
            SearchData.CommandType = CommandType.StoredProcedure;
            SearchData.Parameters.AddWithValue("@ProductID", ProductID);
            SqlDataAdapter SDA = new SqlDataAdapter(SearchData);
            DataSet DS = new DataSet();
            SDA.Fill(DS);
            System.Data.DataTable DT = new System.Data.DataTable();
            DT = DS.Tables[0];
            if (DT.Rows.Count > 0)
            {
                InsertOrderDetail(Convert.ToInt32(TxtSearchInvoice.Text), Convert.ToInt32(ProductID), Convert.ToDouble(DT.Rows[0]["UnitPrice"]), Convert.ToInt32(1), Convert.ToDouble(DT.Rows[0]["UnitPrice"]) * Convert.ToDouble(1), TxtUserLog.Text);
            }
        }

        protected void DataListProductByCategory_ItemCommand(object source, DataListCommandEventArgs e)
        {
            TxtCategoryID.Text = e.CommandArgument.ToString();


            if (TxtCategoryID.Text != "0")
            {
                TxtShowAll.Text = "1";
            }
            else
            {
                TxtShowAll.Text = "0";
            }

            DataList1.DataBind();
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            TxtShowAll.Text = "2";
            TxtCategoryID.Text = "0";
            DataList1.DataBind();
        }

        protected void GridViewListOrderProduct_DataBound(object sender, EventArgs e)
        {
            try
            {
                TotalAmount();
            }
            catch (Exception)
            {

            }
        }


        protected void TotalAmount()
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);

            SqlCommand cmd = new SqlCommand("Pro_AddTocartAmount", con);
            cmd.Parameters.AddWithValue("@UserLog", TxtUserLog.Text);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter sda = new SqlDataAdapter(cmd);

            System.Data.DataTable dt = new System.Data.DataTable();
            sda.Fill(dt);

            Decimal sum = 0;
            foreach (DataRow dr in dt.Rows)
            {
                sum = sum + Convert.ToDecimal(dr["Amount"]);
            }
            LblTotalAmount.Text = sum.ToString("####");

        }
        protected void CutStock(int productID, int QuantityID)
        {

            SqlCommand RemoveData = new SqlCommand();
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            RemoveData = new SqlCommand("Pro_CutStock", con);
            RemoveData.CommandType = CommandType.StoredProcedure;

            RemoveData.Parameters.AddWithValue("@ProductID", productID);
            RemoveData.Parameters.AddWithValue("@Quantity", QuantityID);

            con.Open();

            RemoveData.ExecuteNonQuery();

            con.Close();
            con.Dispose();
        }

        protected void RestoreInStock(int productID, int QuantityID)
        {

            SqlCommand RemoveData = new SqlCommand();
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            RemoveData = new SqlCommand("Pro_RestoreInStock", con);
            RemoveData.CommandType = CommandType.StoredProcedure;

            RemoveData.Parameters.AddWithValue("@ProductID", productID);
            RemoveData.Parameters.AddWithValue("@Quantity", QuantityID);

            con.Open();

            RemoveData.ExecuteNonQuery();

            con.Close();
            con.Dispose();
        }

        protected void GridViewListOrderProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            RestoreInStock(Convert.ToInt32(GridViewListOrderProduct.SelectedRow.Cells[1].Text.ToString()), Convert.ToInt32(GridViewListOrderProduct.SelectedRow.Cells[4].Text.ToString()));

            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand InsertData = new SqlCommand();

            InsertData = new SqlCommand("Pro_RemoveProductFromToCart", con);
            InsertData.CommandType = CommandType.StoredProcedure;

            InsertData.Parameters.AddWithValue("@OrderDetailID", GridViewListOrderProduct.SelectedRow.Cells[0].Text.ToString());

            con.Open();

            InsertData.ExecuteNonQuery();

            con.Close();
            con.Dispose();
            EditInvoiceSelectOrderDetail();
            EditInvoiceUpdateAmountOrder();
        }

        protected void LbtnOrderNow_Click(object sender, EventArgs e)
        {
            EditInvoiceUpdateAmountOrder();
            Response.Redirect("Invoices.aspx?OrderID=" + TxtSearchInvoice.Text);
        }

        protected void EditInvoiceSelectOrderDetail()
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand SearchData = new SqlCommand();
            con.Open();
            SearchData = new SqlCommand("Pro_EditInvoiceSelectOrderDetail", con);
            SearchData.CommandType = CommandType.StoredProcedure;
            SearchData.Parameters.AddWithValue("@OrderID", TxtSearchInvoice.Text);
            SqlDataAdapter SDA = new SqlDataAdapter(SearchData);
            DataSet DS = new DataSet();
            SDA.Fill(DS);
            System.Data.DataTable DT = new System.Data.DataTable();
            DT = DS.Tables[0];
            if (DT.Rows.Count > 0)
            {

                GridViewListOrderProduct.DataSource = DT;
                GridViewListOrderProduct.DataBind();
                TxtUserLog.Text = DT.Rows[0]["UserLog"].ToString();
                //TxtSearchInvoice.Text = "113";
            }
        }

        protected void BtnSearchInvoice_Click(object sender, EventArgs e)
        {
            EditInvoiceSelectOrderDetail();
            TotalAmount();
        }

        protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "AddToCart")
            {
                SearchProduct(Convert.ToInt32(e.CommandArgument.ToString()));
                CutStock(Convert.ToInt32(e.CommandArgument.ToString()), Convert.ToInt32(1));
                EditInvoiceSelectOrderDetail();
                TotalAmount();
                EditInvoiceUpdateAmountOrder();
            }
        }


        protected void EditInvoiceUpdateAmountOrder()
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand InsertData = new SqlCommand();

            InsertData = new SqlCommand("Pro_EditInvoiceUpdateAmountOrder", con);
            InsertData.CommandType = CommandType.StoredProcedure;

            InsertData.Parameters.AddWithValue("@OrderID", TxtSearchInvoice.Text);
            InsertData.Parameters.AddWithValue("@Amount", LblTotalAmount.Text);

            con.Open();

            InsertData.ExecuteNonQuery();

            con.Close();
            con.Dispose();
        }
    }
}