﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace CarpowerWebApplication
{
    public partial class ReplayComment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnInserComment_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand ReplyComment = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            ReplyComment = new SqlCommand("INSERT INTO [dbo].[TblReply]([Reply],[CommentID],[UserID],[ReplyDate]) VALUES (@Reply,@CommentID,@UserID,@ReplyDate)", con);

            ReplyComment.Parameters.AddWithValue("@Reply", txtReply.Text);
            ReplyComment.Parameters.AddWithValue("@CommentID", txtCommentID.Text);
            ReplyComment.Parameters.AddWithValue("@UserID", txtUserID.Text);
            ReplyComment.Parameters.AddWithValue("@ReplyDate", txtReplyDate.Text);

            con.Open();
            ReplyComment.ExecuteNonQuery();
            con.Close();
            GridViewReplyComment.DataBind();
        }

        protected void BtnSearchComment_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [ReplyID],[Reply],[CommentID],[UserID],[ReplyDate]FROM [dbo].[TblReply] WHERE ReplyID = " + txtReplyID.Text;
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {

                txtReplyID.Text = DTable1.Rows[0]["ReplyID"].ToString();
                txtReply.Text = DTable1.Rows[0]["Reply"].ToString();
                txtCommentID.Text = DTable1.Rows[0]["CommentID"].ToString();
                txtUserID.Text = DTable1.Rows[0]["UserID"].ToString();
                txtReplyDate.Text = DTable1.Rows[0]["ReplyDate"].ToString();


            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No data is returned');", true);
            }

            con.Close();
        }

        protected void BtnUpdateComment_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdateReply = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdateReply = new SqlCommand("UPDATE [dbo].[TblReply] SET [Reply] = @Reply,[CommentID] = @CommentID,[UserID] = @UserID,[ReplyDate] = @ReplyDate WHERE ReplyID =@ReplyID", con);

            UpdateReply.Parameters.AddWithValue("@ReplyID", txtReplyID.Text);
            UpdateReply.Parameters.AddWithValue("@Reply", txtReply.Text);
            UpdateReply.Parameters.AddWithValue("@CommentID", txtCommentID.Text);
            UpdateReply.Parameters.AddWithValue("@UserID", txtUserID.Text);
            UpdateReply.Parameters.AddWithValue("@ReplyDate", txtReplyDate.Text);

            con.Open();
            UpdateReply.ExecuteNonQuery();
            con.Close();

            txtReplyID.Focus();
            GridViewReplyComment.DataBind();

        }

        protected void BtnDeleteComment_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteReply = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteReply = new SqlCommand("DELETE FROM [dbo].[TblReply] WHERE ReplyID =@ReplyID", con);

            DeleteReply.Parameters.AddWithValue("@ReplyID", txtReplyID.Text);

            con.Open();
            DeleteReply.ExecuteNonQuery();
            con.Close();

            txtReplyID.Text = "";
            txtReply.Text = "";
            txtReplyID.Focus();
            GridViewReplyComment.DataBind();
        }

        protected void BtnClearComment_Click(object sender, EventArgs e)
        {
            txtReplyID.Text = "";
            txtReply.Text = "";
            txtReplyID.Focus();
        }
    }
}