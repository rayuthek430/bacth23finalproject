﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ReportOrderStatus.aspx.cs" Inherits="CarpowerWebApplication.ReportOrderStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="row">
        <div class="col-6">
            <div class="mb-3">
            </div>
        </div>
        <div class="col-6">
            <div class="mb-3">
                <asp:DropDownList CssClass="form-control" ID="DDLListStatus" runat="server" DataSourceID="SqlDataSourceListStatus" DataTextField="StatusName" DataValueField="StatusID" AutoPostBack="True" OnSelectedIndexChanged="DDLListStatus_SelectedIndexChanged"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSourceListStatus" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [StatusID], [StatusName] FROM [TblStatusOrder]"></asp:SqlDataSource>
            </div>

        </div>
    </div>


    <br />

    <asp:Button ID="BtnPrint" runat="server" OnClientClick="printDiv('PrintReportDaily')" Text="Print" CssClass="btn btn-primary" />
    <asp:Button ID="BtnPDF" runat="server" Text="To PDF" CssClass="btn btn-primary" OnClick="BtnPDF_Click" />
    <asp:Button ID="BtnExcel" CssClass="btn btn-primary" runat="server" Text="To Excel" OnClick="BtnExcel_Click" />
    <br />
    <div class="container">
        <div class="col-lg-12">
            <div id="PrintReportDaily">
                <table border="0" width="900">
                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-lg-12">
                                    <img src="images/Banner invoice noBG.png" style="width: 865px" />
                                </div>
                            </div>

                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <div class="row">
                                <div class="col-lg-12 text-center" style="font-family: 'Khmer OS Siemreap'; font-size: large">
                                    <h6>អាសយដ្ឋាន ឃ្លាំង 203 ផ្លូវជាតិលេខ៣ ,ព្រៃព្រីងខាងត្បូង, ចោមចៅ3, ពោធិ៍សែនជ័យ, រាជធានីភ្នំពេញ</h6>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 300px;">
                            <div class="col-lg-12">
                                <h6>Tel:</h6>
                                <h6>012 85 26 85</h6>
                                <h6>088 918 3168</h6>
                                <h6>016 866 789</h6>
                            </div>
                        </td>
                        <td style="width: 450px;">
                            <div class="col-lg-12" style="margin-top: 70px">
                                <br />
                                <h5 style="font-family: 'Khmer OS Muol'">របាយការណ៍ លក់តាមប្រភេទ Status
                                </h5>
                            </div>
                        </td>
                        <td style="width: 150px;">
                            <div class="col-lg-12" style="margin-top: 40px">
                            </div>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">


                            <asp:GridView ID="GridViewListOrderProduct" CssClass="table table-hover table-bordered border-primary" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceListOrderProduct">
                                <Columns>
                                    <asp:BoundField DataField="OrderDetailID" HeaderText="ID" SortExpression="OrderDetailID" />
                                    <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                                    <asp:BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" />
                                    <asp:BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate" />
                                    <asp:TemplateField HeaderText="image" SortExpression="image">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Image ID="Image1" runat="server" Height="96px" ImageUrl='<%# Eval("image") %>' Width="84px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="StatusName" HeaderText="StatusName" SortExpression="StatusName" />

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <asp:SqlDataSource ID="SqlDataSourceListOrderProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrderDetails.OrderDetailID, TblProduct.ProductName, TblOrderDetails.UnitPrice, TblOrderDetails.Quantity, TblOrderDetails.Amount, TblOrders.OrderDate, TblProduct.image, TblStatusOrder.StatusName FROM TblOrderDetails INNER JOIN TblProduct ON TblOrderDetails.ProductID = TblProduct.ProductID INNER JOIN TblOrders ON TblOrderDetails.OrderID = TblOrders.OrderID INNER JOIN TblStatusOrder ON TblOrderDetails.StatusID = TblStatusOrder.StatusID WHERE (TblOrders.Ordered = 1) AND (TblOrderDetails.StatusID = @StatusID) ORDER BY TblOrderDetails.OrderDetailID DESC">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLListStatus" Name="StatusID" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>
