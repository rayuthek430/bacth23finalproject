﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class Quotation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DivTotalAmount.Visible = true;
            if (Session["SUserID"] != null)
            {
            }
            else
            {
                Response.Redirect("Login.aspx");
            }

            if (Session["UserLog"] != null)
            {

            }
            else
            {
                Session["UserLog"] = System.Net.Dns.GetHostName() + "" + DateTime.Now.ToString("yyyyMMddhhmmss");
            }
            TxtUserLog.Text = Session["UserLog"].ToString();
            if (!Page.IsPostBack)
            {
                try
                {
                    SearchProduct();
                }
                catch (Exception)
                {
                }


            }
        }

        protected void SearchProduct()
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand SearchData = new SqlCommand();
            con.Open();
            SearchData = new SqlCommand("Pro_SearchProductQuotation", con);
            SearchData.CommandType = CommandType.StoredProcedure;
            SearchData.Parameters.AddWithValue("@ProductID", Request.QueryString["ProductID"].ToString());
            SqlDataAdapter SDA = new SqlDataAdapter(SearchData);
            DataSet DS = new DataSet();
            SDA.Fill(DS);
            DataTable DT = new DataTable();
            DT = DS.Tables[0];
            if (DT.Rows.Count > 0)
            {
                InsertOrderDetail(Convert.ToInt32(Request.QueryString["ProductID"]), Convert.ToDouble(DT.Rows[0]["UnitPrice"]), Convert.ToInt32(1), Convert.ToDouble(DT.Rows[0]["UnitPrice"]) * Convert.ToDouble(1), TxtUserLog.Text);
            }
        }


        protected void InsertOrderDetail(int ProductID, double UnitPrice, int Quantity, double Amount, string UserLog)
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand InsertData = new SqlCommand();

            InsertData = new SqlCommand("Pro_InsertQuotationDetail", con);
            InsertData.CommandType = CommandType.StoredProcedure;

            InsertData.Parameters.AddWithValue("@ProductID", ProductID);

            InsertData.Parameters.AddWithValue("@UnitPrice", UnitPrice);
            InsertData.Parameters.AddWithValue("@Quantity", Quantity);
            InsertData.Parameters.AddWithValue("@Amount", Amount);
            InsertData.Parameters.AddWithValue("@UserLog", UserLog);

            con.Open();

            InsertData.ExecuteNonQuery();

            con.Close();
            con.Dispose();
            GridViewListOrderProduct.DataBind();
        }
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            TxtShowAll.Text = "2";
            TxtCategoryID.Text = "0";
            DataList1.DataBind();
        }

        protected void DataListProductByCategory_ItemCommand(object source, DataListCommandEventArgs e)
        {
            TxtCategoryID.Text = e.CommandArgument.ToString();


            if (TxtCategoryID.Text != "0")
            {
                TxtShowAll.Text = "1";
            }
            else
            {
                TxtShowAll.Text = "0";
            }

            DataList1.DataBind();
        }

        protected void GridViewListOrderProduct_DataBound(object sender, EventArgs e)
        {
            try
            {
                TotalAmount();
            }
            catch (Exception)
            {

            }
        }


        protected void TotalAmount()
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);

            SqlCommand cmd = new SqlCommand("Pro_AddTocartAmountQuotation", con);
            cmd.Parameters.AddWithValue("@UserLog", Session["UserLog"]);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter sda = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            sda.Fill(dt);

            Decimal sum = 0;
            foreach (DataRow dr in dt.Rows)
            {
                sum = sum + Convert.ToDecimal(dr["Amount"]);
            }
            LblTotalAmount.Text = sum.ToString("####");

        }

        protected void GridViewListOrderProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand InsertData = new SqlCommand();

            InsertData = new SqlCommand("Pro_RemoveProductFromToCartQuotation", con);
            InsertData.CommandType = CommandType.StoredProcedure;

            InsertData.Parameters.AddWithValue("@QuotationDetailID", GridViewListOrderProduct.SelectedRow.Cells[0].Text.ToString());

            con.Open();

            InsertData.ExecuteNonQuery();

            con.Close();
            con.Dispose();
            GridViewListOrderProduct.DataBind();
        }

        protected void LbtnOrderNow_Click(object sender, EventArgs e)
        {
            if (LblTotalAmount.Text != "0")
            {
                string conStr = WebConfigurationManager.ConnectionStrings["carpowerConnectionString2"].ToString();
                SqlConnection con = new SqlConnection(conStr);
                SqlCommand InsertData = new SqlCommand();

                InsertData = new SqlCommand("Pro_InsertQuotation", con);
                InsertData.CommandType = CommandType.StoredProcedure;
                InsertData.Parameters.AddWithValue("@EmployeeID", Session["SUserID"]);
                InsertData.Parameters.AddWithValue("@CustomerID", DDlCustomer.SelectedValue);

                InsertData.Parameters.AddWithValue("@Amount", LblTotalAmount.Text);
                InsertData.Parameters.AddWithValue("@UserLog", Session["UserLog"]);
                InsertData.Parameters.Add("@QuotationIDOutput", SqlDbType.Int).Direction = ParameterDirection.Output;
                con.Open();

                InsertData.ExecuteNonQuery();
                Session["QuotationID"] = InsertData.Parameters["@QuotationIDOutput"].Value.ToString();

                con.Close();
                con.Dispose();
                GridViewListOrderProduct.DataBind();
                LblTotalAmount.Text = "0";
                Response.Redirect("InvoiceQuotation.aspx?QuotationID=" + Session["QuotationID"]);
            }
        }

        
    }
}