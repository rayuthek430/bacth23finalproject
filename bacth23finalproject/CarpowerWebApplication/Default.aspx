﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CarpowerWebApplication.CarPowerLayout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- Content product--%>
    <div class="row justify-content-center">
        <div>
            <img src="images/YT-Cover.jpg" alt="" style="width: 100%;">
        </div>
        
        <div class="row" style="margin-top:25px">
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [image], [UnitInStock], [UnitPrice], [Quantity], [CategoryID], [ProductName], [ProductID], [detail] FROM [TblProduct]"></asp:SqlDataSource>

            <asp:DataList ID="DataList1" runat="server" DataKeyField="ProductID" DataSourceID="SqlDataSource2" RepeatDirection="Horizontal" RepeatColumns="4">
                <ItemTemplate>
                    <a href="ProductDetail.aspx?ProductID=<%# Eval("ProductID") %>">
                        <div class="card" style="width: 18rem;">
                            <asp:Image ID="ListImg" runat="server" CssClass="card-img-top" ImageUrl='<%# Eval("image") %>' />
                            <div class="card-body">
                                <h5>
                                    <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' />
                                </h5>
                                <asp:Label ID="Label1" runat="server" CssClass="domlai">
                                    តម្លៃ <span style="color: red;"><%# "$" + String.Format("{0:#,0.##}", Eval("UnitPrice")) %></span>
                                </asp:Label>
                                <%-- <div class="card-text">
                             <%# Eval("detail") %>
                         </div>--%>
                            </div>
                        </div>
                    </a>
                </ItemTemplate>
            </asp:DataList>
            <asp:SqlDataSource ID="SqlDataSourcelistProd" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [detail], [image], [UnitPrice], [ProductID], [ProductName], [CategoryID] FROM [TblProduct]"></asp:SqlDataSource>
            <asp:SqlDataSource ID="SqlDataSourceTest" runat="server"></asp:SqlDataSource>
        </div>
    </div>

    <script>

</script>

</asp:Content>
