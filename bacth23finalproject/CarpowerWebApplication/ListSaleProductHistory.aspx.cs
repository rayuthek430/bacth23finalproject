﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class ListSaleProductHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridViewListOrderProduct_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Comments")
            {
                Response.Redirect("ListProductComments.aspx?OrderDetailID=" + e.CommandArgument);
            }
            if (e.CommandName == "AttachDoc")
            {
                Response.Redirect("ListProductAttachDoc.aspx?OrderDetailID=" + e.CommandArgument);
            }
            if (e.CommandName == "ViewInvioce")
            {
                Response.Redirect("Invioce.aspx?OrderID=" + e.CommandArgument);
            }
        }
    }
}