﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormSupplier : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertSupplier_Click(object sender, EventArgs e)

        {
            
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery",
                new ScriptResourceDefinition
                {
                    Path = "~/Scripts/jquery-1.7.2.min.js",
                    DebugPath = "~/Scripts/jquery-1.7.2.js",
                    CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.4.1.min.js",
                    CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.4.1.js"
                });


            SqlConnection con = new SqlConnection();

            SqlCommand InsertSupplier = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertSupplier = new SqlCommand("INSERT INTO [dbo].[TblSuppliers] ([CompanyName],[ContactName],[Address],[City],[Region],[PostalCode],[Country],[Phone],[Fax],[Website]) VALUES (@CompanyName,@ContactName, @Address, @City, @Region, @PostalCode,@Country, @Phone, @Fax, @Website)", con);

            InsertSupplier.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text);
            InsertSupplier.Parameters.AddWithValue("@ContactName", txtContactName.Text);
            InsertSupplier.Parameters.AddWithValue("@Address", txtAddress.Text);
            InsertSupplier.Parameters.AddWithValue("@City", txtCity.Text);
            InsertSupplier.Parameters.AddWithValue("@Region", txtRegion.Text);
            InsertSupplier.Parameters.AddWithValue("@PostalCode", txtPostalCode.Text);
            InsertSupplier.Parameters.AddWithValue("@Country", txtCountry.Text);
            InsertSupplier.Parameters.AddWithValue("@Phone", txtPhone.Text);
            InsertSupplier.Parameters.AddWithValue("@Fax", txtFax.Text);
            InsertSupplier.Parameters.AddWithValue("@Website", txtWebsite.Text);
            
            con.Open();
            
            InsertSupplier.ExecuteNonQuery();
            con.Close();
            GridViewSupplier.DataBind();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('បានជោគជ័យ!', 'ការបញ្ចូលទិន្នន័យបានជោគជ័យ.', 'success');", true);

            txtCompanyName.Text = "";
            txtContactName.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            txtRegion.Text = "";
            txtPostalCode.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
            txtFax.Text = "";
            txtWebsite.Text = "";
        }

        protected void btnUpdateSupplier_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdateSupplier = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdateSupplier = new SqlCommand("UPDATE [dbo].[TblSuppliers] SET [CompanyName] = @CompanyName,[ContactName] = @ContactName,[Address] = @Address,[City] = @City,[Region] = @Region,[PostalCode] = @PostalCode,[Country] = @Country,[Phone] = @Phone,[Fax] = @Fax,[Website] = @Website WHERE SupplierID = @SupplierID", con);

            UpdateSupplier.Parameters.AddWithValue("@SupplierID", txtSupplierID.Text);
            UpdateSupplier.Parameters.AddWithValue("@CompanyName", txtCompanyName.Text);
            UpdateSupplier.Parameters.AddWithValue("@ContactName", txtContactName.Text);
            UpdateSupplier.Parameters.AddWithValue("@Address", txtAddress.Text);
            UpdateSupplier.Parameters.AddWithValue("@City", txtCity.Text);
            UpdateSupplier.Parameters.AddWithValue("@Region", txtRegion.Text);
            UpdateSupplier.Parameters.AddWithValue("@PostalCode", txtPostalCode.Text);
            UpdateSupplier.Parameters.AddWithValue("@Country", txtCountry.Text);
            UpdateSupplier.Parameters.AddWithValue("@Phone", txtPhone.Text);
            UpdateSupplier.Parameters.AddWithValue("@Fax", txtFax.Text);
            UpdateSupplier.Parameters.AddWithValue("@Website", txtWebsite.Text);

            con.Open();
            UpdateSupplier.ExecuteNonQuery();
            con.Close();
            GridViewSupplier.DataBind();
        }

        protected void btnSearchSupplier_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [SupplierID],[CompanyName],[ContactName],[Address],[City],[Region],[PostalCode],[Country],[Phone],[Fax],[Website] FROM [dbo].[TblSuppliers] WHERE SupplierID = " + txtSupplierID.Text; // your id
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {

                txtSupplierID.Text = DTable1.Rows[0]["SupplierID"].ToString();
                txtCompanyName.Text = DTable1.Rows[0]["CompanyName"].ToString();
                txtContactName.Text = DTable1.Rows[0]["ContactName"].ToString();
                txtAddress.Text = DTable1.Rows[0]["Address"].ToString();
                txtCity.Text = DTable1.Rows[0]["City"].ToString();
                txtRegion.Text = DTable1.Rows[0]["Region"].ToString();
                txtPostalCode.Text = DTable1.Rows[0]["PostalCode"].ToString();
                txtCountry.Text = DTable1.Rows[0]["Country"].ToString();
                txtPhone.Text = DTable1.Rows[0]["Phone"].ToString();
                txtFax.Text = DTable1.Rows[0]["Fax"].ToString();
                txtWebsite.Text = DTable1.Rows[0]["Website"].ToString();

            } else
            {
                string message = "No data found for the given SupplierID";
                string script = "alert('" + message + "');";
                ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", script, true);
            }

            con.Close();
        }

        protected void btnDeleteSupplier_Click(object sender, EventArgs e)
        {
           
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteSupplier = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteSupplier = new SqlCommand("DELETE FROM [dbo].[TblSuppliers] WHERE SupplierID = @SupplierID", con);

            DeleteSupplier.Parameters.AddWithValue("@SupplierID", txtSupplierID.Text);

            con.Open();
            DeleteSupplier.ExecuteNonQuery();
            con.Close();
            GridViewSupplier.DataBind();
            if (txtCompanyName.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('មិនមានទិន្នន័យ', 'សូមរើសយកទិន្នន័យណាមួយ.', 'success');", true);
            } 
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('បានជោគជ័យ!', 'ទិន្នន័យលុបបានជោគជ័យ.', 'success');", true);

            // Clear
            txtCompanyName.Text = "";
            txtContactName.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            txtRegion.Text = "";
            txtPostalCode.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
            txtFax.Text = "";
            txtWebsite.Text = "";
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtCompanyName.Text = "";
            txtContactName.Text = "";
            txtAddress.Text = "";
            txtCity.Text = "";
            txtRegion.Text = "";
            txtPostalCode.Text = "";
            txtCountry.Text = "";
            txtPhone.Text = "";
            txtFax.Text = "";
            txtWebsite.Text = "";
        }

    }
}