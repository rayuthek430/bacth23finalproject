﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="AdminListSaleProduct.aspx.cs" Inherits="CarpowerWebApplication.AdminListSaleProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="row" style="margin-top: 30px">
        <div class="col-6">
            <div class="mb-3" style="font-family: 'Khmer OS Siemreap'; font-size: medium; margin-right: 200px;">
                <h4>ការ​​​ Aproval To Proccessing សម្រាប់ការបញ្ជាទិញ</h4>
            </div>
        </div>
        <div class="col-6">
            <div class="mb-3">
               
            </div>

        </div>
    </div>

    <br />
    <div class="container">
        <div class="col-lg-12">
            <asp:GridView ID="GridViewListOrderProduct" CssClass="table table-bordered border-primary" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceListOrderProduct" OnSelectedIndexChanged="GridViewListOrderProduct_SelectedIndexChanged" OnRowCommand="GridViewListOrderProduct_RowCommand">
                <Columns>
                    <asp:BoundField DataField="OrderDetailID" HeaderText="ID" SortExpression="OrderDetailID" />
                    <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                    <asp:BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                    <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" />
                    <asp:BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate" />
                    <asp:TemplateField HeaderText="image" SortExpression="image">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Image ID="Image1" runat="server" Height="96px" ImageUrl='<%# Eval("image") %>' Width="84px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="StatusName" HeaderText="StatusName" SortExpression="StatusName" />
                    <asp:CommandField SelectText="Aproval" ShowSelectButton="True" />
                    
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="ViewInvoice" CommandArgument='<%# Eval("OrderID") %>' Text="Invoice"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle Font-Names="Khmer OS Siemreap" />
            </asp:GridView>
        </div>
    </div>
    <asp:SqlDataSource ID="SqlDataSourceListOrderProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrderDetails.OrderID, TblOrderDetails.OrderDetailID,TblProduct.ProductName, TblOrderDetails.UnitPrice, TblOrderDetails.Quantity, TblOrderDetails.Amount, TblOrders.OrderDate, TblProduct.image, TblStatusOrder.StatusName FROM TblOrderDetails INNER JOIN TblProduct ON TblOrderDetails.ProductID = TblProduct.ProductID INNER JOIN TblOrders ON TblOrderDetails.OrderID = TblOrders.OrderID INNER JOIN TblStatusOrder ON TblOrderDetails.StatusID = TblStatusOrder.StatusID WHERE (TblOrders.Ordered = 1) and (TblOrderDetails.Active = 1) And (TblOrderDetails.StatusID=1) ORDER BY TblOrderDetails.OrderDetailID DESC"></asp:SqlDataSource>

</asp:Content>
