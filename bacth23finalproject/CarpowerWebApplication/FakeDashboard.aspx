﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="FakeDashboard.aspx.cs" Inherits="CarpowerWebApplication.FakeDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="https://demo.dashboardpack.com/architectui-html-free/main.css" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-midnight-bloom">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">ចំនួន ទឹកប្រាក់បានលក់ឡាន</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>$3,335,074.00</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-arielle-smile">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">ចំនួន ឡាន</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>383</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-grow-early">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">ចំនួន អ្នកប្រើ</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>10</span></div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <div class="row">
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content">
                    <div class="widget-content-outer">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">ចំនួន អតិថិជ</div>
                            </div>
                            <div class="widget-content-right">
                                <div class="widget-numbers text-success">1896</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content">
                    <div class="widget-content-outer">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">ចំនួន អ្នកបញ្ជាទិញ</div>
                            </div>
                            <div class="widget-content-right">
                                <div class="widget-numbers text-warning">18</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content">
                    <div class="widget-content-outer">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">ចំនួន Quotation</div>
                            </div>
                            <div class="widget-content-right">
                                <div class="widget-numbers text-danger">9</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

</asp:Content>
