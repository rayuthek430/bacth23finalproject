﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace CarpowerWebApplication
{
    public partial class Comment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnInserComment_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertComment = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertComment = new SqlCommand("INSERT INTO [dbo].[TblComment] ([Comment],[OrderDetailID],[UserID],[CommentDate]) VALUES (@Comment,@OrderDetailID,@UserID,@CommentDate)", con);

            InsertComment.Parameters.AddWithValue("@Comment", txtComment.Text);
            InsertComment.Parameters.AddWithValue("@OrderDetailID", txtOrderDetailID.Text);
            InsertComment.Parameters.AddWithValue("@UserID", textUserID.Text);
            InsertComment.Parameters.AddWithValue("@CommentDate", txtCommentDate.Text);

            con.Open();
            InsertComment.ExecuteNonQuery();
            con.Close();
            GridViewComment.DataBind();
        }

        protected void BtnSearchComment_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [CommentID] ,[Comment],[OrderDetailID],[UserID],[CommentDate]FROM [dbo].[TblComment] WHERE CommentID = " + txtcommentID.Text;
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {

                txtcommentID.Text = DTable1.Rows[0]["CommentID"].ToString();
                txtComment.Text = DTable1.Rows[0]["Comment"].ToString();
                txtOrderDetailID.Text = DTable1.Rows[0]["OrderDetailID"].ToString();
                textUserID.Text = DTable1.Rows[0]["UserID"].ToString();
                txtCommentDate.Text = DTable1.Rows[0]["CommentDate"].ToString();


            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No data is returned');", true);
            }

            con.Close();
        }

        protected void BtnUpdateComment_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand updateComment = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            updateComment = new SqlCommand("UPDATE [dbo].[TblComment] SET [Comment] = @Comment,[OrderDetailID] = @OrderDetailID,[UserID] = @UserID,[CommentDate] = @CommentDate WHERE CommentID = @CommentID", con);

            updateComment.Parameters.AddWithValue("@CommentID", txtcommentID.Text);
            updateComment.Parameters.AddWithValue("@Comment", txtComment.Text);
            updateComment.Parameters.AddWithValue("@OrderDetailID", txtOrderDetailID.Text);
            updateComment.Parameters.AddWithValue("@UserID", textUserID.Text);
            updateComment.Parameters.AddWithValue("@CommentDate", txtCommentDate.Text);


            //updateComment.Parameters.AddWithValue("@ProductID", DropDownListProductIDFormTableProduct.SelectedValue);

            con.Open();
            updateComment.ExecuteNonQuery();
            con.Close();
            GridViewComment.DataBind();
        }

        protected void BtnDeleteComment_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteComment = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteComment = new SqlCommand("DELETE FROM [dbo].[TblComment] WHERE CommentID =@CommentID", con);

            DeleteComment.Parameters.AddWithValue("@CommentID", txtcommentID.Text);

            con.Open();
            DeleteComment.ExecuteNonQuery();
            con.Close();
            txtcommentID.Text= "";
            txtComment.Text = "";
            txtOrderDetailID.Text = "";
            textUserID.Text = "";
            txtcommentID.Focus();
            GridViewComment.DataBind();

        }

        protected void BtnClearComment_Click(object sender, EventArgs e)
        {
            txtcommentID.Text = "";
            txtComment.Text = "";
            txtOrderDetailID.Text = "";
            textUserID.Text = "";
            txtcommentID.Focus();
        }
    }
}