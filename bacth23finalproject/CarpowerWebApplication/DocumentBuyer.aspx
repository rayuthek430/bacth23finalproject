﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="DocumentBuyer.aspx.cs" Inherits="CarpowerWebApplication.DocumentBuyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2>ឯកសារអ្នកទិញ</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label1" runat="server" Text="Document buyer id"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtDocumentBuyerID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label2" runat="server" Text="Buyer name"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtDocumentBuyerName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <%-- upload--%>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <asp:Image ID="DocumentBuyerAttch" runat="server" Height="221px" Width="323px" />
                    <br />
                    <br />
                    <asp:TextBox ID="DocumentBuyerURL" runat="server" CssClass="form-control"></asp:TextBox>
                    <br />
                    <asp:FileUpload ID="UploadDocumentBuyerAttch" runat="server" />
                </div>
            </div>

            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label3" runat="server" Text="Order detail id"></asp:Label>
                    </b>
                    <asp:TextBox ID="BuyerOrderDetailID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <asp:Button ID="btnInsertAds" runat="server" CssClass="btn btn-outline-primary" Text="Insert" OnClick="btnInsertAds_Click" />
                    <asp:Button ID="btnSearchAds" runat="server" CssClass="btn btn-outline-secondary" Text="Search" OnClick="btnSearchAds_Click" />
                    <asp:Button ID="btnUpdateAds" runat="server" CssClass="btn btn-outline-success" Text="Update" OnClick="btnUpdateAds_Click" />
                    <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-outline-danger" Text="Delete" OnClick="btnDelete_Click" />
                    <asp:Button ID="btnClearsAds" runat="server" CssClass="btn btn-outline-warning" Text="Clear" OnClick="btnClearsAds_Click" />
                </div>

            </div>
            <div class="col-6"></div>
        </div>
        <br />
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewListDocumentBuyer" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="DocumentBuyerID" DataSourceID="SqlDataSourceListDocumentBuyer"  CssClass="table table-striped table-hover">
                    <Columns>
                        <asp:BoundField DataField="DocumentBuyerID" HeaderText="DocumentBuyerID" InsertVisible="False" ReadOnly="True" SortExpression="DocumentBuyerID" />
                        <asp:BoundField DataField="DocumentBuyerName" HeaderText="DocumentBuyerName" SortExpression="DocumentBuyerName" />
                        <asp:BoundField DataField="DocumentBuyerAttach" HeaderText="DocumentBuyerAttach" SortExpression="DocumentBuyerAttach" />
                        <asp:BoundField DataField="OrderDetailID" HeaderText="OrderDetailID" SortExpression="OrderDetailID" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListDocumentBuyer" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [DocumentBuyerID], [DocumentBuyerName], [DocumentBuyerAttach], [OrderDetailID] FROM [TblDocumentBuyer]"></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
