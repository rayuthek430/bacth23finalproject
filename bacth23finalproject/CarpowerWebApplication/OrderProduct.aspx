﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="OrderProduct.aspx.cs" Inherits="CarpowerWebApplication.OrderProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .total-amount {
            color: red;
            font-size: 18px;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:TextBox ID="TxtUserLog" runat="server" Visible="false"></asp:TextBox>
    <div class="col-lg-11">
        <asp:GridView ID="GridViewListOrderProduct" CssClass="table table-hover" runat="server" AutoGenerateColumns="False" DataKeyNames="OrderDetailID" DataSourceID="SqlDataSourceListOrderProduct" OnDataBound="GridViewListOrderProduct_DataBound" OnSelectedIndexChanged="GridViewListOrderProduct_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="OrderDetailID" HeaderText="OrderDetailID" InsertVisible="False" ReadOnly="True" SortExpression="OrderDetailID" />
                <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                <asp:BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
                <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                <asp:TemplateField HeaderText="image" SortExpression="image">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" Height="116px" ImageUrl='<%# Eval("image") %>' Width="92px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField SelectText="Remove" ShowSelectButton="True" />
            </Columns>
        </asp:GridView>
    </div>
    <asp:SqlDataSource ID="SqlDataSourceListOrderProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrderDetails.OrderDetailID, TblProduct.ProductName, TblOrderDetails.UnitPrice, TblOrderDetails.Quantity, TblProduct.image FROM TblOrderDetails INNER JOIN TblProduct ON TblOrderDetails.ProductID = TblProduct.ProductID WHERE (TblOrderDetails.UserLog = @UserLog) AND (TblOrderDetails.Ordered = 0) ORDER BY TblOrderDetails.OrderDetailID DESC">
        <SelectParameters>
            <asp:SessionParameter Name="UserLog" SessionField="UserLog" />
        </SelectParameters>
    </asp:SqlDataSource>
    <div id="DivTotalAmount" runat="server">
        តម្លៃសរុប: $
    <asp:Label ID="LblTotalAmount" Text="0" CssClass="total-amount" runat="server"></asp:Label>

        <asp:LinkButton ID="LbtnOrderNow" runat="server" class="btn btn-outline-primary" OnClick="LbtnOrderNow_Click">Order Now</asp:LinkButton>
    </div>
</asp:Content>

