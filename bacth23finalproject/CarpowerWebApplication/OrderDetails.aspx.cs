﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormOrderDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        // Insert
        protected void btnInsertOrderDetail_Click2(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertOrderDetail = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertOrderDetail = new SqlCommand("INSERT INTO [dbo].[TblOrderDetails] ([OrderID],[ProductID],[UnitPrice],[Quantity],[Discount]) VALUES (@OrderID,@ProductID,@UnitPrice,@Quantity,@Discount)", con);

            InsertOrderDetail.Parameters.AddWithValue("@OrderID", txtOrdersID.Text);
            InsertOrderDetail.Parameters.AddWithValue("@ProductID", txtProductID.Text);
            InsertOrderDetail.Parameters.AddWithValue("@UnitPrice", txtUnitPrice.Text);
            InsertOrderDetail.Parameters.AddWithValue("@Quantity", txtQuantity.Text);
            InsertOrderDetail.Parameters.AddWithValue("@Discount", txtDiscount.Text);
           

            con.Open();
            InsertOrderDetail.ExecuteNonQuery();
            con.Close();
            GridViewListGridViewOrderDetails.DataBind();
        }

        // Update
        protected void btnUpdateOrderDetail_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand InsertOrderDetail = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertOrderDetail = new SqlCommand("UPDATE [dbo].[TblOrderDetails] SET [OrderID] = @OrderID,[ProductID] = @ProductID,[UnitPrice] = @UnitPrice,[Quantity] = @Quantity,[Discount] = @Discount WHERE OrderDetailID = @OrderDetailID", con);

            InsertOrderDetail.Parameters.AddWithValue("@OrderDetailID", txtOrderDetailID.Text);
            InsertOrderDetail.Parameters.AddWithValue("@OrderID", txtOrdersID.Text);
            InsertOrderDetail.Parameters.AddWithValue("@ProductID", txtProductID.Text);
            InsertOrderDetail.Parameters.AddWithValue("@UnitPrice", txtUnitPrice.Text);
            InsertOrderDetail.Parameters.AddWithValue("@Quantity", txtQuantity.Text);
            InsertOrderDetail.Parameters.AddWithValue("@Discount", txtDiscount.Text);


            con.Open();
            InsertOrderDetail.ExecuteNonQuery();
            con.Close();
            GridViewListGridViewOrderDetails.DataBind();
        }

        // Search
        protected void btnSearchOrderDetail_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [OrderDetailID],[OrderID],[ProductID],[UnitPrice],[Quantity],[Discount] FROM [dbo].[TblOrderDetails] WHERE OrderDetailID =" + txtOrderDetailID.Text; // your id
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {
                txtOrderDetailID.Text = DTable1.Rows[0]["OrderDetailID"].ToString(); // id
                txtOrdersID.Text = DTable1.Rows[0]["OrderID"].ToString();
                txtProductID.Text = DTable1.Rows[0]["ProductID"].ToString();
                txtUnitPrice.Text = DTable1.Rows[0]["UnitPrice"].ToString();
                txtQuantity.Text = DTable1.Rows[0]["Quantity"].ToString();
                txtDiscount.Text = DTable1.Rows[0]["Discount"].ToString();
                
                // id
            }

            con.Close();
        }
        // Delete
        protected void btnDeleteOrderDetail_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand OrderDetail = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            OrderDetail = new SqlCommand("DELETE FROM [dbo].[TblOrderDetails] WHERE OrderDetailID = @OrderDetailID", con);

            OrderDetail.Parameters.AddWithValue("@OrderDetailID", txtOrderDetailID.Text);

            con.Open();
            OrderDetail.ExecuteNonQuery();
            con.Close();
            GridViewListGridViewOrderDetails.DataBind();

            // Clear
            txtOrderDetailID.Text = "";
            txtOrdersID.Text = "";
            txtProductID.Text = "";
            txtUnitPrice.Text = "";
            txtUnitPrice.Text = "";
            txtQuantity.Text = "";
            txtDiscount.Text = "";
        }
    }
}