﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CarpowerWebApplication
{
    public partial class WebFormProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInsertProduct_Click1(object sender, EventArgs e)
        {
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery",
                new ScriptResourceDefinition
                {
                    Path = "~/Scripts/jquery-1.7.2.min.js",
                    DebugPath = "~/Scripts/jquery-1.7.2.js",
                    CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.4.1.min.js",
                    CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.4.1.js"
                });

            SqlConnection con = new SqlConnection();

            SqlCommand InsertProduct = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            InsertProduct = new SqlCommand("INSERT INTO [dbo].[TblProduct]([ProductName],[SupplierID],[CategoryID],[Quantity],[UnitPrice],[UnitInStock],[image],[detail],[ProductDate],[ShortNumber],[EngineNumber],[TagID],[MV],[CarManufactureDate]) VALUES (@ProductName,@SupplierID,@CategoryID,@Quantity,@UnitPrice,@UnitInStock,@image,@detail,@ProductDate,@ShortNumber,@EngineNumber,@TagID,@MV,@CarManufactureDate)", con);

            InsertProduct.Parameters.AddWithValue("@ProductName", txtProductName.Text);
            InsertProduct.Parameters.AddWithValue("@SupplierID", DropDownListSupplierID.SelectedValue);
            InsertProduct.Parameters.AddWithValue("@CategoryID", DropDownListCategoryID.SelectedValue);
            InsertProduct.Parameters.AddWithValue("@Quantity", txtQuantity.Text);
            InsertProduct.Parameters.AddWithValue("@UnitPrice", txtUnitPrice.Text);
            InsertProduct.Parameters.AddWithValue("@UnitInStock", txtUnitlnStock.Text);

            InsertProduct.Parameters.AddWithValue("@ShortNumber", txtShortNumber.Text);
            InsertProduct.Parameters.AddWithValue("@EngineNumber", textEngineNumber.Text);
            InsertProduct.Parameters.AddWithValue("@TagID", TagIDCar.Text);
            InsertProduct.Parameters.AddWithValue("@MV", MVCar.Text);
            InsertProduct.Parameters.AddWithValue("@CarManufactureDate", textCarManufactureDate.Text);
            InsertProduct.Parameters.AddWithValue("@ProductDate", txtProductDate.Text);
            InsertProduct.Parameters.AddWithValue("@detail", textDailProduct.Text);
            


            if (UploadPicturePro.HasFile)
            {
                UploadPicturePro.SaveAs(Server.MapPath("images/" + UploadPicturePro.FileName));
                ProductImageURL.Text = "images/" + UploadPicturePro.FileName;
            }
            ProductImage.ImageUrl = ProductImageURL.Text;

            InsertProduct.Parameters.AddWithValue("@image", ProductImageURL.Text);

            con.Open();
            InsertProduct.ExecuteNonQuery();
            con.Close();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "insertSuccess", "Swal.fire('បានជោគជ័យ!', 'ការបញ្ចូលទិន្នន័យបានជោគជ័យ.', 'success');", true);

            GridViewProduct.DataBind();

            txtProductID.Text = "";
            txtProductName.Text = "";
            txtQuantity.Text = "";
            txtUnitPrice.Text = "";
            txtUnitlnStock.Text = "";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");
            con.Open();
            string searchdatastu = "SELECT [ProductID],[ProductName],[SupplierID],[CategoryID],[Quantity],[UnitPrice],[UnitInStock],[image],[detail],[ProductDate],[ShortNumber],[EngineNumber],[TagID],[MV],[CarManufactureDate]FROM [dbo].[TblProduct] WHERE ProductID =" + txtProductID.Text;
            SqlDataAdapter dataAd1 = new SqlDataAdapter(searchdatastu, con);
            DataSet dataset1 = new System.Data.DataSet();
            dataAd1.Fill(dataset1);
            DataTable DTable1 = new DataTable();
            DTable1 = dataset1.Tables[0];
            if (DTable1.Rows.Count > 0)
            {
                txtProductID.Text = DTable1.Rows[0]["ProductID"].ToString();
                txtProductName.Text = DTable1.Rows[0]["ProductName"].ToString();
                DropDownListSupplierID.SelectedValue = DTable1.Rows[0]["SupplierID"].ToString();
                DropDownListCategoryID.SelectedValue = DTable1.Rows[0]["CategoryID"].ToString();
                txtQuantity.Text = DTable1.Rows[0]["Quantity"].ToString();
                txtUnitPrice.Text = DTable1.Rows[0]["UnitPrice"].ToString();
                txtUnitlnStock.Text = DTable1.Rows[0]["UnitInStock"].ToString();
                ProductImage.ImageUrl = DTable1.Rows[0]["image"].ToString();

                txtShortNumber.Text = DTable1.Rows[0]["ShortNumber"].ToString();
                textEngineNumber.Text = DTable1.Rows[0]["EngineNumber"].ToString();
                TagIDCar.Text = DTable1.Rows[0]["TagID"].ToString();
                MVCar.Text = DTable1.Rows[0]["MV"].ToString();
                textCarManufactureDate.Text = DTable1.Rows[0]["CarManufactureDate"].ToString();
                textDailProduct.Text = DTable1.Rows[0]["detail"].ToString();
            }

            con.Close();
        }

        protected void btnUpdateProduct_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand UpdateProduct = new SqlCommand();


            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            UpdateProduct = new SqlCommand("UPDATE [dbo].[TblProduct] SET [ProductName] = @ProductName,[SupplierID] = @SupplierID,[CategoryID] = @CategoryID,[Quantity] = @Quantity,[UnitPrice] = @UnitPrice ,[UnitInStock] = @UnitInStock ,[image] = @image,[detail] = @detail,[ProductDate] = @ProductDate,[ShortNumber] = @ShortNumber,[EngineNumber] = @EngineNumber,[TagID] = @TagID,[MV] = @MV,[CarManufactureDate] = @CarManufactureDate WHERE ProductID = @ProductID", con);

            UpdateProduct.Parameters.AddWithValue("@ProductID", txtProductID.Text);
            UpdateProduct.Parameters.AddWithValue("@ProductName", txtProductName.Text);
            UpdateProduct.Parameters.AddWithValue("@SupplierID", DropDownListSupplierID.Text);
            UpdateProduct.Parameters.AddWithValue("@CategoryID", DropDownListCategoryID.Text);
            UpdateProduct.Parameters.AddWithValue("@Quantity", txtQuantity.Text);
            UpdateProduct.Parameters.AddWithValue("@UnitPrice", txtUnitPrice.Text);
            UpdateProduct.Parameters.AddWithValue("@UnitInStock", txtUnitlnStock.Text);

            UpdateProduct.Parameters.AddWithValue("@ShortNumber", txtShortNumber.Text);
            UpdateProduct.Parameters.AddWithValue("@EngineNumber", textEngineNumber.Text);
            UpdateProduct.Parameters.AddWithValue("@TagID", TagIDCar.Text);
            UpdateProduct.Parameters.AddWithValue("@MV", MVCar.Text);
            UpdateProduct.Parameters.AddWithValue("@CarManufactureDate", textCarManufactureDate.Text);
            UpdateProduct.Parameters.AddWithValue("@ProductDate", txtProductDate.Text);
            UpdateProduct.Parameters.AddWithValue("@detail", textDailProduct.Text);

            if (UploadPicturePro.HasFile)
            {
                UploadPicturePro.SaveAs(Server.MapPath("images/" + UploadPicturePro.FileName));
                ProductImageURL.Text = "images/" + UploadPicturePro.FileName;
            }
            ProductImage.ImageUrl = ProductImageURL.Text;

            UpdateProduct.Parameters.AddWithValue("@image", ProductImageURL.Text);


            con.Open();
            UpdateProduct.ExecuteNonQuery();
            con.Close();

           

            GridViewProduct.DataBind();

            txtProductID.Text = "";
            txtProductName.Text = "";
            txtQuantity.Text = "";
            txtUnitPrice.Text = "";
            txtUnitlnStock.Text = "";
            txtShortNumber.Text = "";
            textEngineNumber.Text = "";
            TagIDCar.Text = "";
            MVCar.Text = "";
            textCarManufactureDate.Text = "";
            txtProductDate.Text = "";
            textDailProduct.Text = "";
        }

        protected void btnDeleteProduct_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();

            SqlCommand DeleteProduct = new SqlCommand();

            con = new SqlConnection(@"Data Source=LAPTOP-O5KHSPMS\SQLEXPRESS;Initial Catalog=carpower;Integrated Security=True");

            DeleteProduct = new SqlCommand("DELETE FROM [dbo].[TblProduct] WHERE ProductID = @ProductID", con);

            DeleteProduct.Parameters.AddWithValue("@ProductID", txtProductID.Text);

            con.Open();
            DeleteProduct.ExecuteNonQuery();
            con.Close();
            GridViewProduct.DataBind();

            // Clear
            txtProductID.Text = "";
            txtProductName.Text = "";
            txtQuantity.Text = "";
            txtUnitPrice.Text = "";
            txtUnitlnStock.Text = "";
            txtShortNumber.Text = "";
            textEngineNumber.Text = "";
            TagIDCar.Text = "";
            MVCar.Text = "";
            textCarManufactureDate.Text = "";
            txtProductDate.Text = "";
            textDailProduct.Text = "";
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtProductID.Text = "";
            txtProductName.Text = "";
            txtQuantity.Text = "";
            txtUnitPrice.Text = "";
            txtUnitlnStock.Text = "";
            txtShortNumber.Text = "";
            textEngineNumber.Text = "";
            TagIDCar.Text = "";
            MVCar.Text = "";
            textCarManufactureDate.Text = "";
            txtProductDate.Text = "";
            textDailProduct.Text = "";
        }

        protected void ButtonInSertDecription_Click(object sender, EventArgs e)

        {
            Response.Redirect("Decription.aspx?ProductID=" + txtProductID.Text);

        }

        protected void ButtonInSertImg_Click(object sender, EventArgs e)
        {
            Response.Redirect("Image.aspx?ProductID=" + txtProductID.Text);

        }
    }
}