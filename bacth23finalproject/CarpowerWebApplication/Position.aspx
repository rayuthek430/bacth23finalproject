﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Position.aspx.cs" Inherits="CarpowerWebApplication.WebFormPosition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .applyFont {
            font-weight: bold;
            font-size: 17px
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h2>Form Position</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label1" CssClass="applyFont" runat="server" Text="Position id:"></asp:Label>
                    <asp:TextBox ID="TxtPositionID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label3" CssClass="applyFont" runat="server" Text="PositionName:"></asp:Label>
                    <asp:TextBox ID="TxtPositionName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <asp:Button ID="btnInsertPosition" runat="server" CssClass="btn btn-primary" Text="Insert" OnClick="btnInsertPosition_Click" />
                    <asp:Button ID="btnUpdatePosition" runat="server" CssClass="btn btn-secondary" Text="Update" OnClick="btnUpdatePosition_Click" />
                    <asp:Button ID="btnDeletePosition" runat="server" CssClass="btn btn-danger" Text="Delete" OnClick="btnDeletePosition_Click" />
                    <asp:Button ID="btnSearchPosition" runat="server" CssClass="btn btn-info" Text="Search" OnClick="btnSearchPosition_Click" />
                    <asp:Button ID="btnClearPosition" runat="server" CssClass="btn btn-secondary" Text="Clear" OnClick="btnClearPosition_Click" />
                </div>
            </div>
            <div class="col-6"></div>

        </div>
        <br />
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewPosition" CssClass="table table-striped table-hover" runat="server" AutoGenerateColumns="False" DataKeyNames="PositionID" DataSourceID="SqlDataSourceListGridViewPosition">
                    <Columns>
                        <asp:BoundField DataField="PositionID" HeaderText="PositionID" InsertVisible="False" ReadOnly="True" SortExpression="PositionID" />
                        <asp:BoundField DataField="PositionName" HeaderText="PositionName" SortExpression="PositionName" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListGridViewPosition" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [PositionID], [PositionName] FROM [TblPosition]"></asp:SqlDataSource>
            </div>
            <div class="col-6">
            </div>
        </div>

    </div>
</asp:Content>
