﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="ListProductComments.aspx.cs" Inherits="CarpowerWebApplication.ListProductComments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-lg-12">
        <nav class="navbar navbar-expand navbar-light bg-light">
            <p class="text-center navbar-brand mb-0" style="margin: 12px">​ជជែក</p>
        </nav>
    </div>
    <br />
    <div class="col-lg-12">
        <asp:GridView ID="GridViewListOrderProduct" CssClass="table table-hover" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceListOrderProduct">
            <Columns>
                <asp:BoundField DataField="OrderDetailID" HeaderText="ID" Visible="false" SortExpression="OrderDetailID" />
                <asp:BoundField DataField="ProductName" HeaderText="ប្រភេទទ្បាន" SortExpression="ProductName" />
                <asp:BoundField DataField="UnitPrice" HeaderText="តម្លៃ" SortExpression="UnitPrice" />
                <asp:BoundField DataField="Quantity" HeaderText="ចំនួន" SortExpression="Quantity" />
                <asp:BoundField DataField="Amount" HeaderText="ចំនួនទឹកប្រាក់" SortExpression="Amount" />
                <%--<asp:BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate" />--%>
                <asp:TemplateField HeaderText="រូបភាព" SortExpression="image">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" Height="96px" ImageUrl='<%# Eval("image") %>' Width="84px" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="StatusName" HeaderText="ស្ថានភាព" SortExpression="StatusName" />

            </Columns>
        </asp:GridView>
    </div>
    <asp:SqlDataSource ID="SqlDataSourceListOrderProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblOrderDetails.OrderDetailID,TblProduct.ProductName, TblOrderDetails.UnitPrice, TblOrderDetails.Quantity, (TblOrderDetails.UnitPrice * TblOrderDetails.Quantity)as Amount, TblOrders.OrderDate, TblProduct.image, TblStatusOrder.StatusName FROM TblOrderDetails INNER JOIN TblProduct ON TblOrderDetails.ProductID = TblProduct.ProductID INNER JOIN TblOrders ON TblOrderDetails.OrderID = TblOrders.OrderID INNER JOIN TblStatusOrder ON TblOrderDetails.StatusID = TblStatusOrder.StatusID WHERE (TblOrderDetails.OrderDetailID = @OrderDetailID) ORDER BY TblOrderDetails.OrderDetailID DESC">
        <SelectParameters>
            <asp:QueryStringParameter Name="OrderDetailID" QueryStringField="OrderDetailID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />

    <div class="row" runat="server" id="Buyer">

        <asp:TextBox ID="txtUserID" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>


        <asp:TextBox ID="txtOrderDetailID" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>


        <asp:TextBox ID="txtcommentID" runat="server" CssClass="form-control" Visible="False"></asp:TextBox>

        <div class="col-2">
            <asp:Button ID="BtnNewComment" CssClass="btn btn-outline-primary" runat="server" Text="New Comment" OnClick="BtnNewComment_Click" />
        </div>
        <div class="col-8">
            <div class="mb-3">

                <asp:TextBox ID="txtComment" runat="server" CssClass="form-control"></asp:TextBox>

            </div>
        </div>
        <div class="col-1">
            <div class="mb-3">

                <asp:Button ID="BtnInserComment" runat="server" CssClass="btn btn-outline-primary" Text="Comment" OnClick="BtnInserComment_Click" />
                <asp:Button ID="BtnInsertReply" runat="server" CssClass="btn btn-outline-primary" Text="Reply" OnClick="BtnInsertReply_Click" Visible="False" />
            </div>
        </div>
    </div>



    <br />
    <asp:DataList ID="DataListProductComments" runat="server" Width="100%" DataKeyField="CommentID" DataSourceID="SqlDataSourceListProductComment" OnItemCommand="DataListProductComments_ItemCommand">
        <ItemTemplate>

            <div class="list-group">
                <div class="row">
                    <div class="col-lg-12">

                        <a href="#" style="color: white;" class="list-group-item list-group-item-action active">
                            <asp:Label Visible="false" ID="CommentIDLabel" runat="server" Text='<%# Eval("CommentID") %>' />

                            សំណួរអតិថិជន: [<asp:Label ID="FullNameLabel" runat="server" Text='<%# Eval("FullName") %>' />
                            |

                            <asp:Label ID="CommentDateLabel" runat="server" Text='<%# Eval("CommentDate") %>' />]
                                            <br />
                            <asp:Label ID="CommentLabel" ForeColor="White" runat="server" Text='<%# Eval("Comment") %>' />
                            <br />

                        </a>
                        <asp:LinkButton ID="LbtnReply" CommandName="BuyerReply" CommandArgument='<%# Eval("CommentID") %>' runat="server">Reply</asp:LinkButton>
                        |
                                <asp:LinkButton ID="LbtnRemoveReply" CommandName="RemoveComment" CommandArgument='<%# Eval("CommentID") %>' runat="server">Remove</asp:LinkButton>
                    </div>
                </div>


                <asp:DataList ID="DataListProductReply" runat="server" Width="100%" DataKeyField="ReplyID" DataSourceID="SqlDataSourceListProductReply" OnItemCommand="DataListProductReply_ItemCommand">
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-11">

                                <a href="#" class="list-group-item list-group-item-action">
                                    <asp:Label ID="ReplyIDLabel" Visible="false" runat="server" Text='<%# Eval("ReplyID") %>' />

                                    ការឆ្លើយតប: [<asp:Label ID="FullNameLabel" runat="server" Text='<%# Eval("FullName") %>' />
                                    |
                                                                            <asp:Label ID="ReplyDateLabel" runat="server" Text='<%# Eval("ReplyDate") %>' />]
                                                   <br />
                                    <asp:Label ID="ReplyLabel" ForeColor="Blue" runat="server" Text='<%# Eval("Reply") %>' />
                                    <br />
                                    <asp:LinkButton ID="LbtnReply2" CommandName="SellerReply" CommandArgument='<%# Eval("CommentID") %>' runat="server">Reply</asp:LinkButton>
                                    |
                                                                            <asp:LinkButton ID="LbtnRemoveReply" CommandName="RemoveReply" CommandArgument='<%# Eval("ReplyID") %>' runat="server">Remove</asp:LinkButton>
                                </a>

                                <br />

                            </div>
                        </div>

                    </ItemTemplate>
                </asp:DataList>
                <asp:SqlDataSource ID="SqlDataSourceListProductReply" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblReply.CommentID, TblReply.ReplyID, TblReply.Reply, TblUser.FullName, TblReply.ReplyDate FROM TblReply INNER JOIN TblUser ON TblReply.UserID = TblUser.UserID WHERE (TblReply.CommentID = @CommentID)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="CommentIDLabel" Name="CommentID" PropertyName="Text" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br />
            </div>
        </ItemTemplate>
    </asp:DataList>

    <asp:SqlDataSource ID="SqlDataSourceListProductComment" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblComment.CommentID,TblComment.CommentDate, TblComment.Comment, TblUser.FullName FROM TblComment INNER JOIN TblUser ON TblComment.UserID = TblUser.UserID WHERE (TblComment.OrderDetailID = @OrderDetailID) Order by TblComment.CommentID desc">
        <SelectParameters>
            <asp:QueryStringParameter Name="OrderDetailID" QueryStringField="OrderDetailID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
