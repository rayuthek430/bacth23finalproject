﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Comment.aspx.cs" Inherits="CarpowerWebApplication.Comment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2>Comment</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label1" runat="server" Text="Comment id"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtcommentID" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label2" runat="server" Text="Comment"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtComment" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label3" runat="server" Text="OrderDetail id"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtOrderDetailID" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label4" runat="server" Text="User id"></asp:Label>
                    </b>
                    <asp:TextBox ID="textUserID" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <b>
                        <asp:Label ID="Label5" runat="server" Text="Comment date"></asp:Label>
                    </b>
                    <asp:TextBox ID="txtCommentDate" runat="server" required="required" TextMode="Date" Enabled="True" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <br />
            <div class="col-6">
                <div class="mb-3">
                    <asp:Button ID="BtnInserComment" runat="server" CssClass="btn btn-outline-primary" Text="Insert" OnClick="BtnInserComment_Click" />
                    <asp:Button ID="BtnSearchComment" runat="server" CssClass="btn btn-outline-secondary" Text="Search" OnClick="BtnSearchComment_Click" />
                    <asp:Button ID="BtnUpdateComment" runat="server" CssClass="btn btn-outline-success" Text="Update" OnClick="BtnUpdateComment_Click" />
                    <asp:Button ID="BtnDeleteComment" runat="server" CssClass="btn btn-outline-danger" Text="Delete" OnClick="BtnDeleteComment_Click" />
                    <asp:Button ID="BtnClearComment" runat="server" CssClass="btn btn-outline-warning" Text="Clear" OnClick="BtnClearComment_Click" />
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewComment" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="DocumentID" DataSourceID="SqlDataSourceGridViewComment" CssClass="table table-striped table-hover">
                    <Columns>
                        <asp:BoundField DataField="DocumentID" HeaderText="DocumentID" InsertVisible="False" ReadOnly="True" SortExpression="DocumentID" />
                        <asp:BoundField DataField="DocumentName" HeaderText="DocumentName" SortExpression="DocumentName" />
                        <asp:BoundField DataField="DocumentAttach" HeaderText="DocumentAttach" SortExpression="DocumentAttach" />
                        <asp:BoundField DataField="ProducctID" HeaderText="ProducctID" SortExpression="ProducctID" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceGridViewComment" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [DocumentID], [DocumentName], [DocumentAttach], [ProducctID] FROM [TblDocument]"></asp:SqlDataSource>
            </div>
        </div>
        
       
    </div>
</asp:Content>
