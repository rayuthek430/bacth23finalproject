﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="Quotation.aspx.cs" Inherits="CarpowerWebApplication.Quotation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .truncate-text {
            display: block;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            max-width: 100%;
        }

        a {
            text-decoration: none;
            color: black;
        }

        .card {
            width: 18rem;
            margin-bottom: 1rem;
            border: 1px solid #ccc;
            transition: 0.3s;
            border-radius: 5px;
        }

            .card:hover {
                box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
            }

        .card-img-top {
            height: 134px;
            overflow: hidden;
        }

            .card-img-top img {
                width: 100%;
                height: 100%;
                object-fit: cover;
            }

        .card-body {
            padding: 1rem;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <asp:TextBox ID="TxtCategoryID" Visible="false" runat="server"></asp:TextBox>
        <asp:TextBox ID="TxtShowAll" runat="server" Visible="false" Text="0"></asp:TextBox>

        <div class="row">
            <div class="col-lg-5">
                <asp:TextBox ID="TxtSearch" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <div class="col-lg-1">
                <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-info" OnClick="BtnSearch_Click" />
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-6">
                <asp:DataList ID="DataListProductByCategory" Width="100%" runat="server" DataKeyField="CategoryID" DataSourceID="SqlDataSourceListProductByCategory" RepeatDirection="Horizontal" OnItemCommand="DataListProductByCategory_ItemCommand">
                    <ItemTemplate>


                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="ByCategory" CommandArgument='<%# Eval("CategoryID") %>' BackColor="#007BFF" CssClass="btn btn-success" ForeColor="White" Font-Size="Medium" Height="40px">

                            <asp:Label ID="CategoryNameLabel" runat="server" Text='<%# Eval("CategoryName") %>' />
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:DataList><asp:SqlDataSource ID="SqlDataSourceListProductByCategory" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT [CategoryID], [CategoryName] FROM [TblCategory] union select 0,'Show All'"></asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-6">
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="Pro_ListQuotationProduct" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="TxtCategoryID" Name="CategoryID" PropertyName="Text" Type="Int32" />
                        <asp:ControlParameter ControlID="TxtShowAll" Name="ListAll" PropertyName="Text" Type="Int32" DefaultValue="0" />
                        <asp:ControlParameter ControlID="TxtSearch" DefaultValue="0" Name="ProductName" PropertyName="Text" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:DataList ID="DataList1" runat="server" DataKeyField="ProductID" DataSourceID="SqlDataSource2" RepeatDirection="Horizontal" RepeatColumns="4">
                    <ItemTemplate>
                        <a href="Quotation.aspx?CategoryID=<%# Eval("CategoryID") %>&ProductID=<%# Eval("ProductID") %>">
                            <div class="card" style="width: 10rem;">
                                <asp:Image ID="ListImg" runat="server" CssClass="card-img-top" ImageUrl='<%# Eval("image") %>' />
                                <div class="card-body">
                                    <h5>
                                        <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' Font-Size="Small" CssClass="truncate-text" />
                                    </h5>
                                    <asp:Label ID="Label1" runat="server" CssClass="domlai">
                        តម្លៃ <span style="color: red;"> <%# "$" + String.Format("{0:#,0.##}", Eval("UnitPrice")) %></span>
                                    </asp:Label>
                                </div>
                            </div>
                        </a>
                    </ItemTemplate>
                </asp:DataList>
            </div>
            <div class="col-lg-6">
                <asp:TextBox ID="TxtUserLog" runat="server" Visible="False"></asp:TextBox><div class="col-lg-11">
                    <asp:GridView ID="GridViewListOrderProduct" CssClass="table table-bordered border-primary" runat="server" AutoGenerateColumns="False" DataKeyNames="QuotationDetailID" DataSourceID="SqlDataSourceListOrderProduct" OnDataBound="GridViewListOrderProduct_DataBound" OnSelectedIndexChanged="GridViewListOrderProduct_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="QuotationDetailID" HeaderText="QuotationDetailID" InsertVisible="False" ReadOnly="True" SortExpression="QuotationDetailID" />
                            <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                            <asp:BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                            <asp:TemplateField HeaderText="image" SortExpression="image">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("image") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("image") %>' Width="80px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField SelectText="Remove" ShowSelectButton="True" />
                        </Columns>
                    </asp:GridView>
                </div>
                <asp:SqlDataSource ID="SqlDataSourceListOrderProduct" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT TblQuotationDetails.QuotationDetailID, TblProduct.ProductName, TblQuotationDetails.UnitPrice, TblQuotationDetails.Quantity, TblProduct.image FROM TblQuotationDetails INNER JOIN TblProduct ON TblQuotationDetails.ProductID = TblProduct.ProductID WHERE (TblQuotationDetails.UserLog = @UserLog) AND (TblQuotationDetails.Quotationed = 0) ORDER BY TblQuotationDetails.QuotationDetailID DESC">
                    <SelectParameters>
                        <asp:SessionParameter Name="UserLog" SessionField="UserLog" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <div class="row">
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-5">


                        <asp:DropDownList CssClass="form-control" ID="DDlCustomer" runat="server" DataSourceID="SqlDataSourceCustomer" DataTextField="FullName" DataValueField="UserID"></asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSourceCustomer" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="SELECT UserID, FullName + '  , Tel: ' + PhoneNumber AS FullName
FROM     dbo.TblUser
WHERE  (RoleID = 2) order by UserID desc"></asp:SqlDataSource>
                    </div>
                    <div class="col-lg-1">
                        <asp:HyperLink ID="HyperLink1" NavigateUrl="~/UserControl.aspx" runat="server">Add</asp:HyperLink>
                    </div>
                    <div class="col-lg-5 text-left">
                        <div id="DivTotalAmount" runat="server">
                            តម្លៃសរុប: $
                            <asp:Label ID="LblTotalAmount" Text="0" CssClass="total-amount" runat="server"></asp:Label><asp:LinkButton ID="LbtnOrderNow" runat="server" class="btn btn-outline-primary" OnClick="LbtnOrderNow_Click">Print Now</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
