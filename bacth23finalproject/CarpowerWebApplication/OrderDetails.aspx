﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" AutoEventWireup="true" CodeBehind="OrderDetails.aspx.cs" Inherits="CarpowerWebApplication.WebFormOrderDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .applyFont{
            font-weight:bold;
            font-size:17px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="container">
        <h2>Form OrderDetailID</h2>
        <div class="row">
            <div class="col-6">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label1" CssClass="applyFont" runat="server" Text="OrderDetailID:"></asp:Label>
                    <asp:TextBox ID="txtOrderDetailID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label2" CssClass="applyFont" runat="server" Text="Order ID:"></asp:Label>
                    <asp:TextBox ID="txtOrdersID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label3" CssClass="applyFont" runat="server" Text="ProductID:"></asp:Label>
                    <asp:TextBox ID="txtProductID" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label4" CssClass="applyFont" runat="server" Text="UnitPrice:"></asp:Label>
                    <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label5" CssClass="applyFont" runat="server" Text="Quantity:"></asp:Label>
                    <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3" style="width: 55%">
                    <asp:Label ID="Label6" CssClass="applyFont" runat="server" Text="Discount: "></asp:Label>
                    <asp:TextBox ID="txtDiscount" runat="server" CssClass="form-control"></asp:TextBox>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <%--Button--%>
                <asp:Button ID="btnInsertOrderDetail" runat="server" CssClass="btn btn-primary" Text="Insert" OnClick="btnInsertOrderDetail_Click2" />
                <asp:Button ID="btnUpdateOrderDetail" runat="server" CssClass="btn btn-success" Text="Update" OnClick="btnUpdateOrderDetail_Click" />
                <asp:Button ID="btnDeleteOrderDetail" runat="server" CssClass="btn btn-danger" Text="Delete" OnClick="btnDeleteOrderDetail_Click" />
                <asp:Button ID="btnSearchOrderDetail" runat="server" CssClass="btn btn-info" Text="Search" OnClick="btnSearchOrderDetail_Click" />
                <asp:Button ID="btnClearFormOrderDetail" runat="server" CssClass="btn btn-secondary" Text="Clear" />
            </div>
            <div class="col-6"></div>
        </div>

        <br />
         <div class="row">
            <div class="col-12">
                <asp:GridView ID="GridViewListGridViewOrderDetails" CssClass="table table-striped table-hover" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="OrderDetailID" DataSourceID="SqlDataSourceListGridViewOrderDetails">
                    <Columns>
                        <asp:BoundField DataField="OrderDetailID" HeaderText="OrderDetailID" InsertVisible="False" ReadOnly="True" SortExpression="OrderDetailID" />
                        <asp:BoundField DataField="OrderID" HeaderText="OrderID" SortExpression="OrderID" />
                        <asp:BoundField DataField="ProductID" HeaderText="ProductID" SortExpression="ProductID" />
                        <asp:BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                        <asp:BoundField DataField="Discount" HeaderText="Discount" SortExpression="Discount" />
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSourceListGridViewOrderDetails" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="SELECT [OrderDetailID], [OrderID], [ProductID], [UnitPrice], [Quantity], [Discount] FROM [TblOrderDetails]"></asp:SqlDataSource>
            </div>
          
        </div>
    </div>
</asp:Content>
