﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarPower.Master" AutoEventWireup="true" CodeBehind="SearchProduct.aspx.cs" Inherits="CarpowerWebApplication.SearchProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        <div class="col-lg-6">
            <asp:TextBox ID="TxtSearchProduct" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="col-lg-6">
            <asp:Button ID="BtnSearch" runat="server" Text="ស្វែវរក" CssClass="btn btn-outline-primary" OnClick="BtnSearch_Click" />
        </div>
    </div>
    <br />

    <div class="row justify-content-center">
        <div class="row">
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString %>" SelectCommand="Pro_Search" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:ControlParameter ControlID="TxtSearchProduct" Name="productName" PropertyName="Text" />
                </SelectParameters>
            </asp:SqlDataSource>

            <asp:DataList ID="DataList1" runat="server" DataKeyField="ProductID" DataSourceID="SqlDataSource2" RepeatDirection="Horizontal" RepeatColumns="4">
                <ItemTemplate>
                    <a href="ProductDetail.aspx?ProductID=<%# Eval("ProductID") %>">
                        <div class="card" style="width: 18rem;">
                            <asp:Image ID="ListImg" runat="server" CssClass="card-img-top" ImageUrl='<%# Eval("image") %>' />
                            <div class="card-body">
                                <h5>
                                    <asp:Label ID="ProductNameLabel" runat="server" Text='<%# Eval("ProductName") %>' />
                                </h5>
                                <asp:Label ID="Label1" runat="server" CssClass="domlai">
                                 តម្លៃ <span style="color: red;"><%# "$" + String.Format("{0:#,0.##}", Eval("UnitPrice")) %></span>
                                </asp:Label>
                                <%-- <div class="card-text">
                          <%# Eval("detail") %>
                      </div>--%>
                            </div>
                        </div>
                    </a>
                </ItemTemplate>
            </asp:DataList>

        </div>
    </div>
</asp:Content>
