﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CarpowerMonitoring.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="Report_ListProductBySupplier.aspx.cs" Inherits="CarpowerWebApplication.Report_ListProductBySupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script language="javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div clss="container">
        <h2>Report list product by supplier</h2>
        <br />
        <input type="button" class="btn btn-primary" onclick="printDiv('PrintReportDaily')" value="Print" />
        <asp:Button ID="BtnPDF" runat="server" Text="To PDF" CssClass="btn btn-primary" OnClick="BtnPDF_Click" />
        <asp:Button ID="BtnExcel" CssClass="btn btn-primary" runat="server" Text="To Excel" OnClick="BtnExcel_Click" />
        <br />
        <div class="row">
            <div class="col-12">
                <div id="PrintReportDaily">
                    <table border="0" width="900">
                        <tr>
                            <td colspan="3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <img src="images/Banner invoice noBG.png" style="width: 865px" />
                                    </div>
                                </div>

                            </td>
                        </tr>

                        <tr>
                            <td colspan="3">
                                <div class="row">
                                    <div class="col-lg-12 text-center" style="font-family:'Khmer OS Siemreap'; font-size:large">
                                        <h6>អាសយដ្ឋាន ឃ្លាំង 203 ផ្លូវជាតិលេខ៣ ,ព្រៃព្រីងខាងត្បូង, ចោមចៅ3, ពោធិ៍សែនជ័យ, រាជធានីភ្នំពេញ</h6>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 300px;">
                                <div class="col-lg-12">
                                    <h6>Tel:</h6>
                                    <h6>012 85 26 85</h6>
                                    <h6>088 918 3168</h6>
                                    <h6>016 866 789</h6>
                                </div>
                            </td>
                            <td style="width: 450px;">
                                <div class="col-lg-12" style="margin-top: 70px">
                                    <br />
                                    <h5 style="font-family: 'Khmer OS Muol'">របាយការណ៍ អ្នកផ្គត់ផ្គង់
                                    </h5>
                                </div>
                            </td>
                            <td style="width: 150px;">
                                <div class="col-lg-12" style="margin-top: 40px">
                                </div>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:GridView ID="GridViewReport_ListProductBySupplier" CssClass="table table-striped table-hover" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductID" DataSourceID="SqlDataSourceReport_ListProductBySupplier" AllowPaging="True">
                                    <Columns>
                                        <asp:BoundField DataField="ProductID" HeaderText="ProductID" InsertVisible="False" ReadOnly="True" SortExpression="ProductID" />
                                        <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                                        <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
                                        <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                                        <asp:BoundField DataField="CategoryName" HeaderText="CategoryName" SortExpression="CategoryName" />
                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
                                        <asp:BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
                                        <asp:BoundField DataField="UnitInStock" HeaderText="UnitInStock" SortExpression="UnitInStock" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:SqlDataSource ID="SqlDataSourceReport_ListProductBySupplier" runat="server" ConnectionString="<%$ ConnectionStrings:carpowerConnectionString2 %>" SelectCommand="Report_ListProductBySupplier" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            </div>
        </div>
    </div>
</asp:Content>
